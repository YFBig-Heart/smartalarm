//
/** @file GWP2PMP4Recorder.h @brief 从P2P获取音视频录制到手机 */
//  GWP2P
//
//  Created by USER on 2017/11/7.
//  Copyright © 2017年 gwell. All rights reserved.
//

#import "GWMP4Recorder.h"

/**
 @brief 从P2P获取音视频录制到手机,使用时创建GWP2PMP4Recorder才可以录制监控视频
 */
@interface GWP2PMP4Recorder : GWMP4Recorder

@end
