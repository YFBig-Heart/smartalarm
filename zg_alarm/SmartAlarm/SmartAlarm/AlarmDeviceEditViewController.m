//
//  AlarmDeviceEditViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/24.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "AlarmDeviceEditViewController.h"
#import "HMScanner.h"

@interface AlarmDeviceEditViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (retain,nonatomic) UITableView *AlarmDeviceEdit;
@property (retain,nonatomic) NSArray * ItemsTitle;
@property (retain,nonatomic) NSArray * ItemsIcon;
@property (weak, nonatomic) IBOutlet UIImageView *HeadView;
@property (retain,nonatomic) UITextField * DeviceTitleFD;
@property (retain,nonatomic) UITextField * DevicePhoneFD;
@end

@implementation AlarmDeviceEditViewController

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _ItemsTitle = @[@"设备名称",@"设备手机号"];
    _ItemsIcon = @[@"list_device",@"list_phone"];
    if (_AlarmDeviceEdit == nil) {
        _AlarmDeviceEdit = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    }
    _AlarmDeviceEdit.delegate = self;
    _AlarmDeviceEdit.dataSource = self;
    _AlarmDeviceEdit.sectionHeaderHeight = 0;
    [self.view addSubview:_AlarmDeviceEdit];
    _AlarmDeviceEdit.sd_layout.topSpaceToView(_HeadView, 10.0f).widthRatioToView(self.view, 1.0f).bottomEqualToView(self.view);
    self.title = NSLocalizedString(@"设备信息", nil);
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];
    // Do any additional setup after loading the view from its nib.
    [_HeadView.layer setCornerRadius:_HeadView.width/2];
    [_HeadView.layer setMasksToBounds:YES];
    [_HeadView.layer setBorderWidth:1.0];
    [_HeadView.layer setBorderColor:[UIColor grayColor].CGColor];

}
- (void)Done
{
    NSDictionary * tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"username",_DeviceTitleFD.text,@"name",_DevicePhoneFD.text,@"number",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"30003",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已保存",nil)];
            NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice]];
            [dic setObject:_DeviceTitleFD.text forKey:@"name"];
            [dic setObject:_DevicePhoneFD.text forKey:@"number"];
            [[NSUserDefaults standardUserDefaults] setObject:dic forKey:kDefaultCurrDevice];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _ItemsTitle.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 65.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *const kCellID = @"_cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    switch (indexPath.row) {
        case 0:
        {
            if (_DeviceTitleFD) {
                [_DeviceTitleFD removeFromSuperview];
                _DeviceTitleFD = nil;
            }
            _DeviceTitleFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_DeviceTitleFD setTextColor:[UIColor flatGrayColor]];
            [_DeviceTitleFD setText:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"name"]];
            [_DeviceTitleFD setTextAlignment:NSTextAlignmentRight];
            [_DeviceTitleFD setBackgroundColor:[UIColor clearColor]];
            [_DeviceTitleFD setKeyboardType:UIKeyboardTypeDefault];
            [cell addSubview:_DeviceTitleFD];
            _DeviceTitleFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[self.DeviceTitleFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  self.navigationItem.rightBarButtonItem.enabled = NO;
                  return text.length > 1;
              }]
             subscribeNext:^(id x) {
                 NSLog(@"%@", x);
                 if([self.DeviceTitleFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.DeviceTitleFD.text = [self.DeviceTitleFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 self.navigationItem.rightBarButtonItem.enabled = YES;
             }];
        }
            break;
        case 1:
        {
            if (_DevicePhoneFD) {
                [_DevicePhoneFD removeFromSuperview];
                _DevicePhoneFD = nil;
            }
            _DevicePhoneFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_DevicePhoneFD setTextColor:[UIColor flatGrayColor]];
            [_DevicePhoneFD setText:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"number"]];
            [_DevicePhoneFD setTextAlignment:NSTextAlignmentRight];
            [_DevicePhoneFD setBackgroundColor:[UIColor clearColor]];
            [_DevicePhoneFD setKeyboardType:UIKeyboardTypePhonePad];
            [cell addSubview:_DevicePhoneFD];
            _DevicePhoneFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[self.DevicePhoneFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  self.navigationItem.rightBarButtonItem.enabled = NO;
                  return text.length > 1;
              }]
             subscribeNext:^(id x) {
                 NSLog(@"%@", x);
                 if([self.DevicePhoneFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.DevicePhoneFD.text = [self.DevicePhoneFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 self.navigationItem.rightBarButtonItem.enabled = YES;
             }];
        }
            break;

        default:
            break;
    }
    
    [cell.textLabel setText:NSLocalizedString([_ItemsTitle objectAtIndex:indexPath.row],nil)];
    cell.imageView.image = [UIImage imageNamed:[_ItemsIcon objectAtIndex:indexPath.row]];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            break;
            
        default:
            break;
    }
}

@end
