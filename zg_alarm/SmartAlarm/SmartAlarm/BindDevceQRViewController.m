//
//  BindDevceQRViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/19.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "BindDevceQRViewController.h"
#import "HMScanner.h"
#import "HMScannerController.h"
#import "RootTabViewController.h"
@interface BindDevceQRViewController ()
@property (weak, nonatomic) IBOutlet UILabel *CodeLable;
@property (weak, nonatomic) IBOutlet UIImageView *QrImage;
@property (nonatomic, retain) UILabel *DetailLable;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UIButton *ChangeBtn;
@property (retain,nonatomic) NSDictionary * LocDic;
@end

@implementation BindDevceQRViewController
CGFloat tmpY;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self GetQRCode];
    tmpY = _label2.centerY;
    [_label2 setText:NSLocalizedString(@"扫描此二维码可绑定设备及下载APP", nil)];
    [_ChangeBtn bs_configureAsPrimaryStyle];
    [_ChangeBtn setTitle:NSLocalizedString(@"生成操作员绑定号", nil) forState:UIControlStateNormal];
    _CodeLable.text = [NSString stringWithFormat:@"%@",@"http://alarm.smart4alarm.com"];
    [[_ChangeBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        static bool b = NO;
        if ((b = !b)) {
            [_ChangeBtn setTitle:NSLocalizedString(@"生成普通用户绑定号", nil) forState:UIControlStateNormal];
            [self CreatQRCode:[_LocDic objectForKey:@"AdminCode"]];
            _CodeLable.text = [NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"绑定号:", nil),[_LocDic objectForKey:@"AdminCode"],NSLocalizedString(@"(操作员)",nil)];
        }
        else
        {
            [_ChangeBtn setTitle:NSLocalizedString(@"生成操作员绑定号", nil) forState:UIControlStateNormal];
            [self CreatQRCode:[_LocDic objectForKey:@"UserCode"]];
            _CodeLable.text = [NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"绑定号:", nil),[_LocDic objectForKey:@"UserCode"],NSLocalizedString(@"(普通用户)",nil)];
        }
        [_CodeLable sizeToFit];
        [_CodeLable setNumberOfLines:0];
        _CodeLable.lineBreakMode = UILineBreakModeWordWrap;
    }];
    if (_isHideBackButton) {
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel  target:self action:@selector(Complete)];
    }
}
- (void)Complete
{
    [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
}
- (void)CreatQRCode:(NSString *)Qrcode
{
    NSString *cardName = [NSString stringWithFormat:@"%@%@",@"http://alarm.smart4alarm.com/?ZG:",Qrcode];
    
    [HMScannerController cardImageWithCardName:cardName avatar:nil scale:0.2 completion:^(UIImage *image) {
        [_QrImage setImage:image];
        
        _DetailLable = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.view addSubview:_DetailLable];
        [_DetailLable setText:NSLocalizedString(@"新用户可以扫描以下二维码，管理设备", nil)];
        _DetailLable.sd_layout.centerXEqualToView(_QrImage).bottomSpaceToView(_QrImage,3).widthRatioToView(self.view, 0.8).heightIs(54);
        [_DetailLable setTextAlignment:NSTextAlignmentCenter];
        [_DetailLable sizeToFit];
        [_DetailLable setNumberOfLines:0];
        [_DetailLable setLineBreakMode:NSLineBreakByWordWrapping];
    }];
}
- (void)GetQRCode
{
    NSDictionary * tDic = [[NSDictionary alloc] initWithObjectsAndKeys:_QRCodeImei,@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80068",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [_ChangeBtn setEnabled:NO];
            [_CodeLable setHidden:YES];
            if ([[response objectForKey:@"ret"] intValue] == 1018) {
                [_label2 setText:NSLocalizedString(@"只有管理员可以与新用户共享此设备。请与您的安全系统的管理员联系", nil)];
                [_QrImage setHidden:YES];
                _label2.sd_layout.topSpaceToView(self.navigationController.navigationBar, 30);
            }
            else
            {
                [_QrImage setHidden:NO];
                [_label2 setText:NSLocalizedString(@"扫描此二维码可绑定设备及下载APP", nil)];
                _label2.sd_layout.centerYIs(tmpY);
            }
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [_QrImage setHidden:NO];
            [_ChangeBtn setEnabled:YES];
            [_CodeLable setHidden:NO];
            _LocDic = [[response objectForKey:@"data"] objectAtIndex:0];
            [self CreatQRCode:[_LocDic objectForKey:@"UserCode"]];
             _CodeLable.text = [NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"绑定号:", nil),[_LocDic objectForKey:@"UserCode"],NSLocalizedString(@"(普通用户)",nil)];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
