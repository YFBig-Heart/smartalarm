//
//  RegisterUserViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/11/4.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "RegisterUserViewController.h"
#import "BindDeviceViewController.h"

@interface RegisterUserViewController ()
@property (weak, nonatomic) IBOutlet UIButton *NextBtn;
@property (weak, nonatomic) IBOutlet UITextField *UserIDFD;

@end

@implementation RegisterUserViewController

- (void)viewDidLoad {
    [_NextBtn bs_configureAsPrimaryStyle];
    if (_TitleString) {
        self.title = _TitleString;
    }
    else
    self.title = NSLocalizedString(@"邮箱", nil);
    [super viewDidLoad];
    [[_NextBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self Donext];
    }];
    [[self.UserIDFD.rac_textSignal
      filter:^BOOL(id value) {
          NSString *text = value;
          _NextBtn.enabled = NO;
          [_NextBtn setAlpha:0.5];
          return text.length > 1;
      }]
     subscribeNext:^(id x) {
         NSLog(@"%@", x);
         if([self.UserIDFD.text rangeOfString:@" "].location !=NSNotFound)
         {
             self.UserIDFD.text = [self.UserIDFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
         }
         [_NextBtn setAlpha:1];
         _NextBtn.enabled = YES;
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)Donext
{
    BindDeviceViewController * vc = [[BindDeviceViewController alloc] init];
    vc.userID = [_UserIDFD.text copy];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
