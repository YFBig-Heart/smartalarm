//
//  BindDeviceViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "BindDeviceViewController.h"
#import "HMScanner.h"
#import "HMScannerController.h"
#import "CheckPswViewController.h"
#import "PreEntryIdleViewController.h"
#import "RegisterForPasswordViewController.h"
#import "BindForEditDeviceViewController.h"
#import "RootTabViewController.h"
extern bool isNeedShowInputCode;
@interface BindDeviceViewController ()
@property (weak, nonatomic) IBOutlet UIButton *QRCodeBtn;
@property (weak, nonatomic) IBOutlet UITextField *DeviceID;
@property (weak, nonatomic) IBOutlet UIButton *DoneBtn;
@property (weak, nonatomic) IBOutlet UILabel *BindRemindLabel;

@end

@implementation BindDeviceViewController
- (void)viewDidLoad {
    if (_TitleString) {
        self.title = _TitleString;
    }
    else
    self.title = NSLocalizedString(@"添加设备", nil);
    
    [_BindRemindLabel setText:NSLocalizedString(@"在设备背面可找到二维码标签", nil)];
    [_BindRemindLabel setTextColor:FlatRed];
    [super viewDidLoad];
    [_DoneBtn bs_configureAsPrimaryStyle];
    _QRCodeBtn.showsTouchWhenHighlighted = YES;
    [[_QRCodeBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self Qrcode];
    }];
    [[[_DeviceID rac_textSignal] filter:^BOOL(NSString * _Nullable value) {
        NSString *text = value;
        [_DoneBtn setEnabled:NO];
        [_DoneBtn setAlpha:0.5];
        return text.length > 1;
    }] subscribeNext:^(NSString * _Nullable x) {
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
        NSLog(@"%@", x);
        if (x.length > 15) {
            _DeviceID.text = [_DeviceID.text substringWithRange:NSMakeRange(0, 15)];
        }
        if([self.DeviceID.text rangeOfString:@" "].location !=NSNotFound)
        {
            self.DeviceID.text = [self.DeviceID.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
        }
        [_DoneBtn setAlpha:1];
        [_DoneBtn setEnabled:YES];
    }];
    [_DeviceID setPlaceholder:NSLocalizedString(@"输入绑定设备号", nil)];
    [[_DoneBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        if (_isHideQrcodeBtn) {
            //从IDLE界面绑定过来的？
            [self CheckIMEI:_DeviceID.text];
        }
        else
        [self Done];
    }];
    if (_isHideQrcodeBtn) {
        [_QRCodeBtn setHidden:YES];
    }
    if (_DefaultDeviceID) {
        [_DeviceID setText:_DefaultDeviceID];
    }
    if (_DefaultDeviceID) {
        [_DoneBtn setEnabled:YES];
        [_DeviceID setAlpha:1.0];
    }
}
- (void)viewDidDisappear:(BOOL)animated
{
    isNeedShowInputCode = false;
}
- (void)CheckIMEI:(NSString *)imei
{
    //查询IMEI是否有效
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"username",imei,@"imei", nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80003",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            //绑定
            if ([[[[response objectForKey:@"data"] objectAtIndex:0] objectForKey:@"status"] intValue]) {
                //已有管理员
                [self doNext:[[[[response objectForKey:@"data"] objectAtIndex:0] objectForKey:@"type"] intValue] IMEI:imei];
            }
            else
            {
                //全新设备
                BindForEditDeviceViewController * vc  = [[BindForEditDeviceViewController alloc] init];
                vc.DeviceID = [_DeviceID.text copy];
                vc.DeviceType = [[[[response objectForKey:@"data"] objectAtIndex:0] objectForKey:@"type"] intValue];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)doNext:(int )type IMEI:(NSString *)_imei
{
    NSArray * nameArray = @[@"Alarm Host",@"Alarm Host(Wifi)",@"Smart socket",@"Smart Appliances",@"Alarm",@"Alarm1"];
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"username",_imei,@"imei",[nameArray objectAtIndex:type],@"name", nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00003",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            if ([[[response objectForKey:@"data"] objectAtIndex:0] objectForKey:@"status"] == 0) {
                PreEntryIdleViewController * vc = [[PreEntryIdleViewController alloc] init];
                vc.BindDeviceImei = [_imei copy];
                [self.navigationController pushViewController:vc animated:YES];
            }
            else
            {
                //非全新直接上IDLE界面
                [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
                [self.view.window makeKeyAndVisible];
            }

        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
    
}
- (void)Qrcode
{
    NSString *cardName = @"234";
    UIImage *avatar = [UIImage imageNamed:@"avatar"];
    
    isNeedShowInputCode = true;
    // 实例化扫描控制器
    HMScannerController *scanner = [HMScannerController scannerWithCardName:cardName avatar:avatar completion:^(NSString *stringValue) {
        NSRange range = [stringValue rangeOfString:@"ZG:"];
        if (range.location != NSNotFound) {
            _DeviceID.text = [stringValue substringFromIndex:range.location+range.length];
            if ([_DeviceID.text length] <= 15) {
                [_DoneBtn setAlpha:1];
                [_DoneBtn setEnabled:YES];
            }
        }
        else
            [RKDropdownAlert title:nil message:NSLocalizedString(@"二维码出错",nil)];
        //        self.scanResultLabel.text = stringValue;
        [UIView beginAnimations:@"View Flip" context:nil];
        //动画持续时间
        [UIView setAnimationDuration:1.25];
        //设置动画的回调函数，设置后可以使用回调方法
        [UIView setAnimationDelegate:self];
        //设置动画曲线，控制动画速度
        [UIView  setAnimationCurve: UIViewAnimationCurveEaseInOut];
        //设置动画方式，并指出动画发生的位置
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view  cache:YES];
        //提交UIView动画
        [UIView commitAnimations];
    }];
    
    // 设置导航栏样式
    [scanner setTitleColor:[UIColor whiteColor] tintColor:[UIColor greenColor]];
    
    // 展现扫描控制器
    [self showDetailViewController:scanner sender:nil];
}
- (void)Done
{
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:_userID,@"name",_DeviceID.text,@"code", nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"20003",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            //直接注册了，注册完也再绑定。
            RegisterForPasswordViewController * vc = [[RegisterForPasswordViewController alloc] init];
            vc.userID = [_userID copy];
            vc.DeviceID = [_DeviceID.text copy];
            vc.UseType = self.UseType;
            [self.navigationController pushViewController:vc animated:YES];

        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
