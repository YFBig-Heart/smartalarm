
//
//  VolumSettingViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/6.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "VolumSettingViewController.h"

@interface VolumSettingViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UISwitch *KeyToneSW;
@property (retain,nonatomic) UITextField * CallToneFD;
@property (retain,nonatomic) UITextField * AlarmToneFD;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;

@end

@implementation VolumSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_TitleArray == nil) {
        _TitleArray = @[@"提示音 ",@"报警音",@"按键音"];
    }
    _RemoteInfoDic = [[NSMutableDictionary alloc] init];
    self.title = NSLocalizedString(@"音量设置", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];
    [self synSettings];
}
- (void)Done
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_RemoteInfoDic objectForKey:@"voice"],@"voice",
            [_RemoteInfoDic objectForKey:@"alarm"],@"alarm",
            [_RemoteInfoDic objectForKey:@"keytone"],@"keytone",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80076",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void )synSettings
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00076",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 2:
        {
            if (_KeyToneSW) {
                [_KeyToneSW removeFromSuperview];
                _KeyToneSW = nil;
            }
            _KeyToneSW = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
            cell.accessoryView = _KeyToneSW;
            [_KeyToneSW setOn:([[_RemoteInfoDic objectForKey:@"keytone"] intValue]?YES:NO)];
            @weakify(self)
            [[_KeyToneSW
              rac_signalForControlEvents:UIControlEventValueChanged]
             subscribeNext:^(UISwitch * x) {
                 @strongify(self)
                 NSLog(@"开关  ： %d",x.isOn);
                 [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"keytone"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
             }];
        }
            break;
        case 0:
        {
            if (_CallToneFD) {
                [_CallToneFD removeFromSuperview];
                _CallToneFD = nil;
            }
            _CallToneFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_CallToneFD setTextColor:[UIColor flatGrayColor]];
            [_CallToneFD setKeyboardType:UIKeyboardTypeNumberPad];
            [_CallToneFD setText:[_RemoteInfoDic objectForKey:@"voice"]];
            [_CallToneFD setTextAlignment:NSTextAlignmentRight];
            [_CallToneFD setBackgroundColor:[UIColor clearColor]];
            [cell addSubview:_CallToneFD];
            _CallToneFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).widthRatioToView(self.view, 0.47);
            [[_CallToneFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  [self.navigationItem.rightBarButtonItem setEnabled:NO];
                  return text.length > 0;
              }]
             subscribeNext:^(id x) {
                 if([self.CallToneFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.CallToneFD.text = [self.CallToneFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 [_RemoteInfoDic setObject:_CallToneFD.text forKey:@"voice"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
                 NSLog(@"%@", x);
             }];
        }
            break;
        case 1:
        {
            if (_AlarmToneFD) {
                [_AlarmToneFD removeFromSuperview];
                _AlarmToneFD = nil;
            }
            _AlarmToneFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_AlarmToneFD setTextColor:[UIColor flatGrayColor]];
            [_AlarmToneFD setKeyboardType:UIKeyboardTypeNumberPad];
            [_AlarmToneFD setText:[_RemoteInfoDic objectForKey:@"alarm"]];
            [_AlarmToneFD setTextAlignment:NSTextAlignmentRight];
            [_AlarmToneFD setBackgroundColor:[UIColor clearColor]];
            [cell addSubview:_AlarmToneFD];
            _AlarmToneFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).widthRatioToView(self.view, 0.47);
            [[_AlarmToneFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  [self.navigationItem.rightBarButtonItem setEnabled:NO];
                  return text.length > 0;
              }]
             subscribeNext:^(id x) {
                 if([self.AlarmToneFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.AlarmToneFD.text = [self.AlarmToneFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 [_RemoteInfoDic setObject:_AlarmToneFD.text forKey:@"alarm"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
                 NSLog(@"%@", x);
             }];
        }
            break;
                default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
            break;
        default:
            break;
    }
}


@end
