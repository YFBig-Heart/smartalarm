//
//  SystemSettingViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "SystemSettingViewController.h"
#import "DelaySettingViewController.h"
#import "AutoProtectListViewController.h"
#import "AutoProtectSMSViewController.h"
#import "BatSytemListViewController.h"
#import "AlarmSoundSettingViewController.h"
#import "VolumSettingViewController.h"
#import "ScreenSlaveSettingViewController.h"
#import "STPickerSingle.h"
#import "STPickerView.h"
@interface SystemSettingViewController ()<UITableViewDataSource,STPickerSingleDelegate,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) NSArray * arrayData;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@property (retain,nonatomic) NSArray * ControllerArray;
@end

@implementation SystemSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_TitleArray == nil) {
        if (([[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"type"] intValue] ==2)) {
            _TitleArray = @[@"延时",@"自动设/撤防",@"时间同步",@"设/撤防短信通知",@"电源/电池状态提醒",@"警号/振铃",@"音量"];
        }
        else if([[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"type"] intValue] ==3)
        {
            //排插
            _TitleArray = @[@"延时",@"自动设/撤防",@"时间同步"];
        }
        else if ([CoreHTTPRequest GetDeviceType] == WIFI_ALARM)
        {
            _TitleArray = @[@"延时",@"自动设/撤防",@"时间同步",@"电源/电池状态提醒",@"警号/振铃",@"音量"];
        }
        else
            _TitleArray = @[@"延时",@"自动设/撤防",@"时间同步",@"设/撤防短信通知",@"电源/电池状态提醒",@"警号/振铃",@"音量",@"屏保"];
    }
    _arrayData = @[NSLocalizedString(@"从不",nil),NSLocalizedString(@"30 秒",nil),NSLocalizedString(@"60 秒",nil),NSLocalizedString(@"5 分",nil),NSLocalizedString(@"10 分",nil)];
    self.title = NSLocalizedString(@"报警系统设定", nil);
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    [self synScreenSlave];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)synDevcieTime
{
    NSDictionary * tDic ;
    NSInteger zone = (([NSTimeZone systemTimeZone].secondsFromGMT) / 3600);
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[self getLoaclTime:@"YYYY"],@"year",[self getLoaclTime:@"MM"],@"mon",[self getLoaclTime:@"dd"],@"date",[self getLoaclTime:@"HH"],@"hour",[self getLoaclTime:@"mm"],@"min",[NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:zone]],@"timezone",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80072",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)synScreenSlave
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00077",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)setScreenSlave
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_RemoteInfoDic objectForKey:@"time"],@"time",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80077",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    if (indexPath.row == 7) {
        cell.detailTextLabel.text = [_arrayData objectAtIndex:[[_RemoteInfoDic objectForKey:@"time"] intValue]];
    }
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if ([cell.textLabel.text isEqualToString:NSLocalizedString(@"时间同步", nil)]) {
        cell.detailTextLabel.text = [self getLoaclTime:@"YYYY-MM-dd HH:mm:ss"];;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if([CoreHTTPRequest GetDeviceType] == WIFI_ALARM)
    {
        switch (indexPath.row) {
            case 0:
                [self.navigationController pushViewController:[[DelaySettingViewController alloc] init] animated:YES];
                break;
            case 1:
                [self.navigationController pushViewController:[[AutoProtectListViewController alloc] init] animated:YES];
                break;
            case 2:
                [self synDevcieTime];
                break;
            case 3:
                [self.navigationController pushViewController:[[BatSytemListViewController alloc] init] animated:YES];
                break;
            case 4:
                [self.navigationController pushViewController:[[AlarmSoundSettingViewController alloc] init] animated:YES];
                break;
            case 5:
                [self.navigationController pushViewController:[[VolumSettingViewController alloc] init] animated:YES];
                break;
            case 6:
            {
                STPickerSingle *TypePicker = [[STPickerSingle alloc]init];
                [TypePicker setArrayData:[NSMutableArray arrayWithArray:_arrayData]];
                [TypePicker setTitle:NSLocalizedString(@"选择类型", nil)];
                [TypePicker setTitleUnit:nil];
                [TypePicker setContentMode:STPickerContentModeBottom];
                [TypePicker setDelegate:self];
                [TypePicker setTag:indexPath.row];
                [TypePicker show];
            }
                break;
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row) {
            case 0:
                [self.navigationController pushViewController:[[DelaySettingViewController alloc] init] animated:YES];
                break;
            case 1:
                [self.navigationController pushViewController:[[AutoProtectListViewController alloc] init] animated:YES];
                break;
            case 2:
                [self synDevcieTime];
                break;
            case 3:
                [self.navigationController pushViewController:[[AutoProtectSMSViewController alloc] init] animated:YES];
                break;
            case 4:
                [self.navigationController pushViewController:[[BatSytemListViewController alloc] init] animated:YES];
                break;
            case 5:
                [self.navigationController pushViewController:[[AlarmSoundSettingViewController alloc] init] animated:YES];
                break;
            case 6:
                [self.navigationController pushViewController:[[VolumSettingViewController alloc] init] animated:YES];
                break;
            case 7:
            {
                STPickerSingle *TypePicker = [[STPickerSingle alloc]init];
                [TypePicker setArrayData:[NSMutableArray arrayWithArray:_arrayData]];
                [TypePicker setTitle:NSLocalizedString(@"选择类型", nil)];
                [TypePicker setTitleUnit:nil];
                [TypePicker setContentMode:STPickerContentModeBottom];
                [TypePicker setDelegate:self];
                [TypePicker setTag:indexPath.row];
                [TypePicker show];
            }
                break;
            default:
                break;
        }
    }
}
- (NSString *)getLoaclTime:(NSString *)format
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:format];//@"YYYY-MM-dd hh:mm:ss"];
    NSString *DateTime = [formatter stringFromDate:date];
    return DateTime;
}
- (void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle
{
    NSInteger index = [_arrayData indexOfObject:selectedTitle];
    [_RemoteInfoDic setObject:[NSString stringWithFormat:@"%ld",(long)index] forKey:@"time"];
    [_MainTable reloadData];
    [self setScreenSlave];
}


@end

