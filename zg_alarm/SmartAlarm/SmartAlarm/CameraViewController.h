//
//  CameraViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/21.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraViewController : UIViewController<UIAlertViewDelegate>

//from AddNetWorked
@property (nonatomic, retain) NSString *IDstr;
@property (nonatomic, retain) NSString *Passwordstr;

@end
