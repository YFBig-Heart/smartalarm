//
//  ShowSendEmailDetailViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2018/2/5.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowSendEmailDetailViewController : UIViewController
@property (retain,nonatomic)NSString * EmailAdress;
@end
