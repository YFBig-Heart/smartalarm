//
//  getBatForVule.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/16.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValueForImage : NSObject
+ (UIImage *)getBatImgForValue:(NSUInteger ) index;
+ (UIImage *)getSigImgForValue:(NSUInteger ) index;
@end
