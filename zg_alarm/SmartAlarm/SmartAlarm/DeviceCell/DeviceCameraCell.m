//
//  DeviceCameraCell.m
//  SmartAlarm
//
//  Created by zhangping on 2018/11/15.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "DeviceCameraCell.h"
#import "PlayVideo.h"

@implementation DeviceCameraCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
}

//设置cell信息
- (void)setCellWithInfo:(NSString *)name
{
    self.videoLab.text = NSLocalizedString(name, nil);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//播放视频
- (IBAction)PlayVideo:(UIButton *)sender
{
    NSLog(@"播放视频");
    if ([self.delegate respondsToSelector:@selector(deviceListCell:pushVC:isStoryboard:)])
    {
        [self.delegate deviceListCell:self pushVC:@"PlayVideo" isStoryboard:NO];
    }
}

//回放
- (IBAction)PlayBack:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(deviceListCell:pushVC:isStoryboard:)])
    {
        [self.delegate deviceListCell:self pushVC:@"PlaybackListsVC" isStoryboard:NO];
    }
}

//设置
- (IBAction)SettingBtn:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(deviceListCell:pushVC:isStoryboard:)])
    {
        [self.delegate deviceListCell:self pushVC:@"SettingVC" isStoryboard:NO];
    }
}

//删除
- (IBAction)DeleteBtn:(UIButton *)sender
{
    int row = sender.tag % 100;

    [self.btnDelegate cellBtnClicked:row];
}




@end
