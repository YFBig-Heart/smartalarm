//
//  FullScreenVC.h
//  SmartAlarm
//
//  Created by zhangping on 2018/11/29.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceModel.h"

@interface FullScreenVC : UIViewController

@property (nonatomic, strong) DeviceModel *deviceModel;

@end
