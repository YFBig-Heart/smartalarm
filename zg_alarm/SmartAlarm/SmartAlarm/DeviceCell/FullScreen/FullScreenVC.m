//
//  FullScreenVC.m
//  SmartAlarm
//
//  Created by zhangping on 2018/11/29.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "FullScreenVC.h"
#import "UIDevice+TFDevice.h"
#import "AppDelegate.h"


#import <GWP2P/GWP2P.h>

@interface FullScreenVC ()<GWP2PVideoPlayerProtocol>

@property(nonatomic,strong)UIButton *button;
@property (weak, nonatomic) IBOutlet UIView *playView;

@property (nonatomic, strong) GWP2PVideoPlayer *player;


@end

@implementation FullScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.allowRotation = YES;//允许转成横屏
    [UIDevice switchNewOrientation:UIInterfaceOrientationLandscapeRight];//调用转屏代码
    

    [[UIApplication sharedApplication] setStatusBarHidden:NO];//状态栏

    //添加播放器
    self.player = [[GWP2PVideoPlayer alloc] init];
    self.player.delegate = self;
    self.player.view.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.player.view];
#warning 下面这句话必须要加，否则监控画面不能显示
    [self addChildViewController:self.player.panoViewController];

    //[self.playView bringSubviewToFront:self.activityView];
    //[self.playView bringSubviewToFront:self.screenShotView];
    //self.screenShotView.hidden = YES;
    
    //[self videoPlay];
    
   UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleGesture:)];
    [self.player.view addGestureRecognizer:singleTap];
    
}

#pragma mark --手势触摸
- (void)singleGesture:(UITapGestureRecognizer*)gesture
{
    NSLog(@"一个手指单击了");
    _button = [[UIButton alloc]initWithFrame:CGRectMake(self.view.bounds.size.width-40, self.view.bounds.size.height-25-10, 40, 25)];//340
    //_button.backgroundColor = [UIColor clearColor];
    [_button setBackgroundImage:[UIImage imageNamed:@"half_full_screen_p"] forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(dismissController) forControlEvents:UIControlEventTouchUpInside];
    //[_button setTitle:@"全屏" forState:UIControlStateNormal];
    [self.player.view addSubview:_button];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    //_button.center = self.view.center;
}

#pragma mark --btn回调
- (void)dismissController
{
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.allowRotation = NO;//关闭横屏仅允许竖屏
    //切换到竖屏
    [UIDevice switchNewOrientation:UIInterfaceOrientationPortrait];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//MARK:状态栏的显示（横屏系统默认会隐藏的）
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"PRESENTViewController delloc");
}

#pragma mark --显示开始播放
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self videoPlay];
}

#pragma mark --消失停止播放
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.player p2pStop];
}

#pragma mark --视频播放
- (void)videoPlay
{
    __weak typeof(self) weakSelf = self; //这里的几个代码块都要用弱引用
    //[self.activityView startAnimating];
    
    NSLog(@"deviceID=%@,devicePassword=%@,deviceType=%d,deviceSubtype=%d",_deviceModel.deviceID,_deviceModel.devicePassword,(int)_deviceModel.deviceType,(int)_deviceModel.deviceSubtype);
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [weakSelf.player p2pCallDeviceWithDeviceId:_deviceModel.deviceID password:_deviceModel.devicePassword deviceType:_deviceModel.deviceType deviceSubtype:_deviceModel.deviceSubtype calling:^(NSDictionary *parameters)
         {
             NSLog(@"[p2pCallDevice-Calling],paras=%@",parameters);
             
         } accept:^(NSDictionary *parameters)
         {
             NSLog(@"[p2pCallDevice-Accept],paras=%@",parameters);
             
         } reject:^(GWP2PCallError error, NSString *errorCode)
         {
             NSLog(@"[p2pCallDevice-Reject],error=%ld,errorCode=%@",(unsigned long)error, errorCode);
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 //[weakSelf.activityView stopAnimating];
                 
             });
             
         } ready:^{
             
             NSLog(@"[p2pCallDevice-Ready] %@",[NSThread currentThread]);
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 //[weakSelf.activityView stopAnimating];
                 
             });
             
         }];
    });
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    //self.player.view.frame = self.playView.bounds;
    self.player.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width-50);
}

#pragma mark - 代理回调测试
- (void)p2pPlayer:(GWP2PPlayer *)player recieveGestureRecognizer:(UIGestureRecognizer *)gesture
{
    NSLog(@"-----delegate---recieveGestureRecognizer");
}

- (void)p2pVideoPlayer:(GWP2PVideoPlayer *)player numberOfAudiencesChange:(NSUInteger)currentNumber
{
    NSLog(@"-----delegate---numberOfAudiencesChange");
}


@end
