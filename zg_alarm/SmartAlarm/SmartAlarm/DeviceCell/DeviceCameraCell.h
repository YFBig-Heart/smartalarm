//
//  DeviceCameraCell.h
//  SmartAlarm
//
//  Created by zhangping on 2018/11/15.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceModel.h"

@protocol btnClickedDelegate <NSObject>

-(void)cellBtnClicked:(int)row;

@end


@class DeviceCameraCell;

@protocol DeviceCameraCellDelegate <NSObject>

- (void)deviceListCell:(DeviceCameraCell *)cell pushVC:(NSString *)vcName isStoryboard:(BOOL)isStoryboard;

@end

@interface DeviceCameraCell : UITableViewCell

@property (nonatomic, weak) id<DeviceCameraCellDelegate> delegate;
@property (nonatomic, strong) DeviceModel *deviceModel;
@property (weak, nonatomic) IBOutlet UILabel *videoLab;

@property (nonatomic,weak) id<btnClickedDelegate>  btnDelegate;//cell btn

-(void)setCellWithInfo:(NSString *)name;//设置cell信息


@end
