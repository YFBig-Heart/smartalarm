//
//  PlayVideo.m
//  SmartAlarm
//
//  Created by zhangping on 2018/11/15.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "PlayVideo.h"

#import "GWNet.h"
#import <GWP2P/GWP2P.h>
#import "DeviceModel.h"
#import "EBDropdownListView.h"

#import "FullScreenVC.h"

@interface PlayVideo ()<GWP2PVideoPlayerProtocol>
{
    int orientation;
    
}

@property (nonatomic, strong) DeviceModel *deviceModel;
@property (nonatomic, strong) GWP2PVideoPlayer *player;
@property (nonatomic, strong) GWP2PMP4Recorder *mp4Recorder;


@property (weak, nonatomic) IBOutlet UIView *playView;
@property (weak, nonatomic) IBOutlet UIImageView *screenShotView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;


//btn
@property (weak, nonatomic) IBOutlet UIButton *VideoRecBtn;
@property (weak, nonatomic) IBOutlet UIButton *TakePhotoBtn;
@property (weak, nonatomic) IBOutlet UIButton *VolumeBtn;
@property (weak, nonatomic) IBOutlet UIButton *talkSwitch;

@property (weak, nonatomic) IBOutlet UIButton *screenBtn;

//弹出框
@property (nonatomic,retain) EBDropdownListView *dropdownListView;



@end

@implementation PlayVideo

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = self.deviceModel.deviceID;
    
    NSLog(@"self.deviceModel.deviceID=%@",self.deviceModel.deviceID);
    
    //获取播放器比例
//    CGFloat ratio = [self raitoFromSubtype:_deviceModel.deviceSubtype];
//    [self.playViewRatio setValue:@(ratio) forKey:@"multiplier"];
    
    //添加播放器
    self.player = [[GWP2PVideoPlayer alloc] init];
    self.player.delegate = self;
    self.player.view.backgroundColor = [UIColor redColor];
    [self.playView addSubview:self.player.view];
    
    [self addChildViewController:self.player.panoViewController];
    
    [self.playView bringSubviewToFront:self.activityView];
    [self.playView bringSubviewToFront:self.screenShotView];
    self.screenShotView.hidden = YES;
    
    //self.playView.backgroundColor = [UIColor greenColor];
    
   // [self PlayStart];
    
    [self SmoothSDHDSelect];
    
   [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(deviceOrientationChanged)
                                                name:UIDeviceOrientationDidChangeNotification
                                                object:nil];
    
}

//流畅/标清/高清
- (void)SmoothSDHDSelect
{
    EBDropdownListItem *item1 = [[EBDropdownListItem alloc] initWithItem:@"1" itemName:NSLocalizedString(@"标清",nil)];
    EBDropdownListItem *item2 = [[EBDropdownListItem alloc] initWithItem:@"2" itemName:NSLocalizedString(@"流畅",nil)];
    EBDropdownListItem *item3 = [[EBDropdownListItem alloc] initWithItem:@"3" itemName:NSLocalizedString(@"高清",nil)];
    
    
    //弹出框向上
    self.dropdownListView = [[EBDropdownListView alloc] initWithDataSource:@[item1, item2, item3]];
    self.dropdownListView.frame = CGRectMake(6, self.screenBtn.frame.origin.y, 65, 30); //CGRectMake(20, 100, 130, 30);
    self.dropdownListView.selectedIndex = 2;
    self.dropdownListView.font = [UIFont systemFontOfSize:13.0];
    self.dropdownListView.textColor = [UIColor whiteColor];
    [self.dropdownListView setViewBorder:0.5 borderColor:[UIColor whiteColor] cornerRadius:10];
    [self.view addSubview:self.dropdownListView];

    [self.dropdownListView setDropdownListViewSelectedBlock:^(EBDropdownListView *dropdownListView) {
        NSString *msgString = [NSString stringWithFormat:
                               @"selected name:%@  id:%@  index:%ld"
                               , self.dropdownListView.selectedItem.itemName
                               , self.dropdownListView.selectedItem.itemId
                               , self.dropdownListView.selectedIndex];

        NSLog(@"msgString=%@",msgString);
        
        //选择-流畅/标清/高清
        [self SmoothSDHD:self.dropdownListView.selectedItem.itemName];

    }];
    
}

#pragma mark --即将消失
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.player p2pStop];
    
     self.talkSwitch.hidden = YES;
    self.VideoRecBtn.hidden = YES;
    self.TakePhotoBtn.hidden = YES;
    self.VolumeBtn.hidden = YES;
   
    //self.EBDropdownListView.hidden = YES;
}

#pragma mark --即将显示
- (void)viewWillAppear:(BOOL)animated
{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    
    self.talkSwitch.hidden = NO;
    self.VideoRecBtn.hidden = NO;
    self.TakePhotoBtn.hidden = NO;
    self.VolumeBtn.hidden = NO;
   
    //self.EBDropdownListView.hidden = NO;
    
    [self PlayStart];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    //self.player.view.frame = self.playView.bounds;
    self.player.view.frame = CGRectMake(10, 64, self.view.bounds.size.width-20, 300);
}

#pragma mark - 播放画面比例
- (CGFloat)raitoFromSubtype:(GWP2PDeviceIPCSubtype)subtype {
    GWP2PDeviceVideoRatio ratio = [GWP2PDevice getVideoRatioWithSubtype:subtype];
    if (ratio==GWP2PDeviceVideoRatio16X9) {
        return 16.0 / 9;
    }
    if (ratio==GWP2PDeviceVideoRatio1X1) {
        return 1/1;
    }
    if (ratio==GWP2PDeviceVideoRatio4X3) {
        return 4.0/3;
    }
    return 16.0/9;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc
{
    NSLog(@"%s %d",__func__,__LINE__);
    [self.player p2pStop];
}

//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//     [self.player p2pStop];
//}


#pragma mark --开始播放
- (void)PlayStart
{
    __weak typeof(self) weakSelf = self; //这里的几个代码块都要用弱引用
    [self.activityView startAnimating];
    
     NSLog(@"deviceID=%@,devicePassword=%@,deviceType=%d,deviceSubtype=%d",_deviceModel.deviceID,_deviceModel.devicePassword,(int)_deviceModel.deviceType,(int)_deviceModel.deviceSubtype);
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [weakSelf.player p2pCallDeviceWithDeviceId:_deviceModel.deviceID password:_deviceModel.devicePassword deviceType:_deviceModel.deviceType deviceSubtype:_deviceModel.deviceSubtype calling:^(NSDictionary *parameters)
         {
             NSLog(@"[p2pCallDevice-Calling],paras=%@",parameters);
             
         } accept:^(NSDictionary *parameters)
         {
             NSLog(@"[p2pCallDevice-Accept],paras=%@",parameters);
             
         } reject:^(GWP2PCallError error, NSString *errorCode)
         {
             NSLog(@"[p2pCallDevice-Reject],error=%ld,errorCode=%@",(unsigned long)error, errorCode);
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [weakSelf.activityView stopAnimating];
                 
             });
             
         } ready:^{
             
             NSLog(@"[p2pCallDevice-Ready] %@",[NSThread currentThread]);
             dispatch_async(dispatch_get_main_queue(), ^{

                 [weakSelf.activityView stopAnimating];

             });
            
         }];
    });
    
}

#pragma mark --视频录制
- (IBAction)VideoRecBtn:(UIButton *)sender
{
    [self.mp4Recorder startRecordWithSavePath:[self getRecordFilePathWithDeviceId:self.deviceModel.deviceID] eventHandler:^(MP4RecordEvent stopEvent) {
        
        switch (stopEvent) {
                
            case MP4RecordEventStart:
                
                NSLog(@"录像开始（初始化需要少量时间等待关键帧）");
                break;
                
            case MP4RecordEventStop:
                
                NSLog(@"主动调用了stopRecord");
                break;
                
            case MP4RecordEventRatioChanged:
                
                NSLog(@"切换了监控清晰度，录像停止（正常保存）");
                break;
                
            case MP4RecordEventEncoderInitFailed:
                
                NSLog(@"录像失败，初始化失败");
                break;
                
            default:
                break;
        }
    }];
    
}

- (NSString*)getRecordFilePathWithDeviceId:(NSString*)deviceId
{
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970];
    
    NSString *savePath = [NSString stringWithFormat:@"%@/recordMP4/%@",rootPath,deviceId];
    
    NSFileManager *manager = [NSFileManager defaultManager];
    if(![manager fileExistsAtPath:savePath])
    {
        [manager createDirectoryAtPath:savePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString* fullPath = [NSString stringWithFormat:@"%@/%ld.mp4",savePath,(long)timeInterval];
    
    return fullPath;
}

#pragma mark --拍照
- (IBAction)TakePhotoBtn:(UIButton *)sender
{
    __weak typeof(self) weakself = self;
    //todo: 不停截图内存升高
    [self.player p2pScreenshot:^(UIImage *screenshot, NSTimeInterval timeInterval) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //weakself.screenShotView.image = nil;
            weakself.screenShotView.image = screenshot;
            weakself.screenShotView.hidden = NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                weakself.screenShotView.hidden = YES;
                
            });
            
        });
    }];
}

#pragma mark --静音
- (IBAction)VolumeBtn:(UIButton *)sender
{
    static BOOL isMute = NO;
    isMute = !isMute;
    self.player.mute = isMute;
}

#pragma mark --麦克风
- (IBAction)talkSwitch:(UIButton *)sender
{
    static BOOL talkSwitch = YES;
    talkSwitch = !talkSwitch;
    NSLog(@"talkSwitch %d",talkSwitch);
    
    [self.player p2pEnableSpeak:talkSwitch];
    
}

#pragma mark --全屏
- (IBAction)screenBtn:(UIButton *)sender
{
    
    NSLog(@"全屏");
    self.player.view.frame = self.view.frame;
    
    //导航栏
    //self.navigationController.navigationBar.hidden = YES;
    
    FullScreenVC *present = [[FullScreenVC alloc]init];
    present.view.backgroundColor = [UIColor blackColor];
    present.deviceModel = self.deviceModel;
    [self presentViewController:present animated:YES completion:nil];
    
}

// 设备方向改变的回调方法
- (void)deviceOrientationChanged
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    // 判断当前设备方向是竖屏还是横屏
    if (UIDeviceOrientationIsPortrait((UIDeviceOrientation)orientation))
    {
        NSLog(@"当前设备方向是竖屏");
    }
    else
    {
        NSLog(@"当前设备方向是横屏");
    }
}


//使用这里的代码也是oK的。 这里利用 NSInvocation 调用 对象的消息
//- (void) viewWillAppear:(BOOL)animated
- (void)HorizontalScreen
{
    //[super viewWillAppear:animated];
    if([[UIDevice currentDevice]respondsToSelector:@selector(setOrientation:)]) {
        
        SEL selector = NSSelectorFromString(@"setOrientation:");
        
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val = UIDeviceOrientationLandscapeRight;//横屏
        
        [invocation setArgument:&val atIndex:2];
        
        [invocation invoke];
        
    }
}

#pragma mark --流畅/标清/高清
-(void)SmoothSDHD:(NSString *) namestr
{
    
    if ([namestr isEqualToString:NSLocalizedString(@"流畅",nil)]) {
        
        [self.player p2pSetDefinition:GWP2PPTZDefinitionLD];
    }
    if ([namestr isEqualToString:NSLocalizedString(@"标清",nil)]) {
        
        [self.player p2pSetDefinition:GWP2PPTZDefinitionSD];
    }
    if ([namestr isEqualToString:NSLocalizedString(@"高清",nil)]) {
        
        [self.player p2pSetDefinition:GWP2PPTZDefinitionHD];
    }
    
}

#pragma mark - 代理回调测试
- (void)p2pPlayer:(GWP2PPlayer *)player recieveGestureRecognizer:(UIGestureRecognizer *)gesture
{
    NSLog(@"-----delegate---recieveGestureRecognizer");
}

- (void)p2pVideoPlayer:(GWP2PVideoPlayer *)player numberOfAudiencesChange:(NSUInteger)currentNumber
{
    NSLog(@"-----delegate---numberOfAudiencesChange");
}

#pragma mark --test 横屏
- (void)testHorizontalScreen
{
    //来显示或者隐藏状态栏，一般情况下竖屏时状态栏会显示 横屏则隐藏
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    
    self.navigationController.navigationBarHidden = YES;
    
}




@end
