//
//  TimerEditViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "TimerEditViewController.h"
#import "STPickerView.h"
#import "STPickerDate.h"
#import "HcdDateTimePickerView.h"
#import "STPickerSingle.h"
#import "WeekEditViewController.h"
#import "DaysSelectTableViewController.h"
#import "TimeWeekSelectViewController.h"

@interface TimerEditViewController ()<STPickerDateDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * titleArray;
@property (assign) int Alarm_Hour;
@property (assign) int Alarm_Min;
@property (retain,nonatomic)NSArray * AutoSetting;
@end

@implementation TimerEditViewController

- (void)viewDidLoad {
    _AutoSetting = @[NSLocalizedString(@"停用", nil),NSLocalizedString(@"自动设防", nil ),NSLocalizedString(@"自动撤防", nil),NSLocalizedString(@"自动在家", nil)];
    if (_titleArray == nil) {
        _titleArray = @[@"时间",@"星期",@"类型"];
    }
    if (_BoardDic == nil) {
        _BoardDic = [[NSMutableDictionary alloc] init];
    }
    NSString * minString = [_BoardDic objectForKey:@"timer"];
    _Alarm_Hour = [[minString substringWithRange:NSMakeRange(0, 2)] intValue];
    _Alarm_Min = [[minString substringWithRange:NSMakeRange(3, 2)] intValue];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"weekingString"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];;
    [super viewDidLoad];
    self.title =NSLocalizedString(@"自动设/撤防定时器", nil);
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"TimeDicKEy"]) {
        _BoardDic = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"TimeDicKEy"]];
        [_MainTable reloadData];
    }
}
- (void)viewDidAppear:(BOOL)animated
{
    if (([[NSUserDefaults standardUserDefaults] objectForKey:@"TimeDicKEy"])) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TimeDicKEy"];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)Done
{
    [_BoardDic setObject:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"] forKey:@"imei"];
    [_BoardDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID] forKey:@"UserName"];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:_BoardDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80071",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _titleArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSString * titleString = [_titleArray objectAtIndex:indexPath.row];
    cell.textLabel.text = NSLocalizedString(titleString, nil);
    switch (indexPath.row) {
        case 0:
            cell.detailTextLabel.text =[NSString stringWithFormat:@"%02d:%02d",_Alarm_Hour,_Alarm_Min];
            break;
        case 1:
        {
            [cell.detailTextLabel setFont:[UIFont systemFontOfSize:12.0f]];
            NSArray * weekDayArray = @[@"周日",@"周一",@"周二",@"周三",@"周四",@"周五",@"周六"];
            NSString * weekDayString = [_BoardDic objectForKey:@"week"];
            NSMutableString * detailString = [NSMutableString new];
            for (int v=0; v < 7; v++) {
                if ([[weekDayString substringWithRange:NSMakeRange(v, 1)] intValue]) {
                    [detailString appendString:NSLocalizedString(weekDayArray[v], nil)];
                    [detailString appendString:@" "];
                }
            }
            [cell.detailTextLabel setText:detailString];
            [cell.detailTextLabel setFont:[UIFont systemFontOfSize:12.0f]];
        }
            break;
        case 2:
        {
            NSString * detailString = _AutoSetting[[[_BoardDic objectForKey:@"type"] intValue]];
            [cell.detailTextLabel setText:detailString];
        }
            break;
        default:
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
            HcdDateTimePickerView *dateTimePickerView = [[HcdDateTimePickerView alloc] initWithDatePickerMode:DatePickerHourMinuteMode defaultDateTime:[[NSDate alloc]initWithTimeIntervalSinceNow:0]];
            dateTimePickerView.clickedOkBtn = ^(NSString * datetimeStr){
                NSLog(@"%@", datetimeStr);
                _Alarm_Hour = [[datetimeStr substringWithRange:NSMakeRange(0, 2)] intValue];
                _Alarm_Min = [[datetimeStr substringWithRange:NSMakeRange(3, 2)] intValue];
                [_BoardDic setObject:datetimeStr forKey:@"timer"];
                [self.MainTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            };
            [self.view addSubview:dateTimePickerView];
            [dateTimePickerView showHcdDateTimePicker];
        }
            break;
        case 1:
        {
            TimeWeekSelectViewController * vc = [[TimeWeekSelectViewController alloc] init];
            vc.TimeDic = [[NSMutableDictionary alloc] initWithDictionary:_BoardDic];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:
        {
            STPickerSingle *TypePicker = [[STPickerSingle alloc]init];
            [TypePicker setArrayData:_AutoSetting];
            [TypePicker setTitle:NSLocalizedString(@"选择类型", nil)];
            [TypePicker setTitleUnit:nil];
            [TypePicker setContentMode:STPickerContentModeBottom];
            [TypePicker setDelegate:self];
            [TypePicker show];
        }
            break;
            
        default:
            break;
    }
}
- (void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle
{
    [_BoardDic setObject:[NSString stringWithFormat:@"%ld",[_AutoSetting indexOfObject:selectedTitle]] forKey:@"type"];
    [_MainTable reloadData];
}
@end
