//
//  BindForEditDeviceViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/11/12.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "BindForEditDeviceViewController.h"
#import "APNSettingViewController.h"
#import "WifiConfigViewController.h"
#import "PreEntryIdleViewController.h"

@interface BindForEditDeviceViewController ()
@property (weak, nonatomic) IBOutlet UITextField *DeviceNameFD;
@property (weak, nonatomic) IBOutlet UITextField *DevicePhone;
@property (weak, nonatomic) IBOutlet UIButton *DoneBTN;
@property (weak, nonatomic) IBOutlet UILabel *DetailLabel;

@end

@implementation BindForEditDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    switch (_DeviceType) {
        case 1:
            [_DeviceNameFD setText:NSLocalizedString(@"智能报警器", nil) ];
            break;
        case 2:
            [_DeviceNameFD setText:NSLocalizedString(@"智能报警器(WIFI)",nil)];
            break;
        case 3:
            [_DeviceNameFD setText:NSLocalizedString(@"智能排插",nil)];
            break;
        case 4:
            [_DeviceNameFD setText:NSLocalizedString(@"智能家电",nil)];
            break;
        default:
            [_DeviceNameFD setText:NSLocalizedString(@"智能报警器", nil) ];
            break;
    }
    self.title = NSLocalizedString(@"资料编辑", nil);
    [_DoneBTN bs_configureAsPrimaryStyle];
    [_DoneBTN setTitle:NSLocalizedString(@"下一步", nil) forState:UIControlStateNormal];
    _DevicePhone.placeholder = NSLocalizedString(@"请输入手机号码", nil);
    [[_DoneBTN rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self EditDone];
    }];
    [_DetailLabel setText:NSLocalizedString(@"请输入设备上的手机卡号码。如果您没有使用手机卡连接，则可以输入 000 跳过此步骤", nil)];
}
- (void)EditDone
{
    NSDictionary * tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"username",_DeviceNameFD.text,@"name",_DevicePhone.text,@"number",_DeviceID,@"imei",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00003",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
//            if (_DeviceType == 1) {
//                APNSettingViewController * vc = [[APNSettingViewController alloc] init];
//                vc.EntryType = 1;
//                vc.currIMEI = [NSString stringWithString:_DeviceID];
//                vc.SmsSendNumber = [_DevicePhone.text copy];
//                [self.navigationController pushViewController:vc animated:YES];
//            }
//            else
//            {
//                WifiConfigViewController * vc = [[WifiConfigViewController alloc] init];
//                vc.currIMEI = [NSString stringWithString:_DeviceID];
//                vc.EntryType = 1;
//                [self.navigationController pushViewController:vc animated:YES];
//            }
            PreEntryIdleViewController * vc = [[PreEntryIdleViewController alloc] init];
            vc.BindDeviceImei = [_DeviceID copy];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
