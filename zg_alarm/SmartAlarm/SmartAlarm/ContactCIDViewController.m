//
//  ContactCIDViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/8/7.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "ContactCIDViewController.h"
#import "ContanctSelectViewController.h"

@interface ContactCIDViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) UITextField * TF_CID;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@end

@implementation ContactCIDViewController

- (void)viewDidLoad {
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    self.title = NSLocalizedString(@"Contanct ID",nil);
    if (_TitleArray == nil) {
        _TitleArray = @[@"CID code",@"Report Info"];
        _RemoteInfoDic = [[NSMutableDictionary alloc] init];
    }
    [self synCID];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    [_MainTable reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)Done
{
    [self PostCID];
}
#pragma mark - TableviewDelegate
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreen_Width, 100)];
    footView.backgroundColor = [UIColor clearColor];
    UIButton *sureButton  = [[UIButton alloc]initWithFrame:CGRectMake(80, 25, kScreen_Width-80, 50)];
    sureButton.layer.cornerRadius = 2.0f;
    sureButton.layer.masksToBounds = YES;
    [sureButton setTitle:NSLocalizedString(@"确定", nil) forState:UIControlStateNormal];
    [sureButton addTarget:self action:@selector(Done) forControlEvents:UIControlEventTouchUpInside];
    sureButton.titleLabel.textColor = [UIColor grayColor];
    sureButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [sureButton bs_configureAsPrimaryStyle];
    [footView addSubview:sureButton];
    sureButton.sd_layout.centerXEqualToView(self.view);
    
    return footView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 65.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            if (_TF_CID) {
                [_TF_CID removeFromSuperview];
                _TF_CID = nil;
            }
            _TF_CID = [[UITextField alloc] initWithFrame:CGRectZero];
            [_TF_CID setTextColor:[UIColor flatGrayColor]];
            if ([_RemoteInfoDic objectForKey:@"cid_code"]) {
                [_TF_CID setText:[_RemoteInfoDic objectForKey:@"cid_code"]];
            }
//            [_TF_CID setPlaceholder:NSLocalizedString(@"请输入接入点名称", nil)];
            [_TF_CID setTextAlignment:NSTextAlignmentRight];
            [_TF_CID setBackgroundColor:[UIColor clearColor]];
            [_TF_CID setKeyboardType:UIKeyboardTypeNumberPad];
            [cell addSubview:_TF_CID];
            _TF_CID.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[self.TF_CID.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  if (text.length >= 4) {
                      _TF_CID.text = [_TF_CID.text substringToIndex:4];
                  }
                  return text.length == 4;
              }]
             subscribeNext:^(id x) {
                 
                 [_RemoteInfoDic setObject:_TF_CID.text forKey:@"apn"];
                 NSLog(@"%@", x);
             }];
        }
            break;
        case 1:
        {
            NSString * CIDStatuesString = [[NSUserDefaults standardUserDefaults] objectForKey:@"CIDStatuesString"];
            int  int_tmp =  [CIDStatuesString intValue];
            NSArray * disString = @[@"布防",@"撤防",@" 在家"];
            NSString * totalString = @"";
            for (int i=0; i<3; i++) {
                if(int_tmp & (1<<i))
                {
                    totalString = [NSString stringWithFormat:@"%@ %@",totalString,NSLocalizedString(disString[i], nil)];
                }
            }
            cell.detailTextLabel.text = totalString;
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
            break;
        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
        {
            ContanctSelectViewController * vc = [[ContanctSelectViewController alloc] init];
            vc.StatuesString = [[NSUserDefaults standardUserDefaults] objectForKey:@"CIDStatuesString"];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}
#pragma mark 同步网络
- (void)synCID
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00023",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            NSString * tmpReportString = [_RemoteInfoDic objectForKey:@"report"];
            [[NSUserDefaults standardUserDefaults] setObject:tmpReportString forKey:@"CIDStatuesString"];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)PostCID
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",_TF_CID.text,@"cid_code",[[NSUserDefaults standardUserDefaults] objectForKey:@"CIDStatuesString"],@"report",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00024",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}

@end
