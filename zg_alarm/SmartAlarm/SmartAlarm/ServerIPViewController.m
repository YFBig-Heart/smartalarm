//
//  ServerIPViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "ServerIPViewController.h"
#import <MessageUI/MessageUI.h>

@interface ServerIPViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * ImageArray;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UITextField * ServerIPFD;
@property (retain,nonatomic) UITextField * ServerPortFD;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@end

@implementation ServerIPViewController

- (void)viewDidLoad {
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_TitleArray == nil) {
        _TitleArray = @[@"服务器IP地址",@"端口"];
    }
    _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultServerIP]];
    self.title = NSLocalizedString(@"编辑", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone  target:self action:@selector(Done)];
    [super viewDidLoad];
    [[UINavigationBar appearance] setBackgroundImage:[self imageWithColor:FlatSkyBlue] forBarMetrics:UIBarMetricsDefault];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
}
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage *)imageWithColor:(UIColor *)color {
    static NSCache *imageCache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        imageCache = [[NSCache alloc] init];
    });
    
    UIImage *image = [imageCache objectForKey:color];
    if (image) {
        return image;
    }
    
    image = [self imageWithColor:color size:CGSizeMake(1,1)];
    [imageCache setObject:image forKey:color];
    
    return image;
}

- (void)Done
{
    if ([self canSendSMS] && _SmsSendNumber) {
        //860755#IP#124.232.150.158,7800#
        NSString * smsContent = [NSString stringWithFormat:@"860755#IP#%@,%@",_ServerIPFD.text,_ServerPortFD.text];
        [self sendSMS:smsContent recipient:_SmsSendNumber];
    }
}
- (void)synDeviceServiceConfig
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.imageView.image = [UIImage imageNamed:[_ImageArray objectAtIndex:indexPath.row]];
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            if (_ServerIPFD) {
                [_ServerIPFD removeFromSuperview];
                _ServerIPFD = nil;
            }
            _ServerIPFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_ServerIPFD setTextColor:[UIColor flatGrayColor]];
            [_ServerIPFD setText:[_RemoteInfoDic objectForKey:@"apn"]];
            [_ServerIPFD setTextAlignment:NSTextAlignmentRight];
            [_ServerIPFD setBackgroundColor:[UIColor clearColor]];
            [cell addSubview:_ServerIPFD];
            _ServerIPFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[self.ServerIPFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  [self.navigationItem.rightBarButtonItem setEnabled:NO];
                  return text.length > 1;
              }]
             subscribeNext:^(id x) {
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
                 if([self.ServerIPFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.ServerIPFD.text = [self.ServerIPFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }

                 [_RemoteInfoDic setObject:_ServerPortFD.text forKey:@"apn"];
                 NSLog(@"%@", x);
             }];
        }
            break;
        case 1:
        {
            if (_ServerPortFD) {
                [_ServerPortFD removeFromSuperview];
                _ServerPortFD = nil;
            }
            _ServerPortFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_ServerPortFD setTextColor:[UIColor flatGrayColor]];
            [_ServerPortFD setText:[_RemoteInfoDic objectForKey:@"apn"]];
            [_ServerPortFD setTextAlignment:NSTextAlignmentRight];
            [_ServerPortFD setBackgroundColor:[UIColor clearColor]];
            [cell addSubview:_ServerPortFD];
            _ServerPortFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[self.ServerPortFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return text.length > 2;
              }]
             subscribeNext:^(id x) {
                 if([self.ServerPortFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.ServerPortFD.text = [self.ServerPortFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }

                 [_RemoteInfoDic setObject:_ServerPortFD.text forKey:@"apn"];
                 NSLog(@"%@", x);
             }];
        }
            break;

        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
            break;
        default:
            break;
    }
}
#pragma mark - MFMessageComposeViewController

- (BOOL) canSendSMS {
    return [MFMessageComposeViewController canSendText];
}

- (void) sendSMS:(NSString *)message {
    [self sendSMS:message recipients:nil];
}

- (void) sendSMS:(NSString *)message recipient:(NSString*)recipient {
    if (recipient != nil) {
        [self sendSMS:message recipients:[NSArray arrayWithObject:recipient]];
    }
    else {
        [self sendSMS:message recipients:nil];
    }
}

- (void) sendSMS:(NSString *)message recipients:(NSArray*)recipients {
    if ([self canSendSMS]) {
        MFMessageComposeViewController *smsController = [[MFMessageComposeViewController alloc] init];
        smsController.messageComposeDelegate = self;
        if (message != nil) {
            smsController.body = message;
        }
        if (recipients != nil) {
            smsController.recipients = recipients;
        }
        smsController.modalPresentationStyle = UIModalPresentationFormSheet;
        smsController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentViewController:smsController animated:YES completion:^{
            
        }];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    if (result == MessageComposeResultSent) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionaryWithDictionary:_RemoteInfoDic] forKey:kDefaultServerIP];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (result == MessageComposeResultFailed) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (result == MessageComposeResultCancelled) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
@end
