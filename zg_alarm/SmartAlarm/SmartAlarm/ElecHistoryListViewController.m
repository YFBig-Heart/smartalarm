//
//  ElecHistoryListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/3/13.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "ElecHistoryListViewController.h"
#import "MsgTableViewCell.h"
#import "STPickerView.h"
#import "STPickerDate.h"
#import "STPickerSingle.h"
#import "PTLAlertView.h"
#import "ElecHistoryTableViewCell.h"
@interface ElecHistoryListViewController ()<UITableViewDataSource,UITableViewDelegate,STPickerDateDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) UIActivityIndicatorView *activity;
@property (retain,nonatomic) NSArray * RemoteArray;
@property (retain,nonatomic) NSArray * AutoSetting;
@property (weak, nonatomic) IBOutlet UILabel *DateLabel;
@property (weak, nonatomic) IBOutlet UIButton *SelectBtn;
@property (retain,nonatomic) NSArray * titleArray;



@end

@implementation ElecHistoryListViewController

- (void)viewDidLoad {
    _titleArray = @[@"设备名称:",@"开始时间:",@"结束时间:",@"电量:"];
    [super viewDidLoad];
    _activity = [[UIActivityIndicatorView alloc]
                 initWithFrame : CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)] ;
    [_activity startAnimating];
    [_activity hidesWhenStopped];
    [_SelectBtn setTitle:NSLocalizedString(@"选择", nil) forState:UIControlStateNormal];
    _SelectBtn.sd_layout.centerYEqualToView(_DateLabel);
    self.title = NSLocalizedString(@"电量记录", nil);
    _AutoSetting = @[NSLocalizedString(@"今天",nil),NSLocalizedString(@"一个月",nil),NSLocalizedString(@"三个月内",nil),NSLocalizedString(@"自定义",nil)];
    _MainTable.dataSource = self;
    _MainTable.delegate = self;
    [_DateLabel setText:[NSDate new].yyyyMMddByLineWithDate];
    [_DateLabel setFont:[UIFont systemFontOfSize:13]];
//    [self synDeviceMsg];
    [[_SelectBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        STPickerSingle *TypePicker = [[STPickerSingle alloc]init];
        [TypePicker setArrayData:_AutoSetting];
        [TypePicker setTitle:NSLocalizedString(@"选择", nil)];
        [TypePicker setTitleUnit:nil];
        [TypePicker setContentMode:STPickerContentModeBottom];
        [TypePicker setDelegate:self];
        [TypePicker show];
    }];
}
#pragma mark ---- 将时间戳转换成时间

- (NSString *)getTimeFromTimestamp:(double) timestamp
{
    //将对象类型的时间转换为NSDate类型
    NSDate * myDate=[NSDate dateWithTimeIntervalSince1970:timestamp];
    //设置时间格式
    NSDateFormatter * formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY/MM/dd HH:mm"];
    //将时间转换为字符串
    NSString *timeStr=[formatter stringFromDate:myDate];
    return timeStr;
}
- (NSDate *)GetStart0Time
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    NSDate *startDate = [calendar dateFromComponents:components];
    return startDate;
    //    NSDate *endDate = [calendar dateByAddingUnit:NSCalendarUnitDay value:1 toDate:startDate options:0];
}
#pragma mark --- 将时间转换成时间戳

- (NSString *)getTimestampFromTime{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *nowtimeStr = [formatter stringFromDate:datenow];//----------将nsdate按formatter格式转成nsstring
    
    NSLog(@"%@", nowtimeStr);
    
    // 时间转时间戳的方法:
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    
    NSLog(@"timeSp:%@",timeSp); //时间戳的值
    
    return timeSp;
    
}
- (void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle
{
    NSDate * SelectDate = [NSDate date];
    if ([selectedTitle isEqualToString:NSLocalizedString(@"今天", nil)]) {
        //今天
        NSString * dateString = SelectDate.yyyyMMddNoLineWithDate;
        NSLog(@"DateString: %@",dateString);
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld", (long)[[NSDate new] timeIntervalSince1970]] forKey:kDefaultEndKeyVaule];
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld", (long)[[self GetStart0Time] timeIntervalSince1970]] forKey:kDefaultStartKeyValue];
        [self synDeviceMsg];

    }
    else if([selectedTitle isEqualToString:NSLocalizedString(@"一个月", nil)])
    {
        //本月
        NSString * dateString = SelectDate.yyyyMMddNoLineWithDate;
        NSLog(@"DateString: %@",dateString);
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld", (long)([[NSDate new] timeIntervalSince1970] - 3600*24*30)] forKey:kDefaultStartKeyValue];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld", (long)[[NSDate new] timeIntervalSince1970]] forKey:kDefaultEndKeyVaule];
        [self synDeviceMsg];

    }
    else if([selectedTitle isEqualToString:NSLocalizedString(@"三个月内", nil)])
    {
        //三个月内
        NSString * dateString = SelectDate.yyyyMMddNoLineWithDate;
        NSLog(@"DateString: %@",dateString);
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld", (long)([[NSDate new] timeIntervalSince1970] - 3600*24*30*3)] forKey:kDefaultStartKeyValue];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld", (long)[[NSDate new] timeIntervalSince1970]] forKey:kDefaultEndKeyVaule];
        [self synDeviceMsg];
    }
    else
    {
        //自定义
        PTLAlertView *alertView = [[PTLAlertView alloc]initWithTitle:NSLocalizedString(@"选择", nil) message:@"你好你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈吗哈哈你好你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈吗哈哈你好你好吗哈哈你好你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈你好吗哈哈吗哈哈" cancelButtonTitle:NSLocalizedString(@"确定",nil) otherButtonTitles:NSLocalizedString(@"取消", nil), nil];
        [alertView setSelctBtnBlock:^(NSInteger index, NSString * _Nullable btnCurrentTitle) {
            NSLog(@"hha- %zd ---- %@", index, btnCurrentTitle);
            if (index != 0) {
                double dataDoubleStart = [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultStartKeyValue] doubleValue];
                double dataDoubleEnd = [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultEndKeyVaule] doubleValue];
                [_DateLabel setText:[NSString stringWithFormat:@"%@-%@",[self getTimeFromTimestamp:dataDoubleStart],[self getTimeFromTimestamp:dataDoubleEnd]]];
                [_DateLabel setFont:[UIFont systemFontOfSize:13]];
                [self synDeviceMsg];
            }
        }];
        [alertView show];
    }
    [_MainTable reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)synDeviceMsg
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultEndKeyVaule],@"endtime",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultStartKeyValue],@"starttime",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"90009",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        [_activity setHidden:YES];
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            if (([[response objectForKey:@"data"] isKindOfClass:[NSArray class]])) {
                _RemoteArray = [[NSArray alloc] initWithArray:[response objectForKey:@"data"]];
            }
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _RemoteArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ElecHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ElecHistoryTableViewCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ElecHistoryTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.cellTitle.text = NSLocalizedString(_titleArray[0], nil);
    [cell.cellTitle sizeToFit];
    cell.cellStartTime.text = NSLocalizedString(_titleArray[1], nil);
    [cell.cellStartTime sizeToFit];
    cell.cellEndTimer.text = NSLocalizedString(_titleArray[2], nil);
    [cell.cellEndTimer sizeToFit];
    cell.cellTotalW.text = NSLocalizedString(_titleArray[3], nil);
    [cell.cellTotalW sizeToFit];
    cell.titleLabel.text =[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"name"];
    cell.StartTimeLable.text = [self getTimeFromTimestamp:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultStartKeyValue] doubleValue]];
    cell.EndtimeLabel.text = [self getTimeFromTimestamp:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultEndKeyVaule] doubleValue]];
    cell.TotalLabel.text = [NSString stringWithFormat:@"%@ kw/h",[[_RemoteArray objectAtIndex:0] objectForKey:@"allpower"]];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
            break;
        default:
            break;
    }
}
@end
