//
//  BindDeviceViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BindDeviceViewController : UIViewController
@property (retain,nonatomic) NSString * userID;
@property (retain,nonatomic) NSString * TitleString;
@property (retain,nonatomic) NSString * DefaultDeviceID;
@property (assign) bool isHideQrcodeBtn;
@property (assign) bool isComeFromIdle;//从待机界面调用的。
@property (assign) int UseType; //0 :注册 1:找回密码
@end
