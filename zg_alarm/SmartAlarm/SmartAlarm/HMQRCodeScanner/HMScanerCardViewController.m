//
//  HMScanerCardViewController.m
//  HMQRCodeScanner
//
//  Created by 刘凡 on 16/1/3.
//  Copyright © 2016年 itheima. All rights reserved.
//

#import "HMScanerCardViewController.h"
#import "HMScanner.h"
@interface HMScanerCardViewController()
/// 名片字符串
@property (nonatomic) NSString *cardName;
/// 头像图片
@property (nonatomic) UIImage *avatar;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *DeviceID_FD;


@end

@implementation HMScanerCardViewController {
    UIImageView *cardImageView;
}

#pragma mark - 构造函数
- (instancetype)initWithCardName:(NSString *)cardName avatar:(UIImage *)avatar {
    self = [super init];
    if (self) {
        self.cardName = cardName;
        self.avatar = avatar;
    }
    return self;
}

#pragma mark - 设置界面

- (void)viewDidLoad {
    [[[NSBundle mainBundle] loadNibNamed:@"DeviceIDInput" owner:self options:nil] firstObject];
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone  target:self action:@selector(Done)];
    [_DeviceID_FD setPlaceholder:NSLocalizedString(@"输入绑定设备号", nil)];
//    self.view.backgroundColor = [UIColor lightGrayColor];
    [self prepareNavigationBar];
    
    CGFloat width = self.view.bounds.size.width - 80;

    [[self.DeviceID_FD.rac_textSignal
      filter:^BOOL(id value) {
          NSString *text = value;
          [self.navigationItem.rightBarButtonItem setEnabled:NO];
          return text.length > 1;
      }]
     subscribeNext:^(id x) {
         NSString *text = x;
         if (text.length > 15) {
             _DeviceID_FD.text = [_DeviceID_FD.text substringWithRange:NSMakeRange(0, 15)];
         }
         if([self.DeviceID_FD.text rangeOfString:@" "].location !=NSNotFound)
         {
             self.DeviceID_FD.text = [self.DeviceID_FD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
         }
         NSLog(@"%@", x);
         [self.navigationItem.rightBarButtonItem setEnabled:YES];
     }];
    
}
- (void)Done
{
    //查询IMEI是否有效
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"username",_DeviceID_FD.text,@"imei", nil];
    
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80003",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            //没有被绑定过。则进行下一步。
            [self doNext:[[[[response objectForKey:@"data"] objectAtIndex:0] objectForKey:@"type"] intValue]];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)doNext:(int )type
{
    NSArray * nameArray = @[@"Alarm Host",@"Alarm Host(Wifi)",@"Smart socket",@"Smart Appliances"];
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"username",_DeviceID_FD.text,@"imei",[nameArray objectAtIndex:type],@"name", nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00003",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"绑定成功!",nil)];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];

}
/// 准备导航栏
- (void)prepareNavigationBar {
    // 1> 背景颜色
    [self.navigationController.navigationBar setBarTintColor:FlatSkyBlue];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    // 2> 标题
    self.title = NSLocalizedString(@"输入设备的ID", nil);
}

@end
