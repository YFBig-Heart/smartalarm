//
//  HMScannerViewController.m
//  HMQRCodeScanner
//
//  Created by 刘凡 on 16/1/2.
//  Copyright © 2016年 itheima. All rights reserved.
//

#import "HMScannerViewController.h"
#import "HMScanerCardViewController.h"
#import "HMScannerBorder.h"
#import "HMScannerMaskView.h"
#import "HMScanner.h"
#import "BindDeviceViewController.h"

bool isNeedShowInputCode = false;
bool isNeedShowCustomInput = false;


/// 控件间距
#define kControlMargin  32.0
/// 相册图片最大尺寸
#define kImageMaxSize   CGSizeMake(1000, 1000)

@interface HMScannerViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
/// 名片字符串
@property (nonatomic) NSString *cardName;
/// 头像图片
@property (nonatomic) UIImage *avatar;
/// 完成回调
@property (nonatomic, copy) void (^completionCallBack)(NSString *);
@end

@implementation HMScannerViewController {
    /// 扫描框
    HMScannerBorder *scannerBorder;
    /// 扫描器
    HMScanner *scanner;
    /// 提示标签
    UILabel *tipLabel;
}

- (instancetype)initWithCardName:(NSString *)cardName avatar:(UIImage *)avatar completion:(void (^)(NSString *))completion {
    self = [super init];
    if (self) {
        self.cardName = cardName;
        self.avatar = avatar;
        self.completionCallBack = completion;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareUI];
    
    // 实例化扫描器
    __weak typeof(self) weakSelf = self;
    scanner = [HMScanner scanerWithView:self.view scanFrame:scannerBorder.frame completion:^(NSString *stringValue) {
        // 完成回调
        weakSelf.completionCallBack(stringValue);
        
        // 关闭
        [weakSelf clickCloseButton];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [scannerBorder startScannerAnimating];
    [scanner startScan];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [scannerBorder stopScannerAnimating];
    [scanner stopScan];
}

#pragma mark - 监听方法
/// 点击关闭按钮
- (void)clickCloseButton {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/// 点击相册按钮
- (void)clickAlbumButton {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        tipLabel.text = @"无法访问相册";
        
        return;
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.view.backgroundColor = [UIColor whiteColor];
    picker.delegate = self;
    
    [self showDetailViewController:picker sender:nil];
}

/// 点击名片按钮
- (void)clickCardButton:(UIButton *)btn {
    if (btn.tag != 100) {
        HMScanerCardViewController *vc = [[HMScanerCardViewController alloc] initWithCardName:self.cardName avatar:self.avatar];
        [self showViewController:vc sender:nil];
    }
    else
    {
        BindDeviceViewController * vc = [[BindDeviceViewController alloc] init];
        vc.TitleString = NSLocalizedString(@"绑定", nil);
        vc.userID = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID];
        vc.isHideQrcodeBtn = YES;
        [self showViewController:vc sender:nil];
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *image = [self resizeImage:info[UIImagePickerControllerOriginalImage]];
    
    // 扫描图像
    [HMScanner scaneImage:image completion:^(NSArray *values) {
        
        if (values.count > 0) {
            self.completionCallBack(values.firstObject);
            [self dismissViewControllerAnimated:NO completion:^{
                [self clickCloseButton];
            }];
        } else {
            tipLabel.text = NSLocalizedString(@"没有识别到二维码，请选择其他照片", nil) ;
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

- (UIImage *)resizeImage:(UIImage *)image {
    
    if (image.size.width < kImageMaxSize.width && image.size.height < kImageMaxSize.height) {
        return image;
    }
    
    CGFloat xScale = kImageMaxSize.width / image.size.width;
    CGFloat yScale = kImageMaxSize.height / image.size.height;
    CGFloat scale = MIN(xScale, yScale);
    CGSize size = CGSizeMake(image.size.width * scale, image.size.height * scale);
    
    UIGraphicsBeginImageContext(size);
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return result;
}

#pragma mark - 设置界面
- (void)prepareUI {
    self.view.backgroundColor = [UIColor darkGrayColor];
    
    [self prepareNavigationBar];
    [self prepareScanerBorder];
    [self prepareOtherControls];
    [self perpareSeacherQrcode];
}
////准备寻找二维码标签
- (void)perpareSeacherQrcode
{
   UILabel* tipLabelQrcode = [[UILabel alloc] init];
    tipLabelQrcode.text = NSLocalizedString(@"在设备背面可找到二维码标签", nil);
    tipLabelQrcode.font = [UIFont systemFontOfSize:17];
    tipLabelQrcode.textColor = FlatRed;
    tipLabelQrcode.textAlignment = NSTextAlignmentCenter;
    [tipLabelQrcode sizeToFit];
    [tipLabelQrcode setNumberOfLines:0];
    [self.view addSubview:tipLabelQrcode];
    tipLabelQrcode.sd_layout.topSpaceToView(self.navigationController.navigationBar, 50).widthRatioToView(self.view, 1).centerXEqualToView(self.view);
}
/// 准备提示标签和名片按钮
- (void)prepareOtherControls {
    
    // 1> 提示标签
    tipLabel = [[UILabel alloc] init];
    
    tipLabel.text = NSLocalizedString(@"请将二维码放在识别框内扫描", nil);
    tipLabel.font = [UIFont systemFontOfSize:12];
    tipLabel.textColor = [UIColor whiteColor];
    tipLabel.textAlignment = NSTextAlignmentCenter;
    
    [tipLabel sizeToFit];
    tipLabel.center = CGPointMake(scannerBorder.center.x, CGRectGetMaxY(scannerBorder.frame) + kControlMargin);
    [tipLabel setHidden:YES];
    [self.view addSubview:tipLabel];
    
    UILabel *tip2Label = [[UILabel alloc] init];
    
    tip2Label.text = NSLocalizedString(@"请将二维码放在识别框内扫描", nil);
    tip2Label.font = [UIFont systemFontOfSize:12];
    tip2Label.textColor = [UIColor whiteColor];
    tip2Label.textAlignment = NSTextAlignmentCenter;
    
    [tip2Label sizeToFit];
    [self.view addSubview:tip2Label];
    tip2Label.sd_layout.centerXIs(scannerBorder.center.x).topSpaceToView(self.navigationController.navigationBar, 30).heightIs(26);
    
    // 2> 分享按钮
    UIButton *cardButton = [[UIButton alloc] init];
    
    [cardButton setTitle:NSLocalizedString(@"邀请管理此设备",nil) forState:UIControlStateNormal];
    cardButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [cardButton setTitleColor:self.navigationController.navigationBar.tintColor forState:UIControlStateNormal];
    
    [cardButton sizeToFit];
    cardButton.center = CGPointMake(tipLabel.center.x, CGRectGetMaxY(tipLabel.frame) + kControlMargin);
    
    [self.view addSubview:cardButton];
    
    [cardButton addTarget:self action:@selector(clickCardButton:) forControlEvents:UIControlEventTouchUpInside];
    [cardButton setHidden:YES];
    if (isNeedShowInputCode != true) {
        [cardButton setHidden:YES];
    }
    
    //手动输入
    UIButton *InputButton = [[UIButton alloc] init];
    
    [InputButton setTitle:NSLocalizedString(@"手动输入绑定设备号",nil) forState:UIControlStateNormal];
    InputButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [InputButton setTitleColor:self.navigationController.navigationBar.tintColor forState:UIControlStateNormal];
    
    [InputButton sizeToFit];
    InputButton.center = CGPointMake(tipLabel.center.x, CGRectGetMaxY(tipLabel.frame) + kControlMargin);
    InputButton.tag = 100;
    [self.view addSubview:InputButton];
    
    [InputButton addTarget:self action:@selector(clickCardButton:) forControlEvents:UIControlEventTouchUpInside];
    [InputButton bs_configureAsPrimaryStyle];
    InputButton.sd_layout.widthRatioToView(self.view, 0.6).centerXEqualToView(self.view).heightIs(56);
    if (isNeedShowCustomInput != true) {
        [InputButton setHidden:YES];
    }
}

/// 准备扫描框
- (void)prepareScanerBorder {
    
    CGFloat width = self.view.bounds.size.width - 80;
    scannerBorder = [[HMScannerBorder alloc] initWithFrame:CGRectMake(0, 0, width, width)];
    
    scannerBorder.center = self.view.center;
    scannerBorder.tintColor = self.navigationController.navigationBar.tintColor;
    
    [self.view addSubview:scannerBorder];
    
    HMScannerMaskView *maskView = [HMScannerMaskView maskViewWithFrame:self.view.bounds cropRect:scannerBorder.frame];
    [self.view insertSubview:maskView atIndex:0];
}

/// 准备导航栏
- (void)prepareNavigationBar {
    // 1> 背景颜色
    [self.navigationController.navigationBar setBarTintColor:FlatSkyBlue];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    // 2> 标题
    self.title = NSLocalizedString(@"新增设备", nil);
    
    // 3> 左右按钮
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"关闭",nil) style:UIBarButtonItemStylePlain target:self action:@selector(clickCloseButton)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"相册",nil) style:UIBarButtonItemStylePlain target:self action:@selector(clickAlbumButton)];
}

@end
