//
//  BatSytemListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/6.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "BatSytemListViewController.h"
#import "DisPowerViewController.h"

@interface BatSytemListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;

@end

@implementation BatSytemListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_TitleArray == nil) {
        _TitleArray = @[@"断电提示",@"通电提示",@"低电提示",@"传感器低电提示"];
    }
    self.title = NSLocalizedString(@"电源/电池状态提醒", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.textLabel.text = NSLocalizedString([_TitleArray objectAtIndex:indexPath.row], nil);
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DisPowerViewController * vc = [[DisPowerViewController alloc] init];
    vc.NumberString = [NSString stringWithFormat:@"%ld",indexPath.row];
    vc.TitleString =NSLocalizedString(_TitleArray[indexPath.row], nil);
    [self.navigationController pushViewController:vc animated:YES];

}

@end
