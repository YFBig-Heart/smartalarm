//
//  PreEntryIdleViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/11/8.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "PreEntryIdleViewController.h"
#import "HomeViewController.h"
#import "APNSettingViewController.h"
#import "WifiConfigViewController.h"
#import "BindDevceQRViewController.h"
#import "RootTabViewController.h"
#import "ConfigListViewController.h"
@interface PreEntryIdleViewController ()
@property (nonatomic,retain) UIButton * EntryIdleBtn;
@property (nonatomic,retain) UIImageView * LogoImageView;
@property (weak, nonatomic) IBOutlet UILabel *LabelApn;
@property (weak, nonatomic) IBOutlet UIButton *ButtonApn;
@property (weak, nonatomic) IBOutlet UILabel *LabelWifi;
@property (weak, nonatomic) IBOutlet UIButton *ButtonWifi;
@property (weak, nonatomic) IBOutlet UILabel *LabelShare;
@property (weak, nonatomic) IBOutlet UIButton *ButtonShare;
@property (weak, nonatomic) IBOutlet UIButton *DoneBtn;

@end

@implementation PreEntryIdleViewController

- (void)viewDidLoad {
    self.title = NSLocalizedString(@"绑定成功", nil);
    [self.view setBackgroundColor:FlatWhite];
    [self.view addSubview:_EntryIdleBtn];
    _DoneBtn.sd_layout.centerXEqualToView(self.view).widthRatioToView(self.view, 0.90).heightIs(45.0f).bottomSpaceToView(self.view, 15.0);
    [_DoneBtn setTitle:NSLocalizedString(@"继续", nil) forState: UIControlStateNormal];
    [_DoneBtn bs_configureAsPrimaryStyle];
    [[_DoneBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }];
    [super viewDidLoad];
    [_LabelApn setText:NSLocalizedString(@"1.如果你的设备使用的是移动网络(2G/3G),如果设备不能正常连接网络，请到 管理->网络->APN 进行 APN 设置。", nil)];
    [_ButtonApn setTitle:NSLocalizedString(@"(管理->网络->APN)", nil) forState:UIControlStateNormal];
    [_LabelWifi setText:NSLocalizedString(@"2.如果你的设备使用的是 WiFi 网络，如果设备不能正常连接网络，请到 管理->Wifi 进行WiFi 网络设置。", nil)];
    [_ButtonWifi setTitle:NSLocalizedString(@"(管理 -> WiFi)", nil) forState:UIControlStateNormal];
    [_LabelShare setText:NSLocalizedString(@"3.如果你想将设备分享给其他人员管理，请到 管理->管理成员->邀请管理成员 分享", nil)];
    [_ButtonShare setTitle:NSLocalizedString(@"(管理 -> 管理成员 -> 邀请管理成员)", nil) forState:UIControlStateNormal];
    [[_ButtonApn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        APNSettingViewController * vc = [[APNSettingViewController alloc] init];
        vc.SmsSendNumber = [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"number"];
        vc.isHideBackButton = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    [[_ButtonWifi rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        ConfigListViewController * vc = [[ConfigListViewController alloc] init];
        vc.isHideBackButton = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    [[_ButtonShare rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        BindDevceQRViewController * vc = [[BindDevceQRViewController alloc] init];
        vc.isHideBackButton = YES;
        vc.QRCodeImei = [_BindDeviceImei copy];
        [self.navigationController pushViewController:vc animated:YES];
    }];
}
- (void)viewDidAppear:(BOOL)animated
    {
        if (_BindDeviceImei)
        {
            [self synDevcieTime];
        }
    }
- (NSString *)getLoaclTime:(NSString *)format
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:format];//@"YYYY-MM-dd hh:mm:ss"];
    NSString *DateTime = [formatter stringFromDate:date];
    return DateTime;
}

- (void)synDevcieTime
{
    NSDictionary * tDic ;
    NSInteger zone = (([NSTimeZone systemTimeZone].secondsFromGMT) / 3600);
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:_BindDeviceImei,@"imei",[self getLoaclTime:@"YYYY"],@"year",[self getLoaclTime:@"MM"],@"mon",[self getLoaclTime:@"dd"],@"date",[self getLoaclTime:@"HH"],@"hour",[self getLoaclTime:@"mm"],@"min",[NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:zone]],@"timezone",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80072",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
