//
//  AlarmDeviceEditViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/24.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlarmDeviceEditViewController : UIViewController
@property (retain,nonatomic) NSMutableDictionary * BoardDic;
@end
