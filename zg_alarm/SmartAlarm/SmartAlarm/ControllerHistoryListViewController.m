//
//  ControllerHistoryListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/3/13.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "ControllerHistoryListViewController.h"
#import "MsgTableViewCell.h"

@interface ControllerHistoryListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) UIActivityIndicatorView *activity;
@property (retain,nonatomic) NSArray * RemoteArray;

@end

@implementation ControllerHistoryListViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    _activity = [[UIActivityIndicatorView alloc]
                 initWithFrame : CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)] ;
    [_activity startAnimating];
    [_activity hidesWhenStopped];
    self.title = NSLocalizedString(@"开关记录", nil);
    _MainTable.dataSource = self;
    _MainTable.delegate = self;
    [self synDeviceMsg];
}
- (void)synDeviceMsg
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",@"EN",@"language",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00008",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        [_activity setHidden:YES];
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            if (([[response objectForKey:@"data"] isKindOfClass:[NSArray class]])) {
                _RemoteArray = [[NSArray alloc] initWithArray:[response objectForKey:@"data"]];
            }
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _RemoteArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MsgTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sMsgTableViewCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MsgTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.HeadImg.image = [UIImage imageNamed:@"list_outlets_control"];
    [cell.Title setText:[[_RemoteArray objectAtIndex:indexPath.row] objectForKey:@"litle"]];
    cell.DetailTitle.text = [[_RemoteArray objectAtIndex:indexPath.row] objectForKey:@"msg"];
    cell.Time.text = [[_RemoteArray objectAtIndex:indexPath.row] objectForKey:@"time"];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
            break;
        default:
            break;
    }
}
@end
