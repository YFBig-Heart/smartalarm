//
//  ConfigListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/1/9.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "ConfigListViewController.h"
#import "WifiConfigViewController.h"
#import "ApConfigViewController.h"
#import "RootTabViewController.h"
@interface ConfigListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) NSArray * IconArray;
@end

@implementation ConfigListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    self.title = NSLocalizedString(@"WIFI 设置", nil);
    _TitleArray =@[@"智能配置模式",@"热点配置模式"];
    _IconArray = @[@"wifi_smart",@"wifi_ap"];
    UILabel * FooterLable = [[UILabel alloc] init];
    [FooterLable setText:NSLocalizedString(@"只有你的设备连接不上 APP 的时候，你才需要在这里设置。", nil)];
    [FooterLable setTextColor:FlatGray];
    [FooterLable sizeToFit];
    [FooterLable setFont:[UIFont systemFontOfSize:15]];
    [FooterLable setNumberOfLines:0];
    [FooterLable setTextAlignment:NSTextAlignmentLeft];
    _MainTable.tableFooterView = FooterLable;
    _MainTable.tableFooterView.sd_layout.heightIs(80).widthRatioToView(self.view, 0.8).centerXEqualToView(self.view);
    if (_isHideBackButton) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel  target:self action:@selector(Cance)];
        self.navigationItem.hidesBackButton = YES;
    }

}
- (void)Cance
{
    [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
    [self.view.window makeKeyAndVisible];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    //离section的高度
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    [cell.imageView setImage:[UIImage imageNamed:[_IconArray objectAtIndex:indexPath.row]]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            {
                [self.navigationController pushViewController:[[WifiConfigViewController alloc] init] animated:YES];
            }
            break;
        case 1:
        {
            [self.navigationController pushViewController:[[ApConfigViewController alloc] init] animated:YES];
        }
            break;

        default:
            break;
    }
    
}
@end
