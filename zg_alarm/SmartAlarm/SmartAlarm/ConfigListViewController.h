//
//  ConfigListViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2018/1/9.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfigListViewController : UIViewController
@property (assign) BOOL isHideBackButton;//是否需要隐藏返回导航键
@end
