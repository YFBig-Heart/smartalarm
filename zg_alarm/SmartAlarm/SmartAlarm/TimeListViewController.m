//
//  TimeListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "TimeListViewController.h"
#import "TimeListTableViewCell.h"
#import "EditViewController.h"
#import "TimeEditViewController.h"

@interface TimeListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSMutableDictionary * editBoardDic;
@property (retain,nonatomic)NSArray * AutoSetting;
@end

@implementation TimeListViewController

- (void)viewDidLoad {
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    _AutoSetting = @[NSLocalizedString(@"停用", nil),NSLocalizedString(@"自动开启", nil ),NSLocalizedString(@"自动关闭", nil)];
    if(_editBoardDic == nil)
    {
        _editBoardDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                     @"00:00",@"time1",
                     @"00:00",@"time2",
                     @"00:00",@"time3",
                     @"00:00",@"time4",
                     @"00:00",@"time5",
                     @"00:00",@"time6",
                     @"0",@"type1",
                     @"0",@"type2",
                     @"0",@"type3",
                     @"0",@"type4",
                     @"0",@"type5",
                     @"0",@"type6",
                     @"0000000",@"week1",
                     @"0000000000000",@"week2",
                     @"0000000",@"week3",
                     @"0000000",@"week4",
                     @"0000000",@"week5",
                     @"0000000",@"week6",
                     @"1",@"sn",
                     nil];
    }
    self.title =NSLocalizedString(@"定时器", nil);
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    if([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)
    {
        //排插的定时器
        [self synTimerForSmartOutlets];
    }
    else
    [self synTimeEdit];
}
-(void)synTimerForSmartOutlets
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00007",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _editBoardDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)synTimeEdit
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_BoardDic objectForKey:@"sn"],@"sn",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"40084",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _editBoardDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TimeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimeListTableViewCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TimeListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.TimeLabel.text = [_editBoardDic objectForKey:[NSString stringWithFormat:@"time%ld",indexPath.row+1]];
    NSString * OnoffString = [_AutoSetting objectAtIndex:[[_editBoardDic objectForKey:[NSString stringWithFormat:@"type%ld",indexPath.row+1]] intValue]];
    cell.OnoffLabel.text = NSLocalizedString(OnoffString, nil);
    NSArray * weekDayArray = @[@"周日",@"周一",@"周二",@"周三",@"周四",@"周五",@"周六"];
    NSString * weekDayString = [_editBoardDic objectForKey:[NSString stringWithFormat:@"week%ld",indexPath.row+1]];
    if (![weekDayString isEqualToString:@"0000000"]) {
        for (int v=0,j=0; v < 7; v++) {
            if ([[weekDayString substringWithRange:NSMakeRange(v, 1)] intValue]) {
                UILabel * dayLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 25)];
                dayLable.font = [UIFont systemFontOfSize:11];
                dayLable.text = NSLocalizedString(weekDayArray[v], nil);
                [cell addSubview:dayLable];
                [dayLable sizeToFit];
                dayLable.sd_layout.xIs(15+j*30).centerYIs(cell.centerY*1.2);
                j++;
            }
        }
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TimeEditViewController * vc = [[TimeEditViewController alloc] init];
    vc.BoardDic = [[NSMutableDictionary alloc] initWithDictionary:_editBoardDic];
    vc.currentIndex = indexPath.row + 1;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
