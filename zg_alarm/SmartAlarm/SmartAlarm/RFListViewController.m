//
//  RFListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/21.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "RFListViewController.h"
#import "EditRFViewController.h"
@interface RFListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * remoteCotrollListArray;
@end

@implementation RFListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_remoteCotrollListArray == nil) {
        _remoteCotrollListArray = @[];
    }
    self.title = NSLocalizedString(@"RFID Tags", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd  target:self action:@selector(Add)];;
    [self LayOutDeleteALLBtn];
}
- (void)LayOutDeleteALLBtn
{
    UIButton *_DeleteAllBtn = [[UIButton alloc] initWithFrame:CGRectZero];
    [self.view addSubview:_DeleteAllBtn];
    [_DeleteAllBtn setTitle:NSLocalizedString(@"删除所有", nil) forState:UIControlStateNormal];
    _DeleteAllBtn.sd_layout.leftSpaceToView(self.view, 16).rightSpaceToView(self.view, 16).heightIs(40).bottomSpaceToView(self.view, 20);
    [_DeleteAllBtn bs_configureAsPrimaryStyle];
    [[_DeleteAllBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        UIAlertController * ct = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"您确定删除吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *showAllInfoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self DeleteClick];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消",nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [ct addAction:cancelAction];
        [ct addAction:showAllInfoAction];
        [self presentViewController:ct animated:YES completion:nil];
    }];
}
- (void)DeleteClick
{
    NSDictionary *tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",@"100",@"sn",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"70082",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [self synRemoteCotrolList];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)Add
{
    [self.navigationController pushViewController:[[EditRFViewController alloc] init] animated:YES];
}
- (void)viewWillAppear:(BOOL)animated
{
    [self synRemoteCotrolList];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)synRemoteCotrolList
{
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei", [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00082",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _remoteCotrollListArray = [response objectForKey:@"data"];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _remoteCotrollListArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    if ([[_remoteCotrollListArray objectAtIndex:indexPath.row] objectForKey:@"name"] != [NSNull null]) {
        cell.textLabel.text = [[_remoteCotrollListArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.imageView.image = [UIImage imageNamed:@"List_large_RFID_Tags"];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    EditRFViewController * vc = [[EditRFViewController alloc] init];
    vc.RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[_remoteCotrollListArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
