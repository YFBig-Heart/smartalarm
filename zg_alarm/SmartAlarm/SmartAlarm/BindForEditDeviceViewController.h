//
//  BindForEditDeviceViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/11/12.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BindForEditDeviceViewController : UIViewController
@property (assign) int DeviceType;
@property (retain,nonatomic) NSString * DeviceID;
/*
 
 1:智能报警器
 2:智能报警器(WIFI)
 3:智能排插
 4:智能家电
 
 */
@end
