//
//  BindDevceQRViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/19.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BindDevceQRViewController : UIViewController
@property (assign) BOOL isHideBackButton;//是否需要隐藏返回导航键
@property (retain,nonatomic)NSString * QRCodeImei;
@end
