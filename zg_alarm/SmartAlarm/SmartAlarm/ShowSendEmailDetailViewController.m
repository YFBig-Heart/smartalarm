//
//  ShowSendEmailDetailViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/2/5.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "ShowSendEmailDetailViewController.h"

@interface ShowSendEmailDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *ShowDetailLabel;
@end

@implementation ShowSendEmailDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"忘记密码", nil);
    [_ShowDetailLabel setText:[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"密码已发送至你的邮箱：", nil),_EmailAdress,NSLocalizedString(@"请注意查收", nil)]];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
