//
//  ContanctEditViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContanctEditViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *ContanctEdit;
@property (retain,nonatomic) UITextField * ContanctNameFD;
@property (retain,nonatomic) UITextField * ContanctPhoneFD;
@property (retain,nonatomic) UILabel * ContanctTypeLB;/* 0:Call/SMS 1:Only Call 2:Only SMS 3:CID*/
@property (retain,nonatomic) NSMutableDictionary * ContanctEditDic;
@end
