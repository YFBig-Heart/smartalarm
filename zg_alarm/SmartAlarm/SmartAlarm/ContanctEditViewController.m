//
//  ContanctEditViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "ContanctEditViewController.h"
#import "STPickerView.h"
#import "STPickerSingle.h"

@interface ContanctEditViewController ()<UITableViewDataSource,UITableViewDelegate,STPickerSingleDelegate>
@property (retain, nonatomic)NSArray * arrayData;
@property (retain, nonatomic)NSArray * arrayData_bak;
@property (weak, nonatomic) IBOutlet UIButton *DeleteBtn;
@end

@implementation ContanctEditViewController
bool isAddNew = NO;
- (void)viewDidLoad {
    _ContanctEdit.delegate = self;
    _ContanctEdit.dataSource = self;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];[_DeleteBtn setHidden:NO];
    
    if ([_ContanctEditDic objectForKey:@"number"] == nil) {
        isAddNew = YES;
        [_ContanctEditDic setObject:@"" forKey:@"number"];
        [_ContanctEditDic setObject:@1 forKey:@"type"];
        [_ContanctEditDic setObject:@"" forKey:@"name"];
        [_DeleteBtn setHidden:YES];
        self.title = NSLocalizedString(@"添加联系人信息", nil);
    }
    else
        self.title = NSLocalizedString(@"修改联系人", nil);

    if ([CoreHTTPRequest GetDeviceType] == WIFI_ALARM) {
        _arrayData = @[@"",@"Only Call",@"CID"];
    }
    else
    _arrayData = @[@"",@"Call/SMS",@"Only Call",@"Only SMS",@"CID"];
    
    _arrayData_bak = @[@"",@"Call/SMS",@"Only Call",@"Only SMS",@"CID"];

    [[_DeleteBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self AskDelete];
    }];
    [super viewDidLoad];
    [_DeleteBtn setTitle:NSLocalizedString(@"删除", nil) forState:UIControlStateNormal];
    UILabel * FooterLable = [[UILabel alloc] init];
    [FooterLable setText:NSLocalizedString(@"拨电话需要插电话线或使用SIM卡,发短信需要使用SIM卡。", nil)];
    [FooterLable setTextColor:FlatGray];
    [FooterLable sizeToFit];
    [FooterLable setFont:[UIFont systemFontOfSize:15]];
    [FooterLable setNumberOfLines:0];
    [FooterLable setTextAlignment:NSTextAlignmentLeft];
    _ContanctEdit.tableFooterView = FooterLable;
    _ContanctEdit.tableFooterView.sd_layout.heightIs(50);

}
- (void)AskDelete
{
    UIAlertController * ct = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"您确定删除吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *showAllInfoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self doNext];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消",nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [ct addAction:cancelAction];
    [ct addAction:showAllInfoAction];
    [self presentViewController:ct animated:YES completion:nil];
}
- (void)doNext
{
    NSDictionary * tDic ;
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[_ContanctEditDic objectForKey:@"sn"],@"sn",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"70078",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已保存",nil)];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)Done
{
    NSDictionary * tDic ;
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[_ContanctEditDic objectForKey:@"type"],@"type",_ContanctNameFD.text,@"name",_ContanctPhoneFD.text,@"number",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",[_ContanctEditDic objectForKey:@"sn"],@"sn",nil];
    
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80078",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已保存",nil)];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Contantct_cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    switch (indexPath.row) {
        case 0:
            [cell.textLabel setText:NSLocalizedString(@"姓名", nil)];
            if ([_ContanctEditDic objectForKey:@"name"]) {
                if (_ContanctNameFD) {
                    [_ContanctNameFD removeFromSuperview];
                    _ContanctNameFD = nil;
                }
                _ContanctNameFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_ContanctNameFD setTextColor:[UIColor flatGrayColor]];
                [_ContanctNameFD setText:[_ContanctEditDic objectForKey:@"name"]];
                [_ContanctNameFD setTextAlignment:NSTextAlignmentRight];
                [_ContanctNameFD setBackgroundColor:[UIColor clearColor]];
                [_ContanctNameFD setPlaceholder:NSLocalizedString(@"输入名称", nil)];
                [cell addSubview:_ContanctNameFD];
                _ContanctNameFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            }
            [cell.textLabel setText:NSLocalizedString(@"姓名", nil)];
            break;
        case 1:
            if ([_ContanctEditDic objectForKey:@"number"]) {
                if (_ContanctPhoneFD) {
                    [_ContanctPhoneFD removeFromSuperview];
                    _ContanctPhoneFD = nil;
                }
                _ContanctPhoneFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_ContanctPhoneFD setTextColor:[UIColor flatGrayColor]];
                [_ContanctPhoneFD setText:[_ContanctEditDic objectForKey:@"number"]];
                                [_ContanctPhoneFD setPlaceholder:NSLocalizedString(@"输入电话", nil)];
                [_ContanctPhoneFD setTextAlignment:NSTextAlignmentRight];
                [_ContanctPhoneFD setBackgroundColor:[UIColor clearColor]];
                [_ContanctPhoneFD setKeyboardType:UIKeyboardTypePhonePad];
                [cell addSubview:_ContanctPhoneFD];
                _ContanctPhoneFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
                [[self.ContanctPhoneFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      [self.navigationItem.rightBarButtonItem setEnabled:NO];
                      return text.length > 1;
                  }]
                 subscribeNext:^(id x) {
                     if([self.ContanctPhoneFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.ContanctPhoneFD.text = [self.ContanctPhoneFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     [self.navigationItem.rightBarButtonItem setEnabled:YES];
                     NSLog(@"%@", x);
                 }];

            }
                [cell.textLabel setText:NSLocalizedString(@"电话", nil)];
            break;
        case 2:
            if ([_ContanctEditDic objectForKey:@"type"]) {
                if (_ContanctTypeLB) {
                    [_ContanctTypeLB removeFromSuperview];
                    _ContanctTypeLB = nil;
                }
                _ContanctTypeLB = [[UILabel alloc] initWithFrame:CGRectZero];
                [_ContanctTypeLB setTextColor:[UIColor flatGrayColor]];
                [_ContanctTypeLB setText:[_arrayData_bak objectAtIndex:[[_ContanctEditDic objectForKey:@"type"] intValue]]];
                [_ContanctTypeLB setTextAlignment:NSTextAlignmentRight];
                [_ContanctTypeLB setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_ContanctTypeLB];
                _ContanctTypeLB.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            }
                [cell.textLabel setText:NSLocalizedString(@"类型", nil)];
            break;
        default:
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_ContanctNameFD resignFirstResponder];
    [_ContanctPhoneFD resignFirstResponder];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.row == 2) {
        STPickerSingle *TypePicker = [[STPickerSingle alloc]init];
        [TypePicker setArrayData:_arrayData];
        [TypePicker setTitle:NSLocalizedString(@"选择类型", nil)];
        [TypePicker setTitleUnit:nil];
        [TypePicker setContentMode:STPickerContentModeBottom];
        [TypePicker setDelegate:self];
        [TypePicker show];
    }
}
- (void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle
{
    [_ContanctEditDic setObject:_ContanctNameFD.text forKey:@"name"];
    [_ContanctEditDic setObject:_ContanctPhoneFD.text forKey:@"number"];
    [_ContanctEditDic setObject:[NSString stringWithFormat:@"%ld",[_arrayData_bak indexOfObject:selectedTitle]] forKey:@"type"];
    [_ContanctEdit reloadData];
}
@end
