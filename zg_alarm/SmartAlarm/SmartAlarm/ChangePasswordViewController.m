//
//  ChangePasswordViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/8.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@property (retain,nonatomic) UITextField * OldPasswordFD;
@property (retain,nonatomic) UITextField * NewPasswordFD;
@property (retain,nonatomic) UITextField * reNewPasswordFD;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_TitleArray == nil) {
        _TitleArray = @[@"旧密码",@"新密码",@"重新输入新密码"];
    }
    _RemoteInfoDic = [[NSMutableDictionary alloc] init];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];

}
- (void)Done
{
    if ([_reNewPasswordFD.text isEqualToString:_NewPasswordFD.text]) {
        NSDictionary * tDic ;
        
        tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"name",_OldPasswordFD.text,@"password",_NewPasswordFD.text,@"new_password",nil];
        NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
        NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"10001",@"cmd",dicArray,@"data", nil];
        NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
        NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
        [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
            //OK
            NSLog(@"%@",response);
            if ([[response objectForKey:@"ret"] intValue] != 0) {
                [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
            }
            else
            {
                [HttpRespondError ShowHttpRespondError:0];
                [[NSUserDefaults standardUserDefaults] setObject:_NewPasswordFD.text forKey:kDefaultUserPsw];
                [self.navigationController popViewControllerAnimated:YES];
            }
        } failure:^(NSError *error) {
            //fail
            NSLog(@"fail %@",error);
            [HttpRespondError ShowHttpRespondError:-999];
        }];
    }
    else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"两次密码不一致",nil)];
            [_reNewPasswordFD setText:@""];
            [_NewPasswordFD setText:@""];
            [_MainTable reloadData];
        }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString(_TitleArray[indexPath.row], nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            if (_OldPasswordFD) {
                [_OldPasswordFD removeFromSuperview];
                _OldPasswordFD = nil;
            }
            _OldPasswordFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_OldPasswordFD setTextColor:[UIColor flatGrayColor]];
            [_OldPasswordFD setText:[_RemoteInfoDic objectForKey:@"password"]];
            [_OldPasswordFD setTextAlignment:NSTextAlignmentRight];
            [_OldPasswordFD setBackgroundColor:[UIColor clearColor]];
            [_OldPasswordFD setPlaceholder:NSLocalizedString(@"密码",nil)];
            [cell addSubview:_OldPasswordFD];
            _OldPasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[_OldPasswordFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return text.length > 1;
              }]
             subscribeNext:^(id x) {
                 if([self.OldPasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.OldPasswordFD.text = [self.OldPasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 [_RemoteInfoDic setObject:_OldPasswordFD.text forKey:@"password"];
                 NSLog(@"%@", x);
             }];
        }
            break;
        case 1:
        {
            if (_NewPasswordFD) {
                [_NewPasswordFD removeFromSuperview];
                _NewPasswordFD = nil;
            }
            _NewPasswordFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_NewPasswordFD setTextColor:[UIColor flatGrayColor]];
            [_NewPasswordFD setTextAlignment:NSTextAlignmentRight];
            [_NewPasswordFD setBackgroundColor:[UIColor clearColor]];
            [_NewPasswordFD setPlaceholder:NSLocalizedString(@"新密码",nil)];
            [cell addSubview:_NewPasswordFD];
            _NewPasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [_NewPasswordFD setSecureTextEntry:YES];
            [[_NewPasswordFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return text.length > 1;
              }]
             subscribeNext:^(id x) {
                 if([self.NewPasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.NewPasswordFD.text = [self.NewPasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 NSLog(@"%@", x);
             }];
        }
            break;
        case 2:
        {
            if (_reNewPasswordFD) {
                [_reNewPasswordFD removeFromSuperview];
                _reNewPasswordFD = nil;
            }
            _reNewPasswordFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_reNewPasswordFD setTextColor:[UIColor flatGrayColor]];
            [_reNewPasswordFD setText:[_RemoteInfoDic objectForKey:@"new_password"]];
            [_reNewPasswordFD setTextAlignment:NSTextAlignmentRight];
            [_reNewPasswordFD setPlaceholder:NSLocalizedString(@"重新输入新密码",nil)];
            [_reNewPasswordFD setBackgroundColor:[UIColor clearColor]];
            [_reNewPasswordFD setSecureTextEntry:YES];
            [cell addSubview:_reNewPasswordFD];
            _reNewPasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[_reNewPasswordFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  [self.navigationItem.rightBarButtonItem setEnabled:NO];
                  return text.length > 1;
              }]
             subscribeNext:^(id x) {
                 if([self.reNewPasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.reNewPasswordFD.text = [self.reNewPasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
                 NSLog(@"%@", x);
             }];
        }
            break;

        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
