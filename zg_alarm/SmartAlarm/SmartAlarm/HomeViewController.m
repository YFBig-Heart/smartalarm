//
//  HomeViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/21.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "HomeViewController.h"
#import "HMScanner.h"
#import "HMScannerController.h"
#import "CheckUserViewController.h"
#import "getBatForVule.h"
#import "BindDeviceViewController.h"

extern bool isNeedShowInputCode;
extern bool isNeedShowCustomInput;
@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *HomeBackGroundView;
@property (weak, nonatomic) IBOutlet UIImageView *SignalImage;
@property (weak, nonatomic) IBOutlet UIImageView *BatImage;
@property (weak, nonatomic) IBOutlet UILabel *BatLabel;
@property (weak, nonatomic) IBOutlet UIButton *LockBtn;
@property (weak, nonatomic) IBOutlet UIButton *UnlockBtn;
@property (weak, nonatomic) IBOutlet UIButton *PhoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *SOSBtn;
@property (weak, nonatomic) IBOutlet UIButton *OnhomeBtn;
@property (retain,nonatomic) UIView *MenuBaseView;
@property (retain,nonatomic) UITableView * MenuTableView;
@property (retain,nonatomic) UIButton * LeftButton;
@property (weak, nonatomic) IBOutlet UILabel *singenalLable;
@property (retain,nonatomic) NSArray * DeviceListArray;
@property (retain,nonatomic) UIImageView * imageDot;
@property (retain,nonatomic) UIImageView * imageDotList;
@property (retain,nonatomic) UILabel * DeviceTitle;
@property (retain,nonatomic) NSArray * signalImageArray;
@property (retain,nonatomic) NSArray * DeviceTypeImageArray;
@property (retain,nonatomic) NSArray * BatImageArray;
@end
@implementation HomeViewController
int _CurrAlarm = 2; /* 0:撤防  1:设防 2:在家 */
int _OnOffline; /* 0: 不在线 1:在线 */
UIView * Wifiview;
NSTimer *Powertimer;
- (void)viewDidLoad {

    if (_DeviceListArray == nil) {
        _BatLabel.text = @"0%";
        _singenalLable.text = @"0%";
    }
    else
    self.title = NSLocalizedString(@"智能报警",nil);
    _DeviceTypeImageArray = @[@"",@"list_home",@"list_home",@"list_outlets",@"list_yaokong"];
    UIView * titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 47)];
    [titleView setBackgroundColor:[UIColor clearColor]];
    
    _DeviceTitle = [[UILabel alloc] initWithFrame:CGRectZero];
    [_DeviceTitle setFont:[UIFont systemFontOfSize:15.0f]];
    [_DeviceTitle setTextColor:FlatWhite];
    [_DeviceTitle setText:self.title];
    [_DeviceTitle sizeToFit];
    [titleView addSubview:_DeviceTitle];
    _DeviceTitle.sd_layout.centerXEqualToView(titleView).centerYEqualToView(titleView);

    
    _imageDot = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"online"]];
    [_imageDot setContentMode:UIViewContentModeScaleAspectFit];
    [_imageDot sizeToFit];
    [titleView addSubview:_imageDot];
    _imageDot.sd_layout.widthIs(20.0f).leftSpaceToView(_DeviceTitle, 5).centerYEqualToView(titleView);
    [titleView sizeToFit];
    self.navigationItem.titleView = titleView;
    titleView.sd_layout.centerYEqualToView(self.navigationItem.leftBarButtonItem.customView).centerXEqualToView(self.navigationItem.leftBarButtonItem.customView);

    [super viewDidLoad];
    [self setClickForButtons];
    //注册通知，收到推送通知需要刷新一下主页状态
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sysDeviceOnLineOrOffline) name:@"synDeviceNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(APNsSelectDevice:) name:@"synDeviceNotificationReSelect" object:nil];
}
//设置排插图标
- (void)setPowerBtnStatus:(NSString *)oswitch
{
    if (([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)) {
        if([oswitch intValue] == 0)
        {
            //关闭
            [self.PhoneBtn setImage:[UIImage imageNamed:@"switch_close"] forState:UIControlStateNormal];
        }
        else
        {
            [self.PhoneBtn setImage:[UIImage imageNamed:@"switch_open"] forState:UIControlStateNormal];
            //当在插座页面，如果插座打开的情况下，要5秒刷一次实时功率
            if (Powertimer) {
                [Powertimer invalidate];
                Powertimer = nil;
            }
            Powertimer =  [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(synPlugStatus) userInfo:nil repeats:YES];
        }
    }
    
}
- (void)PostPowerStatus
{
    NSDictionary * tDic;
    if ((([[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultPowerStatues] objectForKey:@"oswitch"] intValue] ==0))) {
             tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",[[self GetCurrectDevice] objectForKey:@"imei"],@"imei",@"1",@"op",nil];
    }
    else
    {
                     tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",[[self GetCurrectDevice] objectForKey:@"imei"],@"imei",@"0",@"op",nil];
    }
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"10006",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            NSDictionary * dic = [[response objectForKey:@"data"] objectAtIndex:0];
            if ([[dic objectForKey:@"online"] intValue] == 0) {
                [HttpRespondError ShowHttpRespondError:1013];
            }
            else
            {
                [self synPlugStatus];
            }
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)setClickForButtons
{
    self.LockBtn.showsTouchWhenHighlighted = YES;
    @weakify(self)
    [[self.LockBtn
      rac_signalForControlEvents:UIControlEventTouchUpInside]
     subscribeNext:^(id x) {
         @strongify(self)
         [self repostDeviceStatus:1];
     }];
    self.UnlockBtn.showsTouchWhenHighlighted = YES;
    [[self.UnlockBtn
      rac_signalForControlEvents:UIControlEventTouchUpInside]
     subscribeNext:^(id x) {
         [self repostDeviceStatus:0];
     }];
    self.PhoneBtn.showsTouchWhenHighlighted = YES;
    [[self.PhoneBtn
      rac_signalForControlEvents:UIControlEventTouchUpInside]
     subscribeNext:^(id x) {
         if (([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)) {
             //排插
             [self PostPowerStatus];
         }
         else
         {
             [self.PhoneBtn setSelected:!self.PhoneBtn.selected];
             NSString * telString = [[NSString alloc] initWithFormat:@"tel:%@",[[self GetCurrectDevice] objectForKey:@"number"]];
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telString] options:@{} completionHandler:nil];
         }
     }];
    self.SOSBtn.showsTouchWhenHighlighted = YES;
    [[self.SOSBtn
      rac_signalForControlEvents:UIControlEventTouchUpInside]
     subscribeNext:^(id x) {
         [self.SOSBtn setSelected:!self.SOSBtn.selected];
         [self repostDeviceStatus:3];
     }];
    self.SOSBtn.showsTouchWhenHighlighted = YES;
    [[self.OnhomeBtn
      rac_signalForControlEvents:UIControlEventTouchUpInside]
     subscribeNext:^(id x) {
         [self repostDeviceStatus:2];
     }];
}
- (NSDictionary *)GetCurrectDevice
{
    if ((_DeviceListArray)&&(_DeviceListArray.count != 0)) {
        if ([[NSUserDefaults standardUserDefaults] integerForKey:kDefaultCurrDeciveIndex] >= _DeviceListArray.count) {
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:kDefaultCurrDeciveIndex];
        }
        [[NSUserDefaults standardUserDefaults] setObject:[_DeviceListArray objectAtIndex:[[NSUserDefaults standardUserDefaults] integerForKey:kDefaultCurrDeciveIndex]] forKey:kDefaultCurrDevice];
        return [_DeviceListArray objectAtIndex:[[NSUserDefaults standardUserDefaults] integerForKey:kDefaultCurrDeciveIndex]];
    }
    return nil;
}
//获取排插状态
- (void)synPlugStatus
{
    
    NSLog(@"synPlugStatus 排插刷新》》》》》》》》》》》》》》》》》》》》》》》》》");

    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",[[self GetCurrectDevice] objectForKey:@"imei"],@"imei",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00006",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequst:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            NSDictionary * dic = [[response objectForKey:@"data"] objectAtIndex:0];
            if ([[dic objectForKey:@"oswitch"] intValue] == 1) {
                NSString * vauleString = [dic objectForKey:@"opower"];
                int vauleInt = [vauleString floatValue]*100;
                vauleString = [NSString stringWithFormat:@"%d",vauleInt];
                int lenthCount = 6 - [vauleString length];
                NSString * ArrayString = [[NSString alloc] init];
                for (int v=0; v<lenthCount; v++) {
                    ArrayString = [ArrayString stringByAppendingString:@"0"];
                }
                ArrayString = [ArrayString stringByAppendingString:vauleString];
                [self LayOutWifiView:ArrayString];
            }
            else
            {
                [self HideWifiView];
            }
            [self setPowerBtnStatus:[dic objectForKey:@"oswitch"]];
            [[NSUserDefaults standardUserDefaults] setObject:dic forKey:kDefaultPowerStatues];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)reloadDeviceStatus
{
    _DeviceTitle.text = [self GetCurrectDevice]?([[self GetCurrectDevice] objectForKey:@"name"]):@"";
    [_DeviceTitle sizeToFit];
    if (_OnOffline == 0) {
        [_imageDot setImage:[UIImage imageNamed:@"offline"]];
    }
    else
    {
        [_imageDot setImage:[UIImage imageNamed:@"online"]];
    }
    if([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)
    {
        //智能排插
        [self synPlugStatus];
        if (([[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultPowerStatues] objectForKey:@"oswitch"] intValue] == 1)) {
            //当在插座页面，如果插座打开的情况下，要5秒刷一次实时功率
            if (Powertimer) {
                [Powertimer invalidate];
                Powertimer = nil;
            }
            Powertimer =  [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(synPlugStatus) userInfo:nil repeats:YES];
        }
    }
    else
    {
        if (Powertimer) {
            [Powertimer invalidate];
            Powertimer = nil;
        }
        [self HideWifiView];
        
    }
}
- (void)reLogin
{
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"name",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserPsw],@"password",
                           [GetSysLanguage GetSystemLanguage],@"language",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00002",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [[UIApplication sharedApplication] unregisterForRemoteNotifications];
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
            UINavigationController * LoginOrRegisterNav = [[UINavigationController alloc] initWithRootViewController:[[CheckUserViewController alloc] init]];
            [self.view.window setRootViewController:LoginOrRegisterNav];
        }
        else
        {
            if ([[response objectForKey:@"data"] count] == 0) {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:kDefaultCurrDevice];
                [_imageDot setHidden:YES];
                [self SetUpMenuList];
                return ;
            }
            [_imageDot setHidden:NO];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            NSString * AliasString = [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID] stringByReplacingOccurrencesOfString:@"@"withString:@"_"];
            AliasString = [AliasString stringByReplacingOccurrencesOfString:@"."withString:@"_"];
            [JPUSHService setAlias:AliasString callbackSelector:nil object:nil];
            [self synDeviceList];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    isNeedShowCustomInput = false;
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
    [self reLogin];
    [self SetupButtonsAction];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self HideMenuList];
    if (_LeftButton) {
        NSLog(@"移动。。。");
        [_LeftButton removeFromSuperview];
        _LeftButton = nil;
    }
    if (Powertimer) {
        [Powertimer invalidate];
        Powertimer = nil;
    }
}

- (void)handleSwipeFrom:(UISwipeGestureRecognizer*)recognizer {
    [self.view removeGestureRecognizer:recognizer];
    [self HideMenuList];
}
- (void)sysDeviceOnLineOrOffline
{
    if ([self GetCurrectDevice] == nil) {
        return;
    }
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[self GetCurrectDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName", nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00004",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        NSDictionary * dataDic = [[response objectForKey:@"data"] objectAtIndex:0];
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            //这里更新一下设备状态
            _OnOffline = [[dataDic objectForKey:@"online"] intValue];
            [self reloadDeviceStatus];
            _singenalLable.text = [NSString stringWithFormat:@"%@%%",[dataDic objectForKey:@"signal"]];
            if ([[dataDic objectForKey:@"bat"] intValue] > 100) {
                _BatLabel.text = @"...";
            }
            else
            _BatLabel.text = [NSString stringWithFormat:@"%@%%",[dataDic objectForKey:@"bat"]];
            _CurrAlarm = [[dataDic objectForKey:@"arm"] intValue];
            ;
            [_BatImage setImage:[ValueForImage getBatImgForValue:[[dataDic objectForKey:@"bat"] integerValue]]];
            [_SignalImage setImage:[ValueForImage getSigImgForValue:[[dataDic objectForKey:@"signal"] integerValue]]];
                [self setDeviceStatus:[[dataDic objectForKey:@"arm"] intValue]];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}

- (void)synDeviceList
{
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"user_name", nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"30001",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequst:Url success:^(id response) {
        //OK
        NSLog(@"zp-------%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            //这里更新一下设备状态
            if ([[response objectForKey:@"data"] count]) {
                _DeviceListArray = [response objectForKey:@"data"];
                [self sysDeviceOnLineOrOffline];
                [self reloadDeviceStatus];
            }
            else
            {
                _DeviceListArray = nil;
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:kDefaultCurrDevice];
            }
            [_MenuTableView reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
    
}
- (void)SetUpMenuList
{
    if (_MenuBaseView == nil) {
        [_LeftButton setHidden:YES];
        [self synDeviceList];
        _MenuBaseView = [[UIView alloc] initWithFrame:CGRectZero];
        [_MenuBaseView setBackgroundColor:FlatWhite];
        [self.view addSubview:_MenuBaseView];
//        _MenuBaseView.sd_layout.widthRatioToView(self.view, 0.55).yIs(self.navigationController.navigationBar.origin.y+self.navigationController.navigationBar.size.height).xIs(0.0).heightIs((_DeviceListArray.count+1)*47);
//        _MenuTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
       
        float tbHight = (_DeviceListArray.count+1)*47;
        if (tbHight > (self.view.frame.size.height - self.navigationController.navigationBar.origin.y-self.navigationController.navigationBar.size.height -self.rdv_tabBarController.tabBar.frame.size.height)) {
            tbHight = (self.view.frame.size.height - self.navigationController.navigationBar.origin.y-self.navigationController.navigationBar.size.height -self.rdv_tabBarController.tabBar.frame.size.height);
        }
        _MenuBaseView.sd_layout.widthRatioToView(self.view, 0.55).yIs(self.navigationController.navigationBar.origin.y+self.navigationController.navigationBar.size.height).xIs(0.0).heightIs(tbHight);
        _MenuTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        [_MenuTableView setDelegate:self];
        [_MenuTableView setDataSource:self];
        _MenuBaseView.layer.shadowOpacity = 0.5;// 阴影透明度
        _MenuBaseView.layer.shadowColor = [UIColor grayColor].CGColor;// 阴影的颜色
        _MenuBaseView.layer.shadowRadius = 3;// 阴影扩散的范围控制
        _MenuBaseView.layer.shadowOffset = CGSizeMake(1, 1);// 阴影的范围
        [_MenuBaseView addSubview:_MenuTableView];
        _MenuTableView.sd_layout.heightRatioToView(_MenuBaseView, 1.0).widthRatioToView(_MenuBaseView, 1.0).xIs(0).yIs(0);
        UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
        self.HomeBackGroundView.userInteractionEnabled = YES;
        [self.HomeBackGroundView addGestureRecognizer:singleTap];
    }
}
- (void)HideMenuList
{
    if(_MenuBaseView)
    {
        [_MenuTableView removeFromSuperview];
       [_MenuBaseView removeFromSuperview];
        _MenuBaseView=nil;
        _MenuTableView = nil;
        [_LeftButton setSelected:NO];
        [_LeftButton setHidden:NO];
        if ((([[[self GetCurrectDevice] objectForKey:@"type"] intValue] == 3))) {
            [_LeftButton setImage:[UIImage imageNamed:@"home_left_outlet"] forState:UIControlStateNormal];
        }
        else
            [_LeftButton setImage:[UIImage imageNamed:@"home_left_menu"] forState:UIControlStateNormal];
    }
}
 /* 0:撤防  1:设防 2:在家 */
- (void)setDeviceStatus:(int) alarm
{
    /* 设置默认图片 */
    if (alarm <= 2) {
        [self.LockBtn setImage:[UIImage imageNamed:@"home_lock_nor"] forState:UIControlStateNormal];
        [self.UnlockBtn setImage:[UIImage imageNamed:@"home_unlock_nor"] forState:UIControlStateNormal];
        [self.OnhomeBtn setImage:[UIImage imageNamed:@"home_inhome_nor"] forState:UIControlStateNormal];
    }
  switch (alarm) {
        case 0:
            [self.UnlockBtn setImage:[UIImage imageNamed:@"home_unlock_sel"] forState:UIControlStateNormal];
            break;
        case 1:
            [self.LockBtn setImage:[UIImage imageNamed:@"home_lock_sel"] forState:UIControlStateNormal];
            break;
        case 2:
            [self.OnhomeBtn setImage:[UIImage imageNamed:@"home_inhome_sel"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    if ((([[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"type"] intValue] != 3))) {
        [self.PhoneBtn setImage:[UIImage imageNamed:@"home_phone_nor"] forState:UIControlStateNormal];
    }
}
- (void)repostDeviceStatus:(int) status
{
    if ([self GetCurrectDevice] == nil) {
        return;
    }
    //查询IMEI是否有效
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[self GetCurrectDevice] objectForKey:@"imei"],@"imei",[NSString stringWithFormat:@"%d",status],@"op",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00005",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            NSDictionary * resultDic;
            if ([response objectForKey:@"data"]) {
                resultDic = [[response objectForKey:@"data"] objectAtIndex:0];
            }
            if(resultDic)
            [self setDeviceStatus:[[resultDic objectForKey:@"arm"] intValue]];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)SetupButtonsAction
{
    if (_LeftButton == nil) {
         _LeftButton = [[UIButton alloc] initWithFrame:CGRectMake(3, 0, 51, 51)];
        if ([[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"type"] intValue] ==3) {
            [_LeftButton setImage:[UIImage imageNamed:@"home_left_outlet"] forState:UIControlStateNormal];
        }
        else
            [_LeftButton setImage:[UIImage imageNamed:@"home_left_menu"] forState:UIControlStateNormal];
        [_LeftButton setCenterY:(15+self.navigationItem.titleView.frame.size.height)];
        [self.navigationController.view addSubview:_LeftButton];
        self.LeftButton.showsTouchWhenHighlighted = YES;
        _LeftButton.layer.cornerRadius = _LeftButton.height / 2;
        _LeftButton.layer.borderColor = [UIColor grayColor].CGColor;
        _LeftButton.layer.borderWidth = 1.0f;
        _LeftButton.layer.masksToBounds = YES;
        @weakify(self)
        [[_LeftButton
          rac_signalForControlEvents:UIControlEventTouchUpInside]
         subscribeNext:^(id x) {
             //菜单List
             @strongify(self)
             [_LeftButton setHidden:YES];
             [_LeftButton setSelected:!_LeftButton.selected];
             if (!_LeftButton.selected) {
                 [self HideMenuList];
             }
             else
                 [self SetUpMenuList];
         }];
    }
}
- (void)Addevice
{
    NSString *cardName = @"null";
    UIImage *avatar = [UIImage imageNamed:@"avatar"];
    
    isNeedShowCustomInput = true;
    // 实例化扫描控制器
    HMScannerController *scanner = [HMScannerController scannerWithCardName:cardName avatar:avatar completion:^(NSString *stringValue) {
        NSRange range = [stringValue rangeOfString:@"ZG:"];
        if (range.location != NSNotFound) {
            BindDeviceViewController * vc = [[BindDeviceViewController alloc] init];
            vc.userID = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID];
            vc.TitleString = NSLocalizedString(@"绑定", nil);
            vc.isHideQrcodeBtn =YES;
            vc.DefaultDeviceID = [stringValue substringFromIndex:range.location+range.length];
            [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"二维码出错",nil)];
        }
        //        self.scanResultLabel.text = stringValue;
        [UIView beginAnimations:@"View Flip" context:nil];
        //动画持续时间
        [UIView setAnimationDuration:1.25];
        //设置动画的回调函数，设置后可以使用回调方法
        [UIView setAnimationDelegate:self];
        //设置动画曲线，控制动画速度
        [UIView  setAnimationCurve: UIViewAnimationCurveEaseInOut];
        //设置动画方式，并指出动画发生的位置
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view  cache:YES];
        //提交UIView动画
        [UIView commitAnimations];
//        [self CheckIMEI:stringValue];
        
    }];
    
    // 设置导航栏样式
    [scanner setTitleColor:[UIColor whiteColor] tintColor:[UIColor greenColor]];
    
    // 展现扫描控制器
    [self showDetailViewController:scanner sender:nil];
}
- (void)CheckIMEI:(NSString *)imei
{
    //查询IMEI是否有效
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"username",imei,@"imei", nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80003",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            //没有被绑定过。则进行下一步。
            [self doNext:[[[[response objectForKey:@"data"] objectAtIndex:0] objectForKey:@"type"] intValue] IMEI:imei];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)doNext:(int )type IMEI:(NSString *)_imei
{
    NSArray * nameArray = @[@"Alarm Host",@"Alarm Host(Wifi)",@"Smart socket",@"Smart Appliances"];
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"username",_imei,@"imei",[nameArray objectAtIndex:type],@"name", nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00003",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"绑定成功!",nil)];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _DeviceListArray.count + 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    if (indexPath.row == (_DeviceListArray.count)) {
        cell.imageView.image = [UIImage imageNamed:@"tag_button_add"];
        [cell.textLabel setText:NSLocalizedString(@"添加设备",nil)];
    }
    else
    {
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.imageView.image = [UIImage imageNamed:[_DeviceTypeImageArray objectAtIndex:[[[_DeviceListArray objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]]];
        [cell.textLabel setText:[[_DeviceListArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
        UIImageView * statusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        if ([[[_DeviceListArray objectAtIndex:indexPath.row] objectForKey:@"online"] integerValue]) {
            [statusImageView setImage:[UIImage imageNamed:@"online"]];
        }
        else
            [statusImageView setImage:[UIImage imageNamed:@"offline"]];
        [statusImageView setContentMode:UIViewContentModeScaleAspectFit];
        [statusImageView sizeToFit];
        cell.accessoryView = statusImageView;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == _DeviceListArray.count) {
        [self Addevice];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setInteger:[[NSNumber numberWithInteger:indexPath.row] integerValue] forKey:kDefaultCurrDeciveIndex];
        [[NSUserDefaults standardUserDefaults] setObject:[_DeviceListArray objectAtIndex:indexPath.row] forKey:kDefaultCurrDevice];
        [self sysDeviceOnLineOrOffline];
        [self reloadDeviceStatus];
    }
    [self HideMenuList];
}
- (void)APNsSelectDevice:(NSNotification *)notification
{
    NSString * _imei = (NSString *)[notification object];
    NSLog(@"--------------------------------------------------------------\r\n");
    NSLog(@"imei:%@ , %@",_imei,_DeviceListArray);
    if (_DeviceListArray) {
        for (NSDictionary  *currDevice in _DeviceListArray) {
            if (currDevice && _imei && [[currDevice objectForKey:@"imei"] isEqualToString:_imei]) {
                [[NSUserDefaults standardUserDefaults] setObject:[_DeviceListArray objectAtIndex:[_DeviceListArray indexOfObject:currDevice]] forKey:kDefaultCurrDevice];
                [[NSUserDefaults standardUserDefaults] setInteger:[[NSNumber numberWithInteger:[_DeviceListArray indexOfObject:currDevice]] integerValue] forKey:kDefaultCurrDeciveIndex];
                [self sysDeviceOnLineOrOffline];
                [self reloadDeviceStatus];
            }
        }
    }
}
#pragma mark - HomeWifiView
- (void)HideWifiView
{
    if (Powertimer) {
        [Powertimer invalidate];
        Powertimer = nil;
    }
    if (Wifiview) {
        [Wifiview removeFromSuperview];
        Wifiview = nil;
    }
}
- (void)LayOutWifiView:(NSString *)opower
{
    if (Wifiview) {
        [Wifiview removeFromSuperview];
        Wifiview = nil;
    }
        Wifiview = [[UIView alloc] initWithFrame:CGRectZero];
        [Wifiview setBackgroundColor:[UIColor colorWithHexString:@"#5CC9F5"]];
        [self.view addSubview:Wifiview];
        Wifiview.sd_layout.widthIs(kScreen_Width).topSpaceToView(_SOSBtn, 20).bottomSpaceToView(self.rdv_tabBarController.tabBar, 0);
        UILabel * PowerLable ;
#define LeveWith 50 //两边预留的空间
#define NumberTips 7 //8个数字？
        for (int v=0; v<NumberTips; v++) {
            UIImageView * ImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"number_home"]];
#define SpeaceWith ((kScreen_Width - ImgView.width*NumberTips - LeveWith*2)/ (NumberTips+1))
            [Wifiview addSubview:ImgView];
            UILabel * label = [[UILabel alloc] init];
            if (v < 6) {
                [label setText:[opower substringWithRange:NSMakeRange(v, 1)]];
                NSLog(@"====[%@] %@",opower,[opower substringWithRange:NSMakeRange(v, 1)]);
                [label sizeToFit];
                [label setTextColor:FlatWhite];
                [ImgView addSubview:label];
                label.sd_layout.centerXEqualToView(ImgView).centerYEqualToView(ImgView);
            }
            ImgView.sd_layout.xIs(12+v*(SpeaceWith + ImgView.width)+SpeaceWith+LeveWith).centerYEqualToView(Wifiview);
            if (! PowerLable) {
                PowerLable = [[UILabel alloc] init];
                [PowerLable setTextColor:FlatWhite];
                [PowerLable setFont:[UIFont systemFontOfSize:13]];
                [PowerLable setText:NSLocalizedString(@"功率", nil)];
                [PowerLable sizeToFit];
                [PowerLable setTextAlignment:NSTextAlignmentLeft];
                //            [PowerLable setBackgroundColor:FlatRed];
                [Wifiview addSubview:PowerLable];
                PowerLable.sd_layout.leftEqualToView(ImgView).bottomSpaceToView(ImgView,2).widthIs(100);
            }
            
            
            if (v == NumberTips-1) {
                //写 W
                [ImgView setHidden:YES];
                UILabel *wLabel = [[UILabel alloc] init];
                [wLabel setText:@"W"];
                [wLabel setTextColor:FlatWhite];
                [wLabel sizeToFit];
                [Wifiview addSubview:wLabel];
                wLabel.sd_layout.xIs(12+v*(SpeaceWith + ImgView.width)+SpeaceWith+LeveWith).heightIs(23).bottomEqualToView(ImgView);
            }
            if (v == NumberTips - 4) {
                //打点
                UILabel *dotLabel = [[UILabel alloc] init];
                [dotLabel setText:@"•"];
                //            [dotLabel setBackgroundColor:FlatRed];
                [dotLabel setTextColor:FlatWhite];
                [dotLabel sizeToFit];
                [Wifiview addSubview:dotLabel];
                dotLabel.sd_layout.heightIs(10).widthIs(SpeaceWith).centerXIs(ImgView.frame.origin.x+ImgView.frame.size.width+SpeaceWith/2).bottomEqualToView(ImgView);
                
            }
        }
}

@end

