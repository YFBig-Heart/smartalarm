//
//  BoardMainViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/2.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "BoardMainViewController.h"
#import "TimeListViewController.h"
#import "TimeEditViewController.h"
#import "EditViewController.h"
#import "AlarmDeviceEditViewController.h"
NSString * CurrDeviceName;
@interface BoardMainViewController ()
@property (weak, nonatomic) IBOutlet UIButton *OnoffBtn;
@property (weak, nonatomic) IBOutlet UIButton *TimeSet;
@property (assign ) BOOL isAddNew;
@end

@implementation BoardMainViewController

- (void)viewDidLoad {
    _isAddNew = NO;
    if (_BoardDic == nil) {
        _BoardDic = [[NSMutableDictionary alloc] init];
        [_BoardDic setObject:@"" forKey:@"name"];
        _isAddNew = YES;
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit  target:self action:@selector(Edit)];;
    self.title = [_BoardDic objectForKey:@"name"];
    CurrDeviceName = [_BoardDic objectForKey:@"name"];
    _OnoffBtn.showsTouchWhenHighlighted = YES;
    _TimeSet.showsTouchWhenHighlighted = YES;
    [[_OnoffBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [_OnoffBtn setSelected:!_OnoffBtn.selected];
        [self DeviceControll:_OnoffBtn.isSelected];
    }];
    [[_TimeSet rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        TimeListViewController * vc = [[TimeListViewController alloc] init];
        vc.BoardDic = [[NSMutableDictionary alloc] initWithDictionary:_BoardDic];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    [super viewDidLoad];
}
- (void)Edit
{
    EditViewController * vc = [[EditViewController alloc] init];
    vc.BoardDic = _BoardDic;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)DeviceControll:(BOOL) isOn
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_BoardDic objectForKey:@"sn"],@"sn",isOn?@"1":@"0",@"op",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"60084",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    self.title = CurrDeviceName;
    [self synDeviceStatus];
}
- (void)synDeviceStatus
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_BoardDic objectForKey:@"sn"],@"sn",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"50084",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            NSDictionary * statusDic = [[response objectForKey:@"data"] objectAtIndex:0];
            [_OnoffBtn setSelected:[[statusDic objectForKey:@"op"] isEqualToString:@"1"]];
//            _BoardDic = [[response objectForKey:@"data"] objectAtIndex:0];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
