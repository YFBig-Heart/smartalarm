//
//  ProtolViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/8.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "ProtolViewController.h"

@interface ProtolViewController ()<UIWebViewDelegate>
{
    UIActivityIndicatorView *activityIndicatorView;
//    UIWebView * webView;
    __weak IBOutlet UIWebView *webView;
}
@end

@implementation ProtolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=NSLocalizedString(@"软件协议",nil);
//    webView = [[UIWebView alloc] initWithFrame:self.view.frame];
}
- (void)viewDidAppear:(BOOL)animated
{
    webView.scalesPageToFit =YES;
    webView.delegate =self;
    activityIndicatorView = [[UIActivityIndicatorView alloc]
                             initWithFrame : CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)] ;
//    [activityIndicatorView setCenter: self.view.center] ;
    [activityIndicatorView setActivityIndicatorViewStyle: UIActivityIndicatorViewStyleWhite] ;
//    [self.view addSubview : activityIndicatorView] ;
//    self.navigationItem.rightBarButtonItem.customView = activityIndicatorView;
    [activityIndicatorView startAnimating];
    [activityIndicatorView hidesWhenStopped];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicatorView];
    [self navigationItem].rightBarButtonItem = barButton;

    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    NSString * htmlPath = [[NSBundle mainBundle] pathForResource:@"protocol_en"
                                                          ofType:@"html"];
    NSString *languageName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if ([languageName rangeOfString:@"zh-Hans"].location != NSNotFound) {
        htmlPath = [[NSBundle mainBundle] pathForResource:@"protocol"
                                                   ofType:@"html"];
    }
    
    
    NSString * htmlCont = [NSString stringWithContentsOfFile:htmlPath
                                                    encoding:NSUTF8StringEncoding
                                                       error:nil];
    [webView setScalesPageToFit:YES];
    [webView loadHTMLString:htmlCont baseURL:baseURL];
    [self.view addSubview:webView];
//    webView.sd_layout
//    .yIs(49+17).widthRatioToView(self.view, 1.0);
}
- (void)loadWebPageWithString:(NSString*)urlString
{
    NSURL *url =[NSURL URLWithString:urlString];
    NSLog(@"url:%@",urlString);
    NSURLRequest *request =[NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [activityIndicatorView startAnimating] ;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    [activityIndicatorView stopAnimating];
}

@end
