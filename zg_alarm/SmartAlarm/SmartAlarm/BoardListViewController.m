
//
//  BoardListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/2.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "BoardListViewController.h"
#import "BoardMainViewController.h"
#import "TimeListTableViewCell.h"
#import "EditViewController.h"

@interface BoardListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@property (retain,nonatomic) NSMutableArray * BoardListArray;
@end

@implementation BoardListViewController

- (void)viewDidLoad {
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_BoardListArray == nil) {
        _BoardListArray = [[NSMutableArray alloc] init];
    }
    self.title = NSLocalizedString(@"排插", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd  target:self action:@selector(Add)];
    [super viewDidLoad];
}
- (void)Add
{
    [self.navigationController pushViewController:[[EditViewController alloc] init]  animated:YES];
}
- (void)viewWillAppear:(BOOL)animated
{
    [self synDeviceDelay];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)synDeviceDelay
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00084",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _BoardListArray = [[NSMutableArray alloc] initWithArray:[response objectForKey:@"data"]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _BoardListArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString([[_BoardListArray objectAtIndex:indexPath.row] objectForKey:@"name"],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [cell.imageView setImage:[UIImage imageNamed:@"list_outlets"]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    BoardMainViewController * vc = [[BoardMainViewController alloc] init];
    vc.BoardDic = [[NSMutableDictionary alloc] initWithDictionary:[_BoardListArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
