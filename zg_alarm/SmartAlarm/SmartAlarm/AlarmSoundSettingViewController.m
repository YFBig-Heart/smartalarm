////
////  AlarmSoundSettingViewController.m
////  SmartAlarm
////
////  Created by Huang Shan on 2017/10/6.
////  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "AlarmSoundSettingViewController.h"

@interface AlarmSoundSettingViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UISwitch *SosSoundOnoffSW;
@property (retain,nonatomic) UITextField * SoundSecFD;
@property (retain,nonatomic) UITextField * GsmSoundCountFD;
@property (retain,nonatomic) UITextField * PSTNSoundCountFD;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@end

@implementation AlarmSoundSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_TitleArray == nil) {
        _TitleArray = @[@"SOS警号音",@"警号鸣响时间(秒)",@"GSM振铃次数",@"PSTN振铃次数"];
    }
    
    NSLog(@"----[CoreHTTPRequest GetDeviceType]=%ld",[CoreHTTPRequest GetDeviceType]);
    if (([CoreHTTPRequest GetDeviceType] == WIFI_ALARM)) {
        _TitleArray = @[@"SOS警号音",@"警号鸣响时间(秒)",@"PSTN振铃次数"];
    }

    _RemoteInfoDic = [[NSMutableDictionary alloc] init];
    self.title = NSLocalizedString(@"警号&响铃", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];
    [self synSettings];
}
- (void)Done
{
    NSDictionary * tDic ;

    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_RemoteInfoDic objectForKey:@"siren"],@"siren",
            [_RemoteInfoDic objectForKey:@"siren_timer"],@"siren_timer",
            [_RemoteInfoDic objectForKey:@"gsm_ring"],@"gsm_ring",
            [_RemoteInfoDic objectForKey:@"pstn_ring"],@"pstn_ring",
            [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",
            nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80075",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void )synSettings
{
    NSDictionary * tDic ;

    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00075",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            if (_SosSoundOnoffSW) {
                [_SosSoundOnoffSW removeFromSuperview];
                _SosSoundOnoffSW = nil;
            }
            _SosSoundOnoffSW = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
            cell.accessoryView = _SosSoundOnoffSW;
            [_SosSoundOnoffSW setOn:([[_RemoteInfoDic objectForKey:@"siren"] intValue]?YES:NO)];
            @weakify(self)
            [[self.SosSoundOnoffSW
              rac_signalForControlEvents:UIControlEventValueChanged]
             subscribeNext:^(UISwitch * x) {
                 @strongify(self)
                 NSLog(@"开关  ： %d",x.isOn);
                 [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"siren"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
             }];
        }
            break;
        case 1:
        {
            if (_SoundSecFD) {
                [_SoundSecFD removeFromSuperview];
                _SoundSecFD = nil;
            }
            _SoundSecFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_SoundSecFD setTextColor:[UIColor flatGrayColor]];
            [_SoundSecFD setKeyboardType:UIKeyboardTypeNumberPad];
            [_SoundSecFD setText:[_RemoteInfoDic objectForKey:@"siren_timer"]];
            [_SoundSecFD setTextAlignment:NSTextAlignmentRight];
            [_SoundSecFD setBackgroundColor:[UIColor clearColor]];
            [cell addSubview:_SoundSecFD];
            _SoundSecFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).widthRatioToView(self.view, 0.47);
            [[self.SoundSecFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  [self.navigationItem.rightBarButtonItem setEnabled:NO];
                  return text.length > 0;
              }]
             subscribeNext:^(id x) {
                 if([self.SoundSecFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.SoundSecFD.text = [self.SoundSecFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 [_RemoteInfoDic setObject:_SoundSecFD.text forKey:@"siren_timer"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
                 NSLog(@"%@", x);
             }];
        }
            break;
        case 2:
        {
            if (_GsmSoundCountFD) {
                [_GsmSoundCountFD removeFromSuperview];
                _GsmSoundCountFD = nil;
            }
            _GsmSoundCountFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_GsmSoundCountFD setTextColor:[UIColor flatGrayColor]];
            [_GsmSoundCountFD setKeyboardType:UIKeyboardTypeNumberPad];
            [_GsmSoundCountFD setText:[_RemoteInfoDic objectForKey:@"gsm_ring"]];
            [_GsmSoundCountFD setTextAlignment:NSTextAlignmentRight];
            [_GsmSoundCountFD setBackgroundColor:[UIColor clearColor]];
            [cell addSubview:_GsmSoundCountFD];
            _GsmSoundCountFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).widthRatioToView(self.view, 0.47);
            [[_GsmSoundCountFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  [self.navigationItem.rightBarButtonItem setEnabled:NO];
                  return text.length > 0;
              }]
             subscribeNext:^(id x) {
                 if([self.GsmSoundCountFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                    self.GsmSoundCountFD.text = [self.GsmSoundCountFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
//                 if ([_SoundSecFD.text intValue] > 20) {
//                     _SoundSecFD.text = @"20";
//                 }
                 [_RemoteInfoDic setObject:_GsmSoundCountFD.text forKey:@"gsm_ring"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
                 NSLog(@"---%@", x);
             }];
        }
            break;
        case 3:
        {
            if (_PSTNSoundCountFD) {
                [_PSTNSoundCountFD removeFromSuperview];
                _PSTNSoundCountFD = nil;
            }
            _PSTNSoundCountFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_PSTNSoundCountFD setTextColor:[UIColor flatGrayColor]];
            [_PSTNSoundCountFD setKeyboardType:UIKeyboardTypeNumberPad];
            [_PSTNSoundCountFD setText:[_RemoteInfoDic objectForKey:@"pstn_ring"]];
            [_PSTNSoundCountFD setTextAlignment:NSTextAlignmentRight];
            [_PSTNSoundCountFD setBackgroundColor:[UIColor clearColor]];
            [cell addSubview:_PSTNSoundCountFD];
            _PSTNSoundCountFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).widthRatioToView(self.view, 0.47);
            [[_PSTNSoundCountFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  [self.navigationItem.rightBarButtonItem setEnabled:NO];
                  return text.length > 0;
              }]
             subscribeNext:^(id x) {
                 if([self.PSTNSoundCountFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.PSTNSoundCountFD.text = [self.PSTNSoundCountFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 if ([_PSTNSoundCountFD.text intValue] > 20) {
                     _PSTNSoundCountFD.text = @"20";
                 }
                 [_RemoteInfoDic setObject:_PSTNSoundCountFD.text forKey:@"pstn_ring"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
                 NSLog(@"%@", x);
             }];
        }
            break;

        default:
            break;
    }

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
            break;
        default:
            break;
    }
}

@end


