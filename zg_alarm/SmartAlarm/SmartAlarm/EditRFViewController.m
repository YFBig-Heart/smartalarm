//
//  EditRFViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/21.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "EditRFViewController.h"
#import "HMScanner.h"
#import "HMScannerController.h"
#import "STPickerView.h"
#import "STPickerDate.h"
#import "HcdDateTimePickerView.h"
#import "STPickerSingle.h"

@interface EditRFViewController ()<STPickerDateDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) UITextField * NameFD;
@property (retain,nonatomic) UITextField * UidFD;
@property (retain,nonatomic) UIButton *UidButton;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) NSArray * selectType;
@property (assign)bool isRemoteCotrollAddNew;
@property (weak, nonatomic) IBOutlet UIButton *DeleteBTN;

@end

@implementation EditRFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_DeleteBTN setTitle:NSLocalizedString(@"删除", nil) forState:UIControlStateNormal];
    _selectType = @[NSLocalizedString(@"管理员", nil),NSLocalizedString(@"普通用户", nil)];
    _TitleArray = @[@"名称",@"授权",@"ID"];
    [_DeleteBTN setHidden:NO];
    _isRemoteCotrollAddNew = false;
    if(_RemoteInfoDic == nil)
    {
        _RemoteInfoDic = [[NSMutableDictionary alloc] init];
        [_RemoteInfoDic setObject:@"" forKey:@"name"];
        [_RemoteInfoDic setObject:@"100" forKey:@"sn"];
        [_RemoteInfoDic setObject:@"" forKey:@"code"];
        [_RemoteInfoDic setObject:@"1" forKey:@"access"];
        _isRemoteCotrollAddNew = true;
        [_DeleteBTN setHidden:YES];
        self.title = NSLocalizedString(@"新增RFID Tag", nil);
    }
    else
    {
        self.title = NSLocalizedString(@"编辑", nil);
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    [[_DeleteBTN rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self AskDelete];
    }];
    
}
- (void)AskDelete
{
    UIAlertController * ct = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"您确定删除吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *showAllInfoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self DeleteRemoteControl];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消",nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [ct addAction:cancelAction];
    [ct addAction:showAllInfoAction];
    [self presentViewController:ct animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)DeleteRemoteControl
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[_RemoteInfoDic objectForKey:@"sn"],@"sn",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"70082",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已保存",nil)];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)Done
{
    NSDictionary * tDic ;
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[_RemoteInfoDic objectForKey:@"sn"],@"sn",[_RemoteInfoDic objectForKey:@"name"],@"name",[_RemoteInfoDic objectForKey:@"code"],@"code",[_RemoteInfoDic objectForKey:@"access"],@"access",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80082",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已保存",nil)];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_remotelist";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.textLabel.text = NSLocalizedString([_TitleArray objectAtIndex:indexPath.row], nil);
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            if ([_RemoteInfoDic objectForKey:@"name"]) {
                if (_NameFD) {
                    [_NameFD removeFromSuperview];
                    _NameFD = nil;
                }
                _NameFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_NameFD setTextColor:[UIColor flatGrayColor]];
                if (([_RemoteInfoDic objectForKey:@"name"] != [NSNull null])) {
                    [_NameFD setText:[_RemoteInfoDic objectForKey:@"name"]];
                }
                [_NameFD setPlaceholder:NSLocalizedString(@"请输入名称", nil)];
                [_NameFD setTextAlignment:NSTextAlignmentRight];
                [_NameFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_NameFD];
                _NameFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
                [[self.NameFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      self.navigationItem.rightBarButtonItem.enabled = NO;
                      return text.length > 0;
                  }]
                 subscribeNext:^(id x) {
                     if([self.NameFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.NameFD.text = [self.NameFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     self.navigationItem.rightBarButtonItem.enabled = YES;
                     [_RemoteInfoDic setObject:self.NameFD.text  forKey:@"name"];
                     NSLog(@"%@", x);
                 }];
            }
            
        }
            break;
        case 1:
        {
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            cell.detailTextLabel.text = [_selectType objectAtIndex:[[_RemoteInfoDic objectForKey:@"access"] intValue]];
        }
            break;
        case 2:
        {
            if ([_RemoteInfoDic objectForKey:@"code"]) {
                if (_UidFD) {
                    [(_UidFD) removeFromSuperview];
                    (_UidFD) = nil;
                }
                _UidFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_UidFD setTextColor:[UIColor flatGrayColor]];
                if (([_RemoteInfoDic objectForKey:@"code"] != [NSNull null])) {
                    [_UidFD setText:[_RemoteInfoDic objectForKey:@"code"]];
                }
                [_UidFD setTextAlignment:NSTextAlignmentRight];
                [_UidFD setBackgroundColor:[UIColor clearColor]];
                [_UidFD setKeyboardType:UIKeyboardTypeNumberPad];
                [cell addSubview:_UidFD];
                _UidFD.sd_layout.xIs(cell.centerX*0.5).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 60.f);
                [[_UidFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      return text.length > 1;
                  }]
                 subscribeNext:^(id x) {
                     if([self.UidFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.UidFD.text = [self.UidFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     [_RemoteInfoDic setObject:_UidFD.text  forKey:@"code"];
                     NSLog(@"%@", x);
                 }];
                _UidButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
                [_UidButton setImage:[UIImage imageNamed:@"QR_code_action"] forState:UIControlStateNormal];
                cell.accessoryView = _UidButton;
                [[self.UidButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
                    [self AddUid];
                }];
            }
            
        }
            break;
        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_UidFD resignFirstResponder];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 1) {
        STPickerSingle *TypePicker = [[STPickerSingle alloc]init];
        [TypePicker setArrayData:_selectType];
        [TypePicker setTitle:NSLocalizedString(@"选择类型", nil)];
        [TypePicker setTitleUnit:nil];
        [TypePicker setContentMode:STPickerContentModeBottom];
        [TypePicker setDelegate:self];
        [TypePicker show];
    }
    
}
- (void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle
{
    [_RemoteInfoDic setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[_selectType indexOfObject:selectedTitle]] forKey:@"access"];;
    [_MainTable reloadData];
}

- (void)AddUid
{
    NSString *cardName = @"2";
    UIImage *avatar = [UIImage imageNamed:@"avatar"];
    
    // 实例化扫描控制器
    HMScannerController *scanner = [HMScannerController scannerWithCardName:cardName avatar:avatar completion:^(NSString *stringValue) {
        NSLog(@"扫描后的 %@",stringValue);
        NSRange range = [stringValue rangeOfString:@"B"];
        if (range.location != NSNotFound) {
            _UidFD.text = [stringValue substringFromIndex:range.location+range.length];
            [_RemoteInfoDic setObject:_UidFD.text  forKey:@"code"];
            [_MainTable reloadData];
        }

        //_UidFD.text = [NSString stringWithFormat:@"%@",stringValue];
        NSLog(@"_UidFD.text=%@",_UidFD.text);
        
    }];
    // 设置导航栏样式
    [scanner setTitleColor:[UIColor whiteColor] tintColor:[UIColor greenColor]];
    // 展现扫描控制器
    [self showDetailViewController:scanner sender:nil];
}

@end
