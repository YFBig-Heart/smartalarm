//
//  DisPowerViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/24.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "DisPowerViewController.h"

@interface DisPowerViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *Maintable;
@property (retain,nonatomic) NSArray * TitleArray;
@end

@implementation DisPowerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _Maintable.delegate = self;
    _Maintable.dataSource = self;
    if (_TitleArray == nil) {
        if ([_TitleString isEqualToString:NSLocalizedString(@"断电提示", nil)]) {
            _TitleArray = @[@"SMS提示",@"语音提示",@"电话提示",@"警号提示"];
        }
        else
            _TitleArray = @[@"SMS提示",@"语音提示"];
    }
    if (([CoreHTTPRequest GetDeviceType] == WIFI_ALARM)) {
        
        _TitleArray = @[@"语音提示",@"电话提示",@"警告提示"];
    }
    
    self.title = _TitleString;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];
    [self synSettings];
    _RemoteInfoDic = [[NSMutableDictionary alloc] init];
}
- (void)Done
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",
            _NumberString,@"num",
            [_RemoteInfoDic objectForKey:@"sms"],@"sms",
            [_RemoteInfoDic objectForKey:@"call"],@"call",
            [_RemoteInfoDic objectForKey:@"voice"],@"voice",
            [_RemoteInfoDic objectForKey:@"siren"],@"siren",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80074",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void )synSettings
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",_NumberString,@"num",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00074",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_Maintable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.textLabel.text = NSLocalizedString([_TitleArray objectAtIndex:indexPath.row], nil);
    
    if (([CoreHTTPRequest GetDeviceType] == WIFI_ALARM)) {
        
        switch (indexPath.row) {
          
            case 0:
            {
                if ([[_RemoteInfoDic objectForKey:@"voice"] intValue]) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
            }
                break;
            case 1:
            {
                if ([[_RemoteInfoDic objectForKey:@"call"] intValue]) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
            }
                break;
            case 2:
            {
                if ([[_RemoteInfoDic objectForKey:@"siren"] intValue]) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
            }
                break;
                
            default:
                break;
        }
        
    }else
    {
        switch (indexPath.row) {
            case 0:
            {
                if ([[_RemoteInfoDic objectForKey:@"sms"] intValue]) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
            }
                break;
            case 1:
            {
                if ([[_RemoteInfoDic objectForKey:@"voice"] intValue]) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
            }
                break;
            case 2:
            {
                if ([[_RemoteInfoDic objectForKey:@"call"] intValue]) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
            }
                break;
            case 3:
            {
                if ([[_RemoteInfoDic objectForKey:@"siren"] intValue]) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
            }
                break;
                
            default:
                break;
        }
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([CoreHTTPRequest GetDeviceType] == WIFI_ALARM) {
        
        switch (indexPath.row) {
                
            case 0:
            {
                if ([[_RemoteInfoDic objectForKey:@"voice"] intValue]) {
                    [_RemoteInfoDic setObject:@"0" forKey:@"voice"];
                }
                else
                    [_RemoteInfoDic setObject:@"1" forKey:@"voice"];
            }
                break;
            case 1:
            {
                if ([[_RemoteInfoDic objectForKey:@"call"] intValue]) {
                    [_RemoteInfoDic setObject:@"0" forKey:@"call"];
                }
                else
                    [_RemoteInfoDic setObject:@"1" forKey:@"call"];
                
            }
                break;
            case 2:
            {
                if ([[_RemoteInfoDic objectForKey:@"siren"] intValue]) {
                    [_RemoteInfoDic setObject:@"0" forKey:@"siren"];
                }
                else
                    [_RemoteInfoDic setObject:@"1" forKey:@"siren"];
            }
                break;
                
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row) {
            case 0:
            {
                if ([[_RemoteInfoDic objectForKey:@"sms"] intValue]) {
                    [_RemoteInfoDic setObject:@"0" forKey:@"sms"];
                }
                else
                    [_RemoteInfoDic setObject:@"1" forKey:@"sms"];
            }
                break;
            case 1:
            {
                if ([[_RemoteInfoDic objectForKey:@"voice"] intValue]) {
                    [_RemoteInfoDic setObject:@"0" forKey:@"voice"];
                }
                else
                    [_RemoteInfoDic setObject:@"1" forKey:@"voice"];
            }
                break;
            case 2:
            {
                if ([[_RemoteInfoDic objectForKey:@"call"] intValue]) {
                    [_RemoteInfoDic setObject:@"0" forKey:@"call"];
                }
                else
                    [_RemoteInfoDic setObject:@"1" forKey:@"call"];
                
            }
                break;
            case 3:
            {
                if ([[_RemoteInfoDic objectForKey:@"siren"] intValue]) {
                    [_RemoteInfoDic setObject:@"0" forKey:@"siren"];
                }
                else
                    [_RemoteInfoDic setObject:@"1" forKey:@"siren"];
            }
                break;
                
            default:
                break;
                
        }
    
    }
    [_Maintable reloadData];
}

@end
