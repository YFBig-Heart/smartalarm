//
//  TimeEditViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeEditViewController : UIViewController
@property (retain,nonatomic) NSMutableDictionary * BoardDic;
@property (assign) NSInteger currentIndex;
@end
