//
//  SecuritySettingViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "SecuritySettingViewController.h"
#import "DevicePasswordViewController.h"
#import "ResetViewController.h"
@interface SecuritySettingViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * ImageArray;
@property (retain,nonatomic) NSArray * TitleArray;
@end

@implementation SecuritySettingViewController

- (void)viewDidLoad {
    if (_TitleArray == nil) {
        _TitleArray = @[@"设备密码",@"恢复出厂设置"];
        _ImageArray = @[@"list_password",@"list_reset"];
    }
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    self.title = NSLocalizedString(@"安全", nil);
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)) {
        return _TitleArray.count -1;
    }
    else
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    if (([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)) {
        cell.imageView.image = [UIImage imageNamed:[_ImageArray objectAtIndex:indexPath.row+1]];
        [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row+1],nil)];
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:[_ImageArray objectAtIndex:indexPath.row]];
        [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    }
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)) {
        [self.navigationController pushViewController:[[ResetViewController alloc] init] animated:YES];
    }
    else
    {
        switch (indexPath.row) {
            case 0:
                [self.navigationController pushViewController:[[DevicePasswordViewController alloc] init] animated:YES];
                break;
            case 1:
                [self.navigationController pushViewController:[[ResetViewController alloc] init] animated:YES];
                break;
            default:
                break;
        }
    }
}

@end
