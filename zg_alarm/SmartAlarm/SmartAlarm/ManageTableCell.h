//
//  ManageTableCell.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/23.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *ContanctBtn;
@property (weak, nonatomic) IBOutlet UIButton *SensorsBtn;
@property (weak, nonatomic) IBOutlet UIButton *OutletsBtn;
@property (weak, nonatomic) IBOutlet UIButton *SecurityBtn;
@property (weak, nonatomic) IBOutlet UIButton *NetworkBtn;
@property (weak, nonatomic) IBOutlet UIButton *WifiBtn;

@property (weak, nonatomic) IBOutlet UILabel *ContanctTitle;
@property (weak, nonatomic) IBOutlet UILabel *SensorsTitle;
@property (weak, nonatomic) IBOutlet UILabel *OutletsTitle;
@property (weak, nonatomic) IBOutlet UILabel *SecurityTitle;
@property (weak, nonatomic) IBOutlet UILabel *NetworkTitle;
@property (weak, nonatomic) IBOutlet UILabel *WifiTltle;

@end
