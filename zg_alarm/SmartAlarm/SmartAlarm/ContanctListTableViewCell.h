//
//  ContanctListTableViewCell.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContanctListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *PhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *TypeLabel;

@end
