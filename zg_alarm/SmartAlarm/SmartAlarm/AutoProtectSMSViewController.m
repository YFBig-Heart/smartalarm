//
//  AutoProtectSMSViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/6.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "AutoProtectSMSViewController.h"

@interface AutoProtectSMSViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UISwitch *AutoLockSwitch;
@property (retain,nonatomic) UISwitch *AutoUnlockSwitch;
@property (retain,nonatomic) UISwitch *InHomeSwitch;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@end

@implementation AutoProtectSMSViewController

- (void)viewDidLoad {
    if (_TitleArray == nil) {
        _TitleArray = @[@"设防短信",@"撤防短信",@"在家短信"];
    }
    _RemoteInfoDic = [[NSMutableDictionary alloc] init];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    self.title = NSLocalizedString(@"设/撤防短信通知", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    [self synAutoProtectSMS];
}
- (void)Done
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_RemoteInfoDic objectForKey:@"arm"],@"arm",[_RemoteInfoDic objectForKey:@"disarm"],@"disarm",[_RemoteInfoDic objectForKey:@"home"],@"home",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80073",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)synAutoProtectSMS
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00073",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [_MainTable reloadData];
            _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    switch (indexPath.row) {
        case 0:
        {
            if (_AutoLockSwitch) {
                [_AutoLockSwitch removeFromSuperview];
                _AutoLockSwitch = nil;
            }
            _AutoLockSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
            cell.accessoryView = _AutoLockSwitch;
                        [_AutoLockSwitch setOn:([[_RemoteInfoDic objectForKey:@"arm"] intValue]?YES:NO)];
            @weakify(self)
            [[self.AutoLockSwitch
              rac_signalForControlEvents:UIControlEventValueChanged]
             subscribeNext:^(UISwitch * x) {
                 @strongify(self)
                 NSLog(@"开关  ： %d",x.isOn);
                 [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"arm"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
             }];
        }
            break;
        case 1:
        {
            if (_AutoUnlockSwitch) {
                [_AutoUnlockSwitch removeFromSuperview];
                _AutoUnlockSwitch = nil;
            }
            _AutoUnlockSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
            cell.accessoryView = _AutoUnlockSwitch;
                        [_AutoUnlockSwitch setOn:([[_RemoteInfoDic objectForKey:@"disarm"] boolValue]?YES:NO)];
            @weakify(self)
            [[self.AutoUnlockSwitch
              rac_signalForControlEvents:UIControlEventValueChanged]
             subscribeNext:^(UISwitch * x) {
                 @strongify(self)
                 NSLog(@"M开关  ： %d",x.isOn);
                 [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"disarm"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
             }];
        }
            break;
        case 2:
        {
            if (_InHomeSwitch) {
                [_InHomeSwitch removeFromSuperview];
                _InHomeSwitch = nil;
            }
            _InHomeSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
            cell.accessoryView = _InHomeSwitch;
            [_InHomeSwitch setOn:([[_RemoteInfoDic objectForKey:@"home"] boolValue]?YES:NO)];
            @weakify(self)
            [[self.InHomeSwitch
              rac_signalForControlEvents:UIControlEventValueChanged]
             subscribeNext:^(UISwitch * x) {
                 @strongify(self)
                 NSLog(@"M开关  ： %d",x.isOn);
                 [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"home"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
             }];
        }
            break;

        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
