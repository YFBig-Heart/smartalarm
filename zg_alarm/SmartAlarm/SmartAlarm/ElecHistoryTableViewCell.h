//
//  ElecHistoryTableViewCell.h
//  SmartAlarm
//
//  Created by Huang Shan on 2018/3/18.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ElecHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UILabel *cellStartTime;
@property (weak, nonatomic) IBOutlet UILabel *cellEndTimer;
@property (weak, nonatomic) IBOutlet UILabel *cellTotalW;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *StartTimeLable;
@property (weak, nonatomic) IBOutlet UILabel *EndtimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *TotalLabel;

@end
