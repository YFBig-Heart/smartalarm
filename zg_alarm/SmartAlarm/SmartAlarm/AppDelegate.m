//
//  AppDelegate.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/21.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "AppDelegate.h"
#import "RootTabViewController.h"
#import <UserNotifications/UserNotifications.h>
#import <YYModel/YYModel.h>
#import "CheckUserViewController.h"
#import "LockViewController.h"
#import "AlarmListViewController.h"
#import "BaseNavigationController.h"
static NSString *appKey = @"42350f22fcba618237708638";
static NSString *UmengAppkey = @"59d6e3ef1061d231d300001f";
static NSString *channel = @"App Store smart alarm";
static BOOL isProduction = YES;

@interface AppDelegate ()<JPUSHRegisterDelegate,UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate

void uncaughtExceptionHandler(NSException*exception){
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@",[exception callStackSymbols]);
    // Internal error reporting
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self UserLoginOrRegister];
    [self UmengConfig];
    [self JpushConfig:launchOptions];
    [Chameleon setGlobalThemeUsingPrimaryColor:FlatSkyBlue withSecondaryColor:[UIColor clearColor] andContentStyle:UIContentStyleLight];

    return YES;
}
- (void)UserLoginOrRegister
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserPsw] != nil) {
        [self.window setRootViewController:[[RootTabViewController alloc] init]];
        [self.window makeKeyAndVisible];
    }
    else
    {
        if (nil == self.window) {
            self.window = [UIApplication sharedApplication].keyWindow;
        }
        UINavigationController * LoginOrRegisterNav = [[UINavigationController alloc] initWithRootViewController:[[CheckUserViewController alloc] init]];
        [self.window setRootViewController:LoginOrRegisterNav];
        [self.window makeKeyAndVisible];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [JPUSHService setBadge:0];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAPNsView" object:nil userInfo:nil];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)UmengConfig
{
    /* 打开调试日志 */
//    [[UMSocialManager defaultManager] openLog:YES];
//    /* 设置友盟appkey */
//    [[UMSocialManager defaultManager] setUmSocialAppkey:UmengAppkey];
//    
//    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wx4637e12e84a476d1" appSecret:@"ee7713b513b9e555b381c953e7ea8f07" redirectURL:@"http://mobile.umeng.com/social"];
//    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"1105821097"  appSecret:nil redirectURL:@"http://mobile.umeng.com/social"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [MobClick setAppVersion:version];
    [UMConfigInstance setAppKey:UmengAppkey];
    [UMConfigInstance setChannelId:@"App Store"];
    [MobClick startWithConfigure:UMConfigInstance];
    [MobClick setLogEnabled:YES];
}
- (void)JpushConfig:(NSDictionary *)launchOptions
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultAPNs] == nil) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kDefaultAPNs];
    }
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    [JPUSHService setupWithOption:launchOptions appKey:appKey
                          channel:channel
                 apsForProduction:isProduction
            advertisingIdentifier:nil];
    
}
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"%@", [NSString stringWithFormat:@"Device Token: %@", deviceToken]);
    [JPUSHService registerDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"did Fail To 注册 For Remote Notifications With Error: %@", error);
}
- (NSString *)logDic:(NSDictionary *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:
(void (^)(UIBackgroundFetchResult))completionHandler {
    [JPUSHService handleRemoteNotification:userInfo];
    NSLog(@"iOS7及以上系统，收到通知:%@", [self logDic:userInfo]);
    
    completionHandler(UIBackgroundFetchResultNewData);
}

#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#pragma mark- JPUSHRegisterDelegate
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    NSDictionary * userInfo = notification.request.content.userInfo;
    
    UNNotificationRequest *request = notification.request; // 收到推送的请求
    UNNotificationContent *content = request.content; // 收到推送的消息内容
    
    NSNumber *badge = content.badge;  // 推送消息的角标
    NSString *body = content.body;    // 推送消息体
    UNNotificationSound *sound = content.sound;  // 推送消息的声音
    NSString *subtitle = content.subtitle;  // 推送消息的副标题
    NSString *title = content.title;  // 推送消息的标题
    
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        NSLog(@"iOS10 前台收到远程通知:%@", [self logDic:userInfo]);
    }
    else {
        // 判断为本地通知
        NSLog(@"iOS10 前台收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
    }
    //通知刷新主页
    if ([userInfo objectForKey:@"type"]&&([[userInfo objectForKey:@"type"] intValue] == 0)) {
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"synDeviceNotification" object:nil]];
//        [[self GetBaseViewController] pushViewController:[[AlarmListViewController alloc] init] animated:YES];
    }
//    else
//        [[self GetBaseViewController] pushViewController:[[LockViewController alloc] init] animated:YES];
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"synDeviceNotificationReSelect" object:[userInfo objectForKey:@"imei"]]];

    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以设置
    //功能：可设置是否在应用内弹出通知
}
- (BaseNavigationController *)GetBaseViewController
{
    RootTabViewController * rootvc = (RootTabViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    return (BaseNavigationController *)rootvc.selectedViewController;
}
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    UNNotificationRequest *request = response.notification.request; // 收到推送的请求
    UNNotificationContent *content = request.content; // 收到推送的消息内容
    
    NSNumber *badge = content.badge;  // 推送消息的角标
    NSString *body = content.body;    // 推送消息体
    UNNotificationSound *sound = content.sound;  // 推送消息的声音
    NSString *subtitle = content.subtitle;  // 推送消息的副标题
    NSString *title = content.title;  // 推送消息的标题
    
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        NSLog(@"iOS10 收到远程通知:%@", [self logDic:userInfo]);
    }
    else {
        // 判断为本地通知
        NSLog(@"iOS10 收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
    }
    //通知刷新主页
    if ([userInfo objectForKey:@"type"]&&([[userInfo objectForKey:@"type"] intValue] == 0)) {
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"synDeviceNotification" object:nil]];
        [[self GetBaseViewController] pushViewController:[[AlarmListViewController alloc] init] animated:YES];
    }
    else
        [[self GetBaseViewController] pushViewController:[[LockViewController alloc] init] animated:YES];
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"synDeviceNotificationReSelect" object:[userInfo objectForKey:@"imei"]]];
    completionHandler();  // 系统要求执行这个方法
}
#endif

#pragma mark --横屏/竖屏
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window
{
    
    if (self.allowRotation == YES) {
        //横屏
        return UIInterfaceOrientationMaskLandscape;
        
    }else{
        //竖屏
        return UIInterfaceOrientationMaskPortrait;
        
    }
    
}





@end
