//
//  BoardMainViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/2.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoardMainViewController : UIViewController
@property (retain,nonatomic) NSMutableDictionary * BoardDic;
@end
