//
//  ContanctListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "ContanctListViewController.h"
#import "ContanctListTableViewCell.h"
#import "ContanctEditViewController.h"

@interface ContanctListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *PhoneListTabelView;
@property (retain,nonatomic) NSMutableArray * ContanctListArray;
@property (retain, nonatomic)NSArray * arrayData;
@property (weak, nonatomic) IBOutlet UILabel *detailLable;
@property (retain,nonatomic)UIButton *DeleteAllBtn;
@end

@implementation ContanctListViewController

- (void)viewDidLoad {
    self.title = NSLocalizedString(@"报警电话", nil);
    if (_ContanctListArray == nil) {
        _ContanctListArray = [[NSMutableArray alloc] init];
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd  target:self action:@selector(Add)];
    
    //设置底部
    _PhoneListTabelView.backgroundColor = [UIColor clearColor];
    _PhoneListTabelView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 42)];
    [_PhoneListTabelView setSeparatorInset: UIEdgeInsetsZero];
    _PhoneListTabelView.estimatedSectionHeaderHeight = 0;
    
    //设置顶部为0
    CGRect frame=CGRectMake(0, 0, 0, CGFLOAT_MIN);
    _PhoneListTabelView.tableHeaderView=[[UIView alloc]initWithFrame:frame];
    
    _PhoneListTabelView.delegate = self;
    _PhoneListTabelView.dataSource = self;
    _arrayData = @[@"",@"Call/SMS",@"Only Call",@"Only SMS",@"CID"];
    [self LayOutDeleteALLBtn];
    [_detailLable setText:NSLocalizedString(@"在此页面上，您可以添加一个或多个人联系人。当警报触发时，系统会通过呼叫或发短信的方式通知列表中的联系人", nil)];
    [_detailLable setHidden:YES];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)LayOutDeleteALLBtn
{
    if (_DeleteAllBtn == nil) {
        _DeleteAllBtn = [[UIButton alloc] initWithFrame:CGRectZero];
        [self.view addSubview:_DeleteAllBtn];
    }
    [_DeleteAllBtn setTitle:NSLocalizedString(@"删除所有", nil) forState:UIControlStateNormal];
    _DeleteAllBtn.sd_layout.leftSpaceToView(self.view, 16).rightSpaceToView(self.view, 16).heightIs(40).bottomSpaceToView(self.view, 20);
    [_DeleteAllBtn bs_configureAsPrimaryStyle];
    [[_DeleteAllBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        UIAlertController * ct = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"您确定删除吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *showAllInfoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self DeleteClick];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消",nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [ct addAction:cancelAction];
        [ct addAction:showAllInfoAction];
        [self presentViewController:ct animated:YES completion:nil];
    }];
}
- (void)DeleteClick
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",@"99",@"sn",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"70078",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [self synAdminList];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    [self synAdminList];
}
- (void)synAdminList
{
    NSDictionary * tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00078",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _ContanctListArray = [response objectForKey:@"data"];
            if ([_ContanctListArray count] == 0) {
                //无联系人
                [_PhoneListTabelView setHidden:YES];
                [self LayOutNullDetailView];
            }
            else
            {
                [self HideNullDetailView];
                [_PhoneListTabelView setHidden:NO];
                [_PhoneListTabelView reloadData];
            }
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)LayOutNullDetailView
{
    if ([_detailLable isHidden] == YES) {
        [_DeleteAllBtn setHidden:YES];
        [_detailLable setHidden:NO];
        [_detailLable setText:NSLocalizedString(@"在此页面上，您可以添加一个或多个人联系人。当警报触发时，系统会通过呼叫或发短信的方式通知列表中的联系人。", nil)];
    }
}
- (void)HideNullDetailView
{
    if ([_detailLable isHidden] == NO) {
        [_DeleteAllBtn setHidden:NO];
        [_detailLable setHidden:YES];
    }
}

- (void)Add
{
    ContanctEditViewController * vc = [[ContanctEditViewController alloc] init];
    vc.ContanctEditDic = [[NSMutableDictionary alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_ContanctListArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ContanctListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Contantct_cell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContanctListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.NameLabel.text = [[_ContanctListArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.PhoneLabel.text = [[_ContanctListArray objectAtIndex:indexPath.row] objectForKey:@"number"];
    cell.TypeLabel.text = [_arrayData objectAtIndex:[[[_ContanctListArray objectAtIndex:indexPath.row] objectForKey:@"type"] intValue]];

    NSLog(@"  -----type=%@",cell.TypeLabel.text);
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ContanctEditViewController * vc = [[ContanctEditViewController alloc] init];
    vc.ContanctEditDic = [[NSMutableDictionary alloc] initWithDictionary:[_ContanctListArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
