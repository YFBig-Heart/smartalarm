//
//  HistoryViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/24.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "HistoryViewController.h"
#import "AlarmListViewController.h"
#import "LockViewController.h"
#import "ElecHistoryListViewController.h"
#import "ControllerHistoryListViewController.h"
@interface HistoryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *MainTabview;
@property (weak, nonatomic) IBOutlet UITableView *MainTable;

@end

@implementation HistoryViewController
NSArray *ItemsTitle;
NSArray *ItemsIcon;

- (void)viewDidLoad {
    self.title = NSLocalizedString(@"历史",nil);
//    if (([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)) {
//        ItemsTitle = @[@"报警记录",@"设/撤防记录",@"开关记录",@"电量记录"];
//        ItemsIcon = @[@"list_alarm",@"list_protect",@"list_outlets_control",@"list_outlets_elect"];
//    }
//    else{
//        ItemsTitle = @[@"报警记录",@"设/撤防记录"];
//        ItemsIcon = @[@"list_alarm",@"list_protect"];
//    }
    [super viewDidLoad];

}
- (void)viewWillAppear:(BOOL)animated
{
    if (([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)) {
        ItemsTitle = @[@"报警记录",@"设/撤防记录",@"开关记录",@"电量记录"];
        ItemsIcon = @[@"list_alarm",@"list_protect",@"list_outlets_control",@"list_outlets_elect"];
    }
    else{
        ItemsTitle = @[@"报警记录",@"设/撤防记录"];
        ItemsIcon = @[@"list_alarm",@"list_protect"];
    }
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    [_MainTable reloadData];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ItemsIcon.count;
}
- (CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.imageView.image = [UIImage imageNamed:[ItemsIcon objectAtIndex:indexPath.row]];
    [cell.textLabel setText:NSLocalizedString([ItemsTitle objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self.navigationController pushViewController:[[LockViewController alloc] init] animated:YES];
            break;
        case 1:
            [self.navigationController pushViewController:[[AlarmListViewController alloc] init] animated:YES];
            break;
        case 2:
            [self.navigationController pushViewController:[[ControllerHistoryListViewController alloc] init] animated:YES];
            break;
        case 3:
            [self.navigationController pushViewController:[[ElecHistoryListViewController alloc] init] animated:YES];
              break;
        default:
            break;
    }
}
    /*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
