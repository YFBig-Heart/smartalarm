//
//  DelaySettingViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/6.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "DelaySettingViewController.h"

@interface DelaySettingViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UITextField * ProtectDelayFD;
@property (retain,nonatomic) UITextField * AlarmDelayFD;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;

@end

@implementation DelaySettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_TitleArray == nil) {
        _TitleArray = @[@"设防延时(秒)",@"报警延时(秒)"];
    }
    _RemoteInfoDic = [[NSMutableDictionary alloc] init];
    self.title = NSLocalizedString(@"延时设置", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];
    [self synDeviceDelay];
}
- (void)synDeviceDelay
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00070",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)Done
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_RemoteInfoDic objectForKey:@"EntryDelay"],@"EntryDelay",[_RemoteInfoDic objectForKey:@"ExitDelay"],@"ExitDelay",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80070",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            if (_ProtectDelayFD) {
                [_ProtectDelayFD removeFromSuperview];
                _ProtectDelayFD = nil;
            }
            _ProtectDelayFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_ProtectDelayFD setTextColor:[UIColor flatGrayColor]];
            [_ProtectDelayFD setText:[_RemoteInfoDic objectForKey:@"ExitDelay"]];
            [_ProtectDelayFD setTextAlignment:NSTextAlignmentRight];
            [_ProtectDelayFD setBackgroundColor:[UIColor clearColor]];
            [_ProtectDelayFD setKeyboardType:UIKeyboardTypeNumberPad];
            [cell addSubview:_ProtectDelayFD];
            _ProtectDelayFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).widthRatioToView(self.view, 0.47);
            [[self.ProtectDelayFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return text.length > 0;
              }]
             subscribeNext:^(id x) {
                 if([self.ProtectDelayFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.ProtectDelayFD.text = [self.ProtectDelayFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 if ([self.ProtectDelayFD.text intValue] > 1800) {
                     self.ProtectDelayFD.text = @"1800";
                 }
                 [_RemoteInfoDic setObject:_ProtectDelayFD.text forKey:@"ExitDelay"];
                 NSLog(@"%@", x);
             }];
        }
            break;
        case 1:
        {
            if (_AlarmDelayFD) {
                [_AlarmDelayFD removeFromSuperview];
                _AlarmDelayFD = nil;
            }
            _AlarmDelayFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_AlarmDelayFD setTextColor:[UIColor flatGrayColor]];
            [_AlarmDelayFD setKeyboardType:UIKeyboardTypeNumberPad];
            [_AlarmDelayFD setText:[_RemoteInfoDic objectForKey:@"EntryDelay"]];
            [_AlarmDelayFD setTextAlignment:NSTextAlignmentRight];
            [_AlarmDelayFD setBackgroundColor:[UIColor clearColor]];
            [cell addSubview:_AlarmDelayFD];
            _AlarmDelayFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).widthRatioToView(self.view, 0.47);
            [[self.AlarmDelayFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  [self.navigationItem.rightBarButtonItem setEnabled:NO];
                  return text.length > 0;
              }]
             subscribeNext:^(id x) {
                 if([self.AlarmDelayFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.AlarmDelayFD.text = [self.AlarmDelayFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 if ([self.AlarmDelayFD.text intValue] > 1800) {
                     self.AlarmDelayFD.text = @"1800";
                 }
                 [_RemoteInfoDic setObject:_AlarmDelayFD.text forKey:@"EntryDelay"];
                 [self.navigationItem.rightBarButtonItem setEnabled:YES];
                 NSLog(@"%@", x);
             }];
        }
            break;
        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
            break;
        default:
            break;
    }
}
@end
