//
//  WifiConfigViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "WifiConfigViewController.h"
#import "ESPTouchTask.h"
#import "ESPTouchResult.h"
#import "ESP_NetUtil.h"
#import "ESPTouchDelegate.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "ESP_NetUtil.h"
#import "PreEntryIdleViewController.h"
#import "RootTabViewController.h"
#import "SmartConfigNextViewController.h"

#import "ESP_WifiUtil.h"

//@interface saEspTouchDelegateImpl : NSObject<ESPTouchDelegate>
//
//@end
//
//@implementation saEspTouchDelegateImpl
//
//-(void) dismissAlert:(UIAlertView *)alertView
//{
//    [alertView dismissWithClickedButtonIndex:[alertView cancelButtonIndex] animated:YES];
//}
//
//-(void) showAlertWithResult: (ESPTouchResult *) result
//{
//    NSString *title = nil;
//    NSString *message = NSLocalizedString(@"成功", nil);
//    NSTimeInterval dismissSeconds = 2.0;
//    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
//    [alertView show];
//    [self performSelector:@selector(dismissAlert:) withObject:alertView afterDelay:dismissSeconds];
//}
//
//-(void) onEsptouchResultAddedWithResult: (ESPTouchResult *) result
//{
//    NSLog(@"EspTouchDelegateImpl onEsptouchResultAddedWithResult bssid: %@", result.bssid);
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self showAlertWithResult:result];
//    });
//}
//
//@end


@interface WifiConfigViewController ()<ESPTouchDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *CompleteBtn;
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UITextField * PasswordFD;
@property (atomic, strong) ESPTouchTask *_esptouchTask;
@property (strong, nonatomic) NSString *bssid;
@property (nonatomic, strong) NSCondition *_condition;
//@property (nonatomic, strong) saEspTouchDelegateImpl *_esptouchDelegate;
@property (retain, nonatomic) UIActivityIndicatorView *_spinner;
@property (retain,nonatomic )UIView * spinnerView;
@property (retain,nonatomic) NSString * ssid;
@property (retain,nonatomic )UIActivityIndicatorView *indicator;
@end

@implementation WifiConfigViewController

- (void)viewDidLoad {
    [ESP_NetUtil tryOpenNetworkPermission];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    _TitleArray = @[@"Wifi SSID:" ,@"Wifi 密码:"];
    self._condition = [[NSCondition alloc]init];
//    self._esptouchDelegate = [[saEspTouchDelegateImpl alloc]init];
    self.title = NSLocalizedString(@"智能配置模式", nil);
    [_CompleteBtn bs_configureAsPrimaryStyle];
    [[_CompleteBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self Done];
    }];
    [_CompleteBtn setTitle:NSLocalizedString(@"配置", nil) forState:(UIControlStateNormal)];
    if (_EntryType != 0) {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone  target:self action:@selector(ItemDone)];
    }
    [super viewDidLoad];
    UILabel * FooterLable = [[UILabel alloc] init];
    //[FooterLable setText:NSLocalizedString(@"暂不支持 5G 路由器", nil)];
    //[FooterLable setTextColor:FlatRed];
    [FooterLable setText:NSLocalizedString(@"请判断你手机当前连接的WIFI是否为5G Wi-Fi,如果是请切换到2.4G Wi-Fi(如果你在使用5G路由器,请将路由器设置成兼容2.4G及5G模式)", nil)];
    [FooterLable setTextColor:FlatGray];
    [FooterLable sizeToFit];
    [FooterLable setFont:[UIFont systemFontOfSize:15]];
    [FooterLable setNumberOfLines:0];
    [FooterLable setTextAlignment:NSTextAlignmentLeft];
    _MainTable.tableFooterView = FooterLable;
    _MainTable.tableFooterView.sd_layout.heightIs(80);
    if (_isHideBackButton) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel  target:self action:@selector(Cance)];
        self.navigationItem.hidesBackButton = YES;
    }
}

- (void)Cance
{
    [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
    [self.view.window makeKeyAndVisible];
}
- (void)ItemDone
{
    PreEntryIdleViewController * vc = [[PreEntryIdleViewController alloc] init];
    vc.BindDeviceImei = [NSString stringWithString:_currIMEI];
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSString *)fetchSsid
{
    NSDictionary *ssidInfo = [self fetchNetInfo];
    
    NSLog(@"wifi SSID=%@",[ssidInfo objectForKey:@"SSID"]);
    
    return [ssidInfo objectForKey:@"SSID"];
    
    
//    WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
//    String tempSsidString = wifiInfo.getSSID();
//    if (tempSsidString != null && tempSsidString.length() > 2) {
//        String wifiSsid = tempSsidString.substring(1, tempSsidString.length() - 1);
//        List<ScanResult> scanResults=wifiManager.getScanResults();
//        for(ScanResult scanResult:scanResults){
//            if(scanResult.SSID.equals(wifiSsid)){
//                //返回4个数字，2开头的话就是2.4G,5开头的话就是5G
//                int frequency = scanResult.frequency;
//                break;
//            }
//        }
//    }
    
}

- (NSString *)fetchBssid
{
    NSDictionary *bssidInfo = [self fetchNetInfo];
    
    return [bssidInfo objectForKey:@"BSSID"];
}





// refer to http://stackoverflow.com/questions/5198716/iphone-get-ssid-without-private-library
- (NSDictionary *)fetchNetInfo
{
    NSArray *interfaceNames = CFBridgingRelease(CNCopySupportedInterfaces());
    
    NSDictionary *SSIDInfo;
    for (NSString *interfaceName in interfaceNames) {
        SSIDInfo = CFBridgingRelease(
                                     CNCopyCurrentNetworkInfo((__bridge CFStringRef)interfaceName));
        
        BOOL isNotEmpty = (SSIDInfo.count > 0);
        if (isNotEmpty) {
            break;
        }
    }
    return SSIDInfo;
}

- (NSArray *) executeForResults
{
    [self._condition lock];
    NSString *apSsid = _ssid;
    NSString *apPwd = _PasswordFD.text;
    NSString *apBssid = self.bssid;
    self._esptouchTask =
    [[ESPTouchTask alloc]initWithApSsid:apSsid andApBssid:apBssid andApPwd:apPwd];
    // set delegate
//    [self._esptouchTask setEsptouchDelegate:self._esptouchDelegate];
    [self._condition unlock];
    NSArray * esptouchResults = [self._esptouchTask executeForResults:1];
    NSLog(@"ESPViewController executeForResult() result is: %@",esptouchResults);
    return esptouchResults;
}
- (void)LayOutAlart
{
    _indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [_CompleteBtn.superview insertSubview:_indicator aboveSubview:_CompleteBtn];
    _indicator.sd_layout.widthIs(47).heightIs(47).centerXIs(_CompleteBtn.centerX).centerYIs(_CompleteBtn.centerY);
    [_CompleteBtn setTitle:@"" forState:UIControlStateNormal];
    [_indicator startAnimating];
    [_CompleteBtn setNeedsDisplay];
}
- (void)Done
{
    NSLog(@"ESPViewController do confirm action...");
    SmartConfigNextViewController * vc = [[SmartConfigNextViewController alloc] init];
    vc.Wifissid = _ssid;
    vc.Wifibssid = _bssid;
    vc.Wifipwd = _PasswordFD.text;
    vc.EntryType = _EntryType;
    [self.navigationController pushViewController:vc animated:YES];
    return;
    
    [self LayOutAlart];
    dispatch_queue_t  queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSLog(@"ESPViewController do the execute work...");
        // execute the task
        NSArray *esptouchResultArray = [self executeForResults];
        // show the result to the user in UI Main Thread
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_indicator removeFromSuperview];
            [_CompleteBtn setTitle:NSLocalizedString(@"配置", nil) forState:UIControlStateNormal];
            ESPTouchResult *firstResult = [esptouchResultArray objectAtIndex:0];
            // check whether the task is cancelled and no results received
            if (!firstResult.isCancelled)
            {
                NSMutableString *mutableStr = [[NSMutableString alloc]init];
                NSUInteger count = 0;
                // max results to be displayed, if it is more than maxDisplayCount,
                // just show the count of redundant ones
                const int maxDisplayCount = 5;
                if ([firstResult isSuc])
                {
                    
                    for (int i = 0; i < [esptouchResultArray count]; ++i)
                    {
                        ESPTouchResult *resultInArray = [esptouchResultArray objectAtIndex:i];
                        [mutableStr appendString:[resultInArray description]];
                        [mutableStr appendString:@"\n"];
                        count++;
                        if (count >= maxDisplayCount)
                        {
                            break;
                        }
                    }
                    
                    if (count < [esptouchResultArray count])
                    {
                        [mutableStr appendString:[NSString stringWithFormat:@"\nthere's %lu more result(s) without showing\n",(unsigned long)([esptouchResultArray count] - count)]];
                    }
                    [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"成功",nil) message:mutableStr delegate:nil cancelButtonTitle:NSLocalizedString(@"确定",nil) otherButtonTitles:nil]show];
                    if (_EntryType == 1) {
                        [self.navigationItem.rightBarButtonItem setEnabled:YES];
                    }
                }
                else
                {
                    [[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"失败",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"确定",nil) otherButtonTitles:nil]show];
                }
            }
            
        });
    });
    [_PasswordFD resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            NSDictionary *netInfo = [self fetchNetInfo];
            cell.detailTextLabel.text = [netInfo objectForKey:@"SSID"];
            NSLog(@"!!! text=%@",cell.detailTextLabel.text);
            _bssid = [NSString stringWithFormat:@"%@",[netInfo objectForKey:@"BSSID"]];
            _ssid = [NSString stringWithFormat:@"%@",[netInfo objectForKey:@"SSID"]];
            [cell.imageView setImage:[UIImage imageNamed:@"list_wifi"]];
        }
            break;
        case 1:
        {
            if (_PasswordFD) {
                [_PasswordFD removeFromSuperview];
                _PasswordFD = nil;
            }
            [cell.imageView setImage:[UIImage imageNamed:@"list_password"]];
            _PasswordFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_PasswordFD setTextColor:[UIColor flatGrayColor]];
            [_PasswordFD setTextAlignment:NSTextAlignmentRight];
            [_PasswordFD setBackgroundColor:[UIColor clearColor]];
            [_PasswordFD setPlaceholder:NSLocalizedString(@"密码",nil)];
            [cell addSubview:_PasswordFD];
            _PasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[_PasswordFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  NSLog(@"@@@ text=%@",text);
                  return YES;
              }]
             subscribeNext:^(id x) {
                 if([self.PasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.PasswordFD.text = [self.PasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 NSLog(@"%@", x);
             }];
        }
            break;
            
        default:
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark - 绘制下一步提示
- (void)LayOutDetailView
{
    UIView * DetailView = [[UIView alloc] init];
    [DetailView setBackgroundColor:FlatWhite];
    [self.view.window addSubview:DetailView];
    DetailView.sd_layout.widthIs(kScreen_Width).heightIs(kScreen_Height);
    
    
    UIView *statesBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 29)];
    [statesBar setBackgroundColor:[UIColor colorWithHexString:@"#5CC9F5"]];
    [DetailView addSubview:statesBar];
    UIButton * leftBtn = [[UIButton alloc] initWithFrame:CGRectZero];
    [leftBtn setTitle:@"< " forState:UIControlStateNormal];
    [leftBtn setTitleColor:FlatWhite forState:UIControlStateNormal];
    [statesBar addSubview:leftBtn];
    
}


@end
