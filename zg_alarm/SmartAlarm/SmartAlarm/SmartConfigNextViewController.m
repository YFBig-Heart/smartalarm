//
//  SmartConfigNextViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/2/2.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "SmartConfigNextViewController.h"
#import "RootTabViewController.h"
#import "ESPTouchTask.h"
#import "ESPTouchResult.h"
#import "ESP_NetUtil.h"
#import "ESPTouchDelegate.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "ESP_NetUtil.h"
#import "ApConfigViewController.h"
#import "ConfigListViewController.h"
#import "GCDAsyncUdpSocket.h"

@interface saEspTouchDelegateImpl : NSObject<ESPTouchDelegate>

@end
@implementation saEspTouchDelegateImpl

-(void) dismissAlert:(UIAlertView *)alertView
{
    [alertView dismissWithClickedButtonIndex:[alertView cancelButtonIndex] animated:YES];
}

-(void) showAlertWithResult: (ESPTouchResult *) result
{
    NSString *title = nil;
    NSString *message = NSLocalizedString(@"成功", nil);
    NSTimeInterval dismissSeconds = 2.0;
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alertView show];
    [self performSelector:@selector(dismissAlert:) withObject:alertView afterDelay:dismissSeconds];
}

-(void) onEsptouchResultAddedWithResult: (ESPTouchResult *) result
{
    NSLog(@"EspTouchDelegateImpl onEsptouchResultAddedWithResult bssid: %@", result.bssid);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showAlertWithResult:result];
    });
}

@end

#define udpPort 8888

@interface SmartConfigNextViewController ()<ESPTouchDelegate,GCDAsyncUdpSocketDelegate>
@property (weak, nonatomic) IBOutlet UILabel *step1;
@property (weak, nonatomic) IBOutlet UILabel *Step2;
@property (weak, nonatomic) IBOutlet UILabel *Step3;
@property (weak, nonatomic) IBOutlet UIButton *ClickConfig;
@property (weak, nonatomic) IBOutlet UIButton *DoneConfig;

//wifi
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UITextField * PasswordFD;
@property (atomic, strong) ESPTouchTask *_esptouchTask;
@property (strong, nonatomic) NSString *bssid;
@property (nonatomic, strong) NSCondition *_condition;
@property (nonatomic, strong) saEspTouchDelegateImpl *_esptouchDelegate;
@property (retain, nonatomic) UIActivityIndicatorView *_spinner;
@property (retain,nonatomic )UIView * spinnerView;
@property (retain,nonatomic) NSString * ssid;
@property (retain,nonatomic )UIActivityIndicatorView *indicator;
//end wifi
@property (nonatomic, strong) GCDAsyncUdpSocket *clientSocket;

@end

@implementation SmartConfigNextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_step1 setText:NSLocalizedString(@"1.参照终端说明书让终端进入智能配置模式。", nil)];
    [_Step2 setText:NSLocalizedString(@"2.终端、手机尽量靠近路由器。", nil)];
    [_Step3 setText:NSLocalizedString(@"3.若配置失败, 请使用 AP 模块配置。", nil)];
    [_ClickConfig setTitle:NSLocalizedString(@"点击此处进入热点配制模式", nil) forState:UIControlStateNormal];
    [[_ClickConfig rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        ApConfigViewController * vc = [[ApConfigViewController alloc] init];
        vc.WifiSSid = _Wifissid;
        [self.navigationController pushViewController:vc animated:YES];
    }];

        [_DoneConfig setTitle:NSLocalizedString(@"开始配置", nil) forState:UIControlStateNormal];
    [_DoneConfig bs_configureAsPrimaryStyle];
    _DoneConfig.sd_layout.widthIs(kScreen_Width - 30).centerXEqualToView(self.view).bottomSpaceToView(self.view, 15).heightIs(45);
    [[_DoneConfig rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self ConfigWifi];
    }];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel  target:self action:@selector(backHome)];;
}
- (void)backHome
{
    [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
    [self.view.window makeKeyAndVisible];
}
- (void)Turn
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - wifi开始配置
- (NSArray *) executeForResults
{
    [self._condition lock];
    NSString *apSsid = _Wifissid;
    NSString *apPwd = _Wifipwd;
    NSString *apBssid = _Wifibssid;
    self._esptouchTask =
    [[ESPTouchTask alloc]initWithApSsid:apSsid andApBssid:apBssid andApPwd:apPwd];
    // set delegate
    [self._esptouchTask setEsptouchDelegate:self._esptouchDelegate];
    [self._condition unlock];
    NSArray * esptouchResults = [self._esptouchTask executeForResults:1];
    NSLog(@"ESPViewController executeForResult() result is: %@",esptouchResults);
    return esptouchResults;
}
- (void)LayOutAlart
{
    _indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [_DoneConfig.superview insertSubview:_indicator aboveSubview:_DoneConfig];
    _indicator.sd_layout.widthIs(47).heightIs(47).centerXIs(_DoneConfig.centerX).centerYIs(_DoneConfig.centerY);
    [_DoneConfig setTitle:@"" forState:UIControlStateNormal];
    [_indicator startAnimating];
    [_DoneConfig setNeedsDisplay];
}
- (void)ConfigWifi
{
    [self LayOutAlart];
    [self StartUdpServer];
    dispatch_queue_t  queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSLog(@"ESPViewController do the execute work...");
        // execute the task
        NSArray *esptouchResultArray = [self executeForResults];
        // show the result to the user in UI Main Thread
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_indicator removeFromSuperview];
            [_DoneConfig setTitle:NSLocalizedString(@"配置", nil) forState:UIControlStateNormal];
            ESPTouchResult *firstResult = [esptouchResultArray objectAtIndex:0];
            // check whether the task is cancelled and no results received
            if (!firstResult.isCancelled)
            {
                NSMutableString *mutableStr = [[NSMutableString alloc]init];
                NSUInteger count = 0;
                // max results to be displayed, if it is more than maxDisplayCount,
                // just show the count of redundant ones
                const int maxDisplayCount = 5;
                if ([firstResult isSuc])
                {
                    
                    for (int i = 0; i < [esptouchResultArray count]; ++i)
                    {
                        ESPTouchResult *resultInArray = [esptouchResultArray objectAtIndex:i];
                        [mutableStr appendString:[resultInArray description]];
                        [mutableStr appendString:@"\n"];
                        count++;
                        if (count >= maxDisplayCount)
                        {
                            break;
                        }
                    }
                    
                    if (count < [esptouchResultArray count])
                    {
                        [mutableStr appendString:[NSString stringWithFormat:@"\nthere's %lu more result(s) without showing\n",(unsigned long)([esptouchResultArray count] - count)]];
                    }
                    [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"WiFi配置成功!",nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"确定",nil) otherButtonTitles:nil] show];
                    if (_EntryType == 1) {
                        [self.navigationItem.rightBarButtonItem setEnabled:YES];
                    }
                }
                else
                {
                    [[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"失败",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"确定",nil) otherButtonTitles:nil] show];
                }
            }
            
        });
    });
}
- (void)StartUdpServer
{
    if (!_clientSocket) {
        
        _clientSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
        NSError * error = nil;
        /*每次扫描，你会收到两条信息，IPv4和IPv6的，根据需求做取舍。*/
        [_clientSocket setIPv6Enabled:NO];
        [_clientSocket bindToPort:udpPort error:&error];
        if (error) {
            NSLog(@"clientSocket_error:%@",error);
            [self removeSocket];

        }else {
            [_clientSocket beginReceiving:&error];
            NSLog(@"socket打开 开始接收信息");
            /*开启后，如果5秒没收到数据，做超时处理*/
            [self performSelector:@selector(checkDidReceiveMessage) withObject:nil afterDelay:60.0];
        }
    }
}

#pragma mark - GCDAsyncUdpSocket delegate
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext {
    
//    NSString *ip = [GCDAsyncUdpSocket hostFromAddress:address];
//    uint16_t port = [GCDAsyncUdpSocket portFromAddress:address];
//    NSLog(@"接收到%@的消息,\n解析到的数据[%@:%d]",sendMessage,ip,port);
    NSArray *messageArr = [[NSString stringWithFormat:@"%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]] componentsSeparatedByString:@"|"];
    
    NSString *mark;
    if (messageArr.count == 3) {
        mark = messageArr[2];
        [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"成功",nil) message:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"WIFI配制成功，绑定号:\r\n", nil),mark] delegate:nil cancelButtonTitle:NSLocalizedString(@"确定",nil) otherButtonTitles:nil] show];
    }
    /*正常连接，10秒后，自动注销监听*/
    [self performSelector:@selector(removeSocket) withObject:nil afterDelay:10.0];
    
}

#pragma mark - 关闭UDP监听
- (void)removeSocket {
    if (_clientSocket) {
        [_clientSocket close];
        _clientSocket = nil;
    }
}
#pragma mark - UDP获取信息超时处理
- (void)checkDidReceiveMessage {
        [self removeSocket];
}
@end
