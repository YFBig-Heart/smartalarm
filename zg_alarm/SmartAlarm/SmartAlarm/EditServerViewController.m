//
//  EditServerViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "EditServerViewController.h"
#import "NSString+ValidateURL.h"
@interface EditServerViewController ()
@property (weak, nonatomic) IBOutlet UITextField *ServerFD;
@property (weak, nonatomic) IBOutlet UIButton *DoneBtn;
@property (retain,nonatomic) NSString * tempString;
@end

@implementation EditServerViewController

- (void)viewDidLoad {
    self.title = NSLocalizedString(@"服务器", nil);
    [[[_ServerFD rac_textSignal] filter:^BOOL(NSString * _Nullable value) {
        NSLog(@"__ %@",value);
        self.DoneBtn.enabled = NO;
        [_DoneBtn setAlpha:0.5];
        if (([value rangeOfString:@"http://"].location == NSNotFound)&&(value.length == 0)) {
            _ServerFD.text = [NSString stringWithFormat:@"%@%@",@"http://",value];
        }
        return [NSString validateURL:_ServerFD.text];
    }] subscribeNext:^(NSString * _Nullable x) {
        //
        if([self.ServerFD.text rangeOfString:@" "].location !=NSNotFound)
        {
            self.ServerFD.text = [self.ServerFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
        }
        [_DoneBtn setAlpha:1];
        self.DoneBtn.enabled = YES;
    }];
//    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultAppServerIP]) {
//        [_ServerFD setText:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultAppServerIP]];
//
//    }
    [[self.DoneBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self Done];
    }];
    [_DoneBtn bs_configureAsPrimaryStyle];
    [super viewDidLoad];
}
- (void)Done
{
    [[NSUserDefaults standardUserDefaults] setObject:_ServerFD.text forKey:kDefaultAppServerIP];
    [HttpRespondError ShowHttpRespondError:0];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
