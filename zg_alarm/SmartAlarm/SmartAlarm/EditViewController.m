//
//  EditViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/2.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "EditViewController.h"
extern NSString* CurrDeviceName;
@interface EditViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) UITextField * NameFD;
@property (assign) BOOL isNew;
@property (weak, nonatomic) IBOutlet UIButton *DeleteBtn;
@end

@implementation EditViewController

- (void)viewDidLoad {
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_BoardDic == nil) {
        _BoardDic = [[NSMutableDictionary alloc] init];
        [_BoardDic setObject:@"" forKey:@"name"];
        [_BoardDic setObject:@"100" forKey:@"sn"];
        _isNew = YES;
        [_DeleteBtn setHidden:YES];
    }
    else
        _isNew = NO;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];;
    [[_DeleteBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"确定删除此排插？", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"取消",nil) otherButtonTitles:NSLocalizedString(@"确定",nil), nil];
        [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
            NSLog(@"%@",x);
            NSInteger  ClickIndex = [(NSNumber *)x integerValue] ;
            if (ClickIndex == 1) {
                [self deleteAction];
            }
        }];
        [alertView show];
    }];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)deleteAction
{
    NSDictionary * tDic ;
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",_NameFD.text,@"name",[_BoardDic objectForKey:@"sn"],@"sn",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"70084",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            int index = (int)[[self.navigationController viewControllers]indexOfObject:self];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(index-2)] animated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)Done
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",_NameFD.text,@"name",[_BoardDic objectForKey:@"sn"],@"sn",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80084",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            CurrDeviceName = _NameFD.text;
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_remotelist";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    switch (indexPath.row) {
        case 0:
        {
            if ([_BoardDic objectForKey:@"name"]) {
                if (_NameFD) {
                    [_NameFD removeFromSuperview];
                    _NameFD = nil;
                }
                [cell.textLabel setText:NSLocalizedString(@"名称",nil)];
                _NameFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_NameFD setTextColor:[UIColor flatGrayColor]];
                [_NameFD setText:[_BoardDic objectForKey:@"name"]];
                [_NameFD setTextAlignment:NSTextAlignmentRight];
                _NameFD.placeholder = NSLocalizedString(@"请输入名称", nil);
                [_NameFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_NameFD];
                _NameFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
                [[self.NameFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      [self.navigationItem.rightBarButtonItem setEnabled:NO];
                      return text.length > 1;
                  }]
                 subscribeNext:^(id x) {
                     if([self.NameFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.NameFD.text = [self.NameFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     [self.navigationItem.rightBarButtonItem setEnabled:YES];
                     //self.NameFD.text = x;
                     NSLog(@"%@", x);
                 }];
                
            }
            
        }
            break;
        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



@end
