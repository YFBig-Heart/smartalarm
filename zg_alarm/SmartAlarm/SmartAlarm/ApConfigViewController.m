//
//  ApConfigViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/1/9.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "ApConfigViewController.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "PreApConfigViewController.h"
#import <arpa/inet.h>
#import <netdb.h>

@interface ApConfigViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) NSArray * IconArray;
@property (retain,nonatomic)UITextField * PasswordFD;
@property (retain,nonatomic)NSString * LocalSSID;
@property (retain,nonatomic) UIButton * NextBTN;

@end

@implementation ApConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    _TitleArray =@[@"Wifi名称",@"Wifi密码"];
    _IconArray = @[@"wifi_smart",@"wifi_ap"];

    self.title = NSLocalizedString(@"热点配置模式", nil);
    _NextBTN = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [_NextBTN setTitle:NSLocalizedString(@"下一步", nil) forState:UIControlStateNormal];
    [self.view addSubview:_NextBTN];
    _NextBTN.sd_layout.widthIs(kScreen_Width - 20).heightIs(48).centerXEqualToView(self.view).centerYEqualToView(self.view);
    [_NextBTN bs_configureAsPrimaryStyle];
    [[_NextBTN rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self DoNext];
    }];
    UILabel * FooterLable = [[UILabel alloc] init];
    //[FooterLable setText:NSLocalizedString(@"暂不支持 5G 路由器", nil)];
    //[FooterLable setTextColor:FlatRed];
    [FooterLable setText:NSLocalizedString(@"请判断你手机当前连接的WIFI是否为5G Wi-Fi,如果是请切换到2.4G Wi-Fi(如果你在使用5G路由器,请将路由器设置成兼容2.4G及5G模式)", nil)];
    [FooterLable setTextColor:FlatGray];
    [FooterLable sizeToFit];
    [FooterLable setFont:[UIFont systemFontOfSize:15]];
    [FooterLable setNumberOfLines:0];
    [FooterLable setTextAlignment:NSTextAlignmentLeft];
    _MainTable.tableFooterView = FooterLable;
    _MainTable.tableFooterView.sd_layout.heightIs(80);

}
- (void)DoNext
{
    if (_LocalSSID) {
        PreApConfigViewController * vc = [[PreApConfigViewController alloc] init];
        vc.LocalPSW = _PasswordFD.text;
        vc.LocalSSID = [_LocalSSID copy];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        [RKDropdownAlert title:nil message:NSLocalizedString(@"您没有接入WIFI",nil)];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSDictionary *)fetchNetInfo
{
    NSArray *interfaceNames = CFBridgingRelease(CNCopySupportedInterfaces());
    
    NSDictionary *SSIDInfo;
    for (NSString *interfaceName in interfaceNames) {
        SSIDInfo = CFBridgingRelease(
                                     CNCopyCurrentNetworkInfo((__bridge CFStringRef)interfaceName));
        
        BOOL isNotEmpty = (SSIDInfo.count > 0);
        if (isNotEmpty) {
            break;
        }
    }
    return SSIDInfo;
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            [cell.imageView setImage:[UIImage imageNamed:@"list_wifi"]];
            NSDictionary *netInfo = [self fetchNetInfo];
            cell.detailTextLabel.text = [netInfo objectForKey:@"SSID"];
            _LocalSSID = [[netInfo objectForKey:@"SSID"] copy];
            NSLog(@"_LocalSSID=%@",_LocalSSID);
        }
            break;
        case 1:
        {
            if (_PasswordFD) {
                [_PasswordFD removeFromSuperview];
                _PasswordFD = nil;
            }
            [cell.imageView setImage:[UIImage imageNamed:@"list_password"]];
            _PasswordFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_PasswordFD setTextColor:[UIColor flatGrayColor]];
            [_PasswordFD setTextAlignment:NSTextAlignmentRight];
            [_PasswordFD setBackgroundColor:[UIColor clearColor]];
            [_PasswordFD setPlaceholder:NSLocalizedString(@"密码",nil)];
            [cell addSubview:_PasswordFD];
            _PasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[_PasswordFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return YES;
              }]
             subscribeNext:^(id x) {
                 if([self.PasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.PasswordFD.text = [self.PasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 NSLog(@"PasswordFD%@", x);
             }];
        }
            break;
            
        default:
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
