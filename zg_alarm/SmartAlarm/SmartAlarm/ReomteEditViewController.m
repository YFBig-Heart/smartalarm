//
//  ReomteEditViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/2.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "ReomteEditViewController.h"
#import "HMScanner.h"
#import "HMScannerController.h"
#import "STPickerView.h"
#import "STPickerDate.h"
#import "HcdDateTimePickerView.h"
#import "STPickerSingle.h"

@interface ReomteEditViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (retain,nonatomic) UITextField * NameFD;
@property (retain,nonatomic) UITextField * UidFD;
@property (retain,nonatomic) UISwitch *AlarmOnoff;
@property (retain,nonatomic) UISwitch *SoundOnoff;
@property (retain,nonatomic) UIButton *UidButton;
@property (weak, nonatomic) IBOutlet UIButton *DeleteBtm;

@end

@implementation ReomteEditViewController
bool isRemoteCotrollAddNew = false;
- (void)viewDidLoad {
    [super viewDidLoad];
    [_DeleteBtm setHidden:NO];
    isRemoteCotrollAddNew = false;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];
    if(_RemoteInfoDic == nil)
    {
        _RemoteInfoDic = [[NSMutableDictionary alloc] init];
        [_RemoteInfoDic setObject:@"" forKey:@"name"];
        [_RemoteInfoDic setObject:@"" forKey:@"code"];
        [_RemoteInfoDic setObject:@"1" forKey:@"siren"];
        [_RemoteInfoDic setObject:@"1" forKey:@"chime"];
        isRemoteCotrollAddNew = true;
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
        [_DeleteBtm setHidden:YES];
        self.title = NSLocalizedString(@"新增遥控器", nil);
    }
    else
    {
        self.title = NSLocalizedString(@"编辑", nil);
    }
    [_DeleteBtm setTitle:NSLocalizedString(@"删除", nil) forState: UIControlStateNormal];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    [[_DeleteBtm rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self AskDelete];
    }];
    // Do any additional setup after loading the view from its nib.
}
- (void)AskDelete
{
    UIAlertController * ct = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"您确定删除吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *showAllInfoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self DeleteRemoteControl];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消",nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [ct addAction:cancelAction];
    [ct addAction:showAllInfoAction];
    [self presentViewController:ct animated:YES completion:nil];
}
- (void)DeleteRemoteControl
{
    NSDictionary * tDic ;

        tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[_RemoteInfoDic objectForKey:@"sn"],@"sn",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"70079",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已保存",nil)];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)Done
{
    NSDictionary * tDic ;
    if (isRemoteCotrollAddNew == YES) {
        tDic = [[NSDictionary alloc] initWithObjectsAndKeys:self.NameFD.text,@"name",_UidFD.text,@"code",_AlarmOnoff.isOn?@"1":@"0",@"siren",_SoundOnoff.isOn?@"1":@"0",@"chime",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    }
    else
        tDic = [[NSDictionary alloc] initWithObjectsAndKeys:self.NameFD.text,@"name",[_RemoteInfoDic objectForKey:@"sn"],@"sn",_UidFD.text,@"code",_AlarmOnoff.isOn?@"1":@"0",@"siren",_SoundOnoff.isOn?@"1":@"0",@"chime",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80079",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已保存",nil)];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return isRemoteCotrollAddNew?_RemoteInfoDic.count:_RemoteInfoDic.count-1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_remotelist";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            if ([_RemoteInfoDic objectForKey:@"name"]) {
                if (_NameFD) {
                    [_NameFD removeFromSuperview];
                    _NameFD = nil;
                }
                [cell.textLabel setText:NSLocalizedString(@"名称",nil)];
                _NameFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_NameFD setTextColor:[UIColor flatGrayColor]];
                if (([_RemoteInfoDic objectForKey:@"name"] != [NSNull null])) {
                    [_NameFD setText:[_RemoteInfoDic objectForKey:@"name"]];
                }
                [_NameFD setTextAlignment:NSTextAlignmentRight];
                [_NameFD setBackgroundColor:[UIColor clearColor]];
                [_NameFD setPlaceholder:NSLocalizedString(@"请输入名称", nil)];
                [cell addSubview:_NameFD];
                _NameFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
                [[self.NameFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      [self.navigationItem.rightBarButtonItem setEnabled:NO];
                      return text.length > 1;
                  }]
                 subscribeNext:^(id x) {
                     if([self.NameFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.NameFD.text = [self.NameFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     [self.navigationItem.rightBarButtonItem setEnabled:YES];
                     NSLog(@"%@", x);
                 }];
            }

        }
            break;
        case 1:
        {
            if ([_RemoteInfoDic objectForKey:@"siren"]) {
                if (_AlarmOnoff) {
                    [_AlarmOnoff removeFromSuperview];
                    _AlarmOnoff = nil;
                }
                cell.textLabel.text = NSLocalizedString(@"警号", nil);
                _AlarmOnoff = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
                [_AlarmOnoff setOn:[[_RemoteInfoDic objectForKey:@"siren"] boolValue]];
                cell.accessoryView = _AlarmOnoff;
                @weakify(self)
                [[self.AlarmOnoff
                  rac_signalForControlEvents:UIControlEventValueChanged]
                 subscribeNext:^(id x) {
                     @strongify(self)
                     NSLog(@"警号  ： %@",x);
                 }];
            }
            
        }
            break;
        case 2:
        {
            if ([_RemoteInfoDic objectForKey:@"chime"]) {
                if (_SoundOnoff) {
                    [_SoundOnoff removeFromSuperview];
                    _SoundOnoff = nil;
                }
                cell.textLabel.text = NSLocalizedString(@"伴音", nil);
                _SoundOnoff = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
                [_SoundOnoff setOn:[[_RemoteInfoDic objectForKey:@"chime"] boolValue]];
                cell.accessoryView = _SoundOnoff;
                @weakify(self)
                [[self.SoundOnoff
                  rac_signalForControlEvents:UIControlEventValueChanged]
                 subscribeNext:^(id x) {
                     @strongify(self)
                     NSLog(@"警声： %@",x);
                 }];
            }
            
        }
            break;
        case 3:
        {
            if ([_RemoteInfoDic objectForKey:@"code"]) {
                if (_UidFD) {
                    [_UidFD removeFromSuperview];
                    _UidFD = nil;
                }
                [cell.textLabel setText:NSLocalizedString(@"编码",nil)];
                _UidFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_UidFD setTextColor:[UIColor flatGrayColor]];
                [_UidFD setText:[_RemoteInfoDic objectForKey:@"code"]];
                [_UidFD setTextAlignment:NSTextAlignmentRight];
                [_UidFD setKeyboardType:UIKeyboardTypeNumberPad];
                [_UidFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_UidFD];
                _UidFD.sd_layout.xIs(cell.centerX*0.6).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 60.0f);
                [_UidFD setKeyboardType:UIKeyboardTypeNumberPad];
                [[self.UidFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      return YES;
                  }]
                 subscribeNext:^(id x) {
                     if([self.UidFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.UidFD.text = [self.UidFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     NSLog(@"%@", x);
                 }];
                _UidButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
                [_UidButton setImage:[UIImage imageNamed:@"QR_code_action"] forState:UIControlStateNormal];
                cell.accessoryView = _UidButton;
                [[self.UidButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
                    [self AddUid];
                }];
            }
        }
            break;

        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_UidFD resignFirstResponder];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)AddUid
{
    NSString *cardName = @"2";
    UIImage *avatar = [UIImage imageNamed:@"avatar"];
    
    // 实例化扫描控制器
    HMScannerController *scanner = [HMScannerController scannerWithCardName:cardName avatar:avatar completion:^(NSString *stringValue) {
        
        NSLog(@"扫描后的 %@",stringValue);
        NSRange range = [stringValue rangeOfString:@"A"];
        if (range.location != NSNotFound) {
            _UidFD.text = [stringValue substringFromIndex:range.location+range.length];
            [_RemoteInfoDic setObject:_UidFD.text forKey:@"code"];
        }

        [UIView beginAnimations:@"View Flip" context:nil];
        //动画持续时间
        [UIView setAnimationDuration:1.25];
        //设置动画的回调函数，设置后可以使用回调方法
        [UIView setAnimationDelegate:self];
        //设置动画曲线，控制动画速度
        [UIView  setAnimationCurve: UIViewAnimationCurveEaseInOut];
        //设置动画方式，并指出动画发生的位置
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view  cache:YES];
        //提交UIView动画
        [UIView commitAnimations];
        
    }];
    
    // 设置导航栏样式
    [scanner setTitleColor:[UIColor whiteColor] tintColor:[UIColor greenColor]];
    
    // 展现扫描控制器
    [self showDetailViewController:scanner sender:nil];
}

@end
