//
//  CameraViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/21.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "CameraViewController.h"
#import "AddDeviceWayVC.h"

//
#import "GWNet.h"
#import <GWP2P/GWP2P.h>
//#import "DeviceListCell.h"
#import "LoginResult.h"

//
//#import "DeviceCell.h"

#import "DeviceCameraCell.h"
#import "AddDeviceWayVC.h"


//@property (nonatomic, strong) GWP2PVideoPlayer *player;


@interface CameraViewController ()<UITableViewDelegate,UITableViewDataSource,DeviceCameraCellDelegate,btnClickedDelegate>//DeviceListCellDelegate
{
     NSTimer *_p2pConnectStatusTimer;
     int IndexRow;//删除对应的行
}

@property (weak, nonatomic) IBOutlet UILabel *SeeLabter;
@property (weak, nonatomic) IBOutlet UILabel *detailLable;
@property (weak, nonatomic) IBOutlet UITableView *MainTable;

@property (retain,nonatomic) NSArray *TitleArray;

@property (nonatomic, retain) NSString *cellname;//设置cell名字

@end

@implementation CameraViewController

- (void)viewDidLoad {
    //self.title = NSLocalizedString(@"监控",nil);
    //[_SeeLabter setText:NSLocalizedString(@"即将开启", nil)];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = NSLocalizedString(@"监控",nil);
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    
    [_MainTable setSeparatorInset:UIEdgeInsetsZero];
    _MainTable.backgroundColor = [UIColor clearColor];
    _MainTable.tableFooterView = [UIView new];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd  target:self action:@selector(AdDevice)];
    
    self.navigationItem.rightBarButtonItem.customView.hidden = YES;
    
    if (_TitleArray == nil) {
        
        //显示lab
        //[_detailLable setText:NSLocalizedString(@"点击左上角的+添加设备", nil)];
       // [_detailLable setHidden:NO];//显示文字
        
        //tableview
        _MainTable.hidden = NO;
        
    }else
    {
        //显示lab
        //[_detailLable setText:NSLocalizedString(@"点击左上角的+添加设备", nil)];
        //[_detailLable setHidden:YES];//隐藏文字
        
        //tableview
        _MainTable.hidden = NO;
    }
    
    //init
    [self initGWNet];
    
    //login
    //[self login:@""];
    [self login:@"" DevicePassword:@""];
    
    //重新连接
    //为解决手机放置一段时间后,sdk没有响应的问题,手机进入后台时,先断开p2p,回到前台时再重新连接
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackgroundNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForegroundNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    //定时器
    _p2pConnectStatusTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(connectStatus) userInfo:nil repeats:YES];

    //监听通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DeviceBtn:) name:@"IDPassword" object:nil];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    GWP2PDeviceLinker *linker = [GWP2PDeviceLinker shareInstance];
    NSArray *listsArr = linker.lanDevices;
    
    NSLog(@"%@",linker.lanDevices);
    });
}

#pragma mark --消息通知返回
- (void)DeviceBtn:(NSNotification *)notification
{
    NSDictionary *dic = notification.object;
    NSLog(@"dic = %@",notification.object);
    
    //NSLog(@"deviceId=%@,deviceName=%@,devicePass=%@",[dic objectForKey:@"deviceId"],[dic objectForKey:@"deviceName"],[dic objectForKey:@"devicePass"]);
    
    //[self login:[dic objectForKey:@"deviceId"]];
    [self login:[dic objectForKey:@"deviceId"] DevicePassword:[dic objectForKey:@"devicePass"]];
    
    self.cellname = [dic objectForKey:@"deviceName"];
    [self.MainTable reloadData];//set cell name
    
}
 
- (void)viewWillAppear:(BOOL)animated
{
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
    
    [self.MainTable reloadData];//视频
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark --P2P
//1) init GWNet
//2) login 登录
//3) 连接P2P库

#pragma mark --初始化GWNet
- (void)initGWNet
{
    //以下GWNet初始化参数仅在本项目测试使用,和bundleID是绑定的,放到其他项目调用GWNet接口会返回errorCode20000
    GWNetSingleton.theAppName = @"GWP2PDemo1";
    GWNetSingleton.theAppVersion = @"06.51.00.00";
    GWNetSingleton.theAppId = @"427f53d92da8996fe02583284c47000a";
    GWNetSingleton.theAppToken = @"5dbd0b82a79e26f1538cf2f7bb7757e6bac5c64508bc9f65af7d069a5f01c837";
    GWNetSingleton.connectWithHttps = YES;
}

#pragma mark --login
- (void)login:(NSString *) DeviceID
    DevicePassword:(NSString *) DevicePassword
{
//    NSString *testUnionID = @"9397251";
//    NSString *testUser = @"5686361760qq.com";
//    NSString *testPassword = @"123";
    //NSString *AppToken = @"5dbd0b82a79e26f1538cf2f7bb7757e6bac5c64508bc9f65af7d069a5f01c837";
    
    //[self saveAccountToPreference:@""];
    //[self savePasswordToPreference:@""];
    
    [GWNetSingleton thirdLoginWithPlatformType:@"2" withUnionID:DeviceID withUser:@"" withPassword:@"" withAppleToken:@"" withOption:@"3" withStoreID:@"" nickName:@"流水" completion:^(BOOL success, NSString *errorCode, NSString *errorString, NSDictionary *json) {

        NSLog(@"登录以后errorCode:%@ errorString:%@ json:%@", errorCode, errorString, json);
        
        //[self connectionto:errorCode json:json];
        [self connectionto:errorCode json:json deviceID:DeviceID devicePassword:DevicePassword];
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//            //登录成功后连接P2P
//            if ([errorCode isEqualToString:@"0"])
//            {
//                NSString *p2pAccount = [NSString stringWithFormat:@"0%d",[json[@"UserID"] intValue] & 0x7fffffff];
//                NSString *p2pVerifyCode1 = json[@"P2PVerifyCode1"];
//                NSString *p2pVerifyCode2 = json[@"P2PVerifyCode2"];
//                NSString *sessionID1 = json[@"SessionID"];
//                NSString *sessionID2 = json[@"SessionID2"];
//
//                //这个连接返回的结果只是指本地处理结果。因为和P2P服务器的连接是异步的,需要通过GWP2PClient的linkStatus属性判断当前与P2P服务器的连接状态。
//                //linkStatus判断的是和服务器连接的情况.当手机和设备处于同一局域网的情况时，如果调用接口时传的是设备局域网IP地址的最后一位，而不是传设备ID，那么将不走我们的服务器，在局域网内直接把信息发给设备，这时，尽管linkStatus返回的状态异常，也可以成功控制设备。
//                NSLog(@"开始连接p2p");
//                BOOL success = [[GWP2PClient sharedClient] connectWithAccount:p2pAccount codeStr1:p2pVerifyCode1 codeStr2:p2pVerifyCode2 sessionID1:sessionID1 sessionID2:sessionID2 customerIDs:nil];
//
//                if (success)
//                {
//                    NSLog(@"连接p2p成功");
//                    [LoginResult setAccount:p2pAccount];
//
//                    [LoginResult setP2PVerifyCode1:p2pVerifyCode1];
//                    [LoginResult setP2PVerifyCode2:p2pVerifyCode2];
//                    [LoginResult setSessionID1:sessionID1];
//                    [LoginResult setSessionID2:sessionID2];
//
//                    //添加
//                    [self addonline];
//
//                    [self getLists];
//
//                }
//            }else
//            {
//                NSLog(@"登录失败");
//            }
//        });
    }];
}

#pragma mark --连接P2P
- (void)connectionto:(NSString *) errorCode
                json:(NSDictionary *) json
            deviceID:(NSString *) deviceID
      devicePassword:(NSString *) devicePassword
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //登录成功后连接P2P
        if ([errorCode isEqualToString:@"0"])
        {
            NSString *p2pAccount = [NSString stringWithFormat:@"0%d",[json[@"UserID"] intValue] & 0x7fffffff];
            NSString *p2pVerifyCode1 = json[@"P2PVerifyCode1"];
            NSString *p2pVerifyCode2 = json[@"P2PVerifyCode2"];
            NSString *sessionID1 = json[@"SessionID"];
            NSString *sessionID2 = json[@"SessionID2"];
            
            NSLog(@"开始连接p2p");
            BOOL success = [[GWP2PClient sharedClient] connectWithAccount:p2pAccount codeStr1:p2pVerifyCode1 codeStr2:p2pVerifyCode2 sessionID1:sessionID1 sessionID2:sessionID2 customerIDs:nil];
            
            if (success)
            {
                NSLog(@"连接p2p成功");
                [LoginResult setAccount:p2pAccount];
                
                [LoginResult setP2PVerifyCode1:p2pVerifyCode1];
                [LoginResult setP2PVerifyCode2:p2pVerifyCode2];
                [LoginResult setSessionID1:sessionID1];
                [LoginResult setSessionID2:sessionID2];
                
                //添加
                //[self addonline];
                
                //[self getLists];
                
                //[self addonlineWithdeviceID:@"9397251" devicePassword:@"123"];
                
                [self addonlineWithdeviceID:deviceID devicePassword:devicePassword];
                
            }
        }else
        {
            NSLog(@"登录失败");
        }
    });
    
}


#pragma mark --p2p状态
- (void)connectStatus
{
    NSLog(@"p2p连接状态:%ld", (long)[GWP2PClient sharedClient].linkStatus);

}

#pragma mark --重新连接
- (void)didEnterBackgroundNotification:(NSNotification *)notification
{
    NSLog(@"didEnterBackgroundNotification");
   // [[GWP2PClient sharedClient] disconnect];//断开连接
}

- (void)willEnterForegroundNotification:(NSNotification *)notification
{
    NSLog(@"willEnterForegroundNotification");
//    [[GWP2PClient sharedClient] connectWithAccount:[LoginResult getAccount] codeStr1:[LoginResult getP2PVerifyCode1] codeStr2:[LoginResult getP2PVerifyCode2] sessionID1:[LoginResult getSessionID1] sessionID2:[LoginResult getSessionID2] customerIDs:nil];
}

#pragma mark --在线验证后添加
- (void)addonlineWithdeviceID:(NSString *) deviceID
               devicePassword:(NSString *) devicePassword
{
    NSLog(@"在线验证后添加");

    //NSString *deviceID = @"9397251";
    //NSString *devicePassword = @"123";

    DeviceModel *deviceModel = [[DeviceModel alloc] init];
    deviceModel.deviceID = @"4215589";
    deviceModel.devicePassword = @"123";
    __weak typeof(self) weakSelf = self;

    //alertView的消失会导致MBProgressHUD一起消失,所以要添加到tabBarController.view
    [[GWP2PClient sharedClient] getMultipleDeviceStatusWithDeviceID:deviceID password:devicePassword completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary)
     {
         NSLog(@"dataDictionary=%@",dataDictionary);
         
         NSNumber *errorValue = dataDictionary[GWP2PMessageErrorValue];
         if (success || errorValue.integerValue == P2PClientMessageErrorNoRight)
         {
             NSLog(@"添加成功");
             deviceModel.deviceType = [dataDictionary[@"device type"] intValue];
             deviceModel.deviceSubtype = [dataDictionary[@"device subtype"] intValue];
             [DeviceModel saveDeviceModel:deviceModel];
             [weakSelf.MainTable reloadData];

         } else
         {
             NSLog(@"添加失败");
         }
     }];

    [self GetListData];
}


//push 添加
- (void)AdDevice
{
    [self.navigationController pushViewController:[[AddDeviceWayVC alloc] init] animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - 设备列表代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"设备列表代理count=%ld",[DeviceModel deviceModels].count);
    return [DeviceModel deviceModels].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DeviceCameraCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeviceCamera"];
    if (!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"DeviceCameraCell" owner:nil options:nil]firstObject];
    }
    
    cell.deviceModel = [DeviceModel deviceModels][indexPath.row];
    NSLog(@"deviceName=%@,deviceModel=%@",cell.deviceModel.deviceName,cell.deviceModel);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    cell.btnDelegate = self;
    
    //设置cell info
    //[cell setCellWithInfo:@"客厅"];
    
    if (self.cellname.length != 0) {
        
        [cell setCellWithInfo:self.cellname];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tableView.frame.size.width * 9 / 16 + 44 + 10;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    DeviceModel *deviceModel = [DeviceModel deviceModels][indexPath.row];
    [DeviceModel removeDeviceModel:deviceModel];
    [tableView reloadData];
}

#pragma mark --删除对应的cell的data btnClickedDelegate
- (void)cellBtnClicked:(int)row
{
    NSLog(@"第%d行被点击",row);
//    DeviceModel *deviceModel = [DeviceModel deviceModels][row];
//    [DeviceModel removeDeviceModel:deviceModel];
//    [_MainTable reloadData];
     IndexRow = row;
    
    //模式对话框
    UIAlertView *alertViewFormat = [[UIAlertView alloc] initWithTitle:@""
                                                              message:@"你确定要删除吗?"
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                                    otherButtonTitles:@"确定", nil];
    alertViewFormat.tag = 555;
    [alertViewFormat show];
    
}


#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //格式化
    if (alertView.tag == 555)
    {
        if (0 == buttonIndex)
        {
            NSLog(@"取消按钮被点击了...");
        }
        if (1 == buttonIndex)
        {
            NSLog(@"确定按钮被点击了...");
            
            //删除对应的cell
            DeviceModel *deviceModel = [DeviceModel deviceModels][IndexRow];
            [DeviceModel removeDeviceModel:deviceModel];
            [_MainTable reloadData];
        }
    }
    
    
}

#pragma mark - 设备列表cell按钮点击代理 DeviceListCellDelegate
- (void)deviceListCell:(DeviceCameraCell *)cell pushVC:(NSString *)vcName isStoryboard:(BOOL)isStoryboard
{
    NSLog(@"deviceID=%@,devicePassword=%@",cell.deviceModel.deviceID,cell.deviceModel.devicePassword);
    
    UIViewController *vc;
    if (isStoryboard)
    {
        vc = [[UIStoryboard storyboardWithName:vcName bundle:nil] instantiateInitialViewController];
        
    } else
    {
        vc = [[NSClassFromString(vcName) alloc] init];
    }
    if ([vc respondsToSelector:@selector(deviceModel)])
    {
        [vc setValue:cell.deviceModel forKey:@"deviceModel"];
    }
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -获取设备回放列表
-(void)getLists
{
    //ts
    NSString *deviceID = @"9397251";
    NSString *devicePassword = @"123";
    
    __weak typeof (self)weakSelf = self;
    GWP2PClient *click = [GWP2PClient sharedClient];
    [click getDevicePlaybackFilesWithDeviceID:deviceID devicePassword:devicePassword startDate:nil endDate:nil completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary) {
        
        NSLog(@"获取回放文件结果:%@", dataDictionary);
        NSString *result = dataDictionary[@"result"];
        if ([result floatValue] == 1)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
               // [weakSelf.tableView reloadData];
            });
          
            NSArray *file = dataDictionary[@"files"];
            NSLog(@"file0=%@,file1=%@,file2=%@,",[file objectAtIndex:0],[file objectAtIndex:1],[file objectAtIndex:2]);

            GWPlaybackFileModel *model = [file objectAtIndex:0];
            NSLog(@"model.discID=%d,model.time=%d", (int)model.discID,(int)model.time);
            NSLog(@"model.cType=%@",model.cType);

            NSLog(@"yearmonthday=%d-%d-%d",(int)model.year,(int)model.month,(int)model.day);
            NSLog(@"hourminutesecondy=%d-%d-%d",(int)model.hour,(int)model.minute,(int)model.second);
            
        }else
        {
            NSLog(@"error");
        }
        
    }];
}

#pragma mark --获取列表data
- (void)GetListData
{
    NSDictionary * tDic;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"user_name",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00090",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"获取列表%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0)
        {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}

#pragma mark --删除摄像头
- (void)DeleteCamera
{
    NSString *uuid = @"9397251";
    
    NSDictionary * tDic;
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"user_name",uuid, @"uuid",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"70090",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"删除摄像头%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0)
        {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        
    }];
}

@end
