//
//  WirelessSirenViewController.m
//  SmartAlarm
//
//  Created by zhangping on 2018/9/1.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "WirelessSirenViewController.h"
#import "HMScanner.h"
#import "HMScannerController.h"

@interface WirelessSirenViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@property (retain,nonatomic) UISwitch *WirelessSound;
@property (retain,nonatomic) UITextField * UidFD;
@property (retain,nonatomic) UIButton * UidButton;

@end

@implementation WirelessSirenViewController

- (void)viewDidLoad {
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_TitleArray == nil) {
        _TitleArray = @[@"警号",@"编码"];
        _RemoteInfoDic = [[NSMutableDictionary alloc] init];
    }
    self.title = NSLocalizedString(@"无线警号", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone  target:self action:@selector(Done)];
    [self synWirelessSound];
    [super viewDidLoad];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)Done
{
    [self reportWirelessSound];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    switch (indexPath.row) {
        case 0:
        {
            if (_WirelessSound) {
                [_WirelessSound removeFromSuperview];
                _WirelessSound = nil;
            }
            _WirelessSound = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
            cell.accessoryView = _WirelessSound;
            if ([[_RemoteInfoDic objectForKey:@"status"] boolValue]) {
                [_WirelessSound setOn:YES];
            }
            else
                [_WirelessSound setOn:NO];
            @weakify(self)
            [[self.WirelessSound
              rac_signalForControlEvents:UIControlEventValueChanged]
             subscribeNext:^(UISwitch * x) {
                 @strongify(self)
                 NSLog(@"开关  ： %@",x);
                 if (x.isOn) {
                     [_RemoteInfoDic setObject:@"1" forKey:@"status"];
                 }
                 else
                     [_RemoteInfoDic setObject:@"0" forKey:@"status"];
                 [self reportWirelessSound];
             }];
        }
            break;
        case 1:
        {
            if ([_RemoteInfoDic objectForKey:@"code"]) {
                if (_UidFD) {
                    [_UidFD removeFromSuperview];
                    _UidFD = nil;
                }
                [cell.textLabel setText:NSLocalizedString(@"编码",nil)];
                _UidFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_UidFD setTextColor:[UIColor flatGrayColor]];
                [_UidFD setText:[_RemoteInfoDic objectForKey:@"code"]];
                [_UidFD setTextAlignment:NSTextAlignmentRight];
                [_UidFD setKeyboardType:UIKeyboardTypeNumberPad];
                [_UidFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_UidFD];
                _UidFD.sd_layout.xIs(cell.centerX*0.6).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 60.0f);
                [_UidFD setKeyboardType:UIKeyboardTypeNumberPad];
                [[self.UidFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      return YES;
                  }]
                 subscribeNext:^(id x) {
                     if([self.UidFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.UidFD.text = [self.UidFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     NSLog(@"%@", x);
                 }];
                _UidButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
                [_UidButton setImage:[UIImage imageNamed:@"QR_code_action"] forState:UIControlStateNormal];
                cell.accessoryView = _UidButton;
                [[self.UidButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
                    [self AddUid];
                }];
            }
        }
            break;
        default:
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}

- (void)AddUid
{
    NSString *cardName = @"2";
    UIImage *avatar = [UIImage imageNamed:@"avatar"];
    
    // 实例化扫描控制器
    HMScannerController *scanner = [HMScannerController scannerWithCardName:cardName avatar:avatar completion:^(NSString *stringValue) {
        NSLog(@"扫描后的 %@",stringValue);
        NSRange range = [stringValue rangeOfString:@"Z"];
        if (range.location != NSNotFound) {
            _UidFD.text = [stringValue substringFromIndex:range.location+range.length];
            [_RemoteInfoDic setObject:_UidFD.text  forKey:@"code"];
            [_MainTable reloadData];
        }
        
        //_UidFD.text = [NSString stringWithFormat:@"%@",stringValue];
        NSLog(@"_UidFD.text=%@",_UidFD.text);
        
    }];
    // 设置导航栏样式
    [scanner setTitleColor:[UIColor whiteColor] tintColor:[UIColor greenColor]];
    // 展现扫描控制器
    [self showDetailViewController:scanner sender:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void )synWirelessSound
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"60083",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)reportWirelessSound
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_RemoteInfoDic objectForKey:@"status"],@"status",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",_UidFD.text,@"code",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"90083",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}

@end
