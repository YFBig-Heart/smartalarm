//
//  DisPowerViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/24.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisPowerViewController : UIViewController
@property (retain,nonatomic)NSString *NumberString;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@property (retain,nonatomic)NSString *TitleString;
@end
