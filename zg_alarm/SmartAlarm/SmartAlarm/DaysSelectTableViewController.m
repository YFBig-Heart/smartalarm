//
//  DaysSelectTableViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "DaysSelectTableViewController.h"

@interface DaysSelectTableViewController ()
@property (retain,nonatomic) NSArray * TitleArray;
@end

@implementation DaysSelectTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(_TitleArray == nil)
    {
        _TitleArray = @[@"周日",@"周一",@"周二",@"周三",@"周四",@"周五",@"周六"];
    }
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone  target:self action:@selector(Done)];;
//    self.tableView.sd_layout.yIs(0);
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
//    self.tableView.contentInset = UIEdgeInsetsMake(-30 , 0 , 0 , 0);
}
- (void)Done
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",_weekString] forKey:@"weekingString"];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _TitleArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.textLabel.text = NSLocalizedString([_TitleArray objectAtIndex:indexPath.row], nil);
    if ([[_weekString substringWithRange:NSMakeRange(indexPath.row, 1)] intValue] == 1) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if([[_weekString substringWithRange:NSMakeRange(indexPath.row, 1)] intValue] == 0)
    {
       _weekString = [NSMutableString stringWithString:[_weekString stringByReplacingCharactersInRange:NSMakeRange(indexPath.row, 1) withString:@"1"]];
    }
    else
               _weekString = [NSMutableString stringWithString:[_weekString stringByReplacingCharactersInRange:NSMakeRange(indexPath.row, 1) withString:@"0"]];
    [self.tableView reloadData];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",_weekString] forKey:@"weekingString"];
}


@end
