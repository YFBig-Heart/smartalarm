//
//  AlarmDeviceViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/24.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "AlarmDeviceViewController.h"
#import "AlarmDeviceEditViewController.h"
#import "HMScanner.h"
#import "HMScannerController.h"
#import "BindDevceQRViewController.h"
#import <QuartzCore/CALayer.h>
#import "RootTabViewController.h"
@interface AlarmDeviceViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *DeviceTableView;
@property (retain,nonatomic) NSArray * ItemsTitle;
@property (weak, nonatomic) IBOutlet UIButton *unBindBtm;
@property (retain,nonatomic) NSArray * ItemsIcon;
@end

@implementation AlarmDeviceViewController

- (void)viewDidLoad {
    _ItemsTitle = @[@"报警器",@"绑定二维码",@"固件版本"];
    _ItemsIcon = @[@"manage_home",@"list_device",@"ic_helper"];
    _DeviceTableView.delegate = self;
    _DeviceTableView.dataSource = self;
    _unBindBtm.showsTouchWhenHighlighted = YES;
    [_unBindBtm setTitle:NSLocalizedString(@"解除绑定", nil) forState:UIControlStateNormal];
    [[_unBindBtm rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"您确定要删除吗？", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"取消",nil) otherButtonTitles:NSLocalizedString(@"确定",nil), nil];
        [[self rac_signalForSelector:@selector(alertView:clickedButtonAtIndex:) fromProtocol:@protocol(UIAlertViewDelegate)] subscribeNext:^(RACTuple *tuple) {
            if ([tuple.second  isEqual: @1]) {
                [self unBindDevice];
            }
        }];
        [alertView show];
    }];
    self.title = NSLocalizedString(@"设备信息", nil);
    self.automaticallyAdjustsScrollViewInsets = false;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    [_DeviceTableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)unBindDevice
{
    NSDictionary * tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"user_name",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00060",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
//            [self.navigationController popViewControllerAnimated:YES];
            [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];

}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _ItemsTitle.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 100.0f;
    }
    else
        return 65.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *const kCellID = @"_cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    if (indexPath.row == 0) {
        [cell.imageView.layer setCornerRadius:52.33/2];
        [cell.imageView.layer setMasksToBounds:YES];
        [cell.imageView.layer setBorderWidth:1.0];
        [cell.imageView.layer setBorderColor:[UIColor grayColor].CGColor];
        cell.detailTextLabel.text = NSLocalizedString(@"资料编辑", nil);
        NSDictionary * DeviceDic = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice];
        [cell.textLabel setText:DeviceDic?[DeviceDic objectForKey:@"name"]:NSLocalizedString(@"设备", ni)];
    }
    else
    [cell.textLabel setText:NSLocalizedString([_ItemsTitle objectAtIndex:indexPath.row],nil)];
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.imageView.image = [UIImage imageNamed:[_ItemsIcon objectAtIndex:indexPath.row]];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (indexPath.row == 2) {
        cell.detailTextLabel.text = [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"ver"];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self.navigationController pushViewController:[[AlarmDeviceEditViewController alloc] init] animated:YES];
            break;
        case 1:
        {
            [self BindDevice];
        }break;
        default:
            break;
    }
}
- (void)BindDevice
{
    BindDevceQRViewController * vc =[[BindDevceQRViewController alloc] init];
    vc.QRCodeImei = [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
