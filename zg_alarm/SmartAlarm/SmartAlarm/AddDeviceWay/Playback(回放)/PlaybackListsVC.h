//
//  PlaybackListsVC.h
//  SmartAlarm
//
//  Created by zhangping on 2018/11/13.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaybackListsVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
