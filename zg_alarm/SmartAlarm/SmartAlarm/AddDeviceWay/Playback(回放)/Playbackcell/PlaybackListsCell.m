//
//  PlaybackListsCell.m
//  SmartAlarm
//
//  Created by zhangping on 2018/11/20.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "PlaybackListsCell.h"

@implementation PlaybackListsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //[self initSubviews];
    }
    return self;
}

//设置cell信息
- (void)setCellWithInfo:(NSArray *)entity
{
    if (entity.count != 0) {
        
        self.videoImage.image = [UIImage imageNamed:[entity objectAtIndex:0]];
        self.dateLab.text = [entity objectAtIndex:1];
        self.timeLab.text = [entity objectAtIndex:2];
        self.setWayLab.text = [entity objectAtIndex:3];
    }
}






@end
