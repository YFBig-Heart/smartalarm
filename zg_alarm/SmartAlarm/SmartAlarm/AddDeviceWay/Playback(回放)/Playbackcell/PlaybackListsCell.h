//
//  PlaybackListsCell.h
//  SmartAlarm
//
//  Created by zhangping on 2018/11/20.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PlaybackListsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *videoImage;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *setWayLab;

/**
 功能：设置cell的详细信息
 参数：实体对象(包括：左边图象，日期，时间，设置方式图片)
 返回值：无
 */
- (void)setCellWithInfo:(NSArray*)entity;


@end
