//
//  PlaybackListsVC.m
//  SmartAlarm
//
//  Created by zhangping on 2018/11/13.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "PlaybackListsVC.h"

#import "DeviceModel.h"
#import <GWP2P/GWP2P.h>

#import "PlaybackListsCell.h"
#import "PlayBackVideoVC.h"
#import "PlaybackHSVC.h"

//时间设置
#import "THDatePickerView.h"

#define StartingtimeTag 6666
#define CurrenttimeTag 7777

@interface PlaybackListsVC ()<UITableViewDelegate,UITableViewDataSource,THDatePickerViewDelegate>
{
    
    int TimeTag;
    
}

@property (nonatomic, retain) NSMutableArray    *arrayData;

@property (nonatomic, retain) NSMutableArray    *arrayDatatest;

//当前设备的信息模型
@property(nonatomic,strong)DeviceModel *deviceModel;
@property(nonatomic,strong)NSMutableArray *listsArr;

//处理传值
@property(nonatomic,strong)NSMutableArray *listsArr2;

//
@property (nonatomic,strong) NSString *StartTimeStr;
@property (nonatomic, strong) NSString *CurrentTimeStr;

@property (weak, nonatomic) IBOutlet UILabel *StartTime;

@property (weak, nonatomic) IBOutlet UILabel *CurrentTime;
@property (weak, nonatomic) IBOutlet UIButton *InquireBtn;//查询

@property (nonatomic, retain) NSMutableArray *ADDary;//

@property (weak, nonatomic) THDatePickerView *dateView;


@end

@implementation PlaybackListsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = NSLocalizedString(@"回放列表",nil);
    self.listsArr = [NSMutableArray array];
    self.arrayData = [NSMutableArray array];
    self.arrayDatatest = [NSMutableArray array];
    
    NSLog(@"deviceID=%@,devicePassword=%@",self.deviceModel.deviceID,self.deviceModel.devicePassword);
    
    [self initialize];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//
//    });
  
   NSLog(@"p2p连接状态:%ld", (long)[GWP2PClient sharedClient].linkStatus);
    
   [_InquireBtn setTitle:NSLocalizedString(@"查询", nil) forState:(UIControlStateNormal)];
   
   [self getLists];
    
}

#pragma mark -获取设备回放列表
-(void)getLists
{
    __weak typeof (self)weakSelf = self;
    GWP2PClient *click = [GWP2PClient sharedClient];
    [click getDevicePlaybackFilesWithDeviceID:self.deviceModel.deviceID devicePassword:self.deviceModel.devicePassword startDate:nil endDate:nil completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary) {
        
        NSLog(@"获取回放文件结果:%@", dataDictionary);
        NSString *result = dataDictionary[@"result"];
        if ([result floatValue] == 1)
        {
            NSArray *file = dataDictionary[@"files"];
            [weakSelf.listsArr addObjectsFromArray:file];
        
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [weakSelf.tableView reloadData];
            });
            
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[MBProgressManager showBriefAlert:@"获取列表失败"];
                //                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                //
                //                    [weakSelf.navigationController popViewControllerAnimated:YES];
                //                });
            });
        }
    }];
}


- (NSString*)hexStringForData:(NSData*)data
{
    if (data == nil) {
        return nil;
    }
    
    NSMutableString* hexString = [NSMutableString string];
    
    const unsigned char *p = [data bytes];
    
    for (int i=0; i < [data length]; i++) {
        [hexString appendFormat:@"%02x", *p++];
    }
    return hexString;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark --初始化
- (void)initialize
{
    
    [self timeset:@""];
    [self CurrentTimeset:@""];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [UIView new];
    
}

//开始时间lab
- (void)timeset:(NSString*)str
{
    if (str.length == 0) {
        
        self.StartTime.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"开始时间:", nil),@"1970-01-01 8:00"];//开始时间
        
    }else
    {
        self.StartTime.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"开始时间:", nil),str];//开始时间
    }
}

//当前时间lab
- (void)CurrentTimeset:(NSString*)str
{
    if (str.length == 0) {
        
       self.CurrentTime.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"结束时间:", nil),[self getCurrentTimes]];//当前时间
        
    }else
    {
        self.CurrentTime.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"结束时间:", nil),str];//当前时间
    }
}


//获取当前时间
-(NSString*)getCurrentTimes
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];//设置想要的格式
    NSDate *datenow = [NSDate date];
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    NSLog(@"currentTimeString =  %@",currentTimeString);
    return currentTimeString;
}

#pragma mark --查询
- (IBAction)InquireBtn:(UIButton *)sender
{
  
    [self InquireBtnData];//查询
}


#pragma mark --查询btn
- (void)InquireBtnData
{
    [self getLists];//获取最新回放data
    
    if (self.listsArr.count != 0)
    {
        for (int n = 0;  n < self.listsArr.count; n++) {
            
            GWPlaybackFileModel *model = [self.listsArr objectAtIndex:n];
            NSString *YMD = [NSString stringWithFormat:@"%d-%d-%d",(int)model.year,(int)model.month,(int)model.day];
            NSString *HMS = [NSString stringWithFormat:@"%d-%d-%d",(int)model.hour,(int)model.minute,(int)model.second];
            NSArray *ary = [NSArray arrayWithObjects:@"icon_record_m",YMD,HMS,model.cType,nil];
            [self.arrayDatatest addObject:ary];
        }
    }
    
    self.arrayData = [NSMutableArray arrayWithArray:self.arrayDatatest];
    [self.tableView reloadData];//重新刷新数据源
}


#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    //return 65.0f;
    return 0.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0f;
}

#pragma mark - UITableViewDataSource
//每一组有多少行
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

  if (self.arrayData && [self.arrayData count])
    {
        return self.arrayData.count;
    }
    return 0;
}

//每一行
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlaybackListsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlaybackLists"];
    if (!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"PlaybackListsCell" owner:nil options:nil]firstObject];
    }
    
    if (self.arrayData && indexPath.row < [self.arrayData count])
    {
        [cell setCellWithInfo:[self.arrayData objectAtIndex:indexPath.row]];//设自定义cell详情
    }
    return cell;
}

#pragma mark - UITableViewDelegate
//选择某一行
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < [self.arrayData count])
    {
        NSArray *ary = [self.arrayData objectAtIndex:indexPath.row];
        NSLog(@"某一行=%@", ary);

#if 0
        PlayBackVideoVC *video = [[PlayBackVideoVC alloc] init];
        video.fillModelArr = self.listsArr;
        video.deviceModel = self.deviceModel;
        video.currentIndex = indexPath.row;
        [self.navigationController pushViewController:video animated:YES];
#endif
        //全屏
        PlaybackHSVC *video = [[PlaybackHSVC alloc] init];
        video.fillModelArr = self.listsArr;
        video.deviceModel = self.deviceModel;
        video.currentIndex = indexPath.row;
        [self.navigationController pushViewController:video animated:YES];
        
    }
}

#pragma mark --开始/当前时间
- (IBAction)StartTimeBtn:(UIButton *)sender {
    
    [self THDatePickerView:StartingtimeTag];
}

- (IBAction)CurrentTimeBtn:(UIButton *)sender {
    
    [self THDatePickerView:CurrenttimeTag];
}

#pragma mark --时间选择器-封装
- (void)THDatePickerView:(int) tag
{
        THDatePickerView *dateView = [[THDatePickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300)];
        dateView.delegate = self;
        dateView.tag = tag;
        dateView.title = NSLocalizedString(@"请选择时间", nil);
        //    dateView.isSlide = NO;
        //    dateView.date = @"2017-03-23 12:43";
        //    dateView.minuteInterval = 1;
        [self.view addSubview:dateView];
        self.dateView = dateView;
    
        [UIView animateWithDuration:0.3 animations:^{
            self.dateView.frame = CGRectMake(0, self.view.frame.size.height - 300, self.view.frame.size.width, 300);
            [self.dateView show];
        }];
        
        TimeTag = tag;
}


#pragma mark - THDatePickerViewDelegate
/**
 保存按钮代理方法
 
 @param timer 选择的数据
 */
- (void)datePickerViewSaveBtnClickDelegate:(NSString *)timer
{
    NSLog(@"保存点击");

    if (TimeTag == 6666)
    {
        [self timeset:timer];
        
        [UIView animateWithDuration:0.3 animations:^{
            self.dateView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300);
        }];
        
    }
    if (TimeTag == 7777)
    {
        [self CurrentTimeset:timer];
        
        [UIView animateWithDuration:0.3 animations:^{
            self.dateView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300);
        }];
    }
}

/**
 取消按钮代理方法
 */
- (void)datePickerViewCancelBtnClickDelegate {
    NSLog(@"取消点击");
    
    [UIView animateWithDuration:0.3 animations:^{
        self.dateView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300);
    }];
}


@end
