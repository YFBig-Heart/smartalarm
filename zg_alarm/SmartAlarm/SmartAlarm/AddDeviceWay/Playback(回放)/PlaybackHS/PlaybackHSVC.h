//
//  PlaybackHSVC.h
//  SmartAlarm
//
//  Created by zhangping on 2018/11/29.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GWP2P/GWP2P.h>
#import "DeviceModel.h"

@interface PlaybackHSVC : UIViewController

///设备录像列表
@property(nonatomic,strong)NSMutableArray *fillModelArr;

///当前设备的信息模型
@property(nonatomic,strong)DeviceModel *deviceModel;

///当前模型位置
@property(nonatomic,assign)NSInteger currentIndex;

@end
