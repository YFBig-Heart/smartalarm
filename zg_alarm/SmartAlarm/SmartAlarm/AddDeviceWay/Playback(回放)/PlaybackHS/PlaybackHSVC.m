//
//  PlaybackHSVC.m
//  SmartAlarm
//
//  Created by zhangping on 2018/11/29.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "PlaybackHSVC.h"

#import "UIDevice+TFDevice.h"
#import "AppDelegate.h"
#import <GWP2P/GWP2P.h>



@interface PlaybackHSVC ()<GWP2PVideoPlayerProtocol>

@property (weak, nonatomic) IBOutlet UIView *playView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;



//播放器
@property (nonatomic, strong) GWP2PPlaybackPlayer *playbackPlayer;
@property(nonatomic,strong)UIButton *button;

@end

@implementation PlaybackHSVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.allowRotation = YES;//允许转成横屏
    [UIDevice switchNewOrientation:UIInterfaceOrientationLandscapeRight];//调用转屏代码
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];//状态栏
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    //hf
    self.playbackPlayer = [[GWP2PPlaybackPlayer alloc]init];
    [self.playView addSubview:self.playbackPlayer.view];
    [self addChildViewController:self.playbackPlayer.panoViewController];
    
    [self.playbackPlayer.view bringSubviewToFront:self.activityView];//act

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleGesture:)];
    [self.playbackPlayer.view addGestureRecognizer:singleTap];
    
}


#pragma mark --手势触摸
- (void)singleGesture:(UITapGestureRecognizer*)gesture
{
    NSLog(@"一个手指单击了");
    _button = [[UIButton alloc]initWithFrame:CGRectMake(self.view.bounds.size.width-40, self.view.bounds.size.height-25, 40, 25)];//340
    //_button.backgroundColor = [UIColor clearColor];
    [_button setBackgroundImage:[UIImage imageNamed:@"half_full_screen_p"] forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(dismissController) forControlEvents:UIControlEventTouchUpInside];
    //[_button setTitle:@"全屏" forState:UIControlStateNormal];
    [self.playbackPlayer.view addSubview:_button];
}

#pragma mark --btn回调
- (void)dismissController
{
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.allowRotation = NO;//关闭横屏仅允许竖屏
    //切换到竖屏
    [UIDevice switchNewOrientation:UIInterfaceOrientationPortrait];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
}

//MARK:状态栏的显示（横屏系统默认会隐藏的）
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark 设置播放器的frame
-(void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    self.playbackPlayer.view.frame = self.playView.bounds;
    
}

#pragma mark 自动播放
-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self PalyVideo];
    
}
#pragma mark --视图消失停止播放
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.playbackPlayer p2pStop];
    
    self.navigationController.navigationBar.hidden = NO;//导航栏
}

#pragma mark --根视图即将显示
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"视图将要显示");
    self.navigationController.navigationBar.hidden = YES;//导航栏
    //statusBar.backgroundColor = [UIColor whiteColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)PalyVideo
{
    NSLog(@"deviceID=%@,devicePassword=%@,deviceType=%d,deviceSubtype=%d,currentIndex=%@",self.deviceModel.deviceID,self.deviceModel.devicePassword,(int)self.deviceModel.deviceType,(int)self.deviceModel.deviceSubtype,self.fillModelArr[self.currentIndex]);
    
    //    self.deviceModel.deviceID = @"9397251";
    //    self.deviceModel.devicePassword = @"123";
    //    self.deviceModel.deviceType = 7;
    //    self.deviceModel.deviceSubtype = 14;
    //NSData *modelfile = GWPlaybackFileModel 0x107bea580 2018-11-22 14:30:0 1800;
    __weak typeof(self) weakSelf = self;
    [self.activityView startAnimating];
    
    [self.playbackPlayer p2pCallPlaybackFileWithDeviceId:self.deviceModel.deviceID password:self.deviceModel.devicePassword deviceType:self.deviceModel.deviceType deviceSubtype:self.deviceModel.deviceSubtype playbackFile:self.fillModelArr[self.currentIndex] calling:^(NSDictionary *parameters)
     {
         NSLog(@"P2PCallingBlock--%@",parameters);
         
     } accept:^(NSDictionary *parameters) {
         
         NSLog(@"P2PAcceptBlock---%@",parameters);
         
     } reject:^(GWP2PCallError error, NSString *errorCode) {
         
         NSLog(@"P2PRejectBlock---%@",errorCode);
         dispatch_async(dispatch_get_main_queue(), ^{
             
             [weakSelf.activityView stopAnimating];
             
         });
         
     } ready:^{
         
         NSLog(@"p2pCallPlaybackFile ready");
         dispatch_async(dispatch_get_main_queue(), ^{
             
             [weakSelf.activityView stopAnimating];
             
         });
         
     }];
    
}

#pragma mark 销毁控制器
- (void)dealloc {
    
    //[self.playbackPlayer removeObserver:self forKeyPath:@"currentTime"];
    //[self.playbackPlayer removeObserver:self forKeyPath:@"playbackState"];
    
    [self.playbackPlayer p2pStop];
    
}








@end
