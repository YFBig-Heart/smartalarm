//
//  PlayBackVideoVC.m
//  SmartAlarm
//
//  Created by zhangping on 2018/11/22.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "PlayBackVideoVC.h"



@interface PlayBackVideoVC ()

@property (weak, nonatomic) IBOutlet UIView *playView;

//播放器
@property (nonatomic, strong) GWP2PPlaybackPlayer *playbackPlayer;

@end

@implementation PlayBackVideoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"视频回放";
    
    self.playbackPlayer = [[GWP2PPlaybackPlayer alloc]init];
    [self.playView addSubview:self.playbackPlayer.view];
    [self addChildViewController:self.playbackPlayer.panoViewController];
    
}


#pragma mark 设置播放器的frame
-(void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    self.playbackPlayer.view.frame = self.playView.bounds;
    
}

#pragma mark 自动播放
-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self PalyVideo];
    
}
#pragma mark --视图消失停止播放
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.playbackPlayer p2pStop];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)PalyVideo
{
    NSLog(@"deviceID=%@,devicePassword=%@,deviceType=%d,deviceSubtype=%d,currentIndex=%@",self.deviceModel.deviceID,self.deviceModel.devicePassword,(int)self.deviceModel.deviceType,(int)self.deviceModel.deviceSubtype,self.fillModelArr[self.currentIndex]);
    
//    self.deviceModel.deviceID = @"9397251";
//    self.deviceModel.devicePassword = @"123";
//    self.deviceModel.deviceType = 7;
//    self.deviceModel.deviceSubtype = 14;
    //NSData *modelfile = GWPlaybackFileModel 0x107bea580 2018-11-22 14:30:0 1800;
    
    [self.playbackPlayer p2pCallPlaybackFileWithDeviceId:self.deviceModel.deviceID password:self.deviceModel.devicePassword deviceType:self.deviceModel.deviceType deviceSubtype:self.deviceModel.deviceSubtype playbackFile:self.fillModelArr[self.currentIndex] calling:^(NSDictionary *parameters)
    {
        NSLog(@"P2PCallingBlock--%@",parameters);
        
    } accept:^(NSDictionary *parameters) {
        
        NSLog(@"P2PAcceptBlock---%@",parameters);
        
    } reject:^(GWP2PCallError error, NSString *errorCode) {
        
        NSLog(@"P2PRejectBlock---%@",errorCode);
    
    } ready:^{
        
        NSLog(@"p2pCallPlaybackFile ready");
    }];

}

#pragma mark 销毁控制器
- (void)dealloc {
    
    //[self.playbackPlayer removeObserver:self forKeyPath:@"currentTime"];
    //[self.playbackPlayer removeObserver:self forKeyPath:@"playbackState"];
    
    [self.playbackPlayer p2pStop];
    
}

@end
