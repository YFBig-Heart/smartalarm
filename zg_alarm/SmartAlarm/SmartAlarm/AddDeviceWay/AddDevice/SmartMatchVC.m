//
//  SmartMatchVC.m
//  SmartAlarm
//
//  Created by zhangping on 2018/9/27.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "SmartMatchVC.h"

#import <SystemConfiguration/CaptiveNetwork.h>

//智能匹配模式
#import "IntelligentMatchingVC.h"


@interface SmartMatchVC ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (weak, nonatomic) IBOutlet UIButton *CompleteBtn;
//
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UITextField * PasswordFD;


@property (nonatomic, strong) NSCondition *_condition;

@property (strong, nonatomic) NSString *bssid;
@property (retain,nonatomic) NSString * ssid;

@end

@implementation SmartMatchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = NSLocalizedString(@"智能匹配",nil);
    self.view.backgroundColor = [UIColor whiteColor];
    
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    _TitleArray = @[@"Wifi SSID:" ,@"Wifi 密码:"];
    
    self._condition = [[NSCondition alloc]init];
    [_CompleteBtn bs_configureAsPrimaryStyle];
    [[_CompleteBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        
        //执行button方法
        NSLog(@"下一步");
        [self NextButton];
        
    }];
    [_CompleteBtn setTitle:NSLocalizedString(@"下一步", nil) forState:(UIControlStateNormal)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _TitleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    
    switch (indexPath.row) {
        case 0:
        {
            NSDictionary *netInfo = [self fetchNetInfo];
            cell.detailTextLabel.text = [netInfo objectForKey:@"SSID"];
            _bssid = [NSString stringWithFormat:@"%@",[netInfo objectForKey:@"BSSID"]];
            _ssid = [NSString stringWithFormat:@"%@",[netInfo objectForKey:@"SSID"]];
            [cell.imageView setImage:[UIImage imageNamed:@"list_wifi"]];
        }
            break;
        case 1:
        {
            if (_PasswordFD)
            {
                [_PasswordFD removeFromSuperview];
                _PasswordFD = nil;
            }
            [cell.imageView setImage:[UIImage imageNamed:@"list_password"]];
            _PasswordFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_PasswordFD setTextColor:[UIColor flatGrayColor]];
            [_PasswordFD setTextAlignment:NSTextAlignmentRight];
            [_PasswordFD setBackgroundColor:[UIColor clearColor]];
            [_PasswordFD setPlaceholder:NSLocalizedString(@"密码",nil)];
            [cell addSubview:_PasswordFD];
            
            _PasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[_PasswordFD.rac_textSignal
              filter:^BOOL(id value) {
                  
                  NSString *text = value;
                  return YES;
                  
              }]
             subscribeNext:^(id x) {
                 
                 if([self.PasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.PasswordFD.text = [self.PasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 NSLog(@"%@", x);
                 
             }];
        }
            break;
            
        default:
            break;
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark --获取wifiname
- (NSString *)fetchSsid
{
    NSDictionary *ssidInfo = [self fetchNetInfo];
    NSLog(@"SSID=%@",[ssidInfo objectForKey:@"SSID"]);
    
    return [ssidInfo objectForKey:@"SSID"];
}

#pragma mark --获取wifi
- (NSString *)fetchBssid
{
    NSDictionary *bssidInfo = [self fetchNetInfo];
    return [bssidInfo objectForKey:@"BSSID"];
}

- (NSDictionary *)fetchNetInfo
{
    NSArray *interfaceNames = CFBridgingRelease(CNCopySupportedInterfaces());
    
    NSDictionary *SSIDInfo;
    for (NSString *interfaceName in interfaceNames) {
        SSIDInfo = CFBridgingRelease(
                                     CNCopyCurrentNetworkInfo((__bridge CFStringRef)interfaceName));
        
        BOOL isNotEmpty = (SSIDInfo.count > 0);
        if (isNotEmpty) {
            break;
        }
    }
    return SSIDInfo;
}

#pragma mark --下一步
- (void)NextButton
{
    if (self.PasswordFD.text.length != 0) {
        
        //push
        IntelligentMatchingVC *configVC = [[IntelligentMatchingVC alloc] init];
        configVC.wifiName = [self fetchSsid];
        configVC.wifiPass = self.PasswordFD.text;
        [self.navigationController pushViewController:configVC animated:YES];
        
        NSLog(@"wifiName=%@,wifiPass=%@", configVC.wifiName, configVC.wifiPass);
        
    }
    
}


@end
