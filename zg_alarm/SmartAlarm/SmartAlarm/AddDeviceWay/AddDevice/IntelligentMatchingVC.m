//
//  IntelligentMatchingVC.m
//  SmartAlarm
//
//  Created by zhangping on 2018/10/7.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "IntelligentMatchingVC.h"


#import "DeviceModel.h"
#import "GWNet.h"
#import "LoginResult.h"

//#import "AddDeviceWayVC.h"
#import "AddNetWorkedVC.h"

@interface IntelligentMatchingVC ()

@property (assign, nonatomic) BOOL isDeviceLinkIn;
@property (weak, nonatomic) IBOutlet UIButton *StartConfigbtn;
@property (weak, nonatomic) IBOutlet UILabel *configLab;

@end

@implementation IntelligentMatchingVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = NSLocalizedString(@"智能匹配",nil);
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSLog(@"wifiName=%@,self.wifiPass=%@",self.wifiName,self.wifiPass);

    self.OneLabel.text = NSLocalizedString(@"1.长按设备RESET键3秒，听到B的一声",nil);
    self.TwoLabel.text = NSLocalizedString(@"2.等待约30s，听到B音提示，（约5秒响一次）",nil);
    self.ThreeLabel.text = NSLocalizedString(@"3.将手机音量调整到最大，并靠近设备30CM内，点击开始配置", nil);
    
    self.configLab.text = NSLocalizedString(@"开始配置",nil);
}

#pragma mark --开始配置
- (IBAction)ConfigBtn:(UIButton *)sender
{
    self.StartConfigbtn.selected = !self.StartConfigbtn.selected;
    NSLog(@"开始配置 wifiName=%@,self.wifiPass=%@",self.wifiName,self.wifiPass);
    
    if ( self.StartConfigbtn.selected == YES) {
        
          self.configLab.text = NSLocalizedString(@"开始配置",nil);
    }
    else
    {
          self.configLab.text = NSLocalizedString(@"停止配置",nil);
    }
    
#if 1
    
    [self.view endEditing:YES];
    __weak typeof(self) weakSelf = self;
    //需要注意deviceLinkIn会被回调很多次
    //self.isDeviceLinkIn = NO;
    
    if (self.StartConfigbtn.isSelected)
    {
        weakSelf.isDeviceLinkIn = NO;
        
        [[GWP2PDeviceLinker shareInstance] p2pSmartLinkDeviceWithWiFiSSID:self.wifiName password:self.wifiPass deviceLinkIn:^(NSDictionary *deviceDict)
         {
             NSLog(@"声波配网成功,返回数据:%@",deviceDict);
             if (weakSelf.isDeviceLinkIn)
             {
                 return;
             }
             
             //NSLog(@"设备 deviceID=%@",[deviceDict objectForKey:@"deviceID"]);
             NSString *deviceIDstr = [[deviceDict objectForKey:@"deviceID"] stringValue];
             //if (deviceIDstr != NULL)
             if (![deviceIDstr isKindOfClass:[NSNull class]])
             {
                 NSLog(@"push deviceIDstr=%@",deviceIDstr);
                 //push
                 AddNetWorkedVC *addnetworkVC = [[AddNetWorkedVC alloc] init];
                 addnetworkVC.deviceID = deviceIDstr;
                 [self.navigationController pushViewController:addnetworkVC animated:YES];
             }
             //weakSelf.isDeviceLinkIn = YES;
             
             //如果智能联机,设备配网成功,就断开发送智能联机
             //[[GWP2PDeviceLinker shareInstance] p2pStopSmartLink];
             
             NSString *deviceID = [deviceDict[@"deviceID"] stringValue];
#warning 配网成功后，调用P2P接口时传入deviceIP(设备在局域网中的IP地址最后一段)会走局域网与设备通信，基本上都能请求成功，如果直接穿入deviceID容易失败
             
             NSString *deviceIP = deviceDict[@"deviceIP"];
             if (![deviceDict[@"isInitPassword"] boolValue])
             { //设备没有初始化密码,设置密码
                 
                 [[GWP2PClient sharedClient] setDeviceInitialPassword:self.wifiName withDeviceID:deviceIP completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary)
                  {
                      NSLog(@"设备没有初始化密码 success:%i %@",success,dataDictionary);
                      if (success)
                      {
                          DeviceModel *deviceModel = [[DeviceModel alloc] init];
                          deviceModel.deviceID = deviceID;
                          deviceModel.devicePassword = self.wifiPass;
                          deviceModel.deviceType = [dataDictionary[@"device type"] intValue];
                          deviceModel.deviceSubtype = [dataDictionary[@"device subtype"] intValue];
                          [DeviceModel saveDeviceModel:deviceModel];
                      }
                      
                  }];
                 
             } else
             { //设备已经初始化密码,可以让用户输入正确的密码验证通过后加到本地列表,这里直接使用123
                 
                 [[GWP2PClient sharedClient] getMultipleDeviceStatusWithDeviceID:deviceIP password:@"123" completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary) {
                     NSLog(@"设备已经初始化密码 success:%i %@",success,dataDictionary);
                     if (success)
                     {
                         DeviceModel *deviceModel = [[DeviceModel alloc] init];
                         deviceModel.deviceID = deviceID;
                         deviceModel.devicePassword = @"123";
                         deviceModel.deviceType = [dataDictionary[@"device type"] intValue];
                         deviceModel.deviceSubtype = [dataDictionary[@"device subtype"] intValue];
                         [DeviceModel saveDeviceModel:deviceModel];
                         
                     }
                 }];
             }
         }];
#endif
        
    }else
    {

        [[GWP2PDeviceLinker shareInstance] p2pStopSmartLink];
    }
    
}

#pragma mark --消毁
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[GWP2PDeviceLinker shareInstance] p2pStopSmartLink];
}



@end
