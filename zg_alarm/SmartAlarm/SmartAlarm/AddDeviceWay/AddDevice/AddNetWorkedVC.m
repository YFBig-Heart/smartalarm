//
//  AddNetWorkedVC.m
//  SmartAlarm
//
//  Created by zhangping on 2018/9/27.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "AddNetWorkedVC.h"

//
#import "STPickerView.h"
#import "STPickerSingle.h"

#import "DeviceModel.h"
#import "CameraViewController.h"
#import "AddDeviceWayVC.h"

@interface AddNetWorkedVC ()<UITableViewDelegate, UITableViewDataSource,STPickerSingleDelegate>

@property (retain, nonatomic)NSMutableArray * arrayData;
@property (retain, nonatomic)NSMutableArray * arrayData_bak;

@property (retain,nonatomic)  UITextField *IDTF;//id
@property (retain, nonatomic) UITextField *NameTF;//名称
@property (retain, nonatomic) UITextField *PasswordTF;//密码

//组1
@property (nonatomic, retain) NSString *NetworkedDeviced;//已联网的设备

@end

@implementation AddNetWorkedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = NSLocalizedString(@"添加已联网设备",nil);
    
    
    if (self.deviceID.length != 0) {
        
         self.arrayData = [NSMutableArray arrayWithObjects:self.deviceID,nil];
    }
    else
    {
         self.arrayData = [NSMutableArray arrayWithObjects:@"",nil];
    }
   
    [self Initlay];
   
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(StorageBtn)];
    
    NSLog(@"push deviceID=%@",self.deviceID);

}

#pragma mark --初始化
- (void)Initlay
{
    //tableview
    _ContanctEdit.delegate = self;
    _ContanctEdit.dataSource = self;
}

#pragma mark --存储
- (void)StorageBtn
{
    
//    self.IDTF.text = @"9397251";
//    self.NameTF.text = @"xxxxxx";
//    self.PasswordTF.text = @"123";

    NSString *IDTFstr = self.IDTF.text;
    if ([self.IDTF.text isEqualToString:@""])
    {
        [self.arrayData addObject:IDTFstr];
        [self.ContanctEdit reloadData];
        
    }else
    {
        [self.arrayData addObject:self.IDTF.text];
        [self.ContanctEdit reloadData];
    }
    
    if (self.IDTF.text.length != 0 || self.PasswordTF.text.length != 0)
    {
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:self.deviceID,@"deviceId",self.NameTF.text,@"deviceName",self.PasswordTF.text,@"devicePass",nil];
        
        //NSLog(@"dic=%@",dic);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"IDPassword" object:dic];//发
        
        [self.navigationController popToRootViewControllerAnimated:YES];//push根视图
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        
        return @"";
    }
    if (section == 1) {
        
        return  NSLocalizedString(@"添加已联网的设备",nil);
    }
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    //return 65.0f;
    return 0.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        
        return 3;
    }
    if (section == 1) {
        
        if (self.arrayData && [self.arrayData count])
        {
            return [self.arrayData count];
        }
    }
    
    return 0;
}

//设置行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0f;
}

//设置cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Contantct_cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
   
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            [cell.textLabel setText:NSLocalizedString(@"ID", nil)];
            _IDTF = [[UITextField alloc] initWithFrame:CGRectMake(100, 0, self.view.bounds.size.width, 45)];
            [_IDTF setTextColor:[UIColor flatGrayColor]];
            [_IDTF setTextAlignment:NSTextAlignmentLeft];
            _IDTF.text = self.NetworkedDeviced;
            [_IDTF setBackgroundColor:[UIColor clearColor]];
            [_IDTF setPlaceholder:NSLocalizedString(@"请输入设备ID",nil)];
            [cell addSubview:_IDTF];
            //            _PasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, self.view.bounds.size.width-50);
            [[_IDTF.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return YES;
              }]
             subscribeNext:^(id x) {
                 if([self.IDTF.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.IDTF.text = [self.IDTF.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 NSLog(@"ID:%@", x);
             }];
        }
        if (indexPath.row == 1)
        {
            [cell.textLabel setText:NSLocalizedString(@"名称", nil)];
            _NameTF = [[UITextField alloc] initWithFrame:CGRectMake(100, 0, self.view.bounds.size.width, 45)];
            [_NameTF setTextColor:[UIColor flatGrayColor]];
            [_NameTF setTextAlignment:NSTextAlignmentLeft];
            [_NameTF setBackgroundColor:[UIColor clearColor]];
            [_NameTF setPlaceholder:NSLocalizedString(@"请输入设备名称",nil)];
            
            [cell addSubview:_NameTF];
            //            _PasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, self.view.bounds.size.width-50);
            [[_NameTF.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return YES;
              }]
             subscribeNext:^(id x) {
                 if([self.NameTF.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     NSLog(@"self.PasswordFD =%@",self.NameTF.text);
                     self.NameTF.text = [self.NameTF.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 NSLog(@"名称:%@", x);
             }];
            
        }
        if (indexPath.row == 2)
        {
            [cell.textLabel setText:NSLocalizedString(@"密码", nil)];
            _PasswordTF = [[UITextField alloc] initWithFrame:CGRectMake(100, 0, self.view.bounds.size.width, 45)];
            [_PasswordTF setTextColor:[UIColor flatGrayColor]];
            [_PasswordTF setTextAlignment:NSTextAlignmentLeft];
            [_PasswordTF setBackgroundColor:[UIColor clearColor]];
            [_PasswordTF setPlaceholder:NSLocalizedString(@"请输入设备密码",nil)];
            [cell addSubview:_PasswordTF];
            //_PasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, self.view.bounds.size.width-50);
            [[_PasswordTF.rac_textSignal
              filter:^BOOL(id value)
              {
                  NSString *text = value;
                  return YES;
              }]
             subscribeNext:^(id x) {
                 if([self.PasswordTF.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.PasswordTF.text = [self.PasswordTF.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 NSLog(@"密码:%@", x);
             }];
        }
    }
    
    //组1
    if (indexPath.section == 1) {
        
        if (self.arrayData && indexPath.row < [self.arrayData count])
        {
            if (self.deviceID.length != 0) {
                
                cell.imageView.image = [UIImage imageNamed:@"Camera_normal"];
                //根据当前索引行获取数组中的字符串
                cell.textLabel.text = [self.arrayData objectAtIndex:indexPath.row];
            }
            
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
//选择
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < [self.arrayData count])
    {
        NSString *str = [self.arrayData objectAtIndex:indexPath.row];
        
        //已联网的设备
        if (indexPath.section == 1) {
            
            NSString *str = [self.arrayData objectAtIndex:indexPath.row];
            
            //点击同步输入框
            self.NetworkedDeviced = str;
            [_ContanctEdit reloadData];
        }
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"IDPassword" object:nil];
}


@end
