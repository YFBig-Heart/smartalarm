//
//  IntelligentMatchingVC.h
//  SmartAlarm
//
//  Created by zhangping on 2018/10/7.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface IntelligentMatchingVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *OneLabel;
@property (weak, nonatomic) IBOutlet UILabel *TwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *ThreeLabel;

//Wi-Fi-info
@property (nonatomic,strong) NSString *wifiName;//wifi名字
@property (nonatomic, strong) NSString *wifiPass;//wifi密码




@end
