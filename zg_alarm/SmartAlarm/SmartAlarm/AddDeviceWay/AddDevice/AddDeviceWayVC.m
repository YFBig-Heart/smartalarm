//
//  AddDeviceWayVC.m
//  SmartAlarm
//
//  Created by zhangping on 2018/9/26.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "AddDeviceWayVC.h"

//
#import "SmartMatchVC.h"
#import "AddNetWorkedVC.h"

@interface AddDeviceWayVC ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *MainTable;

@property (retain,nonatomic) NSArray * ImageArray;
@property (retain,nonatomic) NSArray * TitleArray;


@end

@implementation AddDeviceWayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = NSLocalizedString(@"添加设备",nil);
    self.view.backgroundColor = [UIColor whiteColor];
    
    //tableview
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    
    if (_TitleArray == nil) {
        
        _TitleArray = @[@"智能匹配",@"添加已联网设备"];
        _ImageArray = @[@"intelligent_online_add_p",@"manually_to_add"];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    return _TitleArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    NSInteger ImageArrayIndex;
    ImageArrayIndex = indexPath.row;
    
    cell.textLabel.text = NSLocalizedString([_TitleArray objectAtIndex:ImageArrayIndex], nil);
    cell.imageView.image = [UIImage imageNamed:[_ImageArray objectAtIndex:ImageArrayIndex]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row)
    {
        case 0:
            
            [self.navigationController pushViewController:[[SmartMatchVC alloc] init]animated:YES];
            
            break;
        case 1:
            
            [self.navigationController pushViewController:[[AddNetWorkedVC alloc] init]animated:YES];
            
            break;
        default:
            break;
    }
}


@end
