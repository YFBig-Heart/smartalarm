//
//  AddNetWorkedVC.h
//  SmartAlarm
//
//  Created by zhangping on 2018/9/27.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNetWorkedVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *ContanctEdit;

@property (retain,nonatomic) UITextField * ContanctNameFD;
@property (retain,nonatomic) UITextField * ContanctPhoneFD;
@property (retain,nonatomic) UILabel * ContanctTypeLB;/* 0:Call/SMS 1:Only Call 2:Only SMS 3:CID*/
@property (retain,nonatomic) NSMutableDictionary * ContanctEditDic;

//push
@property (nonatomic, retain) NSString *deviceID;//设备ID

@end
