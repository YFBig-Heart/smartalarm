//
//  SetingPublicVC.h
//  SmartAlarm
//
//  Created by zhangping on 2018/11/13.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceModel.h"

@interface SetingPublicVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (nonatomic, nonatomic) NSString *TileStr;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/** 设备 */
@property (nonatomic, strong) DeviceModel *deviceModel;

@property (nonatomic, strong) NSDictionary *getdataDic;
@property (nonatomic, strong) NSString *imagereversestate;
@property (nonatomic, strong) NSString *manuallyrecordstate;


@end
