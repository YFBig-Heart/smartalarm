//
//  SettingVC.h
//  SmartAlarm
//
//  Created by zhangping on 2018/11/13.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceModel.h"

@interface SettingVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *UITableView;

///当前设备的信息模型
@property(nonatomic,strong)DeviceModel *deviceModel;

@end
