//
//  SetingPublicVC.m
//  SmartAlarm
//
//  Created by zhangping on 2018/11/13.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "SetingPublicVC.h"

#import "DeviceModel.h"
#import "LoginResult.h"

//时间设置
#import "THDatePickerView.h"

@interface SetingPublicVC ()<GWP2PClientProtocol,UIAlertViewDelegate,THDatePickerViewDelegate>

@property (nonatomic, copy) NSMutableArray *arrayData;



//时间
@property (nonatomic, retain) NSString *DateTimestr;

@property (weak, nonatomic) THDatePickerView *dateView;


//安全
@property (retain,nonatomic) UITextField *oldPasswordFD;
@property (retain,nonatomic) UITextField *nPasswordFD;
@property (retain,nonatomic) UITextField *atPasswordFD;
@property (nonatomic, assign) NSString *textFieldstr;

//画面
@property (nonatomic, retain) NSString *volume;
@property (retain,nonatomic) UISwitch *WirelessSound;//镜像反转

//录像
@property (nonatomic, assign) BOOL sdCardID; /**< sd卡ID */
@property (nonatomic, assign) NSString *cardNumber;//卡总容量
@property (nonatomic, assign) NSString *lastNumber;//卡可用
@property (nonatomic, assign) NSInteger formatSD;//格式化
@property (nonatomic, retain) UISwitch *VideoSet;//录像设置


//data
@property (nonatomic, copy) NSString *resultstr;

@property (nonatomic, retain) UIButton *btn1;

//蒙
@property (nonatomic, retain) UIView *MongolView;//蒙层



@end

@implementation SetingPublicVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = NSLocalizedString(self.TileStr, nil);
    
    [self initialize];
    
    [self StatusWithDevice];
    

}

- (void)buttonClicked:(UIButton*)button
{
    //    button.selected = YES; //选中当前按钮
    NSLog(@"%@ 点击了", button.currentTitle);//获取按钮标题
}

#pragma mark --GetDeviceInfo
- (void)StatusWithDevice
{
    //画面
    self.volume = [self.getdataDic objectForKey:@"volume"];
   
    //录像
    NSLog(@"self.imageReverseState=%@,self.manuallyrecordstate=%@",self.imagereversestate,self.manuallyrecordstate);
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear
{
    [super viewDidLoad];
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //self.MongolView.hidden = YES;
    
    //[self.MongolView removeFromSuperview];
}

- (void)dealloc
{
    self.MongolView = nil;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark --data源
- (void)initdata
{
    if ([self.TileStr isEqualToString:@"时间设置"]) {
    
        self.arrayData = [NSMutableArray arrayWithObjects:NSLocalizedString(@"时间设置",nil),nil];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(commonBtn:)];
        self.navigationItem.rightBarButtonItem.tag = 100;
    }
    if ([self.TileStr isEqualToString:@"安全设置"]) {
        
        self.arrayData = [NSMutableArray arrayWithObjects:NSLocalizedString(@"设备名称",nil),@"",@"",@"",nil];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(commonBtn:)];
        self.navigationItem.rightBarButtonItem.tag = 101;
    }
    if ([self.TileStr isEqualToString:@"画面与声音"]) {
        
        self.arrayData = [NSMutableArray arrayWithObjects:NSLocalizedString(@"音量设置",nil),NSLocalizedString(@"镜像翻转",nil),nil];
        [self.tableView reloadData];
        
    }
    if ([self.TileStr isEqualToString:@"录像设置"]) {
        
        self.arrayData = [NSMutableArray arrayWithObjects:NSLocalizedString(@"SD卡",nil),NSLocalizedString(@"SD卡总容量",nil),NSLocalizedString(@"SD可用空间",nil),NSLocalizedString(@"格式化SD卡",nil),NSLocalizedString(@"录像模式",nil),nil];
        
        [self getDeviceStorageInfomationExampleDeviceID:self.deviceModel.deviceID devicePassword:self.deviceModel.devicePassword];
        [self.tableView reloadData];
    }
}



#pragma mark --common公共
- (void)commonBtn:(UIButton *)sender
{
    if ( sender.tag == 100) {
        
        [self setDeviceTimeExample:self.DateTimestr];
    }
    if (sender.tag == 101) {
        
        [self setDeviceAdministratorPasswordExample:self.oldPasswordFD.text newPassword:self.nPasswordFD.text deviceID:self.deviceModel.deviceID];
    }
}


#pragma mark --初始化ui
- (void)initialize
{
    [self initdata];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [UIView new];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.arrayData && [self.arrayData count])
    {
        return self.arrayData.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = [UIColor blackColor];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
       
        if (!([self.TileStr isEqualToString:@"录像设置"] || [self.TileStr isEqualToString:@"画面与声音"] || [self.TileStr isEqualToString:@"安全设置"])) {
             cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    if (self.arrayData && indexPath.row < [self.arrayData count])
    {
        cell.textLabel.text = [self.arrayData objectAtIndex:indexPath.row];//根据当前索引行获取数组中的字符串
    }
    
    if ([self.TileStr isEqualToString:@"时间设置"])
    {
        if (self.DateTimestr == NULL ) {
            
            self.DateTimestr = [self getCurrentTimes];
        }
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@:  %@",[self.arrayData objectAtIndex:indexPath.row],self.DateTimestr];
    }
    
    if ([self.TileStr isEqualToString:@"安全设置"]) {
        
        if (indexPath.row == 0) {
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            if (self.textFieldstr != NULL) //不为空
            {
                 cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",[self.arrayData objectAtIndex:indexPath.row], self.textFieldstr];
            }
            
           
//            //弹窗
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"设备名称" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//            alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
//            UITextField *textField1 = [alertView textFieldAtIndex:0];
//            textField1.placeholder = @"输入新名称";
//            [cell addSubview:alertView];
//            [alertView show];
        }
        
        if (indexPath.row == 1) {
            
            _oldPasswordFD = [[UITextField alloc] initWithFrame:CGRectMake(10, 0, self.view.bounds.size.width, 45)];
            [_oldPasswordFD setTextColor:[UIColor flatGrayColor]];
            [_oldPasswordFD setTextAlignment:NSTextAlignmentLeft];
            [_oldPasswordFD setBackgroundColor:[UIColor clearColor]];
            [_oldPasswordFD setPlaceholder:NSLocalizedString(@"请输入设备密码",nil)];
            [cell addSubview:_oldPasswordFD];
//            _PasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, self.view.bounds.size.width-50);
            [[_oldPasswordFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return YES;
              }]
             subscribeNext:^(id x) {
                 if([self.oldPasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.oldPasswordFD.text = [self.oldPasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 NSLog(@"_oldPasswordFD%@", x);
                 
             }];
        }
        
        if (indexPath.row == 2 ) {
            
            _nPasswordFD = [[UITextField alloc] initWithFrame:CGRectMake(10, 0, self.view.bounds.size.width, 45)];
            [_nPasswordFD setTextColor:[UIColor flatGrayColor]];
            [_nPasswordFD setTextAlignment:NSTextAlignmentLeft];
            [_nPasswordFD setBackgroundColor:[UIColor clearColor]];
            [_nPasswordFD setPlaceholder:NSLocalizedString(@"请输入新密码",nil)];
            [cell addSubview:_nPasswordFD];
            //            _PasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, self.view.bounds.size.width-50);
            [[_nPasswordFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return YES;
              }]
             subscribeNext:^(id x) {
                 if([self.nPasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.nPasswordFD.text = [self.nPasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 NSLog(@"newPasswordFD%@", x);
             }];
        }
        
        if (indexPath.row == 3) {
            
            _atPasswordFD = [[UITextField alloc] initWithFrame:CGRectMake(10, 0, self.view.bounds.size.width, 45)];
            [_atPasswordFD setTextColor:[UIColor flatGrayColor]];
            [_atPasswordFD setTextAlignment:NSTextAlignmentLeft];
            [_atPasswordFD setBackgroundColor:[UIColor clearColor]];
            [_atPasswordFD setPlaceholder:NSLocalizedString(@"请再次输入密码",nil)];
            [cell addSubview:_atPasswordFD];
            //            _PasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, self.view.bounds.size.width-50);
            [[_atPasswordFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return YES;
              }]
             subscribeNext:^(id x) {
                 if([self.atPasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.atPasswordFD.text = [self.atPasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 NSLog(@"_atPasswordFD%@", x);
                
             }];
        }
    }
    
    //画面cell
    if ([self.TileStr isEqualToString:@"画面与声音"]) {
        
        if (indexPath.row == 0) {
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@:  %@",[self.arrayData objectAtIndex:indexPath.row],self.volume];
        }
        if (indexPath.row == 1) {
            
            _WirelessSound = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
            cell.accessoryView = _WirelessSound;
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.arrayData objectAtIndex:indexPath.row]];
            _WirelessSound.on = self.imagereversestate;
            _WirelessSound.tag = 776;
            [_WirelessSound addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
            
        }
    }
    
    //录像cell
   if ([self.TileStr isEqualToString:@"录像设置"]) {
        
        if (indexPath.row == 0) {
            
         //cell.textLabel.text = [NSString stringWithFormat:@"%@:  %@",[self.arrayData objectAtIndex:indexPath.row],@"有"];
         cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.arrayData objectAtIndex:indexPath.row]];
        }
       if (indexPath.row == 1) {
           
           cell.textLabel.text = [NSString stringWithFormat:@"%@:  %@",[self.arrayData objectAtIndex:indexPath.row],_cardNumber];
        }
        if (indexPath.row == 2) {
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@:  %@",[self.arrayData objectAtIndex:indexPath.row],_lastNumber];
        }
       if (indexPath.row == 3) {
           
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
       }
       if (indexPath.row == 4) {
           
           _VideoSet = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
           cell.accessoryView = _VideoSet;
           cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.arrayData objectAtIndex:indexPath.row]];
           _VideoSet.on = self.manuallyrecordstate;
           _VideoSet.tag = 777;
           [_VideoSet addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
           
           //蒙层 test bug
           self.MongolView = [[UIView alloc] initWithFrame:CGRectMake(50, 250, 200, 100)];
           self.MongolView.backgroundColor = [UIColor grayColor];
           self.MongolView.alpha = 1;
           [[UIApplication sharedApplication].keyWindow addSubview:self.MongolView];
           
       }
       
    }
    
    return cell;
}

#pragma mark --switch回调方法
- (void)switchAction:(UISwitch*)aSwitch
{
    //画面
    if (aSwitch.tag == 776) {
        
        if (YES == aSwitch.on)
        {
            NSLog(@"on");
        }
        else
        {
            NSLog(@"off");
        }
    }
    
    //录像
    if (aSwitch.tag == 777) {
        
        if (YES == aSwitch.on)
        {
            NSLog(@"on");
        }
        else
        {
            NSLog(@"off");
        }
    }
    
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < [self.arrayData count])
    {
        NSString *str = [self.arrayData objectAtIndex:indexPath.row];
        NSLog(@"%@", str);
    }
    
    if ([self.TileStr isEqualToString:@"安全设置"]) {
        
        //弹窗
         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"设备名称" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
         alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
         UITextField *textField1 = [alertView textFieldAtIndex:0];
         textField1.delegate = self;
         textField1.placeholder = @"输入新名称";
         [self.tableView addSubview:alertView];
         [alertView show];
    }
    
    if ([self.TileStr isEqualToString:@"时间设置"]) {
    
        //[self systemDatePicker];//选择时，弹时间选择器
        
        [self THDatePickerView];//
    }
    
    if ([self.TileStr isEqualToString:@"录像设置"]) {
        
        if (indexPath.row == 3) {
            
            NSString *message = NSLocalizedString(@"你确定要格式化SD卡吗?",nil);
            NSString *cancelButtonTitle = NSLocalizedString(@"取消",nil);
            NSString *otherButtonTitles = NSLocalizedString(@"确定",nil);
            
            //带有委托代理的模式对话框
            UIAlertView *alertViewFormat = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:message
                                                                delegate:self
                                                       cancelButtonTitle:cancelButtonTitle
                                                       otherButtonTitles:otherButtonTitles,nil];
            alertViewFormat.tag = 555;
            [alertViewFormat show];
        }
        
        if (indexPath.row == 4) {
            
            
            if (_sdCardID == YES) {
                
                [self setDeviceManuallyRecordStateRecordState:_sdCardID DeviceID:self.deviceModel.deviceID devicePassword:self.deviceModel.devicePassword];
            }
            
           
        }
    
    }

}


#pragma mark --时间设置
/**
 设置设备时间
 */
- (void)setDeviceTimeExample:(NSString *) DataStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]]; // 默认使用0时区，所以需要时区的转换
    NSDate *SetData = [dateFormatter dateFromString:DataStr];
    
     //[NSDate date]
    [[GWP2PClient sharedClient] setDeviceTime:SetData withDeviceID:self.deviceModel.deviceID devicePassword:self.deviceModel.devicePassword completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary) {
        NSLog(@"设置设备时间dataDictionary=%@",dataDictionary);
        
    }];
}

//日期时间选择器
- (void)systemDatePicker
{
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-216, self.view.bounds.size.width, 216)];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    datePicker.minuteInterval = 1;//设置时间是1分钟
    
    //最小最大日期范围
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    datePicker.minimumDate = [dateFormatter dateFromString:@"2009-01-01 00:00:00"];
    datePicker.maximumDate = [dateFormatter dateFromString:@"2030-01-01 00:00:00"];
    
    datePicker.date = [NSDate date];//设置默认显示日期
    [datePicker addTarget:self action:@selector(dateChanged:)
         forControlEvents:UIControlEventValueChanged];//添加一个选取器事件
    [self.view addSubview:datePicker];
}

-(void)dateChanged:(UIDatePicker*)datePicker
{
    NSDate *date = datePicker.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    NSLog(@"strDate=%@", strDate);

    self.DateTimestr = strDate;
    [self.tableView reloadData];
}

//获取当前时间
-(NSString*)getCurrentTimes
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *datenow = [NSDate date];
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    return currentTimeString;
}

#pragma mark --时间选择器-封装
- (void)THDatePickerView
{
    
    THDatePickerView *dateView = [[THDatePickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300)];
    dateView.delegate = self;
    dateView.title = NSLocalizedString(@"请选择时间", nil);
    //    dateView.isSlide = NO;
    //    dateView.date = @"2017-03-23 12:43";
    //    dateView.minuteInterval = 1;
    [self.view addSubview:dateView];
    self.dateView = dateView;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        self.dateView.frame = CGRectMake(0, self.view.frame.size.height - 300, self.view.frame.size.width, 300);
        [self.dateView show];
    }];
    
}

#pragma mark - THDatePickerViewDelegate
/**
 保存按钮代理方法
 
 @param timer 选择的数据
 */
- (void)datePickerViewSaveBtnClickDelegate:(NSString *)timer
{
    NSLog(@"保存点击");
   
    [UIView animateWithDuration:0.3 animations:^{
        self.dateView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300);
    }];
    
    self.DateTimestr = timer;
    [self.tableView reloadData];
}

/**
 取消按钮代理方法
 */
- (void)datePickerViewCancelBtnClickDelegate {
    NSLog(@"取消点击");

    [UIView animateWithDuration:0.3 animations:^{
        self.dateView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300);
    }];
}


#pragma mark --安全设置
/**
 初始化设备密码
 注意:手机与设备必须在同一局域网内,否则初始化不成功
 */
- (void)initDevicePasswordExample
{
    [[GWP2PClient sharedClient] setDeviceInitialPassword:_oldPasswordFD.text withDeviceID:self.deviceModel.deviceID completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary) {
        NSLog(@"初始化设备密码 dataDictionary=%@",dataDictionary);
    }];
}

/**
 设置设备管理员密码
 */
- (void)setDeviceAdministratorPasswordExample:(NSString *) OldPassword
                                  newPassword:(NSString *) newPassword
                                     deviceID:(NSString *) deviceID
{
    [[GWP2PClient sharedClient] setDeviceAdministratorPasswordWithOldPassword:self.deviceModel.devicePassword newPassword:_nPasswordFD.text deviceID:self.deviceModel.deviceID completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary) {
        NSLog(@"设置设备管理员密码 dataDictionary=%@",dataDictionary);
    }];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"输入结束...");
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length != 0) {
        
         NSLog(@"xxx=%@", textField.text);//文本变化内容
        
        self.textFieldstr = textField.text;
        [self.tableView reloadData];
    }
    return YES;
}

#pragma mark --画面


#pragma mark --录像设置
/**
 获取SD卡信息
 */
- (void)getDeviceStorageInfomationExampleDeviceID:(NSString *)DeviceID
                                   devicePassword:(NSString *) devicePassword
{
    [[GWP2PClient sharedClient] getDeviceStorageInfomationWithDeviceID:DeviceID devicePassword:devicePassword completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary) {
        
        NSLog(@"dataDictionary=%@",dataDictionary);
    
        //int lastnum = [[dataDictionary objectForKey:@"total storage"] intValue] - [[dataDictionary objectForKey:@"free storage"]intValue];
        
        _sdCardID = [dataDictionary objectForKey:@"storage exist"];
        _cardNumber = [dataDictionary objectForKey:@"total storage"];
        _lastNumber = [dataDictionary objectForKey:@"free storage"];
        
        [self.tableView reloadData];
        
    }];
}

/**
 格式化SD卡
 */
- (void)formatDeviceSDCardSDCard:(NSUInteger) SDCard
                        DeviceID:(NSString *) DeviceID
                  devicePassword:(NSString *) devicePassword
{
    
    [[GWP2PClient sharedClient] formatDeviceSDCard:SDCard withDeviceID:DeviceID devicePassword:devicePassword completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary) {
        NSLog(@"格式化SD卡=%@",dataDictionary);

    }];
    
}

/**
 设置手动录像开关
 */
- (void)setDeviceManuallyRecordStateRecordState:(BOOL) RecordState
                                       DeviceID:(NSString *) DeviceID
                                 devicePassword:(NSString *) devicePassword
{
    [[GWP2PClient sharedClient] setDeviceManuallyRecordState:RecordState withDeviceID:DeviceID devicePassword:devicePassword completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary) {
    
        NSLog(@"设置手动录像开关");
    }];
}


#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //格式化
    if (alertView.tag == 555)
    {
        if (0 == buttonIndex)
        {
            NSLog(@"取消按钮被点击了...");
        }
        if (1 == buttonIndex)
        {
            NSLog(@"确定按钮被点击了...");
            
            NSLog(@"_formatSD=%d,deviceID=%@,devicePassword=%@",(int)self.formatSD,self.deviceModel.deviceID,self.deviceModel.devicePassword);
            
            [self formatDeviceSDCardSDCard:_formatSD DeviceID:self.deviceModel.deviceID devicePassword:self.deviceModel.devicePassword];
            
        }
    }
}

@end
