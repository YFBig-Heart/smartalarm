//
//  SettingVC.m
//  SmartAlarm
//
//  Created by zhangping on 2018/11/13.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "SettingVC.h"
#import "SetingPublicVC.h"

@interface SettingVC ()

@property (nonatomic, retain) NSMutableArray    *arrayData;
@property (nonatomic, copy) NSString *RowTitle;
@property (nonatomic, strong) NSDictionary *dataDic;//deveiceinfo
@property (nonatomic, strong) NSString *imagereversestate;//镜像翻转
@property (nonatomic, strong) NSString *manuallyrecordstate;//录像模式

@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = NSLocalizedString(@"设置", nil);
    
    [self initialize];
    
    [self getdata];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//初始化
- (void)initialize
{
    self.arrayData = [NSMutableArray arrayWithObjects:NSLocalizedString(@"时间设置",nil),NSLocalizedString(@"安全设置",nil),NSLocalizedString(@"画面与声音",nil),NSLocalizedString(@"录像设置",nil),nil];
    
    self.UITableView.dataSource = self;
    self.UITableView.delegate = self;
    
    [self.UITableView setSeparatorInset:UIEdgeInsetsZero];
    self.UITableView.backgroundColor = [UIColor clearColor];
    self.UITableView.tableFooterView = [UIView new];
}

//获取当前时间
-(NSString*)getCurrentTimes
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];//设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    NSDate *datenow = [NSDate date];
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    NSLog(@"currentTimeString =  %@",currentTimeString);
    return currentTimeString;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.arrayData && [self.arrayData count])
    {
        return 4;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = [UIColor blackColor];
        cell.backgroundColor = [UIColor clearColor];
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    if (self.arrayData && indexPath.row < [self.arrayData count])
    {
        cell.textLabel.text = [self.arrayData objectAtIndex:indexPath.row];//根据当前索引行获取数组中的字符串
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < [self.arrayData count])
    {
        NSString *str = [self.arrayData objectAtIndex:indexPath.row];
        NSLog(@"%@", str);
        if ( indexPath.row == 0)
        {
            SetingPublicVC *SetPbulic = [[SetingPublicVC alloc] init];
            SetPbulic.TileStr = @"时间设置";
            SetPbulic.deviceModel = self.deviceModel;
            [self.navigationController pushViewController:SetPbulic animated:YES];
        }
        if (indexPath.row == 1)
        {
            SetingPublicVC *SetPbulic = [[SetingPublicVC alloc] init];
            SetPbulic.TileStr = @"安全设置";
            SetPbulic.deviceModel = self.deviceModel;
            [self.navigationController pushViewController:SetPbulic animated:YES];
        }
        if (indexPath.row == 2)
        {
            SetingPublicVC *SetPbulic = [[SetingPublicVC alloc] init];
            SetPbulic.TileStr = @"画面与声音";
            SetPbulic.deviceModel = self.deviceModel;
            SetPbulic.getdataDic = self.dataDic;
            SetPbulic.imagereversestate = self.imagereversestate;//镜像
            SetPbulic.manuallyrecordstate = self.manuallyrecordstate;//录像
            [self.navigationController pushViewController:SetPbulic animated:YES];
        }
        if (indexPath.row == 3)
        {
            SetingPublicVC *SetPbulic =  [[SetingPublicVC alloc] init];
            SetPbulic.TileStr = @"录像设置";
            SetPbulic.deviceModel = self.deviceModel;
            SetPbulic.imagereversestate = self.imagereversestate;//镜像
            SetPbulic.manuallyrecordstate = self.manuallyrecordstate;//录像
            [self.navigationController pushViewController:SetPbulic animated:YES];
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor grayColor];
    label.font = [UIFont systemFontOfSize:13];
    label.frame = CGRectMake(15, 20, self.view.frame.size.width-15, 20);
    [headerView addSubview:label];
    if (section == 0)
    {
        NSString *allStr = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"设备ID:",nil),self.deviceModel.deviceID];
        label.text = allStr;
    }
    return headerView;
}

#pragma mark --Deviceinfo
- (void)getdata
{
    //id+mm
    NSString *DeviceID = self.deviceModel.deviceID;
    NSString *password = self.deviceModel.devicePassword;
    
    [[GWP2PClient sharedClient] getMultipleDeviceStatusWithDeviceID:@"9397251" password:@"123" completionBlock:^(GWP2PClient *client, BOOL success, NSDictionary<NSString *,id> *dataDictionary) {

        NSLog(@"00 deviceinfo =%@",dataDictionary);
        self.dataDic = dataDictionary;

        //取值
        NSString *imagereversestate = @"image reverse state";
        NSString *manuallyrecordstate = @"manually record state";
        self.imagereversestate = [dataDictionary objectForKey:imagereversestate];
        self.manuallyrecordstate = [dataDictionary objectForKey:manuallyrecordstate];
        
        NSLog(@"image reverse state=%@,manually record state=%@",[dataDictionary objectForKey:imagereversestate],[dataDictionary objectForKey:manuallyrecordstate]);
        
    }];
    
    
}




@end
