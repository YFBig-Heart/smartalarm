//
//  CheckPswViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckPswViewController : UIViewController
@property (retain,nonatomic) NSString * userID;
@property (retain,nonatomic) NSString * DeviceID;
@property (weak, nonatomic) IBOutlet UILabel *StatuesLabel;
@end
