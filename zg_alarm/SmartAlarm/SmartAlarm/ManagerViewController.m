//
//  ManagerViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/9/21.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "ManagerViewController.h"
#import "ManageCell.h"
#import "AlarmDeviceViewController.h"
#import "ContanctListViewController.h"
#import "BoardListViewController.h"
#import "SecuritySettingViewController.h"
#import "NetWorkViewController.h"
#import "WifiConfigViewController.h"
#import "SystemSettingViewController.h"
#import "AdminListViewController.h"
#import "AppSettingViewController.h"
#import "ChangePasswordViewController.h"
#import "SensorTypeListViewController.h"
#import "ManageTableCell.h"
#import "ConfigListViewController.h"
#import "TimeListViewController.h"
#import "OverLoadMainViewController.h"


@interface ManagerViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *ManageTabView;
@property (retain,nonatomic) NSDictionary * BoardDic;
@end

@implementation ManagerViewController
NSArray * _ItemsTitle;
NSArray * _ItemsIcon;

- (void)viewDidLoad {
    _ManageTabView.delegate = self;
    _ManageTabView.dataSource = self;
    _ItemsTitle = @[@"",@"",@"报警系统设定",@"管理成员",@"APP系统设置"];
    _ItemsIcon = @[@"list_alram_ic",@"list_user_ic.imageset",@"list_alram_ic"];
    self.title = NSLocalizedString(@"管理",nil);
//    self.automaticallyAdjustsScrollViewInsets = false;
    [super viewDidLoad];

}
- (void)viewWillAppear:(BOOL)animated
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice]) {
        [_ManageTabView reloadData];
    }
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _ItemsTitle.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        return 200.0f;
    }
    else if(indexPath.row == 0)
        return 100.0f;
    else
        return 65.0f;
}
- (void)RACSetActionForManageCellButton:(ManageTableCell *)cell
{
    if (cell) {
        @weakify(self)
        [[cell.ContanctBtn
          rac_signalForControlEvents:UIControlEventTouchUpInside]
         subscribeNext:^(id x) {
             @strongify(self)
             if ([CoreHTTPRequest isDeviceAlive]) {
                 [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
                 if([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)
                 {
                     //当是智能插座的时候将联系人改为定时器
                     TimeListViewController * vc = [[TimeListViewController alloc] init];
                     [self.navigationController pushViewController:vc animated:YES];
                 }
                 else
                 [self.navigationController pushViewController:[[ContanctListViewController alloc] init] animated:YES];
             }
             else
             {
                 [RKDropdownAlert title:nil message:NSLocalizedString(@"请先绑定设备", nil)];
             }
         }];
        [[cell.SensorsBtn
          rac_signalForControlEvents:UIControlEventTouchUpInside]
         subscribeNext:^(id x) {
             @strongify(self)
             if ([CoreHTTPRequest isDeviceAlive]) {
                 [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
                 [self.navigationController pushViewController:[[SensorTypeListViewController alloc] init] animated:YES];
             }
             else
             {
                 [RKDropdownAlert title:nil message:NSLocalizedString(@"请先绑定设备", nil)];
             }

         }];
        [[cell.OutletsBtn
          rac_signalForControlEvents:UIControlEventTouchUpInside]
         subscribeNext:^(id x) {
             @strongify(self)
             if ([CoreHTTPRequest isDeviceAlive]) {
                 [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
                 if([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)
                 {
                     //当是智能插座的时候将联系人改为定时器
                     OverLoadMainViewController * vc = [[OverLoadMainViewController alloc] init];
                     [self.navigationController pushViewController:vc animated:YES];
                 }
                 else
                     [self.navigationController pushViewController:[[BoardListViewController alloc] init] animated:YES];
             }
             else
             {
                 [RKDropdownAlert title:nil message:NSLocalizedString(@"请先绑定设备", nil)];
             }

         }];
        [[cell.SecurityBtn
          rac_signalForControlEvents:UIControlEventTouchUpInside]
         subscribeNext:^(id x) {
             @strongify(self)
             if ([CoreHTTPRequest isDeviceAlive]) {
                 [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
                 [self.navigationController pushViewController:[[SecuritySettingViewController alloc] init] animated:YES];
             }
             else
             {
                 [RKDropdownAlert title:nil message:NSLocalizedString(@"请先绑定设备", nil)];
             }
         }];
        [[cell.NetworkBtn
          rac_signalForControlEvents:UIControlEventTouchUpInside]
         subscribeNext:^(id x) {
             @strongify(self)
             if ([CoreHTTPRequest isDeviceAlive]) {
                 [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
                 [self.navigationController pushViewController:[[NetWorkViewController alloc] init] animated:YES];
             }
             else
             {
                 [RKDropdownAlert title:nil message:NSLocalizedString(@"请先绑定设备", nil)];
             }

         }];
        [[cell.WifiBtn
          rac_signalForControlEvents:UIControlEventTouchUpInside]
         subscribeNext:^(id x) {
             @strongify(self)
             [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
             [self.navigationController pushViewController:[[ConfigListViewController alloc] init] animated:YES];
         }];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *const kCellID = @"_cell_id_more";
    if (indexPath.row == 1) {
        //九宫格
        ManageTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ManageTableCell"];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ManageTableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }  
        [self RACSetActionForManageCellButton:cell];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)
        {
            //当是智能插座的时候将联系人改为定时器
            [cell.textLabel setText:NSLocalizedString([_ItemsTitle objectAtIndex:indexPath.row],nil)];
            [cell.ContanctBtn setImage:[UIImage imageNamed:@"manage_timer"] forState:UIControlStateNormal];
            [cell.ContanctTitle setText:NSLocalizedString(@"定时器", nil)];
            [cell.OutletsBtn setImage:[UIImage imageNamed:@"manage_overload"] forState:UIControlStateNormal];
            [cell.OutletsTitle setText:NSLocalizedString(@"过载保护 ", nil)];

        }
        return cell;
    }

    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        if(indexPath.row == 0)
        {
            cell.imageView.image = [UIImage imageNamed:@"manage_home"];
            NSDictionary * DeviceDic = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice];
            [cell.textLabel setText:DeviceDic?[DeviceDic objectForKey:@"name"]:NSLocalizedString(@"我的设备", ni)];
                    cell.imageView.layer.cornerRadius = cell.imageView.height / 2;
            [cell.imageView.layer setCornerRadius:52.33/2];
            [cell.imageView.layer setMasksToBounds:YES];
            [cell.imageView.layer setBorderWidth:1.0];
            [cell.imageView.layer setBorderColor:[UIColor grayColor].CGColor];

        }
        else
        {
            [cell.textLabel setText:NSLocalizedString([_ItemsTitle objectAtIndex:indexPath.row],nil)];
            cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
            if (([CoreHTTPRequest isDeviceAlive])) {
                [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
                [self.navigationController pushViewController:[[AlarmDeviceViewController alloc] init] animated:YES];
            }
            else
            {
                [RKDropdownAlert title:nil message:NSLocalizedString(@"请先绑定设备", nil)];
            }
        }
            break;
        case 2:
        {
            if (([CoreHTTPRequest isDeviceAlive])) {
                [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
                [self.navigationController pushViewController:[[SystemSettingViewController alloc] init] animated:YES];
            }
            else
            {
                [RKDropdownAlert title:nil message:NSLocalizedString(@"请先绑定设备", nil)];
            }

        }
            break;
        case 3:
        {
            if (([CoreHTTPRequest isDeviceAlive])) {
                [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
                [self.navigationController pushViewController:[[AdminListViewController alloc] init] animated:YES];
            }
            else
            {
                [RKDropdownAlert title:nil message:NSLocalizedString(@"请先绑定设备", nil)];
            }
            
        }
            break;
        case 4:
            [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
            [self.navigationController pushViewController:[[AppSettingViewController alloc] init] animated:YES];
            break;

        default:
            break;
    }
}

@end
