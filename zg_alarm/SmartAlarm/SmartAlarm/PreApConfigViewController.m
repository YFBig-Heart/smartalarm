//
//  PreApConfigViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/1/10.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "PreApConfigViewController.h"
#import "RootTabViewController.h"
#include <sys/socket.h>
#import <netinet/in.h>
#import <netinet6/in6.h>
#import <arpa/inet.h>
#import <ifaddrs.h>
#include <netdb.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import "getgateway.h"
#import <arpa/inet.h>
#import <netdb.h>
@interface PreApConfigViewController ()
@property (retain,nonatomic) UIButton * NextBTN;
@property (weak, nonatomic) IBOutlet UIButton *TurnBTN;
@property (weak, nonatomic) IBOutlet UILabel *Step1;
@property (weak, nonatomic) IBOutlet UILabel *Step2;
@property (weak, nonatomic) IBOutlet UILabel *Step3;
@property (retain,nonatomic)UIActivityIndicatorView * indicator;
@property (nonatomic) BOOL isSucc;
@end

@implementation PreApConfigViewController

- (void)viewDidLoad {
    _NextBTN = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [_Step1 setText:NSLocalizedString(@"1.参照终端说明书让终端进入热点配置模式", nil)];
    [_Step2 setText:NSLocalizedString(@"2.进入手机\"系统设置\"->\"WIFI设置\"，连接ZG_XXXX热点", nil)];
    [_Step3 setText:NSLocalizedString(@"3.回到本界面点击\"开始配置\"", nil)];
    [_TurnBTN setTitle:NSLocalizedString(@"点击此处跳转系统设置", nil) forState:UIControlStateNormal];
    [[_TurnBTN rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self DoneTurn];
    }];
    [_NextBTN setTitle:NSLocalizedString(@"开始配置", nil) forState:UIControlStateNormal];
    [self.view addSubview:_NextBTN];
    _NextBTN.sd_layout.widthIs(kScreen_Width - 20).heightIs(48).centerXEqualToView(self.view).bottomSpaceToView(self.view, 30);
    [_NextBTN bs_configureAsPrimaryStyle];
    [[_NextBTN rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self DoConfig];
    }];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone  target:self action:@selector(Done)];
    [super viewDidLoad];
    NSLog(@"获取路由网关:%@",[self routerIp]);
}
- (void)ApplicationTurn:(NSString *)urlString
{
    if( [[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:urlString]] ) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlString] options:@{}completionHandler:^(BOOL        success) {
        }];
    }
}
- (void)DoneTurn
{
    //[self ApplicationTurn:@"App-prefs:root=com.wongshan.zg.smart"];
 
    //iOS系统版本 >= iOS10
    [self ApplicationTurn:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (void)Done
{
    [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
    [self.view.window makeKeyAndVisible];
}
- (void)LayOutAlart
{
    _indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [_NextBTN.superview insertSubview:_indicator aboveSubview:_NextBTN];
    _indicator.sd_layout.widthIs(47).heightIs(47).centerXIs(_NextBTN.centerX).centerYIs(_NextBTN.centerY);
    [_NextBTN setTitle:@"" forState:UIControlStateNormal];
    [_indicator startAnimating];
    [_NextBTN setNeedsDisplay];
}

- (void)DoConfig
{
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    
    [self LayOutAlart];
    asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:mainQueue];
    NSString *host = [self routerIp];//@"192.168.1.23";
    uint16_t port = 9999;
    
    NSLog(@"Connecting to \"%@\" on port %hu connecting....", host, port);
    
    NSError *error = nil;
    if (![asyncSocket connectToHost:host onPort:port error:&error])
    {
        NSLog(@"Error connecting: %@", error);
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Socket Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    NSLog(@"socket:%p didConnectToHost:%@ port:%hu", sock, host, port);
    {
#if ENABLE_BACKGROUNDING && !TARGET_IPHONE_SIMULATOR
        {
            // Backgrounding doesn't seem to be supported on the simulator yet
            
            [sock performBlock:^{
                if ([sock enableBackgroundingOnSocket])
                    NSLog(@"Enabled backgrounding on socket");
                else
                    NSLog(@"Enabling backgrounding failed!");
            }];
        }
#endif
    }
    NSString *requestStr = [NSString stringWithFormat:@"@@|Z2|%@|%@|\r\n", _LocalSSID,_LocalPSW];
    NSData *requestData = [requestStr dataUsingEncoding:NSUTF8StringEncoding];
    
    [sock writeData:requestData withTimeout:-1 tag:0];
    [sock readDataToData:[GCDAsyncSocket CRLFData] withTimeout:10 tag:0];
}

- (void)socketDidSecure:(GCDAsyncSocket *)sock
{
    NSLog(@"socketDidSecure:%p", sock);
    //@@|Z2|ssid|password|\r\n
    NSString *requestStr = [NSString stringWithFormat:@"@@|Z2|%@|%@|\r\n", _LocalSSID,_LocalPSW];
    NSData *requestData = [requestStr dataUsingEncoding:NSUTF8StringEncoding];
    
    [sock writeData:requestData withTimeout:-1 tag:0];
    [sock readDataToData:[GCDAsyncSocket CRLFData] withTimeout:-1 tag:0];
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    NSLog(@"socket:%p didWriteDataWithTag:%ld", sock, tag);
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    NSLog(@"socket:%p didReadData:withTag:%ld", sock, tag);
    _isSucc = YES;
    NSString *httpResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"HTTP Response:\n%@", httpResponse);
    //@@|Z2|12345555555555|\r\n
    NSArray * StringArray = [httpResponse componentsSeparatedByString:@"|"];
    NSString * TitleMsg = [[NSString alloc] initWithFormat:@"%@\r\n%@",NSLocalizedString(@"WIFI配制成功，绑定号:", nil),[StringArray objectAtIndex:2]];
    UIAlertController *actionSheetController = [UIAlertController alertControllerWithTitle:nil message:TitleMsg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self Done];
    }];
    [actionSheetController addAction:cancelAction];
        [self presentViewController:actionSheetController animated:YES completion:nil];
    
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    NSLog(@"socketDidDisconnect:%p withError: %@", sock, err);
    if (_isSucc == NO) {
        UIAlertController *actionSheetController = [UIAlertController alertControllerWithTitle:nil message:@"失败，请重试！" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [_indicator removeFromSuperview];
            [_NextBTN setTitle:NSLocalizedString(@"开始配置", nil) forState:UIControlStateNormal];
        }];
        [actionSheetController addAction:cancelAction];
        [self presentViewController:actionSheetController animated:YES completion:nil];
    }
}
- (NSString *) routerIp {
    
    //获取设备路由器地址
    struct in_addr addr;
    getdefaultgateway(&(addr.s_addr));
    NSString * addrStr = [NSString stringWithUTF8String:inet_ntoa(addr)];
    
    NSLog(@"地址：%@",addrStr);
    return addrStr;
    
}
@end
