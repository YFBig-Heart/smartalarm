////
////  NetWorkViewController.m
////  SmartAlarm
////
////  Created by Huang Shan on 2017/10/3.
////  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "NetWorkViewController.h"
#import "APNSettingViewController.h"
#import "ServerIPViewController.h"
@interface NetWorkViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * ImageArray;
@property (retain,nonatomic) NSArray * TitleArray;
@end

@implementation NetWorkViewController

- (void)viewDidLoad {
    if (_TitleArray == nil) {
        //_TitleArray = @[@"APN 设置"];
        _TitleArray = @[@"ID",@"名称",@""];
        _ImageArray = @[@"List_APN",@"list_ip_set"];
    }
    self.title = NSLocalizedString(@"网络", nil);
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)
        return 0;
    else
    return _TitleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.imageView.image = [UIImage imageNamed:[_ImageArray objectAtIndex:indexPath.row]];
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
            APNSettingViewController * apnvc = [[APNSettingViewController alloc] init];
            apnvc.SmsSendNumber = [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"number"];
            [self.navigationController pushViewController:apnvc animated:YES];
        }
        break;
        case 1:
        {
            ServerIPViewController * vc = [[ServerIPViewController alloc] init];
            vc.SmsSendNumber = [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"number"];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}

@end


