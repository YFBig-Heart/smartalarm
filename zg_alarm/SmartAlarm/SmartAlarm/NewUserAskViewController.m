//
//  NewUserAskViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/2/5.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "NewUserAskViewController.h"
#import "RegisterForPasswordViewController.h"
@interface NewUserAskViewController ()
@property (weak, nonatomic) IBOutlet UILabel *AskUserLabel;
@property (weak, nonatomic) IBOutlet UILabel *DetailLable;
@property (weak, nonatomic) IBOutlet UIButton *NextBtn;
@property (weak, nonatomic) IBOutlet UIButton *UserDifEmail;
@property (weak, nonatomic) IBOutlet UILabel *Islabel;

@end

@implementation NewUserAskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _Islabel.text = NSLocalizedString(@" ", nil);
    [_Islabel alignTop];
    NSString * AskLable = NSLocalizedString(@"确定用该邮箱?", nil);
    [_AskUserLabel setText:[NSString stringWithFormat:@"%@%@",_VailEmail,AskLable]];
    [_AskUserLabel alignTop];
    [_DetailLable setText:NSLocalizedString(@"你将在登录时使用此电子邮件地址,当你忘记密码时使用此邮箱找回密码。", nil)];

    [_NextBtn bs_configureAsPrimaryStyle];
    [_NextBtn setTitle:NSLocalizedString(@"下一步", nil) forState:UIControlStateNormal];
    [_UserDifEmail setTitle:NSLocalizedString(@"用不同邮箱地址注册", nil) forState:UIControlStateNormal];
    self.title = NSLocalizedString(@"新用户注册", nil);
    [[_UserDifEmail rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [[_NextBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        RegisterForPasswordViewController * vc  = [[RegisterForPasswordViewController alloc] init];
        vc.userID = _VailEmail;
        [self.navigationController pushViewController:vc animated:YES];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
