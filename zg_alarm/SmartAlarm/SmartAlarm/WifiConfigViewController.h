//
//  WifiConfigViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WifiConfigViewController : UIViewController
@property (retain,nonatomic) NSString * currIMEI;
@property (assign) int EntryType; //0 :设置里进入的。配对成功后不需要退出。1 :从绑定界面进入的。成功后要退出。
@property (assign) BOOL isHideBackButton;//是否需要隐藏返回导航键
@end
