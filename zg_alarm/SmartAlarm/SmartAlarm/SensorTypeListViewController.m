//
//  SensorTypeListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/4.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "SensorTypeListViewController.h"
#import "ReomteEditViewController.h"
#import "RemoteCotrolListViewController.h"
#import "WirelessRemoteControlListViewController.h"
#import "RFListViewController.h"
#import "WirelessSirenViewController.h"


@interface SensorTypeListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * ImageArray;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UISwitch *WirelessSound;
@property (retain,nonatomic) NSMutableDictionary *RemoteInfoDic;
@end

@implementation SensorTypeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    self.title = NSLocalizedString(@"传感器", nil);
    if (_TitleArray == nil) {
//        if ([[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"type"] intValue] ==2) {
        if (0) {
            ///WIFI 报警器时，TYPE==2, 即设备类型不等于 1 时，有些参照不需要显示
            _TitleArray = @[@"遥控器",@"无线传感器",@"无线警号"];
            _ImageArray = @[@"List_large_sensor",@"List_large_Wire",@"List_Large_wiress"];
        }
        else
        {
        _TitleArray = @[@"遥控器",@"无线传感器",@"RFID Tag",@"无线警号"];
        _ImageArray = @[@"List_large_sensor",@"List_large_Wire",@"List_large_RFID_Tags",@"List_Large_wiress"];
        }
    }
    [self synWirelessSound];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)reportWirelessSound
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_RemoteInfoDic objectForKey:@"status"],@"status",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80083",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void )synWirelessSound
{
    NSDictionary * tDic;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00083",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ((([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX))) {
       return _TitleArray.count -1;
    }
    else
    return _TitleArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    NSInteger ImageArrayIndex;
    if (([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)) {
        if ([NSLocalizedString([_TitleArray objectAtIndex:indexPath.row], nil) isEqualToString:NSLocalizedString(@"RFID Tag",nil)]) {
            ImageArrayIndex = indexPath.row+1;
        }
        else
            ImageArrayIndex = indexPath.row;
    }
    else
        ImageArrayIndex = indexPath.row;

    cell.textLabel.text = NSLocalizedString([_TitleArray objectAtIndex:ImageArrayIndex], nil);
    cell.imageView.image = [UIImage imageNamed:[_ImageArray objectAtIndex:ImageArrayIndex]];
//    if ([NSLocalizedString([_TitleArray objectAtIndex:ImageArrayIndex], nil) isEqualToString:NSLocalizedString(@"无线警号",nil)]) {
//        if (_WirelessSound) {
//            [_WirelessSound removeFromSuperview];
//            _WirelessSound = nil;
//        }
//        _WirelessSound = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
//        cell.accessoryView = _WirelessSound;
//        if ([[_RemoteInfoDic objectForKey:@"status"] boolValue]) {
//            [_WirelessSound setOn:YES];
//        }
//        else
//            [_WirelessSound setOn:NO];
//        @weakify(self)
//        [[self.WirelessSound
//          rac_signalForControlEvents:UIControlEventValueChanged]
//         subscribeNext:^(UISwitch * x) {
//             @strongify(self)
//             NSLog(@"开关  ： %@",x);
//             if (x.isOn) {
//                 [_RemoteInfoDic setObject:@"1" forKey:@"status"];
//             }
//             else
//                 [_RemoteInfoDic setObject:@"0" forKey:@"status"];
//             [self reportWirelessSound];
//         }];
//
//    }
//    else
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self.navigationController pushViewController:[[RemoteCotrolListViewController alloc] init]animated:YES];
            break;
        case 1:
            [self.navigationController pushViewController:[[WirelessRemoteControlListViewController alloc] init]animated:YES];
            break;
        case 2:
        {
//            if (((([[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"type"] intValue] !=3)))) {
//                if ([[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"type"] intValue] !=2) {
//                    [self.navigationController pushViewController:[[RFListViewController alloc] init] animated:YES];
//                }
//            }
            
            //修改标计
             [self.navigationController pushViewController:[[RFListViewController alloc] init] animated:YES];
        }
            break;
        case 3:
        {
            [self.navigationController pushViewController:[[WirelessSirenViewController alloc] init] animated:YES];
        }
            
        default:
            break;
    }
}

@end
