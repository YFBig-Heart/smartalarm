//
//  WirelessRemoteControlEditViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/4.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "WirelessRemoteControlEditViewController.h"
#import "HMScanner.h"
#import "HMScannerController.h"
#import "STPickerSingle.h"
#import "STPickerView.h"
#import "STPickerDate.h"
#import "HcdDateTimePickerView.h"

@interface WirelessRemoteControlEditViewController ()<UITableViewDelegate,UITableViewDataSource,STPickerDateDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (weak, nonatomic) IBOutlet UIButton *DeleteBtn;
@property (retain,nonatomic) UITextField * ControllAraFD;
@property (retain,nonatomic) UITextField * NameFD;
@property (retain,nonatomic) UITextField * DelaySecFD;
@property (retain,nonatomic) UITextField * CodeFD;
@property (retain,nonatomic) UISwitch * chimeSW;
@property (retain,nonatomic) UISwitch * ReadySW;
@property (retain,nonatomic) UISwitch * sirenSW;
@property (retain,nonatomic) UIButton *UidButton;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) NSArray * arrayData;
@property (retain,nonatomic) NSArray * arrayTypeData;
@property (retain,nonatomic) NSArray * ModeArrayData;
@property (assign) bool isAddNew;
@property (assign) bool isReadyEnable;
@property (assign) bool isDoorbellEnable;
@end

@implementation WirelessRemoteControlEditViewController

/*
 
 0: 门磁
 1: 红外
 2: 玻破
 3: 水感
 4: 紧急按钮
 5: 烟感
 6: 燃气
 7: CO
 8: 振动传感器
 9: 温感
 10: 门铃
 11: 其它配件
 
 C 门磁
 D 红外
 E 玻破
 F 水感
 G 紧急按钮
 H 烟感
 I  燃气
 J CO
 K 振动传感器
 L 温感
 M 门铃
 N 其它
 */
- (void)viewDidLoad {
    if (_TitleArray == nil) {
        
        _TitleArray = @[@"防区",@"名称",@"类型",@"模式",@"延时(秒)",@"警号",@"提示音",@"非法设防",@"编号"];
    }
    if (_arrayTypeData == nil) {
        _arrayTypeData = @[@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N"];
    }
    //Door Snsor, Motion Sensor, Glass Break Sensor, Water Sensor,Panic Button,Smoke Detector,Gas Detector,CO Detector,Vibration Sensor, Temperature Dector, Doorbell, Other Sensors
    _arrayData = @[NSLocalizedString(@"门磁",nil),NSLocalizedString(@"红外",nil),NSLocalizedString(@"玻破",nil),NSLocalizedString(@"水感",nil),NSLocalizedString(@"紧急按钮",nil),NSLocalizedString(@"烟感",nil),NSLocalizedString(@"燃气",nil),NSLocalizedString(@"一氧化碳",nil),NSLocalizedString(@"振动传感器",nil),NSLocalizedString(@"温感",nil),NSLocalizedString(@"门铃",nil),NSLocalizedString(@"其它配件",nil)];
    _ModeArrayData = @[NSLocalizedString(@"设防",nil),NSLocalizedString(@"在家",nil),NSLocalizedString(@"紧急",nil),NSLocalizedString(@"关 闭",nil)];
    [_DeleteBtn setHidden:NO];
    [[_DeleteBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self AskDelete];
    }];
    [_DeleteBtn setTitle:NSLocalizedString(@"删除", nil) forState: UIControlStateNormal];
    if(_RemoteInfoDic == nil)
    {
        _RemoteInfoDic = [[NSMutableDictionary alloc] init];
        [_RemoteInfoDic setObject:@"" forKey:@"name"];
        [_RemoteInfoDic setObject:@"1" forKey:@"zone"];
        [_RemoteInfoDic setObject:@"0" forKey:@"mode"];
        [_RemoteInfoDic setObject:@"1" forKey:@"siren"];
        [_RemoteInfoDic setObject:@"0" forKey:@"delay"];
        [_RemoteInfoDic setObject:@"0" forKey:@"type"];
        [_RemoteInfoDic setObject:@"0" forKey:@"chime"];
        [_RemoteInfoDic setObject:@"0" forKey:@"code"];
        [_RemoteInfoDic setObject:@"0" forKey:@"ready"];
        _isAddNew = true;
        [_DeleteBtn setHidden:YES];
        self.title = NSLocalizedString(@"新增无线传感器", nil);
    }
    else
    {
        self.title = NSLocalizedString(@"编辑", nil);
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];;
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    _isDoorbellEnable = NO;
    _isReadyEnable = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)AskDelete
{
    UIAlertController * ct = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"您确定删除吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *showAllInfoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self DeleteRemoteControl];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消",nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [ct addAction:cancelAction];
    [ct addAction:showAllInfoAction];
    [self presentViewController:ct animated:YES completion:nil];
}

- (void)Done
{
    NSDictionary * tDic ;
    if (_isAddNew == YES) {
        tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[_RemoteInfoDic objectForKey:@"name"],@"name",[_RemoteInfoDic objectForKey:@"code"],@"code",[_RemoteInfoDic objectForKey:@"type"],@"type",[_RemoteInfoDic objectForKey:@"mode"],@"mode",[_RemoteInfoDic objectForKey:@"zone"],@"zone",[_RemoteInfoDic objectForKey:@"siren"],@"siren",[_RemoteInfoDic objectForKey:@"chime"],@"chime",
                [_RemoteInfoDic objectForKey:@"ready"],@"ready",
                [_RemoteInfoDic objectForKey:@"delay"],@"delay",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    }
    else
        tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[_RemoteInfoDic objectForKey:@"name"],@"name",
                [_RemoteInfoDic objectForKey:@"ready"],@"ready",
                [_RemoteInfoDic objectForKey:@"code"],@"code",[_RemoteInfoDic objectForKey:@"type"],@"type",[_RemoteInfoDic objectForKey:@"mode"],@"mode",[_RemoteInfoDic objectForKey:@"zone"],@"zone",[_RemoteInfoDic objectForKey:@"siren"],@"siren",[_RemoteInfoDic objectForKey:@"chime"],@"chime",[_RemoteInfoDic objectForKey:@"delay"],@"delay",[_RemoteInfoDic objectForKey:@"sn"],@"sn",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80081",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已保存",nil)];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)DeleteRemoteControl
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[_RemoteInfoDic objectForKey:@"sn"],@"sn",[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"username",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"70081",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已保存",nil)];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_remotelist";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    switch (indexPath.row) {
        case 0:
        {
            if ([_RemoteInfoDic objectForKey:@"zone"]) {
                if (_ControllAraFD) {
                    [_ControllAraFD removeFromSuperview];
                    _ControllAraFD = nil;
                }
                _ControllAraFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_ControllAraFD setTextColor:[UIColor flatGrayColor]];
                if (([_RemoteInfoDic objectForKey:@"zone"] != [NSNull null])) {
                    [_ControllAraFD setText:[_RemoteInfoDic objectForKey:@"zone"]];
                }
                [_ControllAraFD setKeyboardType:UIKeyboardTypeNumberPad];
                [_ControllAraFD setTextAlignment:NSTextAlignmentRight];
                [_ControllAraFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_ControllAraFD];
                _ControllAraFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 20.0f);
                [[self.ControllAraFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      [_RemoteInfoDic setObject:_ControllAraFD.text forKey:@"zone"];
                      self.navigationItem.rightBarButtonItem.enabled = NO;
                      return text.length > 0;
                  }]
                 subscribeNext:^(id x) {
                     if([self.ControllAraFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.ControllAraFD.text = [self.ControllAraFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     [_RemoteInfoDic setObject:_ControllAraFD.text forKey:@"zone"];
                     if ((_ControllAraFD.text.length > 0)&&(_NameFD.text.length > 0)) {
                         self.navigationItem.rightBarButtonItem.enabled = YES;
                     }
                     NSLog(@"%@", x);
                 }];
                
            }
        }
            break;
        case 1:
        {
            if ([_RemoteInfoDic objectForKey:@"name"]) {
                if (_NameFD) {
                    [_NameFD removeFromSuperview];
                    _NameFD = nil;
                }
                [_NameFD setPlaceholder:NSLocalizedString(@"请输入名称", nil)];
                _NameFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_NameFD setTextColor:[UIColor flatGrayColor]];
                if (([_RemoteInfoDic objectForKey:@"name"] != [NSNull null])) {
                    [_NameFD setText:[_RemoteInfoDic objectForKey:@"name"]];
                }
                [_NameFD setTextAlignment:NSTextAlignmentRight];
                [_NameFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_NameFD];
                _NameFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
                [[self.NameFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      self.navigationItem.rightBarButtonItem.enabled = NO;
                      return text.length > 0;
                  }]
                 subscribeNext:^(id x) {
                     if([self.NameFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.NameFD.text = [self.NameFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     if ((_ControllAraFD.text.length > 0)&&(_NameFD.text.length > 0)) {
                         self.navigationItem.rightBarButtonItem.enabled = YES;
                     }
                     [_RemoteInfoDic setObject:_NameFD.text forKey:@"name"];
                     NSLog(@"%@", x);
                 }];
            }
            
        }
            break;
        case 2:
        {
            if ([_RemoteInfoDic objectForKey:@"type"]) {
                cell.detailTextLabel.text = [_arrayData objectAtIndex:[[_RemoteInfoDic objectForKey:@"type"] intValue]];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
        }
            break;
        case 3:
        {
            if ([_RemoteInfoDic objectForKey:@"mode"]) {
                cell.detailTextLabel.text = [_ModeArrayData objectAtIndex:[[_RemoteInfoDic objectForKey:@"mode"] intValue]];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
        }
            break;
        case 4:
        {
            if ([_RemoteInfoDic objectForKey:@"delay"]) {
                if (_DelaySecFD) {
                    [_DelaySecFD removeFromSuperview];
                    _DelaySecFD = nil;
                }
                _DelaySecFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_DelaySecFD setTextColor:[UIColor flatGrayColor]];
                if (([_RemoteInfoDic objectForKey:@"delay"] != [NSNull null])) {
                    [_DelaySecFD setText:[_RemoteInfoDic objectForKey:@"delay"]];
                }
                [_DelaySecFD setKeyboardType:UIKeyboardTypeNumberPad];
                [_DelaySecFD setTextAlignment:NSTextAlignmentRight];
                [_DelaySecFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_DelaySecFD];
                _DelaySecFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 20.0f);
                [[self.DelaySecFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      [_RemoteInfoDic setObject:_DelaySecFD.text forKey:@"delay"];
                      return text.length > 1;
                  }]
                 subscribeNext:^(id x) {
                     if([self.DelaySecFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.DelaySecFD.text = [self.DelaySecFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     [_RemoteInfoDic setObject:_DelaySecFD.text forKey:@"delay"];
                     NSLog(@"%@", x);
                 }];
            }
            
        }
            break;

        case 5:
        {
            if ([_RemoteInfoDic objectForKey:@"siren"]) {
                if (_sirenSW) {
                    [_sirenSW removeFromSuperview];
                    _sirenSW = nil;
                }
                _sirenSW = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
                [_sirenSW setOn:[[_RemoteInfoDic objectForKey:@"siren"] boolValue]];
                cell.accessoryView = _sirenSW;
                @weakify(self)
                [[self.sirenSW
                  rac_signalForControlEvents:UIControlEventValueChanged]
                 subscribeNext:^(UISwitch * x) {
                     @strongify(self)
                     [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"siren"];
                 }];
            }
            
        }
            break;
        case 6:
        {
            if ([_RemoteInfoDic objectForKey:@"chime"]) {
                if (_chimeSW) {
                    [_chimeSW removeFromSuperview];
                    _chimeSW = nil;
                }
                _chimeSW = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
                cell.accessoryView = _chimeSW;
                if (_isReadyEnable) {
                    [_chimeSW setOn:YES];
                }
                else
                    [_chimeSW setOn:[[_RemoteInfoDic objectForKey:@"chime"] boolValue]];

                @weakify(self)
                [[self.chimeSW
                  rac_signalForControlEvents:UIControlEventValueChanged]
                 subscribeNext:^(UISwitch * x) {
                     @strongify(self)
                     NSLog(@"警号  ： %d",x.isOn);
                     [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"chime"];
                 }];
                if (_isDoorbellEnable) {
                    cell.textLabel.text = NSLocalizedString(@"短信提示", nil);
                }
            }
            
        }
            break;
        case 7:
        {
            if ([_RemoteInfoDic objectForKey:@"ready"]) {
                if (_ReadySW) {
                    [_ReadySW removeFromSuperview];
                    _ReadySW = nil;
                }
                _ReadySW = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
                [_ReadySW setOn:[[_RemoteInfoDic objectForKey:@"ready"] boolValue]];
                cell.accessoryView = _ReadySW;
                @weakify(self)
                [[_ReadySW
                  rac_signalForControlEvents:UIControlEventValueChanged]
                 subscribeNext:^(UISwitch * x) {
                     @strongify(self)
                     NSLog(@"非法设防  ： %d",x.isOn);
                     [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"ready"];
                 }];
                if (!_isReadyEnable) {
                    [cell.textLabel setTextColor:[UIColor darkTextColor]];
                    [_ReadySW setEnabled:YES];
                }
                else
                    {
                        [cell.textLabel setTextColor:FlatGray];
                        [_ReadySW setEnabled:NO];
                    }
            }
            
        }
            break;
        case 8:
        {
            if ([_RemoteInfoDic objectForKey:@"code"]) {
                if (_CodeFD) {
                    [_CodeFD removeFromSuperview];
                    _CodeFD = nil;
                }
                [cell.textLabel setText:NSLocalizedString(@"编码",nil)];
                _CodeFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_CodeFD setTextColor:[UIColor flatGrayColor]];
                [_CodeFD setText:[_RemoteInfoDic objectForKey:@"code"]];
                [_CodeFD setTextAlignment:NSTextAlignmentRight];
                [_CodeFD setKeyboardType:UIKeyboardTypeNumberPad];
                [_CodeFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_CodeFD];
                _CodeFD.sd_layout.xIs(cell.centerX*0.6).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 60.0f);
                [_CodeFD setKeyboardType:UIKeyboardTypeNumberPad];
                [[self.CodeFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      return YES;
                  }]
                 subscribeNext:^(id x) {
                     if([self.CodeFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.CodeFD.text = [self.CodeFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     [_RemoteInfoDic setObject:_CodeFD.text forKey:@"code"];
                     NSLog(@"%@", x);
                 }];
                _UidButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
                [_UidButton setImage:[UIImage imageNamed:@"QR_code_action"] forState:UIControlStateNormal];
                cell.accessoryView = _UidButton;
                [[self.UidButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
                    [self AddUid];
                }];
            }
        }
            break;

        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_NameFD resignFirstResponder];
    [_CodeFD resignFirstResponder];
    [_ControllAraFD resignFirstResponder];
    [_DelaySecFD resignFirstResponder];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 2:
        {
            STPickerSingle *TypePicker = [[STPickerSingle alloc]init];
            [TypePicker setArrayData:[NSMutableArray arrayWithArray:_arrayData]];
            [TypePicker setTitle:NSLocalizedString(@"选择类型", nil)];
            [TypePicker setTitleUnit:nil];
            [TypePicker setContentMode:STPickerContentModeBottom];
            [TypePicker setDelegate:self];
            [TypePicker setTag:4];
            [TypePicker show];
        }
            break;
        case 3:
        {
            STPickerSingle *TypePicker = [[STPickerSingle alloc]init];
            [TypePicker setArrayData:[NSMutableArray arrayWithArray:_ModeArrayData]];
            [TypePicker setTitle:NSLocalizedString(@"选择类型", nil)];
            [TypePicker setTitleUnit:nil];
            [TypePicker setContentMode:STPickerContentModeBottom];
            [TypePicker setDelegate:self];
            [TypePicker show];
        }
            break;
        default:
            break;
    }
}
- (NSString *)GetSensorTypeByIndexE:(NSString *)TypeIndex
{
    NSArray * numberString = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11"];
    if (TypeIndex&&TypeIndex.length > 2) {
        for (NSString * baseString in _arrayTypeData) {
            if ([TypeIndex containsString:baseString]) {
                return numberString[[_arrayTypeData indexOfObject:baseString]];
            }
        }
    }
    return nil;
}
- (void)AddUid
{
    NSString *cardName = @"2";
    UIImage *avatar = [UIImage imageNamed:@"avatar"];
    
    // 实例化扫描控制器
    HMScannerController *scanner = [HMScannerController scannerWithCardName:cardName avatar:avatar completion:^(NSString *stringValue) {
        
        NSLog(@"扫描后的 %@",stringValue);
       if( [self GetSensorTypeByIndexE:stringValue])
       {
           _CodeFD.text = [stringValue substringFromIndex:1];
           [_RemoteInfoDic setObject:[self GetSensorTypeByIndexE:stringValue] forKey:@"type"];
           [_RemoteInfoDic setObject:_CodeFD.text forKey:@"code"];
           [_MainTable reloadData];
       }
//        NSRange range = [stringValue rangeOfString:@"C"];
//        if (range.location != NSNotFound) {
//            _CodeFD.text = [stringValue substringFromIndex:range.location+range.length];
//            [_RemoteInfoDic setObject:@"0" forKey:@"type"];
//            [_MainTable reloadData];
//        }

        [UIView beginAnimations:@"View Flip" context:nil];
        //动画持续时间
        [UIView setAnimationDuration:1.25];
        //设置动画的回调函数，设置后可以使用回调方法
        [UIView setAnimationDelegate:self];
        //设置动画曲线，控制动画速度
        [UIView  setAnimationCurve: UIViewAnimationCurveEaseInOut];
        //设置动画方式，并指出动画发生的位置
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view  cache:YES];
        //提交UIView动画
        [UIView commitAnimations];
        
    }];
    // 设置导航栏样式
    [scanner setTitleColor:[UIColor whiteColor] tintColor:[UIColor greenColor]];
    
    // 展现扫描控制器
    [self showDetailViewController:scanner sender:nil];
}
- (void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle
{
    if (pickerSingle.tag == 4) {
        NSInteger index = [_arrayData indexOfObject:selectedTitle];
        [_RemoteInfoDic setObject:[NSString stringWithFormat:@"%ld",(long)index] forKey:@"type"];
        if ([selectedTitle isEqualToString:NSLocalizedString(@"门磁",nil)]) {
            //当只有用户选择类型为 Door Sensor(门磁) 时，才需要有 Ready （非法设防）显示出来，其它类型时 不需要显示出来，但提交JOSN时，默认ready 为 0
            _isReadyEnable = YES;
            _isDoorbellEnable = NO;
        }
        else if([selectedTitle isEqualToString:NSLocalizedString(@"门铃",nil)])
        {
            //当用户选择类型为 Doorbell（门铃）时，将 Chime，显示成 SMS，中文也改一下 将 “提示音” 改成 “短信提示”
            _isReadyEnable = NO;
            _isDoorbellEnable = YES;
        }
        else
        {
            _isDoorbellEnable = NO;
            _isReadyEnable = NO;
        }
    }
    else
    {
        NSInteger index = [_ModeArrayData indexOfObject:selectedTitle];
        [_RemoteInfoDic setObject:[NSString stringWithFormat:@"%ld",(long)index] forKey:@"mode"];
    }
    [_MainTable reloadData];
}
@end
