//
//  DaysSelectTableViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DaysSelectTableViewController : UITableViewController
@property (assign) NSInteger currentIndex;
@property (retain,nonatomic) NSMutableString * weekString;
@end
