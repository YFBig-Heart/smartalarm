//
//  AdminListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "AdminListViewController.h"
#import "BindDevceQRViewController.h"

@interface AdminListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (retain,nonatomic) NSArray * TitleArray;
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@property (retain,nonatomic) NSMutableArray * AdminArray;
@property (weak, nonatomic) IBOutlet UIButton *InvitBtn;
@end

@implementation AdminListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_TitleArray == nil) {
        _TitleArray = @[];
    }
    [_InvitBtn setTitle:NSLocalizedString(@"邀请管理成员", nil) forState:(UIControlStateNormal)];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd  target:self action:@selector(Done)];
    self.title = NSLocalizedString(@"管理成员", nil);
    if (_AdminArray == nil) {
        _AdminArray = [[NSMutableArray alloc] init];
    }
    [[_InvitBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        BindDevceQRViewController * vc =[[BindDevceQRViewController alloc] init];
        vc.QRCodeImei = [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    [_InvitBtn bs_configureAsPrimaryStyle];
    [self synAdmin];
}
- (void)Done
{

}
- (void)sortArray:(NSMutableArray *)arry
{
        if (arry && arry.count) {
            NSComparator finderSort = ^(id dic1,id dic2){
                if([[dic1 objectForKey:@"access"] intValue] == 1)
                {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                else if([[dic2 objectForKey:@"access"] intValue] == 1)
                {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                else
                    return (NSComparisonResult)NSOrderedSame;
            };
    
            //数组排序：
            NSArray *resultArray = [_AdminArray sortedArrayUsingComparator:finderSort];
            _AdminArray = [NSMutableArray arrayWithArray:resultArray];
        }
    [_MainTable reloadData];
}

- (void)synAdmin
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00061",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _AdminArray = [NSMutableArray arrayWithArray:[response objectForKey:@"data"]];
            [self sortArray:_AdminArray];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _AdminArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGesture:)];
        [cell addGestureRecognizer:longPressGesture];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:[[_AdminArray objectAtIndex:indexPath.row] objectForKey:@"user_name"]];
    if ([[[_AdminArray objectAtIndex:indexPath.row] objectForKey:@"admin"] intValue]) {
        //管理员
        [cell.detailTextLabel setText:NSLocalizedString(@"管理员", nil)];
    }
    else if ([[[_AdminArray objectAtIndex:indexPath.row] objectForKey:@"access"] intValue])
    {
        [cell.detailTextLabel setText:NSLocalizedString(@"操作员", nil)];
    }
    else
    {
        [cell.detailTextLabel setText:NSLocalizedString(@"普通用户", nil)];
    }
    [cell.imageView setImage:[UIImage imageNamed:@"list_admin"]];

    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    
    return cell;
}
- (void)longPressGesture:(UIGestureRecognizer *)recongnizer
{
    if (recongnizer.state == UIGestureRecognizerStateBegan)
    {
        CGPoint location = [recongnizer locationInView:self.MainTable];
        NSInteger index = [self.MainTable indexPathForRowAtPoint:location].row;
        UIAlertController * ct = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"您确定删除吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *showAllInfoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (index == 0) {
                return ;
            }
            [self DeleteAdmin:index];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消",nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [ct addAction:cancelAction];
        [ct addAction:showAllInfoAction];
        [self presentViewController:ct animated:YES completion:nil];
    }
}
- (void)DeleteAdmin:(NSInteger )index
{
    NSDictionary * tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[_AdminArray objectAtIndex:index] objectForKey:@"user_name"],@"user",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"admin",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"90061",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self synAdmin];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    UIAlertController * ct = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"您确定删除吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *showAllInfoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        if (indexPath.row == 0) {
//            return ;
//        }
//        [self DeleteAdmin:indexPath.row];
//    }];
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消",nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//
//    }];
//    [ct addAction:cancelAction];
//    [ct addAction:showAllInfoAction];
//    [self presentViewController:ct animated:YES completion:nil];

}
@end
