//
//  AboutOurViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/11/21.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "AboutOurViewController.h"
#import "ProtolViewController.h"
@interface AboutOurViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UIImageView * logoImage;
@property (retain,nonatomic) UILabel * TitleLabel;
@property (retain,nonatomic) UILabel * SWLabel;
@end

@implementation AboutOurViewController

- (void)viewDidLoad {
    if (_TitleArray == nil) {
//        _TitleArray =  @[@"软件许可证使用协议",@"APP版本"];
        _TitleArray =  @[@"APP版本"];

    }
    self.title = NSLocalizedString(@"关于我们", nil);
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    _logoImage = [[UIImageView alloc] initWithFrame:CGRectZero];
    [_logoImage setImage:[UIImage imageNamed:@"Launch_img"]];
    [_logoImage setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:_logoImage];
 _logoImage.sd_layout.heightIs(80.0f).widthIs(80.0f).centerXEqualToView(self.view).bottomSpaceToView(_MainTable, 60);
    
    _TitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 20)];
    [_TitleLabel setText:NSLocalizedString(@"智能报警", nil)];
    [_TitleLabel setTextAlignment:NSTextAlignmentCenter];
    [_TitleLabel sizeToFit];
    [self.view addSubview:_TitleLabel];
    _TitleLabel.sd_layout.centerXEqualToView(self.view).heightIs(18).topSpaceToView(_logoImage, 10);
    
    _SWLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 20)];
    [_SWLabel setText:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    [_SWLabel setTextAlignment:NSTextAlignmentCenter];
    [_SWLabel setFont:[UIFont systemFontOfSize:12]];
    [_SWLabel sizeToFit];
    [_SWLabel setTextColor:FlatGrayDark];
    [self.view addSubview:_SWLabel];
    _SWLabel.sd_layout.centerXEqualToView(self.view).heightIs(18).topSpaceToView(_TitleLabel, 5);

    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString(_TitleArray[indexPath.row], nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
//            [self.navigationController pushViewController:[[ProtolViewController alloc] init] animated:YES];
        }
            break;
        case 1:
//            [self.navigationController pushViewController:[[ProtolViewController alloc] init] animated:YES];
            break;
        default:
            break;
    }
}

@end
