//
//  RegisterForPasswordViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/11/12.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "RegisterForPasswordViewController.h"
#import "RootTabViewController.h"

@interface RegisterForPasswordViewController ()
@property (weak, nonatomic) IBOutlet UILabel *TitleLable;
@property (weak, nonatomic) IBOutlet UITextField *PasswordFD;
@property (weak, nonatomic) IBOutlet UITextField *RePasswordFD;
@property (weak, nonatomic) IBOutlet UIButton *DoneBtn;
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;
@end

@implementation RegisterForPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_TitleLabel setText:NSLocalizedString(@"设置密码", nil)];
    
    [_TitleLable setText:NSLocalizedString(@"创建至少有6个字符的密码。", nil)];
    _DoneBtn.showsTouchWhenHighlighted = YES;
    [[_DoneBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        if (_UseType) {
            [self FindPassword];
        }
        else
        [self doNext];
    }];
    [_PasswordFD setPlaceholder:NSLocalizedString(@"请输入您的密码", nil)];
    [_RePasswordFD setPlaceholder:NSLocalizedString(@"请再次输入您的密码", nil)];
    [_DoneBtn bs_configureAsPrimaryStyle];
    [_DoneBtn setTitle:NSLocalizedString(@"注册",nil) forState:UIControlStateNormal];
    [[_PasswordFD.rac_textSignal
      filter:^BOOL(id value) {
          NSString *text = value;
          return text.length > 0;
      }]
     subscribeNext:^(id x) {
         NSString *text = x;
     }];
    [[_RePasswordFD.rac_textSignal
      filter:^BOOL(id value) {
          NSString *text = value;
          return text.length > 0;
      }]
     subscribeNext:^(id x) {
         NSString *text = x;
     }];
}
- (void)doNext
{
    if (([_PasswordFD.text length] == 0)||([_PasswordFD.text isEqualToString:NSLocalizedString(@"请输入密码", nil)] )) {
        _PasswordFD.attributedPlaceholder = [TextFeildAlart TextFeildAlart:NSLocalizedString(@"请输入密码", nil) TextColor:FlatRed];
        return;
    }
    if (([_RePasswordFD.text length] == 0)||([_RePasswordFD.text isEqualToString:NSLocalizedString(@"请再次输入您的密码", nil)] )) {
        _RePasswordFD.attributedPlaceholder = [TextFeildAlart TextFeildAlart:NSLocalizedString(@"请再次输入您的密码", nil) TextColor:FlatRed];
        return;
    }
    if (![_PasswordFD.text isEqualToString:_RePasswordFD.text]) {
        _RePasswordFD.text = nil;
        _PasswordFD.text = nil;
        _PasswordFD.attributedPlaceholder = [TextFeildAlart TextFeildAlart:NSLocalizedString(@"密码不一致", nil) TextColor:FlatRed];;
        _RePasswordFD.attributedPlaceholder = [TextFeildAlart TextFeildAlart:NSLocalizedString(@"密码不一致", nil) TextColor:FlatRed];;
        return;
    }
    if ([_PasswordFD.text length] < 6) {
        _RePasswordFD.text = nil;
        _PasswordFD.text = nil;
        _RePasswordFD.attributedPlaceholder = [TextFeildAlart TextFeildAlart:NSLocalizedString(@"密码应为 6-16 位", nil) TextColor:FlatRed];
        _PasswordFD.attributedPlaceholder = [TextFeildAlart TextFeildAlart:NSLocalizedString(@"密码应为 6-16 位", nil) TextColor:FlatRed];

        return;
    }
    
//    if (_DeviceID) {
    if (1) {
        NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:_userID,@"name",_PasswordFD.text,@"password",nil];
        NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
        NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80001",@"cmd",dicArray,@"data", nil];
        NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
        NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
        [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
            //OK
            NSLog(@"%@",response);
            if ([[response objectForKey:@"ret"] intValue] != 0) {
                [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:_userID forKey:kDefaultUserID];
                [[NSUserDefaults standardUserDefaults] setObject:_PasswordFD.text forKey:kDefaultUserPsw];
                [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
                [self.view.window makeKeyAndVisible];
            }
        } failure:^(NSError *error) {
            //fail
            NSLog(@"fail %@",error);
            [HttpRespondError ShowHttpRespondError:-999];
        }];
    }
//    else
//    {
//        NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:_userID,@"name",_PasswordFD.text,@"password",[GetSysLanguage GetSystemLanguage],@"language", nil];
//        NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
//        NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00002",@"cmd",dicArray,@"data", nil];
//        NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
//        NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
//        [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
//            //OK
//            NSLog(@"%@",response);
//            if ([[response objectForKey:@"ret"] intValue] != 0) {
//                [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
//            }
//            else
//            {
//                [[NSUserDefaults standardUserDefaults] setObject:_userID forKey:kDefaultUserID];
//                [[NSUserDefaults standardUserDefaults] setObject:_PasswordFD.text forKey:kDefaultUserPsw];
//                [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
//                [[UIApplication sharedApplication] registerForRemoteNotifications];
//            }
//        } failure:^(NSError *error) {
//            //fail
//            NSLog(@"fail %@",error);
//            [HttpRespondError ShowHttpRespondError:-999];
//        }];
//    }
}
- (void)FindPassword
{
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:_userID,@"name",_PasswordFD.text,@"password",_DeviceID,@"code",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"10010",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [[NSUserDefaults standardUserDefaults] setObject:_userID forKey:kDefaultUserID];
            [[NSUserDefaults standardUserDefaults] setObject:_PasswordFD.text forKey:kDefaultUserPsw];
            [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
            [self.view.window makeKeyAndVisible];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
