//
//  APNSettingViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "APNSettingViewController.h"
#import <MessageUI/MessageUI.h>
#import "PreEntryIdleViewController.h"
#import "RootTabViewController.h"
@interface APNSettingViewController ()<UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UITextField * APNFD;
@property (retain,nonatomic) UITextField * UserNameFD;
@property (retain,nonatomic) UITextField * PasswordFD;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@end

@implementation APNSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_TitleArray == nil) {
        _TitleArray = @[@"APN",@"用户名",@"密码"];
    }
    _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultAPN]];
    self.title = NSLocalizedString(@"网络", nil);
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone  target:self action:@selector(Done)];
    [self synDeviceAPN];

    [[UINavigationBar appearance] setBackgroundImage:[self imageWithColor:FlatSkyBlue] forBarMetrics:UIBarMetricsDefault];
    if (_isHideBackButton) {
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel  target:self action:@selector(Cance)];
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
}
- (void)Cance
{
    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
    [self.view.window makeKeyAndVisible];
}
- (void)Done
{
    /* 860755#APN#APN,NAME,PASSWORD# */
    if ([self canSendSMS] && _SmsSendNumber) {
        NSString * sendContent = [NSString stringWithFormat:@"860755#APN#%@,%@,%@",_APNFD.text,_UserNameFD.text,_PasswordFD.text];
        [self sendSMS:sendContent recipient:_SmsSendNumber];
    }
}
- (void)synDeviceAPN
{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreen_Width, 100)];
    footView.backgroundColor = [UIColor clearColor];
    UIButton *sureButton  = [[UIButton alloc]initWithFrame:CGRectMake(80, 25, kScreen_Width-80, 50)];
    sureButton.layer.cornerRadius = 2.0f;
    sureButton.layer.masksToBounds = YES;
    [sureButton setTitle:NSLocalizedString(@"确定", nil) forState:UIControlStateNormal];
    [sureButton addTarget:self action:@selector(Done) forControlEvents:UIControlEventTouchUpInside];
    sureButton.titleLabel.textColor = [UIColor grayColor];
    sureButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [sureButton bs_configureAsPrimaryStyle];
    [footView addSubview:sureButton];
    sureButton.sd_layout.centerXEqualToView(self.view);
    
    return footView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 100;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            if (_APNFD) {
                [_APNFD removeFromSuperview];
                _APNFD = nil;
            }
            _APNFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_APNFD setTextColor:[UIColor flatGrayColor]];
            if ([_RemoteInfoDic objectForKey:@"apn"]) {
                [_APNFD setText:[_RemoteInfoDic objectForKey:@"apn"]];
            }
            [_APNFD setPlaceholder:NSLocalizedString(@"请输入接入点名称", nil)];
            [_APNFD setTextAlignment:NSTextAlignmentRight];
            [_APNFD setBackgroundColor:[UIColor clearColor]];
            [cell addSubview:_APNFD];
            _APNFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[self.APNFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return text.length > 1;
              }]
             subscribeNext:^(id x) {
                 if([self.APNFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.APNFD.text = [self.APNFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 [_RemoteInfoDic setObject:_APNFD.text forKey:@"apn"];
                 NSLog(@"%@", x);
             }];
        }
            break;
        case 1:
        {
            if (_UserNameFD) {
                [_UserNameFD removeFromSuperview];
                _UserNameFD = nil;
            }
            _UserNameFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_UserNameFD setTextColor:[UIColor flatGrayColor]];
            if ([_RemoteInfoDic objectForKey:@"apn"]) {
                [_UserNameFD setText:[_RemoteInfoDic objectForKey:@"user"]];
            }
            [_UserNameFD setPlaceholder:NSLocalizedString(@"请输入GPRS帐户", nil)];
            [_UserNameFD setTextAlignment:NSTextAlignmentRight];
            [_UserNameFD setBackgroundColor:[UIColor clearColor]];
            [cell addSubview:_UserNameFD];
            _UserNameFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[self.UserNameFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  return text.length > 0;
              }]
             subscribeNext:^(id x) {
                 if([self.UserNameFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.UserNameFD.text = [self.UserNameFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
                 [_RemoteInfoDic setObject:_UserNameFD.text forKey:@"user"];
                 NSLog(@"%@", x);
             }];
        }
            break;
        case 2:
        {
            if (_PasswordFD) {
                [_PasswordFD removeFromSuperview];
                _PasswordFD = nil;
            }
            _PasswordFD = [[UITextField alloc] initWithFrame:CGRectZero];
            [_PasswordFD setTextColor:[UIColor flatGrayColor]];
            [_PasswordFD setText:[_RemoteInfoDic objectForKey:@"password"]];
            [_PasswordFD setTextAlignment:NSTextAlignmentRight];
            [_PasswordFD setBackgroundColor:[UIColor clearColor]];
            [_PasswordFD setPlaceholder:NSLocalizedString(@"请输入GPRS帐户密码", nil)];
            [cell addSubview:_PasswordFD];
            _PasswordFD.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 5.0f);
            [[self.PasswordFD.rac_textSignal
              filter:^BOOL(id value) {
                  NSString *text = value;
                  [_RemoteInfoDic setObject:_PasswordFD.text forKey:@"password"];
                  return text.length > 1;
              }]
             subscribeNext:^(id x) {
                 NSLog(@"%@", x);
                 if([self.PasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                 {
                     self.PasswordFD.text = [self.PasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                 }
             }];
        }
            break;
        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
            break;
        default:
            break;
    }
}
#pragma mark - MFMessageComposeViewController

- (BOOL) canSendSMS {
    return [MFMessageComposeViewController canSendText];
}

- (void) sendSMS:(NSString *)message {
    [self sendSMS:message recipients:nil];
}

- (void) sendSMS:(NSString *)message recipient:(NSString*)recipient {
    if (recipient != nil) {
        [self sendSMS:message recipients:[NSArray arrayWithObject:recipient]];
    }
    else {
        [self sendSMS:message recipients:nil];
    }
}
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}
- (UIImage *)imageWithColor:(UIColor *)color {
    static NSCache *imageCache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        imageCache = [[NSCache alloc] init];
    });
    
    UIImage *image = [imageCache objectForKey:color];
    if (image) {
        return image;
    }
    
    image = [self imageWithColor:color size:CGSizeMake(1,1)];
    [imageCache setObject:image forKey:color];
    
    return image;
}
-  (void)doAction:(NSNotification *)notification {
    
    NSLog(@"object = %@",notification.object);
    
    NSLog(@"name = %@",notification.name);
    
    NSLog(@"userInfo = %@",notification.userInfo);
    [[RACScheduler mainThreadScheduler]afterDelay:0.2 schedule:^{
        PreEntryIdleViewController * vc = [[PreEntryIdleViewController alloc] init];
        vc.BindDeviceImei = [NSString stringWithString:_currIMEI];
        [self.navigationController pushViewController:vc animated:YES];
    }];
}
- (void) sendSMS:(NSString *)message recipients:(NSArray*)recipients {
    if ([self canSendSMS]) {
        MFMessageComposeViewController *smsController = [[MFMessageComposeViewController alloc] init];
        smsController.messageComposeDelegate = self;
        if (message != nil) {
            smsController.body = message;
        }
        if (recipients != nil) {
            smsController.recipients = recipients;
        }
        [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(doAction:) name:@"smsCbk" object:nil];
        smsController.messageComposeDelegate = self;
        [self presentViewController:smsController animated:YES completion:nil];
    }
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    if (result == MessageComposeResultSent) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionaryWithDictionary:_RemoteInfoDic] forKey:kDefaultAPN];
        [self dismissViewControllerAnimated:YES completion:nil];
        if (_EntryType == 1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"smsCbk" object:@"1" userInfo:nil];
        }
    }
    else if (result == MessageComposeResultFailed) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (result == MessageComposeResultCancelled) {
        if (_EntryType == 1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"smsCbk" object:@"1" userInfo:nil];
        }
        [[UINavigationBar appearance] setBackgroundImage:[self imageWithColor:FlatSkyBlue] forBarMetrics:UIBarMetricsDefault];
            [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
@end
