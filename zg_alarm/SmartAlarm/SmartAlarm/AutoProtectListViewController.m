//
//  AutoProtectListViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/6.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "AutoProtectListViewController.h"
#import "TimeListTableViewCell.h"
#import "EditViewController.h"
#import "TimeEditViewController.h"
#import "TimerEditViewController.h"

@interface AutoProtectListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSMutableDictionary * editBoardDic;
@property (retain,nonatomic)NSArray * AutoSetting;
@property (retain,nonatomic) NSMutableArray * TimeListArray;
@property (retain,nonatomic) NSMutableDictionary * BoardDic;
@property (retain,nonatomic) UIButton * DeleteAllBtn;
@end

@implementation AutoProtectListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    _AutoSetting = @[NSLocalizedString(@"停用", nil),NSLocalizedString(@"自动设防", nil ),NSLocalizedString(@"自动撤防", nil),NSLocalizedString(@"自动在家", nil)];
    self.title =NSLocalizedString(@"自动设/撤防", nil);
    _DeleteAllBtn = [[UIButton alloc] initWithFrame:CGRectZero];
    [self.view addSubview:_DeleteAllBtn];
    [_DeleteAllBtn setTitle:NSLocalizedString(@"删除所有", nil) forState:UIControlStateNormal];
    _DeleteAllBtn.sd_layout.leftSpaceToView(self.view, 16).rightSpaceToView(self.view, 16).heightIs(40).bottomSpaceToView(self.view, 20);
    [_DeleteAllBtn bs_configureAsPrimaryStyle];
    [[_DeleteAllBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self DeleteClick];
    }];
}
- (void)DeleteClick
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"90071",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [self synTimeList];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    [self synTimeList];
}
- (void)synTimeList
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00071",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _TimeListArray = [NSMutableArray arrayWithArray:[response objectForKey:@"data"]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TimeListArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TimeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimeListTableViewCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TimeListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    NSDictionary * TimeDic = [_TimeListArray objectAtIndex:indexPath.row];
    cell.TimeLabel.text = [TimeDic objectForKey:@"timer"];
    NSString * OnoffString = [_AutoSetting objectAtIndex:[[TimeDic objectForKey:@"type"] intValue]];
    cell.OnoffLabel.text = OnoffString;
    NSArray * weekDayArray = @[@"周日",@"周一",@"周二",@"周三",@"周四",@"周五",@"周六"];
    NSString * weekDayString = [TimeDic objectForKey:@"week"];
    if (![weekDayString isEqualToString:@"0000000"]) {
        for (int v=0,j=0; v < 7; v++) {
            if ([[weekDayString substringWithRange:NSMakeRange(v, 1)] intValue]) {
                UILabel * dayLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 25)];
                dayLable.font = [UIFont systemFontOfSize:11];
                dayLable.text = NSLocalizedString(weekDayArray[v], nil);
                [cell addSubview:dayLable];
                [dayLable sizeToFit];
                dayLable.sd_layout.xIs(15+j*30).centerYIs(cell.centerY*1.2);
                j++;
            }
        }
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TimerEditViewController * vc = [[TimerEditViewController alloc] init];
    vc.BoardDic = [[NSMutableDictionary alloc] initWithDictionary:[_TimeListArray objectAtIndex:indexPath.row]];
    vc.currentIndex = indexPath.row + 1;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
