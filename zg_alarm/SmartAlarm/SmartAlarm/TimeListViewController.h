//
//  TimeListViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeListViewController : UIViewController
@property (retain,nonatomic) NSMutableArray * TimeListArray;
@property (retain,nonatomic) NSMutableDictionary * BoardDic;
@end
