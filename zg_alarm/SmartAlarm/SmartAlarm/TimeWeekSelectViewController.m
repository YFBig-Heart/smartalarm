//
//  TimeWeekSelectViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/28.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "TimeWeekSelectViewController.h"

@interface TimeWeekSelectViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) NSMutableString * weekString;
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@end

@implementation TimeWeekSelectViewController

- (void)viewDidLoad {
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    _TitleArray = @[@"周日",@"周一",@"周二",@"周三",@"周四",@"周五",@"周六"];
    if (_TimeDic) {
        _weekString = [[NSMutableString alloc] initWithString:[_TimeDic objectForKey:@"week"]];
    }
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _TitleArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.textLabel.text = NSLocalizedString([_TitleArray objectAtIndex:indexPath.row], nil);
    if ([[_weekString substringWithRange:NSMakeRange(indexPath.row, 1)] intValue] == 1) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if([[_weekString substringWithRange:NSMakeRange(indexPath.row, 1)] intValue] == 0)
    {
        _weekString = [NSMutableString stringWithString:[_weekString stringByReplacingCharactersInRange:NSMakeRange(indexPath.row, 1) withString:@"1"]];
    }
    else
        _weekString = [NSMutableString stringWithString:[_weekString stringByReplacingCharactersInRange:NSMakeRange(indexPath.row, 1) withString:@"0"]];
    [_TimeDic setObject:_weekString forKey:@"week"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionaryWithDictionary:_TimeDic] forKey:@"TimeDicKEy"];
    [_MainTable reloadData];
}
@end
