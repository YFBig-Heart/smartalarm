//
//  WeekEditViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeekEditViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *MainTable;

@end
