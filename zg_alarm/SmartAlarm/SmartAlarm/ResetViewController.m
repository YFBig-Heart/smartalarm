//
//  ResetViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "ResetViewController.h"

@interface ResetViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MianTable;
@property (retain,nonatomic) NSArray * ImageArray;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) NSMutableString * opSeting;
@property (retain,nonatomic) NSMutableDictionary* selectIndexDic;
@property (weak, nonatomic) IBOutlet UIButton *RestoreBTN;

@end

@implementation ResetViewController

- (void)viewDidLoad {
    _MianTable.delegate = self;
    _MianTable.dataSource = self;
    
    NSLog(@"-----[CoreHTTPRequest GetDeviceType]=%ld",[CoreHTTPRequest GetDeviceType]);
    if (([CoreHTTPRequest GetDeviceType] != 1 &&([CoreHTTPRequest GetDeviceType] != 2))) {
        _selectIndexDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"0",@"0",@"0",@"1",@"0",@"2",@"1",@"3", nil];
        _TitleArray = @[@"删除所有遥控器",@"删除所有传感器",@"恢复出厂设置"];
    }
    else
    {
        _selectIndexDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"0",@"0",@"0",@"1",@"0",@"2",@"1",@"3", nil];
        _TitleArray = @[@"删除所有遥控器",@"删除所有传感器",@"删除所有RFID",@"恢复出厂设置"];
    }
    self.title = NSLocalizedString(@"恢复出厂设置", nil);
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_RestoreBTN bs_configureAsPrimaryStyle];
    [_RestoreBTN setTitle:NSLocalizedString(@"恢复出厂设置", nil) forState:UIControlStateNormal];
    [[_RestoreBTN rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"确定恢复出厂设置？恢复出厂设置后需要重新配制系统", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"取消",nil) otherButtonTitles:NSLocalizedString(@"确定",nil), nil];
        [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
            NSLog(@"%@",x);
            NSInteger  ClickIndex = [(NSNumber *)x integerValue] ;
            if (ClickIndex == 1) {
                [self Done];
            }
        }];
        [alertView show];
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    if ([_selectIndexDic count] == 0) {
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    }
}
- (void)Done
{
    NSDictionary * tDic ;
    NSString * KeyString = [NSString stringWithFormat:@"%@%@%@%@",[_selectIndexDic objectForKey:@"0"],[_selectIndexDic objectForKey:@"1"],[_selectIndexDic objectForKey:@"2"],[_selectIndexDic objectForKey:@"3"]];
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",KeyString,@"op",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00086",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated
{
    if (([CoreHTTPRequest GetDeviceType] != 1)&&([CoreHTTPRequest GetDeviceType] != 2)) {
        [_selectIndexDic setObject:@"1" forKey:[NSString stringWithFormat:@"%d",2]];
    }
    else
        [_selectIndexDic setObject:@"1" forKey:[NSString stringWithFormat:@"%d",3]];
    [_MianTable reloadData];
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    if ((([CoreHTTPRequest GetDeviceType] != 1) &&([CoreHTTPRequest GetDeviceType] != 2)) && (indexPath.row == (_TitleArray.count-1))) {
        if ([[_selectIndexDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row+1]] isEqualToString:@"1"]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    else
    {
        if ([[_selectIndexDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] isEqualToString:@"1"]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    
//    if(([CoreHTTPRequest GetDeviceType] == OUTLETS_INDEX)&&(indexPath.row > 1))
//    {
//        [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row+1],nil)];
//        cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
//        if ([[_selectIndexDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] isEqualToString:@"1"]) {
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }
//    }
//    else
//    {
//        [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
//        cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
//        if ([[_selectIndexDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] isEqualToString:@"1"]) {
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }
//    }
    return cell;
}
- (void)ClearALLopVaule
{
    _selectIndexDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"0",@"0",@"0",@"1",@"0",@"2",@"0",@"3", nil];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
//    if (([CoreHTTPRequest GetDeviceType] != 1) && ([CoreHTTPRequest GetDeviceType] != 2)) && (indexPath.row == (_TitleArray.count-1))) {
    if ((([CoreHTTPRequest GetDeviceType] != 1)&&([CoreHTTPRequest GetDeviceType] != 2)) && (indexPath.row == (_TitleArray.count-1))) {
        //无RFID
        if ([[_selectIndexDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row+1]] isEqualToString:@"1"]) {
            [self ClearALLopVaule];
            [_selectIndexDic setObject:@"0" forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row+1]];
        }
        else
        {
            [self ClearALLopVaule];
            [_selectIndexDic setObject:@"1" forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row+1]];
        }

    }
    else{
        if ([[_selectIndexDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] isEqualToString:@"1"]) {
            [self ClearALLopVaule];
            [_selectIndexDic setObject:@"0" forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        }
        else
        {
            [self ClearALLopVaule];
            [_selectIndexDic setObject:@"1" forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [_MianTable reloadData];
}

@end
