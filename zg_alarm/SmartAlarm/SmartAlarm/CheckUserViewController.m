//
//  CheckUserViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "CheckUserViewController.h"
#import "CheckPswViewController.h"
#import "BindDeviceViewController.h"
#import "CoreHTTPRequest.h"
#import "BindDeviceViewController.h"
#import "EditServerViewController.h"
#import "NewUserAskViewController.h"

@interface CheckUserViewController ()
@property (weak, nonatomic) IBOutlet UITextField *UserIDFD;
@property (weak, nonatomic) IBOutlet UIButton *NextBTN;

@end

@implementation CheckUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"智能报警", nil);
    _NextBTN.showsTouchWhenHighlighted = YES;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID] != nil) {
        [_UserIDFD setText:[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID]];
    }
    [_UserIDFD setPlaceholder:NSLocalizedString(@"请输入邮箱地址:example@example.com", nil)];
    
#ifndef __HIDE_SERVER__
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"服务器", nil) style:UIBarButtonItemStylePlain target:self action:@selector(Server)];
#endif
    [_NextBTN bs_configureAsPrimaryStyle];
    [[self.UserIDFD.rac_textSignal
      filter:^BOOL(id value) {
          NSString *text = value;
          _UserIDFD.attributedPlaceholder=[TextFeildAlart TextFeildAlart: NSLocalizedString(@"请输入邮箱地址:example@example.com", nil) TextColor:FlatRed];
          return text.length > 1;
      }]
     subscribeNext:^(id x) {
         NSLog(@"%@", x);
         _UserIDFD.attributedPlaceholder=[TextFeildAlart TextFeildAlart: NSLocalizedString(@"请输入邮箱地址:example@example.com", nil) TextColor:FlatRed];
         if([self.UserIDFD.text rangeOfString:@" "].location !=NSNotFound)
         {
             self.UserIDFD.text = [self.UserIDFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
         }
     }];
    [[self.NextBTN rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self Donext];
    }];
}
- (void)Server
{
    [self.navigationController pushViewController:[[EditServerViewController alloc] init] animated:YES];
}
- (void)Donext
{
    if ([_UserIDFD.text length] == 0) {
        _UserIDFD.attributedPlaceholder=[TextFeildAlart TextFeildAlart: NSLocalizedString(@"请输入邮箱地址:example@example.com", nil) TextColor:FlatRed];
        return;
    }
    
    NSDictionary * tDic = [[NSDictionary alloc] initWithObjectsAndKeys:_UserIDFD.text,@"number", nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00050",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] == 1) {
//            BindDeviceViewController * vc = [[BindDeviceViewController alloc] init];
//            vc.userID = [_UserIDFD.text copy];
//            vc.TitleString = NSLocalizedString(@"注册", nil);
//            [self.navigationController pushViewController:vc animated:YES];
            NewUserAskViewController * vc = [[NewUserAskViewController alloc] init];
            vc.VailEmail = [_UserIDFD.text copy];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if ([[response objectForKey:@"ret"] intValue] == 1019)
        {
            [HttpRespondError ShowHttpRespondError:1019];
            [_UserIDFD setText:nil];
            _UserIDFD.attributedPlaceholder = [TextFeildAlart TextFeildAlart:NSLocalizedString(@"错误的邮箱地址", nil) TextColor:FlatRed];
        }
        else
        {
            CheckPswViewController * vc = [[CheckPswViewController alloc] init];
            vc.userID = [_UserIDFD.text copy];
            vc.StatuesLabel.text = NSLocalizedString(@"你的邮箱已注册,可直接登录", nil);
            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
