//
//  AppSettingViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "AppSettingViewController.h"
#import "CheckUserViewController.h"
#import "ChangePasswordViewController.h"
#import "ProtolViewController.h"
#import "AboutOurViewController.h"
#import "APNsViewController.h"
@interface AppSettingViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (retain,nonatomic) NSArray * TitleArray;
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (weak, nonatomic) IBOutlet UIButton *LoginOutBtn;
@property (retain,nonatomic) UISwitch *APNsSW;
@end

@implementation AppSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_TitleArray == nil) {
        _TitleArray =  @[@"消息提醒",@"修改密码",@"关于智能报警"];
    }
    self.title = NSLocalizedString(@"APP系统设置", nil);
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    [[_LoginOutBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self doLoginOut];
    }];
    [_LoginOutBtn setTitle:NSLocalizedString(@"退出登录", nil) forState: UIControlStateNormal];
}
- (void)doLoginOut
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kDefaultUserPsw];
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    UINavigationController * LoginOrRegisterNav = [[UINavigationController alloc] initWithRootViewController:[[CheckUserViewController alloc] init]];
    [self.view.window setRootViewController:LoginOrRegisterNav];
}
- (void)viewReload
{
//    if(![NotificationUtil isEnabledNotification] && [self.view isTopViewInWindow])
//    {
//        [RKDropdownAlert title:nil message:NSLocalizedString(@"您没有打开推送通知，无法接受推送报警消息!", nil)];
//        [_APNsSW setOn:NO animated:YES];
//    }
}
- (void)viewWillAppear:(BOOL)animated
{
//    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(viewReload) name:@"ReloadAPNsView" object:nil];
//    if(![NotificationUtil isEnabledNotification])
//    {
//        [RKDropdownAlert title:nil message:NSLocalizedString(@"您没有打开推送通知，无法接受推送报警消息!", nil)];
//        [_APNsSW setOn:NO animated:YES];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString(_TitleArray[indexPath.row], nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    switch (indexPath.row) {
        case 0:
        {
            if (_APNsSW) {
                [_APNsSW removeFromSuperview];
                _APNsSW = nil;
            }
            _APNsSW = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
//            cell.accessoryView = _APNsSW;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//            [_APNsSW setOn:([[NSUserDefaults standardUserDefaults] boolForKey:kDefaultAPNs]?YES:NO)];
//            [[_APNsSW
//              rac_signalForControlEvents:UIControlEventValueChanged]
//             subscribeNext:^(UISwitch * x) {
//                 if (!x.isOn) {
//                     [[UIApplication sharedApplication] unregisterForRemoteNotifications];
//                 }
//                 else
//                 {
//                     if (([NotificationUtil isEnabledNotification])) {
//                         [[UIApplication sharedApplication] registerForRemoteNotifications];
//                     }
//                     else
//                     {
////                         [RKDropdownAlert title:nil message:NSLocalizedString(@"请到 系统-设置-通知菜单打开推送通知", nil)];
//                     }
//                 }
//                 [[NSUserDefaults standardUserDefaults] setBool:x.isOn forKey:kDefaultAPNs];
//                 NSLog(@"开关  ： %d",x.isOn);
//             }];
//            if(![NotificationUtil isEnabledNotification])
//            {
//                [_APNsSW setOn:NO animated:YES];
//            }
//            [_APNsSW setEnabled:NO];
        }
            break;
            
        default:
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
            [self.navigationController pushViewController:[[APNsViewController alloc] init] animated:YES];
        }
            break;
        case 1:
        {
            [self.navigationController pushViewController:[[ChangePasswordViewController alloc] init] animated:YES];
        }
            break;
        case 2:
                        [self.navigationController pushViewController:[[AboutOurViewController alloc] init] animated:YES];
            break;
        default:
            break;
    }
}

@end
