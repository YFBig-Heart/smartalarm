//
//  APNsViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/12/12.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "APNsViewController.h"

@interface APNsViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *DitalLable;
@property (weak, nonatomic) IBOutlet UILabel *NewAPNsTitle;
@property (weak, nonatomic) IBOutlet UILabel *APNsStatuesTitle;
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (weak, nonatomic) IBOutlet UIButton *ButtonSysSetting;
@property (retain,nonatomic) UISwitch * AlarmSoundSW;
@property (retain,nonatomic) UISwitch * EmailSW;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@property (retain,nonatomic) NSArray * CellTitleArray;
@end

@implementation APNsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _RemoteInfoDic = [NSMutableDictionary new];
    [_RemoteInfoDic setObject:@"1" forKey:@"alarm_switch"];
    [_RemoteInfoDic setObject:@"1" forKey:@"email_switch"];
    [_ButtonSysSetting setTitle:NSLocalizedString(@"系统设置", nil) forState: UIControlStateNormal];
    [[_ButtonSysSetting rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self Done];
    }];
    _CellTitleArray = @[@"报警音",@"邮箱通知"];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"系统设置", nil) style:UIBarButtonItemStylePlain target:self action:@selector(Done)];
    [_DitalLable setText:NSLocalizedString(@"如果要开启或关闭推送功能，请到 IOS系统设置->通知 找到本APP进行设置", nil)];
    [_NewAPNsTitle setText:NSLocalizedString(@"接受新的消息通知",nil)];
    [_APNsStatuesTitle setText:NSLocalizedString(@"已开启",nil)];
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
//    UIView * heardView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 80)];
//    [heardView setBackgroundColor:FlatRed];
//    _MainTable.tableHeaderView = heardView;
//    _MainTable.tableHeaderView.sd_layout.heightIs(160).widthRatioToView(self.view, 1.0).xIs(0).topSpaceToView(self.navigationController.navigationBar, 10);
}
- (void)viewReload
{
    if(![NotificationUtil isEnabledNotification] && [self.view isTopViewInWindow])
    {
        [_APNsStatuesTitle setText:NSLocalizedString(@"已关闭",nil)];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(viewReload) name:@"ReloadAPNsView" object:nil];
    if(![NotificationUtil isEnabledNotification])
    {
        [_APNsStatuesTitle setText:NSLocalizedString(@"已关闭",nil)];
    }
    [self SynSwitchStatues];

}
- (void)ApplicationTurn:(NSString *)urlString
{
    if( [[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:urlString]] ) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlString] options:@{}completionHandler:^(BOOL        success) {
        }];
    }
}
- (void)Done
{
    //[self ApplicationTurn:@"App-prefs:root=com.wongshan.zg.smart"];
    //iOS系统版本 >= iOS10
    [self ApplicationTurn:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.textLabel.text = NSLocalizedString(_CellTitleArray[indexPath.row], nil);
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    switch (indexPath.row) {
        case 0:
        {
            if (_AlarmSoundSW) {
                [_AlarmSoundSW removeFromSuperview];
                _AlarmSoundSW = nil;
            }
            _AlarmSoundSW = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
            cell.accessoryView = _AlarmSoundSW;
            [_AlarmSoundSW setOn:([[_RemoteInfoDic objectForKey:@"alarm_switch"] intValue]?YES:NO)];
            [[_AlarmSoundSW
              rac_signalForControlEvents:UIControlEventValueChanged]
             subscribeNext:^(UISwitch * x) {
                 NSLog(@"开关  ： %d",x.isOn);
                 [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"alarm_switch"];
                 [self ReportStatues];
            }];
        }
            break;
        case 1:
        {
            if (_EmailSW) {
                [_EmailSW removeFromSuperview];
                _EmailSW = nil;
            }
            _EmailSW = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
            cell.accessoryView = _EmailSW;
            [_EmailSW setOn:([[_RemoteInfoDic objectForKey:@"email_switch"] intValue]?YES:NO)];
            [[_EmailSW
              rac_signalForControlEvents:UIControlEventValueChanged]
             subscribeNext:^(UISwitch * x) {
                 NSLog(@"开关  ： %d",x.isOn);
                 [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"email_switch"];
                 [self ReportStatues];
             }];
        }
            break;
        default:
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark 网络请求
- (void)SynSwitchStatues
{
    NSDictionary *tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"90068",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
                [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [_RemoteInfoDic setObject:[[[response objectForKey:@"data"] objectAtIndex:0] objectForKey:@"email_switch"] forKey:@"email_switch"];
            [_RemoteInfoDic setObject:[[[response objectForKey:@"data"] objectAtIndex:0] objectForKey:@"alarm_switch"] forKey:@"alarm_switch"];
            [_MainTable reloadData];
            
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)ReportStatues
{
    NSDictionary *tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",[_RemoteInfoDic objectForKey:@"alarm_switch"],@"alarm_switch",[_RemoteInfoDic objectForKey:@"email_switch"],@"email_switch",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00068",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {

        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}

@end
