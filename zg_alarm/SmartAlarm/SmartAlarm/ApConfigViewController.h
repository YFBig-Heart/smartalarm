//
//  ApConfigViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2018/1/9.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GCDAsyncSocket;

@interface ApConfigViewController : UIViewController
{
    GCDAsyncSocket *asyncSocket;
}
@property (retain,nonatomic) NSString * WifiSSid;
@end
