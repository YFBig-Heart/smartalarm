//
//  OverLoadMainViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/3/18.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "OverLoadMainViewController.h"

@interface OverLoadMainViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSMutableDictionary * mainDic;
@property (retain,nonatomic) UITextField * OverLoadTF;
@property (weak, nonatomic) IBOutlet UIButton *Done;

@end

@implementation OverLoadMainViewController

- (void)viewDidLoad {
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    self.title = NSLocalizedString(@"过载保护",nil);
    [super viewDidLoad];
    UILabel * FooterLable = [[UILabel alloc] init];
    [FooterLable setText:NSLocalizedString(@"1. 过载保护功率参数可设置 0~2000W，当为 0 时，关闭此功能。\r\n2.当负载大于此值时，排插将会关闭电源。", nil)];
    [FooterLable setTextColor:FlatGray];
    [FooterLable sizeToFit];
    [FooterLable setFont:[UIFont systemFontOfSize:15]];
    [FooterLable setNumberOfLines:0];
    [FooterLable setTextAlignment:NSTextAlignmentLeft];
    _MainTable.tableFooterView = FooterLable;
    _MainTable.tableFooterView.sd_layout.heightIs(80).widthRatioToView(self.view, 0.8).centerXEqualToView(self.view);
    [_Done bs_configureAsPrimaryStyle];
    [_Done setTitle:NSLocalizedString(@"完成",nil) forState:UIControlStateNormal];
    [[_Done rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self reportDeviceOverload];
    }];
    self.view.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fingerTapped:)];
    [self.view addGestureRecognizer:singleTap];
    [self synDeviceOverLoad];
}
-(void)fingerTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    [self.view endEditing:YES];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)synDeviceOverLoad
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00010",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _mainDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)reportDeviceOverload
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",_OverLoadTF.text,@"overload",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80010",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    //离section的高度
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString(@"过载保护",nil)];
//    cell.detailTextLabel.text = [_mainDic objectForKey:@"overload"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    _OverLoadTF = [[UITextField alloc] initWithFrame:CGRectZero];
    [_OverLoadTF setTextColor:[UIColor flatGrayColor]];
    [_OverLoadTF setTextAlignment:NSTextAlignmentRight];
    [_OverLoadTF setBackgroundColor:[UIColor clearColor]];
    if (_mainDic != nil) {
        [_OverLoadTF setPlaceholder:[_mainDic objectForKey:@"overload"]];
    }
    else
        [_OverLoadTF setPlaceholder:@"0"];
    [cell addSubview:_OverLoadTF];
    [_OverLoadTF setKeyboardType:UIKeyboardTypeNumberPad];
//    [_OverLoadTF setBackgroundColor:FlatRed];
    _OverLoadTF.sd_layout.xIs(cell.centerX).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 45.0f);
    [[_OverLoadTF.rac_textSignal
      filter:^BOOL(id value) {
          NSString *text = value;
          return YES;
      }]
     subscribeNext:^(id x) {
         if([_OverLoadTF.text integerValue] > 2000)
         {
            _OverLoadTF.text = @"2000";
         }

         NSLog(@"%@", x);
     }];
    UILabel * UnitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 15)];
    [UnitLabel setTextColor:[UIColor flatGrayColor]];
    [UnitLabel setText:@"W"];
    [UnitLabel sizeToFit];
    [cell addSubview:UnitLabel];
    UnitLabel.sd_layout.centerYEqualToView(_OverLoadTF).leftSpaceToView(_OverLoadTF, 1);

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
//            [self.navigationController pushViewController:[[WifiConfigViewController alloc] init] animated:YES];
        }
            break;
        case 1:
        {
//            [self.navigationController pushViewController:[[ApConfigViewController alloc] init] animated:YES];
        }
            break;
            
        default:
            break;
    }
    
}
@end
