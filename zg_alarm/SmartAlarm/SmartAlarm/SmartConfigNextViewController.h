//
//  SmartConfigNextViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2018/2/2.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmartConfigNextViewController : UIViewController
@property (retain,nonatomic) NSString * Wifibssid;
@property (retain,nonatomic) NSString * Wifissid;
@property (retain,nonatomic) NSString * Wifipwd;
@property (assign) int EntryType; //0 :设置里进入的。配对成功后不需要退出。1 :从绑定界面进入的。成功后要退出。
@end
