//
//  getBatForVule.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/16.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "getBatForVule.h"

@implementation ValueForImage

+ (UIImage *)getBatImgForValue:(NSUInteger ) index
{
    NSArray * imaArray = @[@"FF_02-1",@"FF_02-2",@"FF_02-3",@"FF_02-4",@"FF_02_C"];
    if (index <= 5) {
        return [UIImage imageNamed:imaArray[0]];
    }
    else if (index <= 25) {
        return [UIImage imageNamed:imaArray[0]];
    }
    else if (index <= 50)
    {
        return [UIImage imageNamed:imaArray[1]];
    }
    else if (index <= 75)
    {
        return [UIImage imageNamed:imaArray[2]];
    }
    else if (index <=100)
    {
        return [UIImage imageNamed:imaArray[3]];
    }
    else if (index <= 200)
    {
        return [UIImage imageNamed:imaArray[4]];
    }
    else
        return [UIImage imageNamed:imaArray[3]];
}
+ (UIImage *)getSigImgForValue:(NSUInteger ) index
{
    NSArray * imaArray = @[@"FF_01-1",@"FF_01-2",@"FF_01-3",@"FF_01-4",@"FF_01-5"];
    if (index <= 10) {
        return [UIImage imageNamed:imaArray[0]];
    }
    else if (index <= 25)
    {
        return [UIImage imageNamed:imaArray[1]];
    }
    else if (index <= 50)
    {
        return [UIImage imageNamed:imaArray[2]];
    }
    else if (index <= 75)
    {
        return [UIImage imageNamed:imaArray[3]];
    }
    else
        return [UIImage imageNamed:imaArray[4]];
}

@end
