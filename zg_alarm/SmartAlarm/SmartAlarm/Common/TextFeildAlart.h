//
//  TextFeildAlart.h
//  SmartAlarm
//
//  Created by Huang Shan on 2018/2/5.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TextFeildAlart : NSObject
+ (NSAttributedString *)TextFeildAlart:(NSString *) AlartString TextColor:(UIColor *)Color;

@end
