//
//  RootTabViewController.m
//  wmp_ble
//
//  Created by Huang Shan on 16/11/28.
//  Copyright © 2016年 Huang Shan. All rights reserved.
//

#import "RootTabViewController.h"
#import "BaseNavigationController.h"
//#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "HomeViewController.h"
#import "CameraViewController.h"
#import "HistoryViewController.h"
#import "ManagerViewController.h"

#define JPUSH_APPKEY @"42350f22fcba618237708638"
#define WECHAT_APPKEY

@interface RootTabViewController ()<RDVTabBarDelegate,RDVTabBarControllerDelegate>

@end

@implementation RootTabViewController

- (void)viewDidLoad {
    [self setupViewControllers];
    self.delegate = self;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)setupViewControllers
{
    HomeViewController * home = [[HomeViewController alloc] init];
    home.tabBarItem.tag = 100;
    UINavigationController * nav_home = [[BaseNavigationController alloc] initWithRootViewController:home];
    CameraViewController * Camera = [[CameraViewController alloc] init];
    UINavigationController * Camera_nav = [[BaseNavigationController alloc] initWithRootViewController:Camera];
    HistoryViewController * History = [[HistoryViewController alloc] init];
    UINavigationController * History_nav = [[BaseNavigationController alloc] initWithRootViewController:History];
    ManagerViewController* Manager = [[ManagerViewController alloc] init];
    UINavigationController * Manager_nav = [[BaseNavigationController alloc] initWithRootViewController:Manager];
    
    [self setViewControllers:@[nav_home, Camera_nav, History_nav,Manager_nav]];
    self.delegate = self;
    [self customizeTabBarForController];
}
- (void)customizeTabBarForController
{
//    UIImage *finishedImage = [UIImage imageNamed:@"tabbar_selected_background"];
//    UIImage *unfinishedImage = [UIImage imageNamed:@"tabbar_normal_background"];
    NSArray *tabBarItemImages = @[@"Home", @"Camera",@"History",@"Manage"];
    NSArray *tabBarItemTitles = @[@"主页", @"监控", @"历史",@"管理"];
    NSInteger index = 0;
    for (RDVTabBarItem *item in [[self tabBar] items]) {
        //[item setBackgroundSelectedImage:finishedImage withUnselectedImage:unfinishedImage];
        [item setBackgroundColor:FlatWhite];//[UIColor colorWithHexString:@"#273646"]];
        UIImage *selectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",
                                                      [tabBarItemImages objectAtIndex:index]]];
        UIImage *unselectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",
                                                        [tabBarItemImages objectAtIndex:index]]];
        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
        [item setTitle:NSLocalizedString([tabBarItemTitles objectAtIndex:index],nil)];
        [item setTintColor:[UIColor redColor]];
        
        NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
        selectedAttrs[NSForegroundColorAttributeName] = FlatMint;
        
        NSMutableDictionary *UnselectedAttrs = [NSMutableDictionary dictionary];
        UnselectedAttrs[NSForegroundColorAttributeName] = FlatGray;

        [item setSelectedTitleAttributes:selectedAttrs];
        [item setUnselectedTitleAttributes:UnselectedAttrs];
        if(Device_Is_iPhoneX)
        {
            item.badgePositionAdjustment = UIOffsetMake(0, 18);
        }
        index++;
    }
    self.tabBar.translucent = YES;
    if(Device_Is_iPhoneX)
    {
//        [self.tabBar setContentEdgeInsets:UIEdgeInsetsMake(18, 0, 0, 0)];
        [self.tabBar setHeight:83];
    }
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tabBar.bounds)-0.5, kScreen_Width - 0, 0.5)];
    lineView.backgroundColor = FlatWhiteDark;
    [self.tabBar addSubview:lineView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark RDVTabBarControllerDelegate
- (BOOL)tabBarController:(RDVTabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
//    if (tabBarController.selectedViewController != viewController) {
//        return YES;
//    }
//    if (![viewController isKindOfClass:[UINavigationController class]]) {
//        return YES;
//    }
//    UINavigationController *nav = (UINavigationController *)viewController;
//    if (nav.topViewController != nav.viewControllers[0]) {
//        return YES;
//    }
//#if 0
//    if ([nav isKindOfClass:[RKSwipeBetweenViewControllers class]]) {
//        RKSwipeBetweenViewControllers *swipeVC = (RKSwipeBetweenViewControllers *)nav;
//        if ([[swipeVC curViewController] isKindOfClass:[BaseViewController class]]) {
//            BaseViewController *rootVC = (BaseViewController *)[swipeVC curViewController];
//            [rootVC tabBarItemClicked];
//        }
//    }else{
//        if ([nav.topViewController isKindOfClass:[BaseViewController class]]) {
//            BaseViewController *rootVC = (BaseViewController *)nav.topViewController;
//            [rootVC tabBarItemClicked];
//        }
//    }
//#endif
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] == nil) {
                    [HttpRespondError ShowHttpRespondError:-5];
        }
    return YES;
//    if (tabBarController.tabBarItem.tag != 100) {
//        if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] == nil) {
//            [HttpRespondError ShowHttpRespondError:-5];
//            return NO;
//        }
//        else
//            return YES;
//    }
//    else
//        return YES;
}
@end
