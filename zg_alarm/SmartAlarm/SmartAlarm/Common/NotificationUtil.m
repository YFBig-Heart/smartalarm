//
//  NotificationUtil.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/11/22.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "NotificationUtil.h"

@implementation NotificationUtil
+ (UIUserNotificationType)getCurrentRegistedNotificationType {
    UIUserNotificationType notificationType = [[UIApplication sharedApplication] currentUserNotificationSettings].types;
    return notificationType;
}

+ (BOOL)isEnabledNotification {
    UIUserNotificationType notificationType = [[UIApplication sharedApplication] currentUserNotificationSettings].types;
    
    return notificationType != UIUserNotificationTypeNone;
}
@end
