//
//  BaseNavigationController.h
//  wmp_ble
//
//  Created by Huang Shan on 16/11/29.
//  Copyright © 2016年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController

@end
