//
//  NotificationUtil.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/11/22.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationUtil : NSObject
+ (UIUserNotificationType)getCurrentRegistedNotificationType;

+ (BOOL)isEnabledNotification;
@end
