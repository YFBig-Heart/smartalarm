//
//  GetSysLanguage.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/24.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetSysLanguage : NSObject
+ (NSString *)GetSystemLanguage;
+(id)changeType:(id)myObj;
@end
