//
//  RootTabViewController.h
//  wmp_ble
//
//  Created by Huang Shan on 16/11/28.
//  Copyright © 2016年 Huang Shan. All rights reserved.
//

#import <RDVTabBarController/RDVTabBarController.h>
@interface RootTabViewController : RDVTabBarController<RDVTabBarControllerDelegate>

@end
