//
//  UILabel+VerticalAlign.h
//  SmartAlarm
//
//  Created by Huang Shan on 2018/2/27.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#pragma mark VerticalAlign
@interface UILabel (VerticalAlign)
- (void)alignTop;
- (void)alignBottom;
@end
