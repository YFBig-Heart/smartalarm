//
//  HttpRespondError.m
//  CoreIOS
//
//  Created by Huang Shan on 2017/5/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "HttpRespondError.h"

@implementation HttpRespondError
/*
 "SQL数据库出错"="SQL Database error!";
 "用户名或密码错误"="Wrong user name or passwrod";
 "用户名错误"="Wrong user name";
 "绑定码错误"="Wrong bind code";
 "创建组错误"="Error in setting group!";
 "此终端无记录"="No info of this device!";
 "错误的组ID"="Wrong group ID!";
 "数据库无默认组"="No default group in database";
 "无此终端"="No this device!";
 "指令发送失败,终端不在线"="Send command failed! Terminal offline";
 "已绑定,不可重复绑定"="Already bound, can’t be bound again!";
 "没有此传感器"="Without this sensor";
 "密码错误"="Wrong password";
 "你没有权限操作"="You are not authorized!";
 "指令错误"="Wrong command!";
 "服务器出错"="Server error";
 */
+ (void)ShowHttpRespondError:(long) ErrorCode
{
    switch (ErrorCode) {
        case 0:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"设置成功",nil)];
            break;
        case 1002:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"SQL数据库出错",nil)];
            break;
        case 1003:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"用户名或密码错误",nil)];
            break;
        case 1004:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"用户名错误",nil)];
            break;
        case 1005:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"绑定码错误",nil)];
            break;
        case 1006:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"创建组错误",nil)];
            break;
        case 1007:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"此终端无记录",nil)];
            break;
        case 1008:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"错误的组ID",nil)];
            break;
        case 1009:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"数据库无默认组",nil)];
            break;
        case 1010:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"无此终端",nil)];
            break;
        case 1013:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"指令发送失败,终端不在线",nil)];
            break;
        case 1015:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已绑定,不可重复绑定",nil)];
            break;
        case 1016:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"没有此传感器",nil)];
            break;
        case 1017:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"密码错误",nil)];
            break;
        case 1018:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"你没有权限操作",nil)];
            break;
        case 1019:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"错误的邮箱地址",nil)];
            break;
        case 8888:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"指令错误",nil)];
            break;
        case 9999:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"服务器出错",nil)];
            break;
        case -999:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"无法连接服务器",nil)];
            break;
        default:
            break;
    }
}
+ (NSAttributedString *)HttpRespondErrorForTextFiled:(long) ErrorCode{
    switch (ErrorCode) {
        case 0:
            return [TextFeildAlart TextFeildAlart: NSLocalizedString(@"设置成功",nil) TextColor:FlatRed];
            break;
        case 1002:
            return [TextFeildAlart TextFeildAlart: NSLocalizedString(@"SQL数据库出错",nil) TextColor:FlatRed];
            break;
        case 1003:
            return [TextFeildAlart TextFeildAlart: NSLocalizedString(@"密码错误",nil) TextColor:FlatRed];
            break;
        case 1004:
            return [TextFeildAlart TextFeildAlart: NSLocalizedString(@"用户名错误",nil) TextColor:FlatRed];
            break;
        case 1005:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"绑定码错误",nil)];
            break;
        case 1006:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"创建组错误",nil)];
            break;
        case 1007:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"此终端无记录",nil)];
            break;
        case 1008:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"错误的组ID",nil)];
            break;
        case 1009:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"数据库无默认组",nil)];
            break;
        case 1010:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"无此终端",nil)];
            break;
        case 1013:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"指令发送失败,终端不在线",nil)];
            break;
        case 1015:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"已绑定,不可重复绑定",nil)];
            break;
        case 1016:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"没有此传感器",nil)];
            break;
        case 1017:
//            [RKDropdownAlert title:nil message:NSLocalizedString(@"密码错误",nil)];
            return [TextFeildAlart TextFeildAlart: NSLocalizedString(@"密码错误",nil) TextColor:FlatRed];

            break;
        case 1018:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"你没有权限操作",nil)];
            break;
        case 1019:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"错误的邮箱地址",nil)];
            break;
        case 8888:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"指令错误",nil)];
            break;
        case 9999:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"服务器出错",nil)];
            break;
        case -999:
            [RKDropdownAlert title:nil message:NSLocalizedString(@"无法连接服务器",nil)];
            break;
        default:
            break;
    }
    return nil;
}
@end
