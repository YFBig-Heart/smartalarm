//
//  CoreHTTPRequest.h
//  CoreIOS
//
//  Created by Huang Shan on 2017/5/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 请求成功的block
 
 @param response 响应体数据
 */
typedef void(^CoreRequestSuccess)(id response);
/**
 请求失败的block
 */
typedef void(^CoreRequestFailure)(NSError *error);
@interface CoreHTTPRequest : NSObject
+ (NSURLSessionTask *)HttpRequst:(id)Urlparameters success:(CoreRequestSuccess)success failure:(CoreRequestFailure)failure;
+ (NSURLSessionTask *)HttpRequstPro:(id)Urlparameters success:(CoreRequestSuccess)success failure:(CoreRequestFailure)failure;
+ (NSURLSessionTask *)EasyHttpRequst:(id)Urlparameters success:(CoreRequestSuccess)success failure:(CoreRequestFailure)failure;
+ (BOOL)isDeviceAlive;
+ (NSInteger )GetDeviceType;//获取设备类型
@end
