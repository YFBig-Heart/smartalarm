//
//  CoreHTTPRequest.m
//  CoreIOS
//
//  Created by Huang Shan on 2017/5/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "CoreHTTPRequest.h"
#import "PPNetworkHelper.h"
//test
#define kApiPrefix_test @"http://localhost:8080/interface/user"
//URL 前缀
#define kApiPrefix HTTP_DOMAIN

#define kNormalApi @"/api?"
@implementation CoreHTTPRequest

+ (NSURLSessionTask *)HttpRequst:(id)Urlparameters success:(CoreRequestSuccess)success failure:(CoreRequestFailure)failure
{
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kApiPrefix,kNormalApi,Urlparameters];
    NSLog(@"%@", [url URLDecodeUsingEncoding:NSUTF8StringEncoding]);
    return [self requestWithURL:url parameters:nil success:success failure:failure];
}
+ (NSURLSessionTask *)HttpRequstPro:(id)Urlparameters success:(CoreRequestSuccess)success failure:(CoreRequestFailure)failure
{
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kApiPrefix,kNormalApi,Urlparameters];
    NSLog(@"%@", [url URLDecodeUsingEncoding:NSUTF8StringEncoding]);
    return [self requestWithURLPro:url parameters:nil success:success failure:failure];
}
+ (NSURLSessionTask *)EasyHttpRequst:(id)Urlparameters success:(CoreRequestSuccess)success failure:(CoreRequestFailure)failure
{
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kApiPrefix,kNormalApi,Urlparameters];
    NSLog(@"%@", [url URLDecodeUsingEncoding:NSUTF8StringEncoding]);
    return [self requestWithURLPro:url parameters:nil success:success failure:failure];
}
/*
 配置好PPNetworkHelper各项请求参数,封装成一个公共方法,给以上方法调用,
 相比在项目中单个分散的使用PPNetworkHelper/其他网络框架请求,可大大降低耦合度,方便维护
 在项目的后期, 你可以在公共请求方法内任意更换其他的网络请求工具,切换成本小
 */

#pragma mark - 请求的公共方法

+ (NSURLSessionTask *)requestWithURL:(NSString *)URL parameters:(NSDictionary *)parameter success:(CoreRequestSuccess)success failure:(CoreRequestFailure)failure
{
    // 在请求之前你可以统一配置你请求的相关参数 ,设置请求头, 请求参数的格式, 返回数据的格式....这样你就不需要每次请求都要设置一遍相关参数
    // 设置请求头
    [PPNetworkHelper setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [PPNetworkHelper setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // 发起请求
    return [PPNetworkHelper GET:URL parameters:parameter success:^(id responseObject) {
        
        success(responseObject);
        // 在这里你可以根据项目自定义其他一些重复操作,比如加载页面时候的等待效果, 提醒弹窗....
        
    } failure:^(NSError *error) {
        // 同上
        failure(error);
    }];
}

+ (NSURLSessionTask *)requestWithURLPro:(NSString *)URL parameters:(NSDictionary *)parameter success:(CoreRequestSuccess)success failure:(CoreRequestFailure)failure
{
    // 在请求之前你可以统一配置你请求的相关参数 ,设置请求头, 请求参数的格式, 返回数据的格式....这样你就不需要每次请求都要设置一遍相关参数
    // 设置请求头
    static int hubTag = 0;
    [PPNetworkHelper setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [PPNetworkHelper setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // 发起请求
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    [hud setTag:hubTag++];
    return [PPNetworkHelper GET:URL parameters:parameter success:^(id responseObject) {
        
        [hud hideAnimated:YES];
        [hud removeFromSuperview];
        success(responseObject);
        // 在这里你可以根据项目自定义其他一些重复操作,比如加载页面时候的等待效果, 提醒弹窗....
        
    } failure:^(NSError *error) {
        // 同上
        [hud hideAnimated:YES];
        [hud removeFromSuperview];
        failure(error);
    }];
}

//是否有绑定设备
+ (BOOL)isDeviceAlive
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice]?YES:NO;
}
+ (NSInteger )GetDeviceType
{
    return [[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"type"] intValue];
    
    //return 5;
}
@end
