//
//  HttpRespondError.h
//  CoreIOS
//
//  Created by Huang Shan on 2017/5/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpRespondError : NSObject
+ (void)ShowHttpRespondError:(long) ErrorCode;
+ (NSAttributedString *)HttpRespondErrorForTextFiled:(long) ErrorCode;
@end
