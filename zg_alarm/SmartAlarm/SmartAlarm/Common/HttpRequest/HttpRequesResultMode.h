//
//  HttpRequesResultMode.h
//  CoreIOS
//
//  Created by Huang Shan on 2017/5/25.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpRequesResultMode : NSObject
@property (nonatomic, retain) NSString * describeCn;
@property (nonatomic, retain) NSString * describeEn;
@property (nonatomic, assign) long result;
@end
