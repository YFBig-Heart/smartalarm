//
//  PTLAlertView.m
//  PTLAlertView
//
//  Created by soliloquy on 2018/1/15.
//  Copyright © 2018年 soliloquy. All rights reserved.
//

#import "PTLAlertView.h"
#import "STPickerView.h"
#import "STPickerDate.h"
#import "STPickerSingle.h"
#import "WSDatePickerView.h"

#define kScreenBounds ([UIScreen mainScreen].bounds)
#define kScreenWidth  ([UIScreen mainScreen].bounds.size.width)
#define kScreenHeight  ([UIScreen mainScreen].bounds.size.height)
#define rgb(r,g,b) ([UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1])
/// 按钮高度
#define BUTTON_HEIGHT 40
/// 标题高度
#define TITLE_HEIGHT 40


@interface PTLAlertView()<STPickerDateDelegate,UITextFieldDelegate>

/** title */
@property (nonatomic, copy) NSString *title;
/** message */
@property (nonatomic, copy) NSString *message;
/** 取消 */
@property (nonatomic, copy) NSString *cancelButtonTitle;
/** 其他按钮标题 */
@property (nonatomic, strong) NSMutableArray *otherButtonTitleArray;
/** 所有按钮Title */
@property (nonatomic, strong) NSMutableArray *allButtonTitlesArr;
/** 遮罩View */
@property (nonatomic, strong) UIView *shadeView;
/** 标题 Label */
@property (nonatomic, strong) UILabel *titleLabel;
/** message Label */
@property (nonatomic, strong) UILabel *messageLabel;
/** 所有按钮 */
@property (nonatomic, strong) NSMutableArray *allButtons;
/** otherView */
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIStackView *stackView;
@property (retain,nonatomic) UITextField * textStart;
@property (retain,nonatomic) UITextField * textEnd;

@end

@implementation PTLAlertView

#pragma mark - init
- (instancetype _Nullable )initWithTitle:(nullable NSString *)title message:(nullable NSString *)message cancelButtonTitle:(nullable NSString *)cancelButtonTitle otherButtonTitles:(nullable NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION {
    if (self = [super initWithFrame:CGRectZero]) {
        
        self.title = [title copy];
        self.message = [message copy] ?: @"";
        self.cancelButtonTitle = [cancelButtonTitle copy] ?: @"取消";
        
        if (otherButtonTitles)
        {
            va_list args;
            va_start(args, otherButtonTitles);
            [self.otherButtonTitleArray addObject:otherButtonTitles];
            NSString *otherString;
            while ((otherString = va_arg(args, NSString *)))
            {
                //依次取得所有参数
                [self.otherButtonTitleArray addObject:otherString];
            }
            va_end(args);
        }
        
        [self.allButtonTitlesArr addObjectsFromArray:self.otherButtonTitleArray];
        [self.allButtonTitlesArr addObject:self.cancelButtonTitle];

        
        [self setupUI];
        
    }
    return self;
}

- (NSDate *)GetStart0Time
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    NSDate *startDate = [calendar dateFromComponents:components];
    return startDate;
//    NSDate *endDate = [calendar dateByAddingUnit:NSCalendarUnitDay value:1 toDate:startDate options:0];
}
- (void)setupStartDateLable
{
    UILabel * StartTimelable = [[UILabel alloc] initWithFrame:CGRectZero];
    StartTimelable.textAlignment = NSTextAlignmentRight;
    [StartTimelable setText:NSLocalizedString(@"开始时间:", nil)];
    [StartTimelable setFont:[UIFont systemFontOfSize:13]];
    [self.topView addSubview:StartTimelable];
    StartTimelable.sd_layout.widthIs(80).heightIs(45).xIs(5).topSpaceToView(self.titleLabel, 10);
    NSDate * date = [NSDate date];
    _textStart = [[UITextField alloc] initWithFrame:CGRectZero];
    _textStart.delegate =self;
    [_textStart setBorderStyle:UITextBorderStyleRoundedRect];
    _textStart.textAlignment = NSTextAlignmentCenter;
    _textStart.tag = 1;
    [_textStart setText:date.yyyyMMddHHmmByLineWithDate];
//    [_textStart setBackgroundColor:FlatRed];
    [_textStart setFont:[UIFont systemFontOfSize:15]];
    [self.topView addSubview:_textStart];
    _textStart.sd_layout.topSpaceToView(self.titleLabel, 10).widthRatioToView(self, 0.6).heightIs(45).leftSpaceToView(StartTimelable, 10);
    NSString *timespStart = [NSString stringWithFormat:@"%ld", (long)[[self GetStart0Time] timeIntervalSince1970]];
    NSLog(@"timespStart %@",timespStart);
    [[NSUserDefaults standardUserDefaults] setObject:timespStart forKey:kDefaultStartKeyValue];
}
- (void)setupEndDateLable
{
    UILabel * StartTimelable = [[UILabel alloc] initWithFrame:CGRectZero];
    StartTimelable.textAlignment = NSTextAlignmentRight;
    [StartTimelable setText:NSLocalizedString(@"结束时间:", nil)];
    [StartTimelable setFont:[UIFont systemFontOfSize:13]];
    [self.topView addSubview:StartTimelable];
    StartTimelable.sd_layout.widthIs(80).heightIs(45).xIs(5).topSpaceToView(_textStart, 20);

    NSDate * date = [NSDate date];
    _textEnd = [[UITextField alloc] initWithFrame:CGRectZero];
    [_textEnd setBorderStyle:UITextBorderStyleRoundedRect];
    _textEnd.delegate =self;
    _textEnd.textAlignment = NSTextAlignmentCenter;
    [_textEnd setText:date.yyyyMMddHHmmByLineWithDate];
//    [_textEnd setBackgroundColor:FlatRed];
    _textEnd.font = [UIFont systemFontOfSize:15];
    [self.topView addSubview:_textEnd];
    _textEnd.sd_layout.topSpaceToView(_textStart, 20).widthRatioToView(self, 0.6).heightIs(45).leftSpaceToView(StartTimelable, 10);
    NSString *timespEnd = [NSString stringWithFormat:@"%ld", (long)[[NSDate new] timeIntervalSince1970]];
    NSLog(@"timespEnd %@",timespEnd);
    [[NSUserDefaults standardUserDefaults] setObject:timespEnd forKey:kDefaultEndKeyVaule];

}
- (void)setupUI {
    
    self.backgroundColor = [UIColor yellowColor];
    [self.shadeView addSubview:self];
    [self addSubview:self.bgView];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 10;
    
    
    [self.bgView addSubview:self.topView];
    [self.topView addSubview:self.titleLabel];
    [self setupStartDateLable];
    [self setupEndDateLable];

//    [self.topView addSubview:self.messageLabel];
    
    
    [self.bgView addSubview:self.bottomView];
    [self.bottomView addSubview:self.stackView];
    
    
    for (NSInteger i = 0; i < self.allButtonTitlesArr.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:self.cancelButtonTitle forState:UIControlStateNormal];
        [btn setTitleColor:rgb(38, 204, 206) forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        [btn addTarget:self action:@selector(selectButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:self.allButtonTitlesArr[i] forState:UIControlStateNormal];
        btn.tag = i;
        [self.allButtons addObject:btn];
        [self.stackView addArrangedSubview:btn];
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_textStart resignFirstResponder];
    [_textEnd resignFirstResponder];
    WSDatePickerView *datepicker = [[WSDatePickerView alloc] initWithDateStyle:DateStyleShowYearMonthDayHourMinute CompleteBlock:^(NSDate *selectDate) {
        NSString *dateString = [selectDate stringWithFormat:@"yyyy-MM-dd HH:mm"];
        NSLog(@"选择的日期：%@",dateString);
        if (textField.tag == 1) {
            [_textStart setText:dateString];
            NSString *timespStart = [NSString stringWithFormat:@"%ld", (long)[selectDate timeIntervalSince1970]];
            NSLog(@"timespStart %@",timespStart);
            [[NSUserDefaults standardUserDefaults] setObject:timespStart forKey:kDefaultStartKeyValue];
        }
        else
        {
            NSString *timespEnd = [NSString stringWithFormat:@"%ld", (long)[selectDate timeIntervalSince1970]];
            NSLog(@"timespEnd %@",timespEnd);
            [[NSUserDefaults standardUserDefaults] setObject:timespEnd forKey:kDefaultEndKeyVaule];
            [_textEnd setText:dateString];
        }
        _textStart.sd_layout.topSpaceToView(self.titleLabel, 10).widthRatioToView(self, 1.0).heightIs(45).centerXEqualToView(self);
        _textEnd.sd_layout.topSpaceToView(_textStart, 20).widthRatioToView(self, 1.0).heightIs(45).centerXEqualToView(self);
    }];
    datepicker.dateLabelColor = [UIColor orangeColor];//年-月-日-时-分 颜色
    datepicker.datePickerColor = [UIColor blackColor];//滚轮日期颜色
    datepicker.doneButtonColor = [UIColor orangeColor];//确定按钮的颜色
    [datepicker show];
}


- (void)pickerDate:(STPickerDate *)pickerDate year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day
{
    NSString *text = [NSString stringWithFormat:@"%zd年%zd月%zd日", year, month, day];
    if (pickerDate.tag == 1) {
        _textStart.text = text;
    }
    else
        _textEnd.text = text;
    _textStart.sd_layout.topSpaceToView(self.titleLabel, 10).widthRatioToView(self, 1.0).heightIs(45).centerXEqualToView(self);
    _textEnd.sd_layout.topSpaceToView(_textStart, 20).widthRatioToView(self, 1.0).heightIs(45).centerXEqualToView(self);

}
-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat w = kScreenWidth * 0.8;
    CGFloat x = (kScreenWidth-w)/2;
    
    CGFloat topH = 0;
    CGFloat titleH = TITLE_HEIGHT;
    CGFloat marge1 = 20;
    self.titleLabel.frame = CGRectMake(0, 0, w, titleH);
    
    CGFloat msgX = 10;
    CGFloat msgW = w - 2 * msgX;
    
    CGFloat msgH = [self sizeWithFont:_messageTextFont ?: [UIFont systemFontOfSize:15] maxSize:CGSizeMake(msgW, MAXFLOAT) string:self.message].height;
    
    if (msgH > 400) {
        msgH = 400;
    }
    
    self.messageLabel.frame = CGRectMake(msgX, CGRectGetMaxY(self.titleLabel.frame)+marge1, msgW, msgH);
    
    topH = titleH + msgH + marge1;

    self.topView.frame = CGRectMake(0, 0, w, topH);
    
    CGFloat bottomH = 0;
    CGFloat marge2 = 20;
    
    for(NSInteger i = 0; i < self.allButtonTitlesArr.count; i++) {
        
        if (self.allButtonTitlesArr.count > 2) { // 纵向排
            bottomH = BUTTON_HEIGHT * self.allButtonTitlesArr.count;
        }else {
            bottomH = BUTTON_HEIGHT;
            self.stackView.axis = UILayoutConstraintAxisHorizontal;
        }
        CGFloat bottomY = CGRectGetMaxY(self.topView.frame)+marge2;
        self.bottomView.frame = CGRectMake(0, bottomY, w, bottomH);
        self.stackView.frame = self.bottomView.bounds;
    }
    
    CGFloat y1 = (kScreenHeight-(bottomH+topH))/2;
    CGFloat yH = bottomH+topH+marge2;
    self.frame = CGRectMake(x, y1, w, yH);
    self.bgView.frame = self.bounds;
}


- (void)show {
    
    self.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
    [UIView animateWithDuration:0.25 animations:^{
        [self dissmiss];
        self.transform = CGAffineTransformIdentity;
        [[UIApplication sharedApplication].keyWindow addSubview:self.shadeView];
    } completion:^(BOOL finished) {
    }];
}

- (void)dissmiss {
    NSEnumerator *subviewsEnum = [[UIApplication sharedApplication].keyWindow.subviews reverseObjectEnumerator];
    for (UIView *subview in subviewsEnum) {
        if (subview.tag == 1000) {
            [subview removeFromSuperview];
        }
    }
}


- (void)selectButtonClick:(UIButton *)btn {
    
    NSInteger cancelBtnIndex = ((UIButton *)self.allButtons[btn.tag]).tag;
//    if (cancelBtnIndex == self.allButtons.count-1) {
//        [self dissmiss];
//        return;
//    }
    
    if (self.selctBtnBlock) {
        self.selctBtnBlock(btn.tag, btn.currentTitle);
        self.selctBtnBlock = nil;
        [self dissmiss];
    }
}

#pragma mark - private faction
-(CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize string:(NSString *)string{
    
    NSDictionary *attrs = @{NSFontAttributeName:font};
    return [string boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:attrs context:nil].size;
}


#pragma mark - getter/setter
-(void)setTitleBackgroundColor:(UIColor *)titleBackgroundColor {
    if (!titleBackgroundColor) return;
    _titleBackgroundColor = titleBackgroundColor;
    self.titleLabel.backgroundColor = titleBackgroundColor;
    
}

-(void)setTitleTextColor:(UIColor *)titleTextColor {
    if (!titleTextColor) return;
    _titleTextColor = titleTextColor;
    self.titleLabel.textColor = titleTextColor;
}

-(void)setTitleTextFont:(UIFont *)titleTextFont {
    if (!titleTextFont) return;
    _titleTextFont = titleTextFont;
    self.titleLabel.font = titleTextFont;
}

-(void)setMessageTextColor:(UIColor *)messageTextColor {
    if (!messageTextColor) return;
    _messageTextColor = messageTextColor;
    self.messageLabel.textColor = messageTextColor;
}

-(void)setMessageTextFont:(UIFont *)messageTextFont {
    if (!messageTextFont) return;
    _messageTextFont = messageTextFont;
    self.messageLabel.font = messageTextFont;
}

-(void)setCancelBtnTextColor:(UIColor *)cancelBtnTextColor {
    if (!cancelBtnTextColor) return;
    _cancelBtnTextColor = cancelBtnTextColor;
    
    UIButton *cancelBtn = self.allButtons.lastObject;
    if (cancelBtn != nil) {
        [cancelBtn setTitleColor:cancelBtnTextColor forState:UIControlStateNormal];
    }
}

-(void)setCancelBtnTextFont:(UIFont *)cancelBtnTextFont {
    if (!cancelBtnTextFont) return;
    _cancelBtnTextFont = cancelBtnTextFont;
    
    UIButton *cancelBtn = self.allButtons.lastObject;
    if (cancelBtn != nil) {
        cancelBtn.titleLabel.font = cancelBtnTextFont;
    }
}

-(void)setOtherBtnTextColor:(UIColor *)otherBtnTextColor {
    if (!otherBtnTextColor) return;
    _otherBtnTextColor = otherBtnTextColor;
    
    // 最后一个是cancel button 故不设置
    for (NSInteger i = 0; i < self.allButtons.count-1; i ++) {
        UIButton *otherBtn = self.allButtons[i];
        if (otherBtn != nil) {
            [otherBtn setTitleColor:otherBtnTextColor forState:UIControlStateNormal];
        }
    }
}

-(void)setOtherBtnTextFont:(UIFont *)otherBtnTextFont {
    if (!otherBtnTextFont) return;
    _otherBtnTextFont = otherBtnTextFont;
    
    for (NSInteger i = 0; i < self.allButtons.count-1; i ++) {
        UIButton *otherBtn = self.allButtons[i];
        if (otherBtn != nil) {
            otherBtn.titleLabel.font = otherBtnTextFont;
        }
    }
}

-(UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

-(UIStackView *)stackView {
    if (!_stackView) {
        _stackView = [[UIStackView alloc]init];
        _stackView.axis = UILayoutConstraintAxisVertical;
        _stackView.spacing = 1;
        _stackView.distribution = UIStackViewDistributionFillEqually;
    }
    return _stackView;
}

-(UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc]init];
    }
    return _topView;
}

-(UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}


-(NSMutableArray *)allButtons {
    
    if (!_allButtons) {
        _allButtons = [[NSMutableArray alloc]init];
    }
    return _allButtons;
}

-(UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = self.title;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.backgroundColor = rgb(38, 204, 206);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

-(UILabel *)messageLabel {
    if (!_messageLabel) {
        _messageLabel = [[UILabel alloc]init];
        _messageLabel.text = self.message;
        _messageLabel.numberOfLines = 0;
        _messageLabel.font = _messageTextFont ?: [UIFont systemFontOfSize:15];
        _messageLabel.textAlignment = NSTextAlignmentCenter;
        _messageLabel.textColor = [UIColor blackColor];
        _messageLabel.backgroundColor = [UIColor whiteColor];
    }
    return _messageLabel;
}

-(UIView *)shadeView {
    if (!_shadeView) {
        _shadeView = [[UIView alloc]initWithFrame:kScreenBounds];
        _shadeView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.6];
        _shadeView.tag = 1000;
    }
    return _shadeView;
}

-(NSMutableArray *)allButtonTitlesArr {
    if (!_allButtonTitlesArr) {
        _allButtonTitlesArr = [[NSMutableArray alloc]init];
    }
    return _allButtonTitlesArr;
}

-(NSMutableArray *)otherButtonTitleArray {
    if (!_otherButtonTitleArray) {
        _otherButtonTitleArray = [[NSMutableArray alloc]init];
    }
    return _otherButtonTitleArray;
}


@end
