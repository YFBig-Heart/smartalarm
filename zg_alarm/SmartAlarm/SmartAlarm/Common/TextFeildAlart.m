//
//  TextFeildAlart.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/2/5.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "TextFeildAlart.h"

@implementation TextFeildAlart

+ (NSAttributedString *)TextFeildAlart:(NSString *) AlartString TextColor:(UIColor *)Color
{
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSForegroundColorAttributeName] = FlatRed;
    NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:AlartString attributes:attrs];
    return attStr;
}
@end
