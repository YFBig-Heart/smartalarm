//
//  ContanctSelectViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2018/8/7.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import "ContanctSelectViewController.h"
#define PROTECT_STA (0) //布防
#define UNPROTECT_STA (1) //撤防
#define INHOME_STA (0) //在家

@interface ContanctSelectViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;

@end

@implementation ContanctSelectViewController

- (void)viewDidLoad {
    if (_TitleArray == nil) {
        _TitleArray = @[@"布防",@"撤防",@" 在家"];
    }
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    self.title = NSLocalizedString(@"选择",nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone  target:self action:@selector(Done)];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)Done
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _TitleArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.textLabel.text = NSLocalizedString([_TitleArray objectAtIndex:indexPath.row], nil);
    if ([_StatuesString intValue] & (1 << indexPath.row)) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    return cell;
}
//110
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    int ByteStatue = [_StatuesString intValue];
    ByteStatue = ByteStatue ^ (1 << indexPath.row);
    _StatuesString = [NSString stringWithFormat:@"%d",ByteStatue];
    [self.MainTable reloadData];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",_StatuesString] forKey:@"CIDStatuesString"];
}
@end
