//
//  RegisterForPasswordViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/11/12.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterForPasswordViewController : UIViewController
@property (retain,nonatomic) NSString * userID;
@property (retain,nonatomic) NSString * DeviceID;
@property (assign) int UseType; // 0 就是注册。1：找回密码
@end
