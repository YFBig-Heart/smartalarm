//
//  DevicePasswordViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "DevicePasswordViewController.h"

@interface DevicePasswordViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *MainTable;
@property (retain,nonatomic) NSArray * TitleArray;
@property (retain,nonatomic) UISwitch *PadLockSwitch;
@property (retain,nonatomic) UISwitch *MenuLOckSwtch;
@property (retain,nonatomic) UITextField * AdminPasswordFD;
@property (retain,nonatomic) UITextField * UserPasswordFD;
@property (retain,nonatomic) NSMutableDictionary * RemoteInfoDic;
@end

@implementation DevicePasswordViewController

- (void)viewDidLoad {
    _MainTable.delegate = self;
    _MainTable.dataSource = self;
    if (_TitleArray == nil) {
        if (([[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"type"] intValue] ==2)) {
            _TitleArray = @[@"管理员密码",@"用户密码"];
        }
        else if([CoreHTTPRequest GetDeviceType] == WIFI_ALARM)
        {
            _TitleArray = @[@"用户密码"];
        }
        else
        _TitleArray = @[@"管理员密码",@"用户密码",@"键盘锁",@"菜单锁"];
    }
    _RemoteInfoDic = [[NSMutableDictionary alloc] init];
    self.title = NSLocalizedString(@"设备密码", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave  target:self action:@selector(Done)];;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    [self synDevicePassword];
}

- (void)Done
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[_RemoteInfoDic objectForKey:@"menu_lock"],@"menu_lock",[_RemoteInfoDic objectForKey:@"keypad_lock"],@"keypad_lock",[_RemoteInfoDic objectForKey:@"pw_admin"],@"pw_admin",[_RemoteInfoDic objectForKey:@"pw_user"],@"pw_user",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"80085",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            [HttpRespondError ShowHttpRespondError:0];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)synDevicePassword
{
    NSDictionary * tDic ;
    
    tDic = [[NSDictionary alloc] initWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultCurrDevice] objectForKey:@"imei"],@"imei",[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserID],@"UserName",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00085",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            [HttpRespondError ShowHttpRespondError:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            _RemoteInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[[response objectForKey:@"data"] objectAtIndex:0]];
            [_MainTable reloadData];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *const kCellID = @"cell_id_more";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.textLabel setText:NSLocalizedString([_TitleArray objectAtIndex:indexPath.row],nil)];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.f];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    if ([CoreHTTPRequest GetDeviceType] == WIFI_ALARM) {
        switch (indexPath.row) {
            case 0:
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                if (_UserPasswordFD) {
                    [_UserPasswordFD removeFromSuperview];
                    _UserPasswordFD = nil;
                }
                _UserPasswordFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_UserPasswordFD setTextColor:[UIColor flatGrayColor]];
                [_UserPasswordFD setText:[_RemoteInfoDic objectForKey:@"pw_user"]];
                [_UserPasswordFD setTextAlignment:NSTextAlignmentRight];
                [_UserPasswordFD setKeyboardType:UIKeyboardTypeNumberPad];
                [_UserPasswordFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_UserPasswordFD];
                _UserPasswordFD.sd_layout.xIs(cell.centerX*0.6).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 10.0f);
                [_UserPasswordFD setKeyboardType:UIKeyboardTypeNumberPad];
                [[self.UserPasswordFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      [self.navigationItem.rightBarButtonItem setEnabled:NO];
                      return text.length >= 4;
                  }]
                 subscribeNext:^(id x) {
                     if([self.UserPasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.UserPasswordFD.text = [self.UserPasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     if (self.UserPasswordFD.text.length > 4) {
                         self.UserPasswordFD.text = [self.UserPasswordFD.text substringWithRange:NSMakeRange(0, 4)];
                     }
                     [self.navigationItem.rightBarButtonItem setEnabled:YES];
                     [_RemoteInfoDic setObject:_UserPasswordFD.text forKey:@"pw_user"];
                     NSLog(@"%@", x);
                 }];
            }
                break;
            default:break;
        }

    }else
    {
        switch (indexPath.row) {
            case 0:
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                if (_AdminPasswordFD) {
                    [_AdminPasswordFD removeFromSuperview];
                    _AdminPasswordFD = nil;
                }
                _AdminPasswordFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_AdminPasswordFD setTextColor:[UIColor flatGrayColor]];
                [_AdminPasswordFD setText:[_RemoteInfoDic objectForKey:@"pw_admin"]];
                [_AdminPasswordFD setTextAlignment:NSTextAlignmentRight];
                [_AdminPasswordFD setKeyboardType:UIKeyboardTypeNumberPad];
                [_AdminPasswordFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_AdminPasswordFD];
                _AdminPasswordFD.sd_layout.xIs(cell.centerX*0.6).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 10.0f);
                [_AdminPasswordFD setKeyboardType:UIKeyboardTypeNumberPad];
                [[self.AdminPasswordFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      [self.navigationItem.rightBarButtonItem setEnabled:NO];
                      return text.length >= 6;
                  }]
                 subscribeNext:^(id x) {
                     if([self.AdminPasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.AdminPasswordFD.text = [self.AdminPasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     if (self.AdminPasswordFD.text.length > 6) {
                         self.AdminPasswordFD.text = [self.AdminPasswordFD.text substringWithRange:NSMakeRange(0, 6)];
                     }
                     [self.navigationItem.rightBarButtonItem setEnabled:YES];
                     [_RemoteInfoDic setObject:_AdminPasswordFD.text forKey:@"pw_admin"];
                     NSLog(@"%@", x);
                 }];
            }
                break;
            case 1:
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                if (_UserPasswordFD) {
                    [_UserPasswordFD removeFromSuperview];
                    _UserPasswordFD = nil;
                }
                _UserPasswordFD = [[UITextField alloc] initWithFrame:CGRectZero];
                [_UserPasswordFD setTextColor:[UIColor flatGrayColor]];
                [_UserPasswordFD setText:[_RemoteInfoDic objectForKey:@"pw_user"]];
                [_UserPasswordFD setTextAlignment:NSTextAlignmentRight];
                [_UserPasswordFD setKeyboardType:UIKeyboardTypeNumberPad];
                [_UserPasswordFD setBackgroundColor:[UIColor clearColor]];
                [cell addSubview:_UserPasswordFD];
                _UserPasswordFD.sd_layout.xIs(cell.centerX*0.6).centerYEqualToView(cell).heightIs(45).rightSpaceToView(cell, 10.0f);
                [_UserPasswordFD setKeyboardType:UIKeyboardTypeNumberPad];
                [[self.UserPasswordFD.rac_textSignal
                  filter:^BOOL(id value) {
                      NSString *text = value;
                      [self.navigationItem.rightBarButtonItem setEnabled:NO];
                      return text.length >= 4;
                  }]
                 subscribeNext:^(id x) {
                     if([self.UserPasswordFD.text rangeOfString:@" "].location !=NSNotFound)
                     {
                         self.UserPasswordFD.text = [self.UserPasswordFD.text stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
                     }
                     if (self.UserPasswordFD.text.length > 4) {
                         self.UserPasswordFD.text = [self.UserPasswordFD.text substringWithRange:NSMakeRange(0, 4)];
                     }
                     [self.navigationItem.rightBarButtonItem setEnabled:YES];
                     [_RemoteInfoDic setObject:_UserPasswordFD.text forKey:@"pw_user"];
                     NSLog(@"%@", x);
                 }];
            }
                break;
            case 2:
            {
                if (_PadLockSwitch) {
                    [_PadLockSwitch removeFromSuperview];
                    _PadLockSwitch = nil;
                }
                _PadLockSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
                [_PadLockSwitch setOn:([[_RemoteInfoDic objectForKey:@"keypad_lock"] intValue]?YES:NO)];
                cell.accessoryView = _PadLockSwitch;
                @weakify(self)
                [[self.PadLockSwitch
                  rac_signalForControlEvents:UIControlEventValueChanged]
                 subscribeNext:^(UISwitch * x) {
                     @strongify(self)
                     NSLog(@"开关  ： %d",x.isOn);
                     [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"keypad_lock"];
                     [self.navigationItem.rightBarButtonItem setEnabled:YES];
                 }];
            }
                break;
            case 3:
            {
                if (_MenuLOckSwtch) {
                    [_MenuLOckSwtch removeFromSuperview];
                    _MenuLOckSwtch = nil;
                }
                _MenuLOckSwtch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
                [_MenuLOckSwtch setOn:([[_RemoteInfoDic objectForKey:@"menu_lock"] intValue]?YES:NO)];
                cell.accessoryView = _MenuLOckSwtch;
                @weakify(self)
                [[self.MenuLOckSwtch
                  rac_signalForControlEvents:UIControlEventValueChanged]
                 subscribeNext:^(UISwitch * x) {
                     @strongify(self)
                     NSLog(@"M开关  ： %d",x.isOn);
                     [_RemoteInfoDic setObject:(x.isOn?@"1":@"0") forKey:@"menu_lock"];
                     [self.navigationItem.rightBarButtonItem setEnabled:YES];
                 }];
            }
                break;

            default:
                break;
        }

    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
