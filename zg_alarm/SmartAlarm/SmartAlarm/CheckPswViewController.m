//
//  CheckPswViewController.m
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/3.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import "CheckPswViewController.h"
#import "RootTabViewController.h"
#import "BindDeviceViewController.h"
#import "CheckUserViewController.h"
#import "RegisterUserViewController.h"
#import "ShowSendEmailDetailViewController.h"
@interface CheckPswViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *PasswordFD;
@property (weak, nonatomic) IBOutlet UIButton *DoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *ForgetPasswordBtn;
@property (weak, nonatomic) IBOutlet UILabel *TitleTips;
@property (weak, nonatomic) IBOutlet UILabel *DetailLabel;

@end

@implementation CheckPswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"登陆", nil);
    [_PasswordFD setPlaceholder:NSLocalizedString(@"请输入登录密码", nil)];
    _PasswordFD.delegate = self;
    _DoneBtn.showsTouchWhenHighlighted = YES;
    [[_DoneBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self doNext];
    }];
    [_ForgetPasswordBtn setTitle:NSLocalizedString(@"忘记密码?", nil) forState:UIControlStateNormal];
    [_DoneBtn bs_configureAsPrimaryStyle];
    [[_ForgetPasswordBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self RequstForFindPassworld];
    }];
    [_DoneBtn setTitle:NSLocalizedString(@"登 陆", nil) forState:UIControlStateNormal];
    [_TitleTips setText:NSLocalizedString(@"请输入登录密码", nil)];
    [_DetailLabel setText:NSLocalizedString(@"你的邮箱已注册,可直接登录", nil)];
}
- (void)RequstForFindPassworld
{
    NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:_userID,@"name",nil];
    NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
    NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"90010",@"cmd",dicArray,@"data", nil];
    NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
    NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
    [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
        //OK
        NSLog(@"%@",response);
        if ([[response objectForKey:@"ret"] intValue] != 0) {
            _PasswordFD.attributedPlaceholder=[HttpRespondError HttpRespondErrorForTextFiled:[[response objectForKey:@"ret"] intValue]];
        }
        else
        {
            ShowSendEmailDetailViewController * vc = [[ShowSendEmailDetailViewController alloc] init];
            vc.EmailAdress = [_userID copy];
            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(NSError *error) {
        //fail
        NSLog(@"fail %@",error);
        [HttpRespondError ShowHttpRespondError:-999];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)doNext
{
    if (_DeviceID) {
        NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:_userID,@"name",_PasswordFD.text,@"password",_DeviceID,@"code",nil];
        NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
        NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00001",@"cmd",dicArray,@"data", nil];
        NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
        NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
        [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
            //OK
            NSLog(@"%@",response);
            if ([[response objectForKey:@"ret"] intValue] != 0) {
                _PasswordFD.attributedPlaceholder=[HttpRespondError HttpRespondErrorForTextFiled:[[response objectForKey:@"ret"] intValue]];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:_userID forKey:kDefaultUserID];
                [[NSUserDefaults standardUserDefaults] setObject:_PasswordFD.text forKey:kDefaultUserPsw];
                [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
                [self.view.window makeKeyAndVisible];
            }
        } failure:^(NSError *error) {
            //fail
            NSLog(@"fail %@",error);
            [HttpRespondError ShowHttpRespondError:-999];
        }];
    }
    else
    {
        NSDictionary * tDic = [NSDictionary dictionaryWithObjectsAndKeys:_userID,@"name",_PasswordFD.text,@"password",[GetSysLanguage GetSystemLanguage],@"language", nil];
        NSArray * dicArray = [[NSArray alloc] initWithObjects:tDic, nil];
        NSDictionary * BodyDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"00002",@"cmd",dicArray,@"data", nil];
        NSData *reportJson = [NSJSONSerialization dataWithJSONObject:BodyDic options:0 error:NULL];
        NSString * Url= [[[NSString alloc] initWithData:reportJson encoding:NSUTF8StringEncoding] URLEncode];
        [CoreHTTPRequest HttpRequstPro:Url success:^(id response) {
            //OK
            NSLog(@"%@",response);
            if ([[response objectForKey:@"ret"] intValue] != 0) {
                _PasswordFD.text = nil;
                _PasswordFD.attributedPlaceholder=[HttpRespondError HttpRespondErrorForTextFiled:[[response objectForKey:@"ret"] intValue]];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:_userID forKey:kDefaultUserID];
                [[NSUserDefaults standardUserDefaults] setObject:_PasswordFD.text forKey:kDefaultUserPsw];
                [self.view.window setRootViewController:[[RootTabViewController alloc] init]];
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        } failure:^(NSError *error) {
            //fail
            NSLog(@"fail %@",error);
            [HttpRespondError ShowHttpRespondError:-999];
        }];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)aTextfield {
    [_PasswordFD resignFirstResponder];//关闭键盘
    return YES;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
