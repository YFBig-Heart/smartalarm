//
//  MsgTableViewCell.h
//  SmartAlarm
//
//  Created by Huang Shan on 2017/10/14.
//  Copyright © 2017年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MsgTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *HeadImg;
@property (weak, nonatomic) IBOutlet UILabel *Title;
@property (weak, nonatomic) IBOutlet UILabel *DetailTitle;
@property (weak, nonatomic) IBOutlet UILabel *Time;
@end
