//
//  PreApConfigViewController.h
//  SmartAlarm
//
//  Created by Huang Shan on 2018/1/10.
//  Copyright © 2018年 Huang Shan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GCDAsyncSocket;

@interface PreApConfigViewController : UIViewController
{
    GCDAsyncSocket *asyncSocket;
}
@property (retain,nonatomic) NSString * LocalSSID;
@property (retain,nonatomic) NSString * LocalPSW;

@end
